<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de empleados

function operacionesPersonal(){
	$res=true;

	if(isset($_POST['codigo'])){
    	$res=actualizaPersonal();
  	}
  	elseif(isset($_POST['nombre'])){
    	$res=insertaDatos('personal','','../documentos/AdjuntosPersonal');
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatosConFichero('personal','ficheroAdjunto','../documentos/AdjuntosPersonal/');
	}

	mensajeResultado('nombre',$res,'Empleado');
    mensajeResultado('elimina',$res,'Empleado', true);
}

function actualizaPersonal(){
	$res=true;
	if(isset($_FILES['ficheroAdjunto']) && $_FILES['ficheroAdjunto']['name'] != '' && isset($_POST['nombreAdjunto'])){
		unlink('../documentos/AdjuntosPersonal/'.$_POST['nombreAdjunto']);
		$res=consultaBD("UPDATE personal SET ficheroAdjunto = 'NO' where codigo=".$_POST['codigo'],true);
	}
	$res=actualizaDatos('personal','','../documentos/AdjuntosPersonal');
	return $res;
}

function imprimePersonal(){
	global $_CONFIG;

	$codigoS=$_SESSION['codigoU'];
	$consulta=consultaBD("SELECT personal.codigo AS codigo, nombre, apellidos, funcion, fechaAlta FROM personal LEFT JOIN puestosTrabajo ON personal.codigoPuestoTrabajo=puestosTrabajo.codigo ORDER BY apellidos, nombre, fechaAlta;", true);

	while($datos=mysql_fetch_assoc($consulta)){
		$fechaAlta=formateaFechaWeb($datos['fechaAlta']);
		echo "
		<tr>
        	<td> ".$datos['nombre']." ".$datos['apellidos']."</td>
        	<td> ".$datos['funcion']." </td>
        	<td> $fechaAlta </td>
        	<td class='td-actions'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-zoom-in'></i> Ver datos</i></a>
        	</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function gestionPersonal(){
	operacionesPersonal();

	abreVentanaGestion('Gestión de empleados','index.php','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('personal');

	campoTexto('nombre','Nombre',$datos,'span4');
	campoTexto('apellidos','Apellidos',$datos,'span4');
	campoSelectConsulta('codigoPuestoTrabajo','Puesto / función',"SELECT codigo, funcion AS texto FROM puestosTrabajo;",$datos);
	campoFecha('fechaAlta','Fecha de entrada en la compañía',$datos);
	campoFecha('fechaDocumentacion','Fecha de entrega de la Documentación inicial',$datos);
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');
	if($datos && $datos['ficheroAdjunto'] != '' && $datos['ficheroAdjunto'] != 'NO'){
		campoDescarga('ficheroAdjuntoDescarga','Adjunto','../documentos/AdjuntosPersonal/',$datos['ficheroAdjunto']);
		campoFichero('ficheroAdjunto','Para sustituir el anterior, sube uno nuevo',0,'','../documentos/AdjuntosPersonal/');
		campoOculto($datos['ficheroAdjunto'],'nombreAdjunto');
	} else {
		campoFichero('ficheroAdjunto','Adjunto',0,$datos,'../documentos/AdjuntosPersonal/');
	}

	campoOculto($_SESSION['codigoU'],'codigoUsuario');

	cierraVentanaGestion('index.php');
}


function creaEstadisticasPersonal(){
	$datos=array();

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM personal;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['personal']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function listadoPlanificacion($objPHPExcel){
	conexionBD();
	//Esta consulta selecciona un listado de los servicios y el personal asignado a los mismos
	$consulta=consultaBD("SELECT nombre, apellidos, funcion, fechaAlta, fechaDocumentacion FROM personal INNER JOIN puestosTrabajo ON personal.codigoPuestoTrabajo=puestosTrabajo.codigo;");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);

	$fecha=date('d')."/".date('m')."/".date('Y');
	$fila=2;//Fila de partida
	$hoja=0;//Hoja de partida

	encabezadoExcel($objPHPExcel,$hoja,$fecha);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);//Ajusta el ancho de la columna al contenido
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

	$posEncabezado='A2';
	encabezadoExcelPlanificacion($objPHPExcel,"A2:D2",$fila);//Le paso el rango donde debe ir el encabezado (para combiar las celdas)*/
	$fila=$fila+2;//Baja 2 filas: la del nombre del cliente y la de los encabezados de los campos

	while($reg!=0){
		$objPHPExcel->getActiveSheet()->getCell('A'.$fila)->setValue($reg['nombre']." ".$reg['apellidos']);
		$objPHPExcel->getActiveSheet()->getCell('B'.$fila)->setValue($reg['funcion']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$fila)->setValue(formateaFechaWeb($reg['fechaAlta']));
		$objPHPExcel->getActiveSheet()->getCell('D'.$fila)->setValue(formateaFechaWeb($reg['fechaDocumentacion']));
		
		$fila++;
		$reg=mysql_fetch_assoc($consulta);
	}
	bordeCeldasExcelPlanificacion($objPHPExcel,$posEncabezado,$fila-1);
}

function encabezadoExcelPlanificacion($objPHPExcel,$rango,$fila){
	$objPHPExcel->getActiveSheet()->mergeCells($rango);//Combina celdas
	$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Pone el alineado centrado
	$objPHPExcel->getActiveSheet()->getStyle($rango)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('428BCA');//Les pone el fondo morado
	//$objPHPExcel->getActiveSheet()->getCell('A'.$fila)->setValue($cliente);//Nombre del cliente

	$fila++;//Baja una fila

	$objPHPExcel->getActiveSheet()->getCell('A'.$fila)->setValue("TRABAJADOR");
	$objPHPExcel->getActiveSheet()->getCell('B'.$fila)->setValue("PUESTO/FUNCIÓN");
	$objPHPExcel->getActiveSheet()->getCell('C'.$fila)->setValue("FECHA DE INCORPORACIÓN A LA COMPAÑÍA");
	$objPHPExcel->getActiveSheet()->getCell('D'.$fila)->setValue("FECHA DE ENTREGA DE LA DOCUMENTACIÓN INICIAL");

	$objPHPExcel->getActiveSheet()->getStyle('A'.$fila.":D".$fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Pone el alineado centrado

	$styleArray = array(
       'font' => array(
             'bold' => true,
       )
	);
	
	$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$fila.":D".$fila)->applyFromArray($styleArray);
}



function bordeCeldasExcelPlanificacion($objPHPExcel,$posEncabezado,$fila){
	$estiloBorde = array(
       'borders' => array(
            'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => '00000000'),
			),
		)
	);
	$objPHPExcel->getActiveSheet()->getStyle($posEncabezado.':D'.$fila)->applyFromArray($estiloBorde);
}


function encabezadoExcel($objPHPExcel,$hoja,$fecha){
	$objPHPExcel->setActiveSheetIndex($hoja);//Selección hoja
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);//Ajusta el ancho de la columna al contenido
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

	//Encabezado hoja
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
	$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setSize(16);

	$objPHPExcel->getActiveSheet()->setTitle('CONTRATOS');//Cambio nombre de hoja
	$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("CONTRATOS A FECHA DE $fecha");
	
}
//Fin parte de empleados


