<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de Agenda

function operacionesAgenda(){
    $res=true;

    if(isset($_POST['repetir'])){
        $res=repiteEventoSemanalmente();
    }
    elseif(isset($_GET['realizada'])){
        $res=cambiaValorCampo('estado','REALIZADA',$_GET['codigo'],'tareas');
    }
    elseif(isset($_POST['codigo'])){
        $res=actualizaTarea();
    }
    elseif(isset($_POST['tarea'])){
        $res=insertaTarea();
    }
    elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
        $res=eliminaDatos('tareas');
    }
    elseif(isset($_POST['destinatarios'])){
   		$res=enviaCorreos();
  	}

    mensajeResultado('tarea',$res,'sesión');
    mensajeResultado('repetir',$res,'sesión');
    mensajeResultado('elimina',$res,'sesión', true);
    mensajeResultadoCorreo('destinatarios',$res,'Correo');
}

function insertaTarea(){
	$res = true;
	$datos=arrayFormulario();
	
	if($datos['todoDia'] == 'true'){
		$_POST['todoDia'] = 'SI';
		$_POST['horaInicio']='';
		$_POST['horaFin']='';
	} else {
		$_POST['todoDia'] = 'NO';
	}

	$res = insertaDatos('tareas');
	if($res){
		$consulta=consultaBD("INSERT INTO clientesTareas VALUES(NULL,'".$datos['empresa']."', '".$datos['localidad']."', '".$datos['contacto']."', '".$datos['telefono']."',
		 	'".$datos['mail']."', '$res');");
	}

	return $res;
}

function actualizaTarea(){
	$res = true;
	$datos=arrayFormulario();
	
	if($datos['todoDia'] == 'true'){
		$_POST['todoDia'] = 'SI';
		$_POST['horaInicio']='';
		$_POST['horaFin']='';
	} else {
		$_POST['todoDia'] = 'NO';
	}
	$res = actualizaDatos('tareas');
	if($res){
		$consulta=consultaBD("UPDATE clientesTareas SET empresa='".$datos['empresa']."', localidad='".trim($datos['localidad'])."', contacto='".$datos['contacto']."', 
		telefono='".$datos['telefono']."', mail='".$datos['mail']."' WHERE codigoTarea='".$datos['codigo']."';");
	}

	return $res;
}

function generaEventosCalendarioTareas(){
	//$codigoU=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	conexionBD();
	$consulta=consultaBD("SELECT tareas.codigo, empresa, tarea, fechaInicio, fechaFin, horaInicio, horaFin, todoDia, prioridad FROM tareas INNER JOIN clientesTareas ON tareas.codigo=clientesTareas.codigoTarea $where AND estado='pendiente';");

	$datos=mysql_fetch_assoc($consulta);
	$tareas="";
	$clases=array('ALTA'=>"backgroundColor: '#ec5b58', borderColor: '#b94a48'",'NORMAL'=>"backgroundColor: '#5e96ea', borderColor: '#3f85f5'",'BAJA'=>"backgroundColor: '#b0b0b0', borderColor: '#979797'");

	while($datos!=false){
		$fechaInicio=explode('-',$datos['fechaInicio']);
		$fechaFin=explode('-',$datos['fechaFin']);
		$fechaInicio[1]--;
		$fechaFin[1]--;

		$tareas.="
			{
				id: ".$datos['codigo'].",
		    	title: '-".$datos['empresa'].": ".$datos['tarea']."',";
		if($datos['todoDia']=='SI'){
			$tareas.="
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2]."),
		    	allDay: true,
		    	url: 'gestion.php?codigo=".$datos['codigo']."',
		    	".$clases[$datos['prioridad']]."
		    	
	    	},";
		}
		else{
			$horaInicio=explode(':',$datos['horaInicio']);
			$horaFin=explode(':',$datos['horaFin']);

		    $tareas.="
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2].", ".$horaInicio[0].", ".$horaInicio[1]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2].", ".$horaFin[0].", ".$horaFin[1]."),
		    	allDay: false,
		    	url: 'gestion.php?codigo=".$datos['codigo']."',
		    	".$clases[$datos['prioridad']]."
	    	},";
		}

		$datos=mysql_fetch_assoc($consulta);
  	}
  	$tareas=substr_replace($tareas, '', strlen($tareas)-1, strlen($tareas));//Para quitar última coma
  	echo $tareas;
}


function generaDatosGraficoTareas(){
	$datos=array();
	//$codigoS=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(tareas.codigo) AS codigo FROM tareas INNER JOIN clientesTareas ON clientesTareas.codigoTarea=tareas.codigo $where AND tareas.estado='pendiente';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['pendientes']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(tareas.codigo) AS codigo FROM tareas INNER JOIN clientesTareas ON clientesTareas.codigoTarea=tareas.codigo $where AND tareas.estado='realizada';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['realizadas']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function repiteEventoSemanalmente(){
	$res=true;
	$codigoEvento=$_POST['codigo'];
	
	conexionBD();
	$datos=consultaBD("SELECT * FROM tareas WHERE codigo='$codigoEvento';",false,true);

	date_default_timezone_set('Europe/Madrid');//Necesario para que el servidor no dé error.
	$fechaInicio=sumaDiasFecha($datos['fechaInicio'],7);
	$fechaFin=sumaDiasFecha($datos['fechaFin'],7);

	$datos['codigoCliente']=compruebaCampoNULL($datos['codigoCliente']);
	$datos['codigoIncidencia']=compruebaCampoNULL($datos['codigoIncidencia']);

	$res=$res && consultaBD("INSERT INTO tareas VALUES(NULL, '".$datos['tarea']."', '$fechaInicio', '$fechaFin', '".$datos['horaInicio']."','".$datos['horaFin']."','".$datos['todoDia']."','".$datos['fechaRecordatorio']."','PENDIENTE','".$datos['prioridad']."','".$datos['observaciones']."','".$datos['codigoUsuario']."',".$datos['codigoCliente'].",".$datos['codigoIncidencia'].");");
	cierraBD();

	return $res;
}

function compruebaCampoNULL($campo){
	if($campo==NULL){
		$campo='NULL';
	}

	return $campo;
}

function gestionEventos(){
	$res=false;

	$res = insertaDatos('tareas');
	if($res){
		$consulta=consultaBD("INSERT INTO clientesTareas VALUES(NULL,'".$_POST['empresa']."', '".$_POST['localidad']."', '".$_POST['contacto']."', '".$_POST['telefono']."',
		 	'".$_POST['mail']."', '$res');",true);
	}

	echo $res;
}

function actualizaTiempo(){
	$res=false;
	$res=consultaBD("UPDATE tareas SET fechaInicio='".$_POST['fechaI']."', horaInicio='".$_POST['horaI']."', horaFin='".$_POST['horaF']."', todoDia='".$_POST['todoDia']."' WHERE codigo=".$_POST['codigo'],true);
	echo "UPDATE tareas SET fechaInicio='".$_POST['fechaI']."', horaInicio='".$_POST['horaI']."', horaFin='".$_POST['horaF']."', todoDia='".$_POST['todoDia']."' WHERE codigo=".$_POST['codigo'];
}

function gestionAgenda(){
	operacionesAgenda();

	abreVentanaGestion('Gestión de Agenda','index.php');
	$datos=compruebaDatos('tareas');
	$datosCliente=datosRegistro('clientesTareas',$datos['codigo'],'codigoTarea');


	campoTexto('tarea','Tarea',$datos,'span6');
	campoTexto('empresa','Alumno',$datosCliente);
	campoTexto('localidad','Localidad',$datosCliente);
	campoTexto('contacto','Contacto',$datosCliente);
	campoTexto('mail','Email',$datosCliente);
	campoTexto('telefono','Teléfono',$datosCliente,'input-small');
	campoFecha('fechaInicio','Fecha de inicio',$datos);
	campoFecha('fechaFin','Fecha de fin',$datos);

	$diaCompleto = $datos['todoDia'] == 'SI' ? 'true':'false';
	campoRadio('todoDia','Día completo',$diaCompleto,'false',array('Si','No'),array('true','false'));
	echo "<div id='todoDia'>";
		campoHora('horaInicio','Hora de inicio',$datos);
		campoHora('horaFin','Hora de fin',$datos);
	echo "</div>";
	campoRadio('estado','Estado',$datos,'PENDIENTE',array('pendiente','realizada'),array('PENDIENTE','REALIZADA'));

	campoSelect('prioridad','Prioridad',array('Normal','Baja','Alta'),array('NORMAL','BAJA','ALTA'),$datos,'selectpicker span2 show-tick',"");
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');
	
	campoOculto($datos,'codigoUsuario',$_SESSION['codigoU']);

	cierraVentanaGestion('index.php');
}

function campoSelectConsultaBlanco($nombreCampo,$texto,$consulta,$valor=false,$clase='selectpicker span3 show-tick busquedaSelect',$busqueda="data-live-search='true'",$disabled='',$tipo=0,$blanco=false){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($disabled){
		$disabled="disabled='disabled'";
	}

	if($tipo==0){
		echo "
		<div class='control-group'>
			<label class='control-label' for='$nombreCampo'>$texto:</label>
			<div class='controls'>";
	}
	elseif($tipo==1){
		echo "<td>";
	}

	echo "<select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda $disabled>";

		$consulta=consultaBD($consulta,true);
		$datos=mysql_fetch_assoc($consulta);
		if($blanco){
			echo "<option value='NULL'></option>";
		}
		while($datos!=false){
			echo "<option value='".$datos['codigo']."'";

			if($valor!=false && $valor==$datos['codigo']){
				echo " selected='selected'";
			}

			echo ">".$datos['texto']."</option>";
			$datos=mysql_fetch_assoc($consulta);
		}

	echo "</select>";

	if($tipo==0){
		echo "
			</div> <!-- /controls -->
		</div> <!-- /control-group -->";
	}
	elseif($tipo==1){
		echo "</td>";
	}
}

function compruebaBotonesAgenda($codigo,$estado){
    global $_CONFIG;

    $res="<a href='gestion.php?codigo=".$codigo."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>";
    if($estado=='PENDIENTE'){
        $res="<div class='btn-group'>
                <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
                <ul class='dropdown-menu' role='menu'>
                    <li><a href='".$_CONFIG['raiz']."tareas/gestion.php?codigo=".$codigo."'><i class='icon-search-plus'></i> Detalles</a></li>
                    <li class='divider'></li>
                    <li><a href='?&tarea=".$codigo."&realizada'><i class='icon-check-circle'></i> Marcar como realizada</a></li>
                </ul>
             </div>";
    }
    return $res;
}




//Fin parte de Agenda