<?php
  $seccionActiva=15;
  include_once('../cabecera.php');

  operacionesObjetivos();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para los objetivos:</h6>
                  <div id="big_stats" class="cf">
                  
                    <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
                  
                    <div class="leyenda">
                      <span class="grafico grafico3"></span>Conseguidos: <span id="valor1"></span><br>
                      <span class="grafico grafico2"></span>Por alcanzar: <span id="valor2"></span><br>
                    </div>
                    
                    <!-- .stat --> 
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Objetivos e Indicadores</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
                <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Añadir Objetivo</span> </a>
                <a href="../indicadores/index.php" class="shortcut"><i class="shortcut-icon icon-dashboard"></i><span class="shortcut-label">Indicadores</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
        </div>

        
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Objetivos registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Definición </th>
                    <th> ¿Conseguido? </th>
                    <th class="td-actions"> </th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeObjetivos();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<script type="text/javascript">
    <?php
      $datos=generaDatosGraficoObjetivos();
    ?>

    var pieData = [
        {
            value: <?php echo $datos['conseguidos']; ?>,
            color: "#428bca"
        },
        {
            value: <?php echo $datos['noConseguidos']; ?>,
            color: "#B02B2C"
        }
      ];

    var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
    
    $("#valor1").text("<?php echo $datos['conseguidos']; ?>");
    $("#valor2").text("<?php echo $datos['noConseguidos']; ?>");
</script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js"></script>