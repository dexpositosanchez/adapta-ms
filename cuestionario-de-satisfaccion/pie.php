<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy;  <?php echo date('Y'); ?>  <a href='http://adaptams.com/' target='_blank'>Adapta management services</a> &copy; <?php echo date('Y').' '.$_CONFIG['textoPie']; ?>  

          <div id="google_translate_element"></div><script type="text/javascript">
            function googleTranslateElementInit() {
              new google.translate.TranslateElement({pageLanguage: 'es', autoDisplay: false}, 'google_translate_element');
            }
          </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 

<!-- Javascript
================================================== -->
<script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>

<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/base.js" type="text/javascript"></script>
<script src="../js/bootstrap-datepicker.js" type="text/javascript"></script>

</body>
</html>
