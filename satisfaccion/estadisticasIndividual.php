<?php
  $seccionActiva=15;
  include_once("../cabecera.php");
  
  $codigo=$_GET['codigo'];
  
  $datos=datosPorcentajesIndividual($codigo);
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Encuesta de Satisfacción de Clientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" name="formulario">
                  <fieldset>
                    <br>
                
                    <center>    
                      <div class="widget widget-nopad mitadAncho">
                        <!-- /widget-header -->
                        <div class="widget-content">
                          <div class="widget big-stats-container sinMargenAb">
                            <div class="widget-content">
                              <h6 class="bigstats">Grado de satisfacción global:</h6>
                              <div id="big_stats" class="cf">
                                <div class="stat"> <i class="icon-check"></i> <span class="value"><?php echo $datos; ?>%</span> <br><span class="valoracion">
                								<?php if ($datos<60){ ?>
                									Valoración: Muy Baja
                								<?php }elseif ($datos<80){ ?>
                									Valoración: Baja
                								<?php }else{ ?>
                									Valoración: Alta
                								<?php } ?>
                								</span></div>
                                <!-- .stat -->
                              </div>
                            </div>
                            <!-- /widget-content --> 
                            
                          </div>
                        </div>
                      </div>
                    </center>
                    <br><br>
					
                     <br />
                    
                      
                    <div class="form-actions">
                      <a href="<?php echo $_CONFIG['raiz']; ?>satisfaccion" class="btn btn-default"><i class="icon-arrow-left"></i> Volver</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('../pie.php'); ?>

