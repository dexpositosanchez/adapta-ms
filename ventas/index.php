<?php
  $seccionActiva=11;
  include_once('../cabecera.php');
    
  operacionesVentas();
  $estadisticas=estadisticasGenericas('presupuestos',true,"aceptado='SI'");
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de ventas:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-tags"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Presupuestos Aceptados</div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
        
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Presupuestos Emitidos/Aceptados</h6>
                  <canvas id="graficoBarras" class="chart-holder" width="500" height="250"></canvas>
                  <span class="grafico-2 grafico4"></span>Emitidos: <span id="valor1"></span>
                  <span class="grafico-2 grafico5"></span>Aceptados: <span id="valor2"></span>
                </div>
              </div>
            </div>
          </div>
        </div>


      <div class="span12">        
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Presupuestos aceptados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Referencia </th>
                  <th> Empresa </th>
                  <th> Fase de Presupuesto </th>
                  <th> Colaborador </th>
                  <th> Comercial </th>
				  <th> Facturada </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

                <?php
                  imprimeVentas();
                ?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content --> 
        </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>

<script type="text/javascript">
  <?php
    $datosGrafico=generaDatosGraficoVentas();
    $datosEscala=escalaGraficoComercial($datosGrafico['max']);
  ?>

  var barChartData = {
    labels: [""],
    datasets: [
      {
          fillColor: "rgba(220,220,220,0.5)",
          strokeColor: "rgba(220,220,220,1)",
          data: [<?php echo $datosGrafico['totales']; ?>]
      },
      {
          fillColor: "rgba(151,187,205,0.5)",
          strokeColor: "rgba(151,187,205,1)",
          data: [<?php echo $datosGrafico['confirmados']; ?>]
      }
    ]
  }

  var opciones={
    scaleOverride : true,
    scaleSteps :  <?php echo $datosEscala['max']; ?>,
    scaleStepWidth : <?php echo $datosEscala['escala']; ?>, //Si el número de incidencias es menor que 10, la escala del gráfico va de 1 en 1, sino de 5 en 5.
    scaleStartValue : 0,
    barValueSpacing:60,
    barDatasetSpacing:20
  }

  var myLine = new Chart(document.getElementById("graficoBarras").getContext("2d")).Bar(barChartData,opciones);

  $("#valor1").text("<?php echo $datosGrafico['totales']; ?>");
  $("#valor2").text("<?php echo $datosGrafico['confirmados']; ?>");

  $('.facturarBt').click(function(){
    //e.preventDefault();
    var codigoPresu=$(this).attr('codigoPresupuesto');
    var codigoClie=$(this).attr('codigoCliente');
    validaPlazosPresupuestos(codigoPresu,codigoClie);
  });

function validaPlazosPresupuestos(codigoPresu,codigoClie){

    var nombrePresupuesto=codigoPresu;
    //Obtengo los datos de los campos (con $(selector).val()) y los metó en una matriz para pasársela como parámetro a la función AJAX
    //En este caso, para el nombre de la asesoria creo una variable antes porque si te fijas vuelvo a usar ese dato para el select una vez que se ha hecho la insercion
    var campos={
        'codigoPresupuesto':nombrePresupuesto,
    }
    //Fin obtención campos

    //$('#registraPedidosObras').html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga en el botón de registro

    var consulta=$.post('../listadoAjax.php?include=ventas&funcion=plazosVentas();',campos);//Para no tener 200 ficheros distintos para las múltiples gestiones AJAX, tengo un único archivo en la raíz

    consulta.done(function(respuesta){//La variable respuesta tiene el echo de la función creaAsesoriaCliente() de PHP
        if(respuesta=='fallo'){
            alert('Debe rellenar los plazos en Ventas.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
        }
        else{
            str1='../facturas/gestion.php?codigoPresupuesto=';
            str2=str1.concat(codigoPresu);
            window.location=str2;
        }

    });    

}

</script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/eventoGrafico.js"></script>

<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>