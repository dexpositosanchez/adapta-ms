<?php
  $seccionActiva=3;
  include_once('../cabecera.php');

  operacionesAgenda();
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      
      <div class="row">
        <div class="span12">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-calendar"></i>
               <h3>Agenda de tareas</h3>
            </div>
            <div class="widget-content">
              <div id='calendar'></div>
            </div>
          </div>
        </div>

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container tareas">
                <div class="widget-content">
                  <h6 class="bigstats tareas">Estadísticas del sistema para el área de tareas:</h6>

                   <canvas id="grafico" class="chart-holder" height="250" width="538"></canvas>
                   <div class="leyenda" id="leyenda"></div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
        </div>


        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Tareas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Registrar tarea</span></a>
                <a href="#" id='email' class="shortcut noAjax"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
                <a href="javascript:void" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      </div>

      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-flag"></i>
          <h3>Tareas pendientes</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th> Alumno </th>
                <th> Localidad </th>
                <th> Contacto </th>
                <th> Teléfono </th>
                <th> Tarea </th>
                <th> Fecha de inicio </th>
                <th> Proridad </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareas(true);
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
      </div>

      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-flag"></i>
          <h3>Tareas realizadas</h3>
        </div>
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th> Aluno </th>
                <th> Localidad </th>
                <th> Contacto </th>
                <th> Teléfono </th>
                <th> Tarea </th>
                <th> Fecha de inicio </th>
                <th> Proridad </th>
                <th> Estado </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareas(false);
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
      </div>


      </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 


  <!-- Caja de tarea -->
  <div id='cajaTarea' class='modal hide fade cajaSelect'> 
    <div class='modal-header'> 
      <a class='close noAjax' id='cierra'>&times;</a>
      <h3> <i class="icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-plus-circle"></i> &nbsp; Nueva tarea</h3> 
    </div> 
    <div class='modal-body cajaSelect'>
      
      <form id="edit-profile" class="form-horizontal" method="post">
        <fieldset>
          <?php
            campoTexto('nombreTarea','Tarea');
            campoTexto('empresa','Alumno');
            campoTexto('localidad','Localidad');
            campoTexto('contacto','Contacto');
            campoTexto('mail','Email');
            campoTexto('telefono','Teléfono','','input-small');
            campoSelect('prioridad','Prioridad',array('Normal','Baja','Alta'),array('NORMAL','BAJA','ALTA'),'','selectpicker span2 show-tick',"");
            areaTexto('observacionesTarea','Observaciones');
            campoOculto('PENDIENTE','estadoTarea');
            campoOculto('','codigoUsuario',$_SESSION['codigoU']);
  
          ?>
        </fieldset>
      </form>

    </div> 
    <div class='modal-footer'> 
      <button type="button" class="btn btn-propio" id="registrar"><i class="icon-check"></i> Registrar tarea</button> 
      <button type="button" class="btn btn-default" id='cancelar'><i class="icon-remove"></i> Cancelar</button> 
    </div> 
  </div>
  <!-- Fin caja de tarea -->

  <!-- Caja de opciones -->
  <div id='cajaOpciones' class='modal hide fade'> 
    <div class='modal-header'> 
      <a class='close' data-dismiss='modal'>&times;</a>
      <h3> <i class="icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-cogs"></i> &nbsp; Opciones de tarea </h3> 
    </div> 
    <div class='modal-body centro'>
      <a href='#' class="btn btn-primary" id='enlace'><i class="icon-search-plus"></i> Detalles de tarea</a> 
      <button type="button" class="btn btn-success" id='repetirCita'><i class="icon-calendar"></i> Repetir semanalmente</button>
      <button type="button" class="btn btn-danger" id='eliminarTarea'><i class="icon-trash"></i> Eliminar tarea</button>
    </div> 
  </div>
  <!-- Fin caja de opciones -->

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/full-calendar/jquery-ui.custom.min.js" type="text/javascript" ></script><!-- Habilita el drag y el resize -->
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/full-calendar/fullcalendar.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript" ></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/moment.js" type="text/javascript"></script><!-- Para formatear fecha calendario -->

<script type="text/javascript">
$(document).ready(function() {

  var eventosCreados=0;//Para corregir un BUG que se produce con el callback select de Fullcalendar y el resourceView: se crean en un mismo resource tantos eventos como se han creado anteriormente sin recargar la página.
  var eventoActual=1;

  var colorFondo={'NORMAL':"#7ba9ee",'ALTA':"#ea807e",'BAJA':"#7acc76"};
  var colorBorde={'NORMAL':"#3f85f5",'ALTA':"#b94a48",'BAJA':"#00a100"};

  var calendario = $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultView: 'agendaDay',
    selectable:true,
    selectHelper: true,
    editable:true,
    select: function(start, end, allDay, event) {
      abreVentana();
      $('#registrar').click(function(){
          cierraVentana();

          var nombreTarea=$('#nombreTarea').val();
          var empresa=$('#empresa').val();
          var localidad=$('#localidad').val();
          var contacto=$('#contacto').val();
          var email=$('#email').val();
          var telefono=$('#telefono').val();
          var estadoTarea=$('#estadoTarea').val();
          var prioridad=$('#prioridad').val();
          var observacionesTarea=$('#observacionesTarea').val();
          var codigoUsuario=$('#codigoUsuario').val();
      
          var fechaI = $.fullCalendar.formatDate(start, 'dd/MM/yyyy');
          var fechaF = $.fullCalendar.formatDate(end, 'dd/MM/yyyy');
          var horaI = $.fullCalendar.formatDate(start, 'HH:mm');
          var horaF = $.fullCalendar.formatDate(end, 'HH:mm');
          var todoDia='SI';
          if(!allDay){
            todoDia='NO';
          }
          
          eventosCreados++;
          if(eventosCreados==eventoActual){
            eventosCreados=0;
            eventoActual++;
            //Creación-renderización del evento
            var creacion=$.post("gestionEventos.php", {tarea: nombreTarea, fechaInicio:fechaI, fechaFin:fechaF, horaInicio: horaI, horaFin: horaF, todoDia: todoDia, estado: estadoTarea, prioridad: prioridad, observaciones: observacionesTarea, codigoUsuario: codigoUsuario, empresa: empresa, localidad: localidad, contacto: contacto, telefono: telefono, mail: email});
            creacion.done(function(datos){
              calendario.fullCalendar('renderEvent',
              {
                  id: datos,
                  title: ' ',
                  start: start,
                  end: end,
                  allDay: allDay,
                  backgroundColor: colorFondo[prioridad],
                  borderColor: colorBorde[prioridad]
                },
                true
              );
              calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
            });
            //Fin creación-renderización
          }

        });
      calendario.fullCalendar('unselect');
    },

    eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {//Actualización del evento cuando se modifica tu tamaño (duración)
      //var fechaI = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var fechaI = moment(calendario.fullCalendar('getDate')).format('YYYY-MM-DD');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');
      var todoDia='SI';
      if(!event.allDay){
        todoDia='NO';
      }
      accion = 'actualiza';
      var creacion=$.post("gestionEventos.php", {codigo: event._id, fechaI:fechaI, horaI: horaI, horaF: horaF, todoDia: todoDia, tipoAccion: accion});
      creacion.done(function(datos){
        calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
      });
    },
    eventDrop: function (event, dayDelta, minuteDelta) {//Actualización del evento cuando se mueve de hora
      var fechaI = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');
      var todoDia='SI';
      if(!event.allDay){
        todoDia='NO';
      }
      accion = 'actualiza';
      var creacion=$.post("gestionEventos.php", {codigo: event._id, fechaI:fechaI, horaI: horaI, horaF: horaF, todoDia: todoDia, tipoAccion: accion});
      creacion.done(function(datos){
        calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
      });
    },

    firstDay:1,//Añadido por mi
    titleFormat: {
      month: 'MMMM yyyy',
      week: "d MMM [ yyyy]{ '&#8212;' d [ MMM] yyyy}",
      day: 'dddd, d MMM, yyyy'
    },
    columnFormat: {
      month: 'ddd',
      week: 'ddd d/M',
      day: 'dddd d/M'
    },
    axisFormat: 'H:mm',
    minTime:7,
    maxTime:23,
    firstHour:7,
    slotMinutes:30,
    timeFormat: 'H:mm { - H:mm}',//Fin añadido por mi
    events:[
      <?php
        generaEventosCalendarioTareas();
      ?>]
  });

});

<?php
    $datos=generaDatosGraficoTareas();
  ?>
  var pieData = [
      {
          value: <?php echo $datos['pendientes']; ?>,
          color: "#428bca",
          label: 'Pendientes'
      },
      {
          value: <?php echo $datos['realizadas']; ?>,
          color: "#ACACAC",
          label: 'Realizadas'
      }
    ];

    var grafico = new Chart(document.getElementById("grafico").getContext("2d")).Pie(pieData);


$('#cancelar, #cierra').click(function(){
  cierraVentana();
});

$('#cajaOpciones a,#cajaOpciones button').mouseup(function(e){//Esta función oculta el modal de la caja de opciones cuando se pulsa alguno de sus botones. El evento es mousedown porque el enlace se envia antes de que se ejecute el click()
  $('#cajaOpciones').modal('hide');
});


function abreVentana(){
  $('#cajaTarea').find('.selectpicker').selectpicker('refresh');
  $('#cajaTarea').modal({'show':true,'backdrop':'static','keyboard':false});
}

function cierraVentana(){
  $('#tarea').val('');
  $('#observaciones').val('');

  $('#registrar').unbind();//Necesario para que no se acumulen clicks en el botón de envío al AJAX.
  $('#cajaTarea').modal('hide');
}


function abreVentanaOpciones(evento){
  $('#enlace').attr('href','gestion.php?codigo='+evento.id);
  $('#repetirCita').attr('onclick','repetirCita('+evento.id+')');
  $('#eliminarTarea').attr('onclick','eliminarTarea('+evento.id+')');
  $('#cajaOpciones').modal({'show':true,'backdrop':'static','keyboard':false});
}

function repetirCita(codigoEvento){
  var valoresChecks=[];

  valoresChecks['codigo']=codigoEvento;
  valoresChecks['repetir']='SI';
  creaFormulario('index.php',valoresChecks,'post');
}

function eliminarTarea(codigoEvento){
  var valoresChecks=[];

  valoresChecks['codigo0']=codigoEvento;
  valoresChecks['elimina']='SI';
  creaFormulario('index.php',valoresChecks,'post');
}

$('#email').click(function(){
    var valoresChecks=recorreChecks();
    creaFormulario('../enviarEmail.php?seccion=12',valoresChecks,'post');
  });


</script><!-- /Calendar -->
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/eventoGrafico.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>