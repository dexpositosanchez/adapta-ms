<?php
  $seccionActiva=5;
  include_once('../cabecera.php');
  
  $res=false;

  $codigoCliente=$_CONFIG['codigoIncidenciasSoftware'];
  $estadisticas=estadisticasIncidencias($codigoCliente);

?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
		      mensajeResultado('observaciones',$res,'incidencia');
        ?>


        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para las incidencias de software:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-laptop"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Total registradas</div>
                     <div class="stat"> <i class="icon-flag"></i> <span class="value"><?php echo $estadisticas['pendientes']?></span> <br>Pendientes</div>
                     <div class="stat"> <i class="icon-clock-o"></i> <span class="value"><?php echo $estadisticas['desarrollo']?></span> <br>En desarrollo</div>
                     <div class="stat"> <i class="icon-check-square-o"></i> <span class="value"><?php echo $estadisticas['finalizadas']?></span> <br>Finalizadas</div>
                   </div>
                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Incidencias de software</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
                <a href="index.php" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
                <a href="notificaIncidencia.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva Incidencia</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


        </div><!-- /row -->

        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Incidencias de software finalizadas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th class='nowrap'> Fecha de notificación </th>
                    <th class='nowrap'> Fecha prevista de resolución </th>
                    <th> Descripción </th>
                    <th> Estado </th>
                    <th class='nowrap'> Fecha de resolución </th>
                    <th class='nowrap'> Comentarios programador </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>

                  <?php                    
                    imprimeIncidenciasSoftware($codigoCliente,true);
                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>



    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>

<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>