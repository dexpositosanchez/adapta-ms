<?php
  $seccionActiva=7;
  include_once("../cabecera.php");
  
  gestionMensajesConversacion();
?>

<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-wysihtml5.es-ES.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
    $('#mensaje').wysihtml5({locale: "es-ES"});
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

    $('.form-actions button[type=submit]').click(function(e){
      e.preventDefault();
      $('#cajaRespuesta').modal({'show':true,'backdrop':'static','keyboard':false});
    });

    $('#enviaMensaje').mouseup(function(){
      $('#cajaRespuesta').modal('hide');
    });

  });
</script>

<!-- /contenido --></div>
<?php include_once('../pie.php'); ?>