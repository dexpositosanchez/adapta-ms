<?php
  $seccionActiva=2;
  include_once("../cabecera.php");
  gestionRemesa();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.mensajeAviso').hover(function(){
		var titulo=$(this).attr("titulo");
		var contenido=$(this).attr("mensaje");
		$(this).popover({
			 title: titulo,
			 content: contenido,
			 placement:'top'
		});
	});
  });
  
</script>
</div><!-- contenido -->
<?php include_once('../pie.php'); ?>