<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores

function operacionesProgramas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaPrograma();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaPrograma();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('programas');
	}

	mensajeResultado('nombre',$res,'Programa');
    mensajeResultado('elimina',$res,'Programa', true);
}

function insertaPrograma(){
	$res=true;
	$res=insertaDatos('programas');
	if($res){
		$_POST['codigo']=mysql_insert_id();
		$res=insertaSesiones();
	}
	return $res;
}

function actualizaPrograma(){
	$res=true;
	$res=actualizaDatos('programas');
	if($res){
		$res=insertaSesiones();
	}
	return $res;
}

function insertaSesiones(){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM programas_sesiones WHERE codigoPrograma='".$_POST['codigo']."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['actividad'.$i])){
		if($datos['actividad'.$i] != ""){
			$res = $res && consultaBD("INSERT INTO programas_sesiones VALUES (NULL,'".$_POST['codigo']."','".$datos['fecha'.$i]."','".$datos['actividad'.$i]."',".$datos['codigoDocente'.$i].",'".$datos['horas'.$i]."','".$datos['id'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function listadoProgramas(){
	global $_CONFIG;

	$columnas=array('codigoInterno','precio','modalidad','codigoInterno','codigoInterno');
	$having=obtieneWhereListado("WHERE 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, codigoInterno, precio, modalidad, incluyeMatricula FROM programas $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, codigoInterno, precio, modalidad, incluyeMatricula FROM programas $having;");
	cierraBD();
	$modalidades=array('PRESENCIAL'=>'Presencial','SEMIPRESENCIAL'=>'Semipresencial','A DISTANCIA'=>'A distancia');
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		if($datos['incluyeMatricula']){
			$precio=$datos['precio'].' € (Incluido el importe de matrícula)';
		} else {
			$precio=$datos['precio'].' €';
		}
		$fila=array(
			$datos['codigoInterno'],
			$precio,
			$modalidades[$datos['modalidad']],
			botonAcciones(array('Detalles'),array('programas/gestion.php?codigo='.$datos['codigo']),array("icon-search-plus")),
        	"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionProgramas(){
	operacionesProgramas();

	abreVentanaGestion('Gestión de Programas','index.php');
	$datos=compruebaDatos('programas');
	
	abreColumnaCampos();
		campoTexto('codigoInterno','Código identificativo',$datos,'input-small');
		campoSelectConsulta('codigoCategoria','Categoría',"SELECT codigo, CONCAT(id,' - ',categoria) AS texto FROM categorias ORDER BY id",$datos);
		campoSelectConsulta('codigoSubcategoria','Subcategoría',"SELECT codigo, CONCAT(id,' - ',subcategoria) AS texto FROM subcategorias ORDER BY id",$datos);
		campoSelectConsulta('codigoCurso','Módulo',"SELECT codigo, curso AS texto FROM cursos ORDER BY curso",$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('nombre','Nombre',$datos);
		campoTextoSimbolo('precio','Precio (PVP)','€',$datos);
		campoRadio('incluyeMatricula','¿Incluye importe matrícula?',$datos);
		campoTextoSimbolo('importeMatricula','Importe matrícula','€',$datos);
	cierraColumnaCampos();
	echo '<br clear="all">';
	abreColumnaCampos();
		campoTexto('horasLectivas','Horas lectivas',$datos);
		campoTexto('diasPrevistos','Días previstos',$datos);
		campoTexto('lugar','Lugar de celebración',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('horario','Horario',$datos);
		campoTexto('previstaFecha','Fecha/ previstas/a',$datos);
		campoSelect('modalidad','Modalidad',array('Presencial','Semipresencial','A distancia'),array('PRESENCIAL','SEMIPRESENCIAL','DISTANCIA'),$datos);
	cierraColumnaCampos();
	echo '<br clear="all">';
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');

	echo "<h3 class='apartadoFormulario'> Sesiones</h3>";
	tablaSesiones($datos);

	cierraVentanaGestion('index.php',true);
}

function tablaSesiones($datos){
	echo"	Solo se guardarán las sesiones que tengan relleno el campo actividad
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaSesiones'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Fecha </th>
							<th> Actividad </th>
							<th> Docente </th>
							<th> Horas </th>
							<th> ID </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$sesiones=consultaBD('SELECT * FROM programas_sesiones WHERE codigoPrograma='.$datos['codigo']);				  
		while($sesion=mysql_fetch_assoc($sesiones)){
		 	echo "<tr>";
				campoFechaTabla('fecha'.$i,$sesion['fecha']);
				areaTextoTabla('actividad'.$i,$sesion['actividad']);
				campoSelectConsulta('codigoDocente'.$i,'','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM usuarios WHERE tipo="DOCENTE";',$sesion['codigoDocente'],'selectpicker show-tick',"data-live-search='true'",false,1);
				campoTextoTabla('horas'.$i,$sesion['horas'],'input-mini pagination-right');
				campoTextoTabla('id'.$i,$sesion['id'],'input-mini pagination-right');
			echo"</tr>";
			$i++;
		}
	}

	if($i==0 || !$datos){
	 	echo "<tr>";
			campoFechaTabla('fecha'.$i);
			areaTextoTabla('actividad'.$i);
			campoSelectConsulta('codigoDocente'.$i,'','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM usuarios WHERE tipo="DOCENTE";','','selectpicker show-tick',"data-live-search='true'",false,1);
			campoTextoTabla('horas'.$i,'','input-mini pagination-right');
			campoTextoTabla('id'.$i,'','input-mini pagination-right');
		echo"</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaSesiones\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
		 	";
			
	return $i;
}
