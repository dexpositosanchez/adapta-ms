<?php
session_start();
include_once("funciones.php");

compruebaCookie();
if(isset($_POST['usuario']) && isset($_POST['clave'])){
	$error=loginAcademia($_POST['usuario'],$_POST['clave']);
}
?>

<!DOCTYPE html>
<html lang="es">
  
<head>
    <meta charset="utf-8">
    <title>Acceso Privado - <?php echo $_CONFIG['title']; ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
	<link href="css/bootstrap.min.php" rel="stylesheet" type="text/css" />
	<link href="../api/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

	<link href="../api/css/font-awesome.css" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    
	<link href="css/style.php" rel="stylesheet" type="text/css" />
	<link href="css/login.php" rel="stylesheet" type="text/css" />
	<link href="../api/css/animate.css" rel="stylesheet" type="text/css" />

</head>

<body>

<div id="cajaLogin" class="logo margenAr">
	<img src="img/logo.png" alt="<?php echo $_CONFIG['altLogo']; ?>">	
</div>

<?php
if(isset($error) && $error==1){
	mensajeError('nombre de usuario y/o contraseña incorrectos.');
}
?>

<div class="account-container">
	
	<div class="content clearfix">
		
		<form action="?" method="post">

			<h1><?php echo $_CONFIG['textoAcceso']; ?></h1>
			
			<div class="login-fields">
				
				<p>Por favor, especifique sus datos de acceso:</p>
				
				<div class="field">
					<label for="username">Usuario</label>
					<input type="text" id="username" name="usuario" value="" placeholder="Usuario" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Contraseña:</label>
					<input type="password" id="password" name="clave" value="" placeholder="Contraseña" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			

			<div class="login-actions">
				
				<span class="login-checkbox">
					<input id="sesion" name="sesion" type="checkbox" class="field login-checkbox" value="si" tabindex="4" />
					<label class="choice" for="sesion">Mantener sesión iniciada</label>
				</span>
									
				<button class="button btn btn-large btn-propio">Acceder <i class="icon-chevron-right"></i></button>
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->

<script src="../api/js/jquery-1.7.2.min.js"></script>
<script src="../api/js/bootstrap.js"></script>
<script type="text/javascript">

<?php
	if(!isset($error)){//Animación en caso de que no se haya metido usuario y contraseña mal
		echo "
		$('.account-container').addClass('hide');
		$('#cajaLogin').addClass('animated fadeInUp');
		$('#cajaLogin').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			$('.account-container').removeClass('hide');
			$('.account-container').addClass('animated fadeIn');
			$('#username').focus();
		});";
	}
?>

</script>

</body>

</html>