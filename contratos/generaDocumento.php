<?php
    session_start();
    include_once("funciones.php");
    compruebaSesion();

    if($_GET['documento']=='solicitud'){
    	$contenido=generaSolicitud($_GET['codigo']);
    	$nombre='Solicitud'.$_GET['codigo'].'.pdf';
	}

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('../documentos/solicitudes/'.$nombre, 'f');

    header("Content-Type: application/pdf");
    header("Content-Disposition: attachment; filename=solicitud".$_GET['codigo'].".pdf");
    header("Content-Transfer-Encoding: binary");

    readfile('documentos/solicitudes/'.$nombre);    
?>