<?php
  $seccionActiva=15;
  include_once('../cabecera.php');

  operacionesAuditorias();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Resultados de las auditorías internas:</h6>
                  <div id="big_stats" class="cf">
                  
                    <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
                  
                    <div class="leyenda">
                      <span class="grafico grafico12"></span>OK: <span id="valor4"></span><br>
                      <span class="grafico grafico9"></span>NA: <span id="valor1"></span><br>
                      <span class="grafico grafico10"></span>DESVIACION: <span id="valor2"></span><br>
                      <span class="grafico grafico7"></span>ODM: <span id="valor3"></span><br>
                    </div>
                    
                    <!-- .stat --> 
                  </div>
                </div>            
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Auditorías internas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
                <a href="<?php echo $_CONFIG['raiz'];?>auditorias_internas/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva Auditoría</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


        </div><!-- /row -->

        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Auditorías internas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Empresa </th>
                    <th> Fecha </th>
                    <th> Auditor Jefe </th>
                    <th> Auditor </th>
                    <th> Finalizada </th>
                    <th class="td-actions"> </th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeAuditorias();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>



    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript">
     <?php
      $datos=generaDatosGraficoAuditoriaInterna();
    ?>
    var pieData = [
        {
            value: <?php echo $datos['OK']; ?>,
            color: "#B02B2C"
        },
        {
            value: <?php echo $datos['NA']; ?>,
            color: "#6BBA70"
        },
        {
            value: <?php echo $datos['DESVIACION']; ?>,
            color: "#356AA0"
        },
        {
            value: <?php echo $datos['ODM']; ?>,
            color: "#f89406"
        }
        
      ];

    var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
    
    $("#valor4").text("<?php echo $datos['OK']; ?>");
    $("#valor1").text("<?php echo $datos['NA']; ?>");
    $("#valor2").text("<?php echo $datos['DESVIACION']; ?>");
    $("#valor3").text("<?php echo $datos['ODM']; ?>");
</script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js"></script>
