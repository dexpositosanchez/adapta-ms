<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de comunicación interna

function operacionesComunicacionInterna(){
	$res=true;

	if(isset($_POST['asunto'])){//Creación de conversación
		$res=creaConversacion();
	}
	elseif(isset($_POST['codigoConversacion'])){//Creación de mensajes de respuesta
		$res=creaMensajeConversacion($_POST['codigoConversacion']);
	}
	elseif(isset($_GET['elimina'])){//Eliminación de mensajes
		$res=eliminaMensajeConversacion($_GET['elimina']);
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){//Eliminación de conversación
		$res=eliminaDatos('conversaciones');
	}

	mensajeResultado('mensaje',$res,'Mensaje');
    mensajeResultado('elimina',$res,'Mensaje', true);
}

function creaConversacion(){
	$_POST['hora']=hora();
	$_POST['fecha']=fecha();

	$res=insertaDatos('conversaciones');
	if($res){
		$codigoC=$res;
		$datos=arrayFormulario();
		
		$_POST['codigoConversacion']=$codigoC;
		$codigoMensaje=insertaDatos('mensajes',time(),'documentos/comunicacionInterna');
		$res=$res && $codigoMensaje;

		conexionBD();
		for($i=1;$i<=$datos['numDestinatarios'];$i++){//Empieza en 1 porque el 0 es "todos"
			if(isset($datos['destinatarios'.$i])){
				$res=$res && consultaBD("INSERT INTO destinatarios_conversacion VALUES(NULL,'$codigoC','".$datos['destinatarios'.$i]."');");
				$res=$res && consultaBD("INSERT INTO destinatarios_mensajes VALUES('$codigoMensaje','".$datos['destinatarios'.$i]."','NO');");
			}
		}
		$res=$res && consultaBD("INSERT INTO destinatarios_conversacion VALUES(NULL,'$codigoC','".$_SESSION['codigoU']."');");
		$res=$res && consultaBD("INSERT INTO destinatarios_mensajes VALUES('$codigoMensaje','".$_SESSION['codigoU']."','NO');");
		cierraBD();
	}

	return $res;
}

function creaMensajeConversacion($codigoC){
	$_POST['hora']=hora();
	$_POST['fecha']=fecha();
	$_POST['codigoConversacion']=$codigoC;
	$_POST['codigoUsuario']=$_SESSION['codigoU'];
	$_POST['leido']='NO';

	$codigoMensaje=insertaDatos('mensajes',time(),'documentos/comunicacionInterna');
	
	conexionBD();
	$consulta=consultaBD("SELECT codigoUsuario FROM destinatarios_conversacion WHERE codigoConversacion='$codigoC';");
	while($datosConversacion=mysql_fetch_assoc($consulta)){
		$consultaDos=consultaBD("INSERT INTO destinatarios_mensajes VALUES('$codigoMensaje','".$datosConversacion['codigoUsuario']."','NO');");
	}
	cierraBD();
	
	return $codigoMensaje;
}

function eliminaMensajeConversacion($codigoMensaje){
	return consultaBD("DELETE FROM mensajesConversaciones WHERE codigo='$codigoMensaje';",true);
}

function creaEstadisticasComunicacionInterna(){
	$codigoU=$_SESSION['codigoU'];
	return consultaBD("SELECT COUNT(DISTINCT conversaciones.codigo) AS total FROM conversaciones INNER JOIN destinatarios_conversacion ON conversaciones.codigo=destinatarios_conversacion.codigoConversacion WHERE conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversacion.codigoUsuario='$codigoU';",true,true);
}


function gestionConversacion(){
	abreVentanaGestion('Nueva conversación','conversaciones.php','','icon-comments-o','',true);

	campoDestinatariosConversacion();
	campoTexto('asunto','Asunto','','span4');
	areaTexto('mensaje','Mensaje');
	//campoFichero('ficheroAdjunto','Adjunto');
	campoOculto('NO','ficheroAdjunto');
	campoOculto($_SESSION['codigoU'],'codigoUsuario');
	campoOculto('NO','leido');
	

	cierraVentanaGestion('index.php',false,true,'Enviar','icon-send');
}


function campoDestinatariosConversacion($valor=false){
	$valoresCampos=array('todos');
	$textosCampos=array('Todos');

    $consulta=consultaBD("SELECT codigo, CONCAT(apellidos,', ',nombre) AS texto FROM usuarios;",true);
    $i=0;
    while($datos=mysql_fetch_assoc($consulta)){
    	if($datos['codigo']!=$_SESSION['codigoU']){
    		array_push($valoresCampos,$datos['codigo']);
    		array_push($textosCampos,$datos['texto']);
    		$i++;
    	}
    }

	echo '<div id="cajaDestinatarios">';
    campoCheck('destinatarios','Destinatario/s',$valor, $textosCampos,$valoresCampos,true);
    echo '</div>';
    campoOculto($i,'numDestinatarios');
}


function gestionMensajesConversacion(){
	operacionesComunicacionInterna();
	
	abreVentanaGestion('Conversación','?','','icon-comments-o');
	$codigoConversacion=obtieneCodigoConversacion();
  	
  	imprimeMensajesConversacion($codigoConversacion);
  	creaCajaMensajeNuevo($codigoConversacion);
  	actualizaIndicadorMensajes();
	
	cierraVentanaGestion('index.php',true,true,'Nuevo mensaje','icon-plus-circle');
}

function imprimeMensajesConversacion($codigoConversacion){
	global $_CONFIG;

	$sinLeer=0;
	echo "<ul class='messages_layout'>";

	conexionBD();
	$consulta=consultaBD("SELECT mensajes.*, CONCAT(apellidos,', ',nombre) AS autor FROM mensajes INNER JOIN usuarios ON mensajes.codigoUsuario=usuarios.codigo WHERE codigoConversacion='$codigoConversacion' ORDER BY codigo;");
	while($datos=mysql_fetch_assoc($consulta)){
		$aviso='';
		$opciones='';
		$clase='from_user left';

		if($datos['codigoUsuario']==$_SESSION['codigoU']){
			$clase='by_myself right';
			$opciones="<div class='options_arrow'>
		                <div class='dropdown pull-right'> <a class='dropdown-toggle ' id='dLabel' role='button' data-toggle='dropdown' data-target='#' href='#'> <i class=' icon-caret-down'></i> </a>
		                  <ul class='dropdown-menu ' role='menu' aria-labelledby='dLabel'>
		                    <li><a href='conversaciones.php?codigo=".$codigoConversacion."&elimina=".$datos['codigo']."'><i class=' icon-trash icon-large'></i> Eliminar</a></li>
		                  </ul>
		                </div>
		              </div>";
		}
		if($datos['ficheroAdjunto']!='NO'){
			$datos['mensaje'].="<br /><br /><a class='btn btn-inverse' href='../documentos/comunicacionInterna/".$datos['ficheroAdjunto']."' target='_blank'><i class='icon-download'></i> Descargar Adjunto</a>";
		}
		$mensajeLeido=consultaBD("SELECT leido FROM destinatarios_mensajes WHERE codigoMensaje='".$datos['codigo']."' AND codigoUsuario='".$_SESSION['codigoU']."';",false,true);
		if($mensajeLeido['leido']=='NO' && $datos['codigoUsuario']!=$_SESSION['codigoU']){
			$aviso="<span class='label label-danger'><i class='icon-flag'></i></span>";
			consultaBD("UPDATE destinatarios_mensajes SET leido='SI' WHERE codigoMensaje='".$datos['codigo']."' AND codigoUsuario='".$_SESSION['codigoU']."';");
			$sinLeer++;
		}

		echo "
			<li class='sinFlotar ".$clase."'> <a class='avatar'><i class='icon-user'></i></a>
	          <div class='message_wrap'> <span class='arrow'></span>
	            <div class='info'> <a class='name'>".$datos['autor']." |</a> <span class='time'>El ".formateaFechaWeb($datos['fecha'])." a las ".formateaHoraWeb($datos['hora'])." ".$aviso."</span>
	              ".$opciones."
	            </div>
	            <div class='text'> ".$datos['mensaje']." </div>
	          </div>
	        </li>";
    }
    cierraBD();
    $_SESSION['mensajesSinLeer']=$_SESSION['mensajesSinLeer']-$sinLeer;

	echo "</ul>";
}

function obtieneCodigoConversacion(){//Esta función obtiene el código de la conversación actual, ya se venga desde el listado o desde la creación de una
	$codigoConversacion=false;
	if(isset($_POST['codigoConversacion'])){
		$codigoConversacion=$_POST['codigoConversacion'];//Viene de la creación de conversaciones/mensajes nuevos
	}
	elseif(isset($_GET['codigo'])){
		$codigoConversacion=$_GET['codigo'];//Viene del listado
	}

	return $codigoConversacion;
}

function creaCajaMensajeNuevo($codigoConversacion){
	echo "
	<div id='cajaRespuesta' class='modal hide fade cajaSelect'> 
	  <div class='modal-header'> 
	    <a class='close noAjax' data-dismiss='modal'>&times;</a>
	    <h3> <i class='icon-comments-o'></i><i class='icon-chevron-right'></i><i class='icon-plus-circle'></i> Nuevo mensaje </h3>
	  </div> 
	  <form class='form-horizontal sinMargenAb' method='post' action='?' enctype='multipart/form-data'>
	    <div class='modal-body cajaSelect'>
	        <fieldset>";

	            areaTexto('mensaje','Mensaje');
	            //campoFichero('ficheroAdjunto','Adjunto');
	            campoOculto('NO','ficheroAdjunto');
	            campoOculto($codigoConversacion,'codigoConversacion');
	          
	echo "  </fieldset>
	    </div> 
	    <div class='modal-footer sinFlotar'> 
	      <button type='submit' class='btn btn-propio' id='enviaMensaje'><i class='icon-send-o'></i> Enviar mensaje</button> 
	      <button type='button' class='btn btn-default' data-dismiss='modal'><i class='icon-remove'></i> Cancelar</button> 
	    </div> 
	  </form>
	</div>";
}

function actualizaIndicadorMensajes(){
	$mensajesSinLeer=$_SESSION['mensajesSinLeer'];
	if($mensajesSinLeer>0){
	echo "<script type='text/javascript'>
			$('#avisoMensajes').addClass('badge').text($mensajesSinLeer);
		</script>";
	}
	else{
		echo "<script type='text/javascript'>
			$('#avisoMensajes').removeClass('badge').text('');
		</script>";
	}
}

//Fin parte de comunicación interna