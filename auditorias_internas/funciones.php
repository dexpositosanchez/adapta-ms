<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesAuditorias(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaAuditoria();
	}
	elseif(isset($_POST['pregunta1'])){
		$res=insertaAuditoria();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('auditorias_internas');
	}

	mensajeResultado('pregunta1',$res,'Auditoría interna');
    mensajeResultado('elimina',$res,'Auditoría interna', true);
}

function insertaAuditoria(){
	$res = true;
	$_POST['datos']=crearDatos();
	$res=insertaDatos('auditorias_internas');
	return $res;
}

function actualizaAuditoria(){
	$res = true;
	$_POST['datos']=crearDatos();
	$res=actualizaDatos('auditorias_internas');
	return $res;
}

function crearDatos(){
	$i=1;
	$query='';
	$datos=arrayFormulario();
	while(isset($datos['pregunta'.$i])){
		$query.="pregunta$i=>".$datos['pregunta'.$i]."&{}&";
		if ($i > 6){
			$query.="observacion$i=>".$datos['observacion'.$i]."&{}&";
		}
		$i++;
	}
	return $query;
}

function datosAuditoria($codigo){
	$datos=array();

	conexionBD();
	if($codigo==false){
		for($i=0;$i<=320;$i++){
			$datos["pregunta$i"]="";
			$datos["observacion$i"]="";

		}
	}
	else{
		$consulta=consultaBD("SELECT datos FROM auditorias_internas WHERE codigo='$codigo';");
		$consulta=mysql_fetch_assoc($consulta);
		$auditoria=substr($consulta['datos'],0,-4);
		$array=explode('&{}&',$auditoria);
		foreach($array as $indice=>$valor){//Recorro el array de campos
			$par=explode("=>",$valor);//Separo la clave del valor
			$datos[$par[0]]=$par[1];//Creo array asociativo (sustituyendo los _ por .)
		}
	}
	cierraBD();
	return $datos;
}

function gestionAuditorias(){
	operacionesAuditorias();

//http://qmaconsultores.com/oficina-virtual
//antonio/antonio	
	abreVentanaGestion('Auditoría interna','index.php');
	$datos=compruebaDatos('auditorias_internas');
	$auditoria=datosAuditoria($datos['codigo']);
	if($datos){
		$auditoria['pregunta3'] = formateaFechaBD($auditoria['pregunta3']);
	} else {
		$auditoria['pregunta3'] = fechaBD();
	}

	creaPestaniasAPI(array('Datos generales','4. Contexto de la organización','5. Liderazgo','6. Planificación','7. Apoyo','8. Operación','9. Evaluación del desempeño','10. Mejora'));

		abrePestaniaAPI(1,true);
			campoOculto($datos['enviado'],'enviado','NO');
			campoSelectConsulta('pregunta1','Empresa','SELECT codigo, nombre AS texto FROM clientes WHERE activo="SI"',$auditoria);
			campoTexto('pregunta2','Lugar de la Auditoría (dirección/es)',$auditoria);
			campoFecha('pregunta3','Fecha Auditoría',$auditoria);
			campoTexto('pregunta4','Alcance',$auditoria);
			campoTexto('pregunta5','Auditor Jefe',$auditoria);
			campoTexto('pregunta6','Auditor',$auditoria);
			campoDato('Norma','ISO 9001:2015');
			echo "Por favor, complete a continuación el siguiente CUESTIONARIO, teniendo en cuenta que en cada apartado auditado podrá seleccionar entre:<br/><br/>
				<ul>
					<li style='padding-bottom:20px;'><b>OK</b>: Apartado correcto. Vd. ha verificado, mediante evidencias, que la empresa está llevando a cabo dicho apartado de forma correcta.</li>

					<li style='padding-bottom:20px;'><b>DESVIACION</b>: Apartado incorrecto. Existen evidencias que demuestran el incumplimiento de dicho apartado o no existen evidencias suficiente que justifiquen su validación. La empresa deberá llevar a cabo acciones correctivas.</li>

					<li style='padding-bottom:20px;'><b>ODM:</b> Observación de mejora. Apartado correcto en el que Vd. Ha identificado posibles mejoras de procesos en la empresa Auditada.</li>

					<li style='padding-bottom:20px;'><b>NA</b>: No aplica.</li>

				</ul>
				<br/>
				Puede realizar la auditoria en diferentes días y momentos; para ello, vaya guardando la información mediante el botón <b>Guardar Formulario</b> y, una vez finalizada, proceda a <b>Imprimir Informe</b> para generar el mismo y entregarlo a su cliente. ";
		cierraPestaniaAPI();

		abrePestaniaAPI(2);
			creaTablaAuditoria();
				creaEncabezadoTablaAuditoria('4.1 Comprensión de la organización y de su contexto');
					creaPreguntaTablaAuditoria($auditoria,'Ha determinado la Organización cuestiones externas e internas que son pertinentes para su propósito y su dirección estratégica, y que afectan a su capacidad para lograr los resultados previstos de su sistema de gestión de la calidad? Como lo ha documentado?',7);
					creaPreguntaTablaAuditoria($auditoria,'Se han tenido en cuenta para la consideración del contexto externo cuestiones, positivas o negativas,  que surgen de los entornos legal, tecnológico, competitivo, de mercado, cultural, social y económico, ya sea internacional, nacional, regional o local.',8);
					creaPreguntaTablaAuditoria($auditoria,'Se han tenido en cuenta para la comprensión del contexto interno cuestiones, positivas o negativas, relativas a los valores, la cultura, los conocimientos y el desempeño de la organización.',9);
					creaPreguntaTablaAuditoria($auditoria,'Como está previsto realizar el seguimiento y la revisión de la información sobre estas cuestiones externas e internas? Cada cuando tiempo?',10);
				
				creaEncabezadoTablaAuditoria('4.2 Comprensión de las necesidades y expectativas de las partes interesadas');
					creaPreguntaTablaAuditoria($auditoria,'Ha determinado la organización las partes interesadas que sean pertinentes para el sistema de gestión la calidad?',11);
					creaPreguntaTablaAuditoria($auditoria,'Ha determinado la organización los requisitos de dichas partes interesadas que afecten al sistema de gestión de la Calidad?',12);
					creaPreguntaTablaAuditoria($auditoria,'Como está previsto realizar el seguimiento y la revisión de la información sobre estas cuestiones relativas a las partes interesadas? Cada cuando tiempo?',13);
				
				creaEncabezadoTablaAuditoria('4.3 Determinación del alcance del sistema de gestión de la calidad');
					creaPreguntaTablaAuditoria($auditoria,'Qué alcance ha determinado la organización para su sistema de gestión de la Calidad?',14);
					creaPreguntaTablaAuditoria($auditoria,'Ha tenido en cuenta los factores externos e internos indicados en 4.1, los de las partes interesadas del 4.2 y los de sus productos o servicios a la hora de determinar el alcance?',321);
					creaPreguntaTablaAuditoria($auditoria,'Está documentado el alcance? Donde?',15);
					creaPreguntaTablaAuditoria($auditoria,'Incluye el alcance los productos / servicios cubiertos?',16);
					creaPreguntaTablaAuditoria($auditoria,'Se ha excluido algun requisito de la norma ISO 9001:2015?',17);
					creaPreguntaTablaAuditoria($auditoria,'Está justificada la exclusión teniendo en cuenta el alcance?',18);
					creaPreguntaTablaAuditoria($auditoria,'Afectan estas exclusiones a la responsabilidad de la organización para asegurar la conformidad ',19);
					creaPreguntaTablaAuditoria($auditoria,'Afectan estas exclusiones a la capacidad de la organización de aumentar la satisfacción del cliente?',20);
				
				creaEncabezadoTablaAuditoria('4.4 Sistema de gestión de la calidad y sus procesos');
					creaPreguntaTablaAuditoria($auditoria,'Se han determinado los procesos necesarios para la organización y sus interacciones? Donde está documentado?',21);
					creaPreguntaTablaAuditoria($auditoria,'Se han determinado las entradas y salidas esperadas de los procesos?',22);
					creaPreguntaTablaAuditoria($auditoria,'Se ha determinado la secuencia e interacción de los procesos?',23);
					creaPreguntaTablaAuditoria($auditoria,'Se han determinado los criterios y los métodos (incluyendo el seguimiento, las mediciones y los indicadores del desempeño relacionados) necesarios para asegurarse de la operación eficaz y el control de estos procesos?',24);
					creaPreguntaTablaAuditoria($auditoria,'Se han determinado los recursos necesarios para estos procesos? Como se asegura su disponibilidad?',25);
					creaPreguntaTablaAuditoria($auditoria,'Se han asignado responsabilidades y autoridades para los procesos?',26);
					creaPreguntaTablaAuditoria($auditoria,'Se han determinado los riesgos y oportunidades para los procesos según el punto 6.1 de la norma?',27);
					creaPreguntaTablaAuditoria($auditoria,'Como se evalúan los procesos? Como se prevé la implementación de cambios para asegurar que logran los resultados previstos?',28);
					creaPreguntaTablaAuditoria($auditoria,'Como se lleva a cabo la mejora de los procesos y del Sistema de gestión de la calidad?',29);
					creaPreguntaTablaAuditoria($auditoria,'Se ha considerado necesario mantener información documentada para apoyar la operación de los Procesos? Como se ha documentado?',30);
					creaPreguntaTablaAuditoria($auditoria,'Se ha considerado necesario conservar la información documentada para tener la confianza de que los procesos se realizan según lo planificado?',31);
					creaPreguntaTablaAuditoria($auditoria,'En caso de no haberse documentado, como se demuestra que los procesos se realizan según lo planificado?',32);
			cierraTablaAuditoria();
		cierraPestaniaAPI();

		abrePestaniaAPI(3);
			creaTablaAuditoria();
				creaEncabezadoTablaAuditoria('5.1 Liderazgo y compromiso');
					creaPreguntaTablaAuditoria($auditoria,'Ha asumido la alta dirección la responsabilidad y obligación de rendir cuentas con relación a la eficacia del sistema de gestión de la calidad?',33);
					creaPreguntaTablaAuditoria($auditoria,'Se ha asegurado la Dirección de que se establezcan la política de la calidad y los objetivos de la calidad para el sistema de gestión de la calidad, y que éstos sean compatibles con el contexto y la dirección estratégica de la organización?',34);
					creaPreguntaTablaAuditoria($auditoria,'Se ha asegurado de la integración de los requisitos del sistema de gestión de la calidad en los procesos de negocio de la organización? Como?',35);
					creaPreguntaTablaAuditoria($auditoria,'Que acciones ha llevado a cabo la dirección para promover el uso del enfoque a procesos y el pensamiento basado en riesgos?',36);
					creaPreguntaTablaAuditoria($auditoria,'Como asegura la Dirección de que los recursos necesarios para el sistema de gestión de la calidad estén disponibles?',37);
					creaPreguntaTablaAuditoria($auditoria,'Como ha comunicado la importancia de una gestión de la calidad eficaz y de la conformidad con los requisitos del sistema de gestión de la calidad?',38);
					creaPreguntaTablaAuditoria($auditoria,'Que acciones ha llevado a cabo para comprometer, dirigir y apoyar a las personas, para contribuir a la eficacia del sistema de gestión de la calidad?',39);
					creaPreguntaTablaAuditoria($auditoria,'Como apoya la Dirección otros roles pertinentes de la dirección, para demostrar su liderazgo en la forma en la que aplique a sus áreas de responsabilidad?',40);

				creaEncabezadoTablaAuditoria('5.1.2 Enfoque al cliente');
					creaPreguntaTablaAuditoria($auditoria,'Como se determinan, comprenden y se cumplen regularmente los requisitos del cliente y los legales y reglamentarios aplicables; Como participa la alta dirección en dicho análisis?',41);
					creaPreguntaTablaAuditoria($auditoria,'Como se determinan y se consideran los riesgos y oportunidades que pueden afectar a la conformidad de los productos y servicios y a la capacidad de aumentar la satisfacción del cliente? Como participa la alta dirección en dicho análisis y en mantener el enfoque de mejora de la satisfacción del cliente?',42);

				creaEncabezadoTablaAuditoria('5.2 Política');
				creaEncabezadoTablaAuditoria('5.2.1 Establecimiento de la política de la calidad');
					creaPreguntaTablaAuditoria($auditoria,'Que alcance ha determinado la organización para su sistema de gestión de la Calidad?',43);
					creaPreguntaTablaAuditoria($auditoria,'Ha tenido en cuenta los factores externos e internos indicados en 4.1, los de las partes interesadas del 4.2 y los de sus productos o servicios a la hora de determinar el alcance?',44);
					creaPreguntaTablaAuditoria($auditoria,'Está documentado el alcance? Donde?',118);
					creaPreguntaTablaAuditoria($auditoria,'Incluye el alcance los productos / servicios cubiertos?',46);
					creaPreguntaTablaAuditoria($auditoria,'Se ha excluido algún requisito de la norma ISO 9001:2015?',47);
					creaPreguntaTablaAuditoria($auditoria,'Está justificada la exclusión teniendo en cuenta el alcance?',48);
					creaPreguntaTablaAuditoria($auditoria,'Afectan estas exclusiones a la responsabilidad de la organización para asegurar la conformidad de los productos o servicios?',49);
					creaPreguntaTablaAuditoria($auditoria,'Afectan estas exclusiones a la capacidad de la organización de aumentar la satisfacción del cliente?',50);

				creaEncabezadoTablaAuditoria('5.2.2 Comunicación de la política de la calidad');
					creaPreguntaTablaAuditoria($auditoria,'a) La Política de Calidad, está disponible y se mantiene como información documentada?',51);
					creaPreguntaTablaAuditoria($auditoria,'b) Se ha comunicado, se entiende y se aplica dentro de la organización? Con que personas se ha verificado este requisito?',52);
					creaPreguntaTablaAuditoria($auditoria,'c) Cómo está disponible para las partes interesadas?',53);

				creaEncabezadoTablaAuditoria('5.3 Roles, responsabilidades y autoridades en la organización');
					creaPreguntaTablaAuditoria($auditoria,'Las responsabilidades y autoridades para los roles pertinentes se asignan, se comunican y se entienden en toda la organización? Como se asegura la alta dirección de ello? Incluya ejemplos',54);
					creaPreguntaTablaAuditoria($auditoria,'a) Ha asignado la alta dirección responsabilidades y autoridades para asegurarse de que el sistema de gestión de la calidad es conforme con los requisitos de esta Norma Internacional?',55);
					creaPreguntaTablaAuditoria($auditoria,'b) Ha asignado la alta dirección responsabilidades y autoridades para asegurarse de que los procesos están generando y proporcionando las salidas previstas?',56);
					creaPreguntaTablaAuditoria($auditoria,'c) Ha asignado la alta dirección responsabilidades y autoridades para informar, en particular, sobre el desempeño del sistema de gestión de la calidad y sobre las oportunidades de mejora (véase 10.1)?',57);
					creaPreguntaTablaAuditoria($auditoria,'d) Ha asignado la alta dirección responsabilidades y autoridades para asegurarse de que se promueve el enfoque al cliente en toda la organización? Indique ejemplos',58);
					creaPreguntaTablaAuditoria($auditoria,'e) Ha asignado la alta dirección responsabilidades y autoridades para asegurarse de que la integridad del SGC se mantiene cuando se planifican e implementan cambios en el sistema?',59);
			cierraTablaAuditoria();
		cierraPestaniaAPI();

		abrePestaniaAPI(4);
			creaTablaAuditoria();
				creaEncabezadoTablaAuditoria('6.1 Acciones para abordar riesgos y oportunidades');
				creaEncabezadoTablaAuditoria('6.1.1 Se han tenido en cuenta al planificar el sistema de gestión de la calidad, las cuestiones referidas a los riesgos y los factores ambientales, externos e internos?');
					creaPreguntaTablaAuditoria($auditoria,'a)  se han determinado los riesgos y oportunidades que es necesario abordar con el fin de asegurar que el sistema de gestión de la calidad pueda lograr sus resultados previstos?',60);
					creaPreguntaTablaAuditoria($auditoria,'b)  se han determinado los riesgos y oportunidades que es necesario abordar con el fin de aumentar los efectos deseables?',61);
					creaPreguntaTablaAuditoria($auditoria,'c)  se han determinado los riesgos y oportunidades que es necesario abordar con el fin de prevenir o reducir efectos no deseados?',62);
					creaPreguntaTablaAuditoria($auditoria,'d)  se han determinado los riesgos y oportunidades que es necesario abordar con el fin de lograr la mejora?',63);

				creaEncabezadoTablaAuditoria('6.1.2 La organización debe planificar:');
					creaPreguntaTablaAuditoria($auditoria,'a) Ha planificado la organización las acciones para abordar los riesgos y oportunidades?',64);
					creaPreguntaTablaAuditoria($auditoria,'b.1) Ha planificado la organización la manera de  integrar e implementar las acciones en sus procesos del sistema de gestión de la calidad según 4.4',65);
					creaPreguntaTablaAuditoria($auditoria,'b.2) Ha planificado la organización la manera de evaluar la eficacia de estas acciones? Indique algún ejemplo',66);
					creaPreguntaTablaAuditoria($auditoria,'Son estas acciones tomadas para abordar los riesgos y oportunidades proporcionales al impacto potencial en la conformidad de los productos y los servicios?',67);
					creaEncabezadoTablaAuditoria('NOTA 1 Las opciones para abordar los riesgos pueden incluir: evitar riesgos, asumir riesgos para perseguir una oportunidad, eliminar la fuente de riesgo, cambiar la probabilidad o las consecuencias, compartir el riesgo o mantener riesgos mediante decisiones informadas.');
					creaEncabezadoTablaAuditoria('NOTA 2 Las oportunidades pueden conducir a la adopción de nuevas prácticas, lanzamiento de nuevos productos, apertura de nuevos mercados, acercamiento a nuevos clientes, establecimiento de asociaciones, utilización de nuevas tecnologías y otras posibilidades deseables y viables para abordar las necesidades de la organización o las de sus clientes.');

				creaEncabezadoTablaAuditoria('6.2 Objetivos de la calidad y planificación para lograrlos');
				creaEncabezadoTablaAuditoria('6.2.1 La organización debe establecer objetivos de la calidad para las funciones y niveles pertinentes y los procesos. necesarios para el sistema de gestión de la calidad.');
					creaPreguntaTablaAuditoria($auditoria,'a) Son los objetivos coherentes con la política de la calidad? Indique un ejemplo',45);
					creaPreguntaTablaAuditoria($auditoria,'b) Son los objetivos medibles?',68);
					creaPreguntaTablaAuditoria($auditoria,'c) Tienen en cuenta los objetivos los requisitos aplicables?',69);
					creaPreguntaTablaAuditoria($auditoria,'d) Son los objetivos pertinentes para la conformidad de los productos y servicios y para el aumento de la satisfacción del cliente?',70);
					creaPreguntaTablaAuditoria($auditoria,'e) Son los objetivos seguidos para evaluar su consecución? Indique un ejemplo y fechas de seguimiento.',71);
					creaPreguntaTablaAuditoria($auditoria,'f) Se han comunicado los objetivos? Indique un ejemplo de comunicación.',72);
					creaPreguntaTablaAuditoria($auditoria,'g) Se actualizan los objetivos, según corresponda en su planificación?.',73);
					creaPreguntaTablaAuditoria($auditoria,'Están documentados los objetivos?',74);

				creaEncabezadoTablaAuditoria('6.2.2 Al planificar cómo lograr sus objetivos de la calidad, la organización debe determinar:');
					creaPreguntaTablaAuditoria($auditoria,'a) Incluye la planificación de objetivos las acciones a realizar para su consecución? Incluya un ejemplo',75);
					creaPreguntaTablaAuditoria($auditoria,'b) Incluye la planificación de objetivos qué recursos se requerirán para conseguirlos?  Incluya un ejemplo',76);
					creaPreguntaTablaAuditoria($auditoria,'c) Incluye la planificación de objetivos quién será responsable de cada objetivo? Incluya un ejemplo',77);
					creaPreguntaTablaAuditoria($auditoria,'d) Incluye la planificación de objetivos un plazo para su consecución? Incluya un ejemplo',78);
					creaPreguntaTablaAuditoria($auditoria,'e) Incluye la planificación de objetivos cómo se evaluará su consecución? Incluya un ejemplo',79);

				creaEncabezadoTablaAuditoria('6.3 Planificación de los cambios');
					creaPreguntaTablaAuditoria($auditoria,'Existe una metodología para la planificación de cambios en el sistema de gestión de la calidad?',80);
					creaPreguntaTablaAuditoria($auditoria,'a) Incluye dicha metodología el análisis del propósito de los cambios y sus consecuencias potenciales?',81);
					creaPreguntaTablaAuditoria($auditoria,'b) Incluyen medidas para mantener la integridad del sistema de gestión de la calidad?',82);
					creaPreguntaTablaAuditoria($auditoria,'c) Incluyen medidas para asegurar la disponibilidad de recursos y poder llevar a cabo el cambio?',83);
					creaPreguntaTablaAuditoria($auditoria,'d) Incluye medidas para la asignación o reasignación de responsabilidades y autoridades, cuando el cambio así lo requiera?',84);
			cierraTablaAuditoria();
		cierraPestaniaAPI();

		abrePestaniaAPI(5);
			creaTablaAuditoria();
				creaEncabezadoTablaAuditoria('7.1 Recursos');
				creaEncabezadoTablaAuditoria('7.1.1 Generalidades');
					creaPreguntaTablaAuditoria($auditoria,'La organización debe determinar y proporcionar los recursos necesarios para el establecimiento, implementación, mantenimiento y mejora continua del sistema de gestión de la calidad.',85);
					creaEncabezadoTablaAuditoria('La organización debe considerar:');
					creaPreguntaTablaAuditoria($auditoria,'a) las capacidades y limitaciones de los recursos internos existentes;',86);
					creaPreguntaTablaAuditoria($auditoria,'b) qué se necesita obtener de los proveedores externos.',87);
					creaPreguntaTablaAuditoria($auditoria,'Es suficiente la combinación de recursos internos y expertos para la gestión eficaz del Sistema de la Organización? Como se evidencia?',88);

				creaEncabezadoTablaAuditoria('7.1.2 Personas');
					creaPreguntaTablaAuditoria($auditoria,'La organización debe determinar y proporcionar las personas necesarias para la implementación eficaz de su sistema de gestión de la calidad y para la operación y control de sus procesos.',89);

				creaEncabezadoTablaAuditoria('7.1.3 Infraestructura');
					creaEncabezadoTablaAuditoria('Se ha determinado la infraestructura minima necesaria para lograr la conformidad de los productos y servicios? Cómo? Se ha documentado (no es un requisito)? En concreto se han determinado las necesidades sobre:');
					creaPreguntaTablaAuditoria($auditoria,'a) edificios y servicios asociados; Con las instalaciones disponibles, se cumple la normativa que afecte a los productos y servicios?',90);
					creaPreguntaTablaAuditoria($auditoria,'b) equipos, incluyendo hardware y software. Con los software disponibles se controla eficazmente los productos y servicios?',91);
					creaPreguntaTablaAuditoria($auditoria,'c) Son los recursos de transporte suficientes para los productos y servicios ofrecidos por la organización?',92);
					creaPreguntaTablaAuditoria($auditoria,'d) tecnologías de la información y la comunicación. Se usan de forma eficiente? Existen copias de seguridad de la información?',93);

				creaEncabezadoTablaAuditoria('7.1.4 Ambiente para la operación de los procesos');
					creaPreguntaTablaAuditoria($auditoria,'La organización debe determinar, proporcionar y mantener el ambiente necesario para la operación de sus procesos y para lograr la conformidad de los productos y servicios.',94);
					creaEncabezadoTablaAuditoria('Se ha determinado como afectan los siguientes factores a la conformidad de los productos y servicios?:');
					creaPreguntaTablaAuditoria($auditoria,'a) sociales (por ejemplo, no discriminatorio, ambiente tranquilo, libre de conflictos);',95);
					creaPreguntaTablaAuditoria($auditoria,'b) psicológicos (por ejemplo, reducción del estrés, prevención del síndrome de agotamiento, cuidado de las emociones);',96);
					creaPreguntaTablaAuditoria($auditoria,'c) físicos (por ejemplo, temperatura, calor, humedad, iluminación, circulación del aire, higiene, ruido).',97);

				creaEncabezadoTablaAuditoria('7.1.5 Recursos de seguimiento y medición');
				creaEncabezadoTablaAuditoria('7.1.5.1 Generalidades');	
					creaPreguntaTablaAuditoria($auditoria,'Para verificar la conformidad de los productos y servicios con los requisitos, se han determinado y proporcionado los recursos necesarios para asegurarse de la validez y fiabilidad de los resultados cuando se realice este seguimiento o medición?',98);
					creaPreguntaTablaAuditoria($auditoria,'a) Los recursos son apropiados para el tipo específico de actividades de seguimiento y medición realizadas?',99);
					creaPreguntaTablaAuditoria($auditoria,'b) Los recursos se mantienen para asegurarse de la idoneidad continua para su propósito? Existe, por ejemplo, un plan de mantenimiento y verificación?',100);
					creaPreguntaTablaAuditoria($auditoria,'Existen registros de evaluación - comprobación como evidencia de que los recursos de seguimiento y medición son idóneos para su propósito? Por ejemplo, certificados de calibración, documentos internos de comprobación o verificación, etc?',101);

				creaEncabezadoTablaAuditoria('7.1.5.2 Trazabilidad de las mediciones');
					creaPreguntaTablaAuditoria($auditoria,'Se considera un requisito la trazabilidad de las mediciones? Es un requisito legal o interno de la organización? En caso afirmativo….',102);
					creaPreguntaTablaAuditoria($auditoria,'a) Existen evidencias de la calibración o verificación (o ambas), a intervalos especificados, o antes de su utilización, contra patrones de medición trazables a patrones de medición internacionales o nacionales? (cuando no existan tales patrones, debe conservarse como información documentada la base utilizada para la calibración o la verificación)',103);
					creaPreguntaTablaAuditoria($auditoria,'b) Están los recursos identificados para determinar su estado (sobre todo aquellos que no deban ser usados)?',104);
					creaPreguntaTablaAuditoria($auditoria,'c) Están protegidos contra ajustes, daño o deterioro que pudieran invalidar el estado de calibración y los posteriores resultados de la medición? Como?',105);
					creaPreguntaTablaAuditoria($auditoria,'Se ha determinado si la validez de los resultados de medición previos se ha visto afectada de manera adversa cuando el equipo de medición se considere no apto para su propósito previsto? Que tipo de acciones se toman en caso de invalidez de los resultados?.',106);

				creaEncabezadoTablaAuditoria('7.1.6 Conocimientos de la organización');
					creaPreguntaTablaAuditoria($auditoria,'Existe algún tipo de Know How propio de la organización para lograr la conformidad de sus productos y servicios? Se ha documentado para su transmisión al personal? Que hace la organización para adaptar ese Know How propio a los cambios futuros? ',107);
					creaPreguntaTablaAuditoria($auditoria,'Como se utiliza y se comparte este conocimiento para lograr los objetivos de la organización?',108);
					creaPreguntaTablaAuditoria($auditoria,'a) Indique algún ejemplo de fuentes internas  empleadas para el desarrollo y mejora de estos conocimientos (por ejemplo, propiedad intelectual; conocimientos adquiridos con la experiencia; lecciones aprendidas de los fracasos y de proyectos de éxito; capturar y compartir conocimientos y experiencia no documentados; los resultados de las mejoras en los procesos, productos y servicios)',109);
					creaPreguntaTablaAuditoria($auditoria,'b) Indique algún tipo de fuentes externas empleadas para el desarrollo y mejora de estos conocimientos (por ejemplo, normas; academia; conferencias; recopilación de conocimientos provenientes de clientes o proveedores externos)',110);

				creaEncabezadoTablaAuditoria('7.2 Competencia');
					creaEncabezadoTablaAuditoria('La organización debe:');
					creaPreguntaTablaAuditoria($auditoria,'a) Se han determinado las competencias necesarias de las personas que realizan cualquier trabajo que pueda afectar al desempeño y eficacia del sistema de gestión de la calidad? Como? (Fichas de puesto, etc)',111);
					creaPreguntaTablaAuditoria($auditoria,'b) Se asegura que estas personas sean competentes, basándose en la educación, formación o experiencia apropiadas?',112);
					creaPreguntaTablaAuditoria($auditoria,'c) En caso de ser necesario, se toman acciones para adquirir la competencia necesaria y evaluar la eficacia de las acciones tomadas? (planes formativos, tutoría, reasignación de puestos, subcontratación, etc)',113);
					creaPreguntaTablaAuditoria($auditoria,'d) Como se conserva la información documentada apropiada como evidencia de la competencia? (fichas de personal, etc)',114);

				creaEncabezadoTablaAuditoria('7.3 Toma de conciencia');
					creaPreguntaTablaAuditoria($auditoria,'Como se determinan que comunicaciones internas y externas son pertinentes al sistema de gestión de la calidad?',115);
					creaPreguntaTablaAuditoria($auditoria,'Incluye, en su caso, un análisis sobre: a) qué comunicar b) cuándo comunicar c) a quién comunicar d) cómo comunicar e) quién comunica.',116);

				creaEncabezadoTablaAuditoria('6.3 Planificación de los cambios');
					creaPreguntaTablaAuditoria($auditoria,'Están documentados los objetivos?',117);

				creaEncabezadoTablaAuditoria('7.5 Información documentada');
				creaEncabezadoTablaAuditoria('7.5.1 Generalidades');
					creaPreguntaTablaAuditoria($auditoria,'a) Incluye el Sistema de Gestión de la Calidad la información documentada mínima requerida por la norma ISO 9001:2015?',119);
					creaPreguntaTablaAuditoria($auditoria,'b) Se han determinado como necesaria para la eficacia del sistema de gestión de la calidad alguna documentación adicional.',120);
					creaPreguntaTablaAuditoria($auditoria,'c) Existen documentos no controlados, que se hayan determinado como necesarios para el control de los productos y servicios? (si existen y no se han controlado, supondría una no conformidad)',121);

				creaEncabezadoTablaAuditoria('7.5.2 Creación y actualización');
					creaPreguntaTablaAuditoria($auditoria,'Se ha determinado de forma documentada una metodología de control de la información documentada, para controlar en su creación y/o modificaciones?',122);
					creaPreguntaTablaAuditoria($auditoria,'a) la identificación y descripción (por ejemplo, título, fecha, autor o número de referencia);',123);
					creaPreguntaTablaAuditoria($auditoria,'b) el formato (por ejemplo, idioma, versión del software, gráficos) y los medios de soporte (por ejemplo, papel, electrónico);',124);
					creaPreguntaTablaAuditoria($auditoria,'c) los mecanismos necesarios para la revisión y aprobación con respecto a la conveniencia y adecuación (es decir, análisis y revisión por las partes afectadas por los documentos, etc)?.',125);

				creaEncabezadoTablaAuditoria('7.5.3 Control de la información documentada');
					creaPreguntaTablaAuditoria($auditoria,'a) Están la información documentada (procedimientos, instrucciones, normas internas, registros…) disponible y son idónea para su uso, donde y cuando se necesita?',126);
					creaPreguntaTablaAuditoria($auditoria,'b) Está protegida adecuadamente (por ejemplo, contra pérdida de la confidencialidad, uso inadecuado o pérdida de integridad)?',127);

				creaEncabezadoTablaAuditoria('7.5.3.2 Para el control de la información documentada, la organización debe abordar las siguientes actividades, según corresponda:');
					creaPreguntaTablaAuditoria($auditoria,'a) Se han determinado las medidas para su distribución, acceso, recuperación y uso?',128);
					creaPreguntaTablaAuditoria($auditoria,'b) Se han determinado las medidas para su almacenamiento y preservación, incluida la preservación de la legibilidad?',129);
					creaPreguntaTablaAuditoria($auditoria,'c) Se han determinado las medidas para el control de cambios (por ejemplo, control de versión) ',130);
					creaPreguntaTablaAuditoria($auditoria,'d) Se han determinado las medidas para su almacenamiento y preservación, incluida la preservación de la legibilidad;',131);
					creaPreguntaTablaAuditoria($auditoria,'e) Se han determinado las medidas para la conservación y disposición de la información documentada?.',132);
					creaPreguntaTablaAuditoria($auditoria,'Se ha identificado, según sea apropiado, y controlado, la información documentada de origen externo, que la organización determina como necesaria para la planificación y operación del sistema de gestión de la calidad?',133);
					creaPreguntaTablaAuditoria($auditoria,'Como se protege la información documentada conservada como evidencia de la conformidad contra modificaciones no intencionadas?',134);
					creaPreguntaTablaAuditoria($auditoria,'Implicar esta protección decisiones en relación al permiso, solamente para consultar la información documentada, o al permiso y a la autoridad para consultar y modificar la información documentada?',135);
			cierraTablaAuditoria();
		cierraPestaniaAPI();

		abrePestaniaAPI(6);
			creaTablaAuditoria();
				creaEncabezadoTablaAuditoria('8.1 Planificación y control operacional');
					creaEncabezadoTablaAuditoria('La organización debe planificar, implementar y controlar los procesos (véase 4.4) necesarios para cumplir los requisitos para la provisión de productos y servicios, y para implementar las acciones determinadas en el capítulo 6, mediante:');
					creaPreguntaTablaAuditoria($auditoria,'a) Se han determinado los requisitos para los productos y servicios?',136);
					creaPreguntaTablaAuditoria($auditoria,'b1) Se han establecido criterios para el control de los procesos de producción y prestación de servicios?',137);
					creaPreguntaTablaAuditoria($auditoria,'b2) Se han establecido criterios la aceptación de los productos y servicios?',138);
					creaPreguntaTablaAuditoria($auditoria,'c) Se han determinado los recursos necesarios para lograr la conformidad con los requisitos de los productos y servicios?',139);
					creaPreguntaTablaAuditoria($auditoria,'d) Se ha implementado un control de los procesos de acuerdo con los criterios indicados en la letra a)?',140);
					creaPreguntaTablaAuditoria($auditoria,'e1) Se ha determinado, se mantiene y conserva información documentada que demuestre que los procesos se han llevado a cabo según lo planificado?',141);
					creaPreguntaTablaAuditoria($auditoria,'e2) Se ha determinado, se mantiene y conserva información documentada que demuestre la conformidad de los productos y servicios con sus requisitos?',142);
					creaPreguntaTablaAuditoria($auditoria,'Se controla que la salida de esta planificación sea la adecuada para las operaciones de la organización?',143);
					creaPreguntaTablaAuditoria($auditoria,'En caso de cambios, se controlan y revisan las consecuencias de los cambios no previstos, tomando acciones para mitigar cualquier efecto adverso, según sea necesario? Existe una planificación de cambios?',144);
					creaPreguntaTablaAuditoria($auditoria,'Existen procesos contratados externamente en la planificación?  estan controlados (véase 8.4)?',145);

				creaEncabezadoTablaAuditoria('8.2 Requisitos para los productos y servicios');
				creaEncabezadoTablaAuditoria('8.2.1 Comunicación con el cliente');
					creaPreguntaTablaAuditoria($auditoria,'a) Incluye la comunicación con el cliente información relativa a los productos y servicios?',146);
					creaPreguntaTablaAuditoria($auditoria,'b)  Incluye la comunicación con el cliente tratar las consultas, los contratos o los pedidos, incluyendo los cambios?',147);
					creaPreguntaTablaAuditoria($auditoria,'c)  Incluye la comunicación con el cliente obtener la retroalimentación de los clientes relativa a los productos y servicios, incluyendo las quejas de los clientes?',148);
					creaPreguntaTablaAuditoria($auditoria,'d)  Incluye la comunicación con el cliente manipular o controlar la propiedad del cliente?',149);
					creaPreguntaTablaAuditoria($auditoria,'e)  Incluye la comunicación con el cliente establecer los requisitos específicos para las acciones de contingencia, cuando sea pertinente?',150);

				creaEncabezadoTablaAuditoria('8.2.2 Determinación de los requisitos para los productos y servicios');
					creaPreguntaTablaAuditoria($auditoria,'a1) Se han incluido en la definición de la información de los productos y servicios  cualquier requisito legal y reglamentario aplicable?',151);
					creaPreguntaTablaAuditoria($auditoria,'a2) Se han incluido en la definición de la información de los productos y servicios  aquellos considerados necesarios por la organización?',152);
					creaPreguntaTablaAuditoria($auditoria,'b) la organización puede cumplir t demostrar las declaraciones acerca de los productos y servicios que ofrece?',153);

				creaEncabezadoTablaAuditoria('8.2.3 Revisión de los requisitos para los productos y servicios');
				creaEncabezadoTablaAuditoria('8.2.3.1 La organización debe asegurarse de que tiene la capacidad de cumplir los requisitos para los productos y servicios que se van a ofrecer a los clientes. La organización debe llevar a cabo una revisión antes de comprometerse a suministrar productos y servicios a un cliente, para incluir:');
					creaPreguntaTablaAuditoria($auditoria,'a) Antes de comprometerse a servir productos o servicios, se aseguran los requisitos especificados por el cliente, incluyendo los requisitos para las actividades de entrega y las posteriores a la misma;',154);
					creaPreguntaTablaAuditoria($auditoria,'b) Antes de comprometerse a servir productos o servicios, se tienen en cuenta los requisitos no establecidos por el cliente, pero necesarios para el uso especificado o previsto, cuando sea conocido',155);
					creaPreguntaTablaAuditoria($auditoria,'c) Antes de comprometerse a servir productos o servicios, se verifican los requisitos especificados por la organización?',156);
					creaPreguntaTablaAuditoria($auditoria,'d) Antes de comprometerse a servir productos o servicios se confirman los requisitos legales y reglamentarios aplicables a los productos y servicios?',157);
					creaPreguntaTablaAuditoria($auditoria,'e) Antes de comprometerse a servir productos o servicios se revisan las posibles diferencias existentes entre los requisitos del contrato o pedido y los expresados previamente?',158);
					creaPreguntaTablaAuditoria($auditoria,'En caso de  diferencias existentes entre los requisitos del contrato o pedido y los expresados previamente, como asegura la organización que se resuelven?',159);
					creaPreguntaTablaAuditoria($auditoria,'Como confirma la organización los requisitos del cliente antes de la aceptación, cuando el cliente no proporciona una declaración documentada de sus requisitos?',160);
					creaPreguntaTablaAuditoria($auditoria,'En casos en que es irrealizable llevar a cabo una revisión formal para cada pedido (como las ventas por internet) se revisa  la información del producto pertinente, como catálogos, fichas, etc?',161);

				creaEncabezadoTablaAuditoria('8.2.3.2 La organización debe conservar la información documentada, cuando sea aplicable:');
					creaPreguntaTablaAuditoria($auditoria,'a) Existe información documentada sobre los resultados de la revisión de los pedidos?',162);
					creaPreguntaTablaAuditoria($auditoria,'b)  Existe información documentada sobre cualquier requisito nuevo para los productos y servicios?',163);

				creaEncabezadoTablaAuditoria('8.2.4 Cambios en los requisitos para los productos y servicios');
					creaPreguntaTablaAuditoria($auditoria,'En caso de que se cambien los requisitos para los productos y servicios, la información documentada pertinente se modificad, y de que las personas pertinentes, sobre todo los clientes, sean conscientes de los requisitos modificados?',164);

				creaEncabezadoTablaAuditoria('8.3 Diseño y desarrollo de los productos y servicios');
				creaEncabezadoTablaAuditoria('8.3.1 Generalidades');
					creaPreguntaTablaAuditoria($auditoria,'La organización debe establecer, implementar y mantener un proceso de diseño y desarrollo que sea adecuado para asegurarse de la posterior provisión de productos y servicios.',165);

				creaEncabezadoTablaAuditoria('8.3.2 Planificación del diseño y desarrollo');
					creaEncabezadoTablaAuditoria('Al determinar las etapas y controles para el diseño y desarrollo, la organización debe considerar:');
					creaPreguntaTablaAuditoria($auditoria,'a) La naturaleza, duración y complejidad de las actividades de diseño y desarrollo;',166);
					creaPreguntaTablaAuditoria($auditoria,'b) Las etapas del proceso requeridas, incluyendo las revisiones del diseño y desarrollo aplicables;',167);
					creaPreguntaTablaAuditoria($auditoria,'c) Las actividades requeridas de verificación y validación del diseño y desarrollo;',168);
					creaPreguntaTablaAuditoria($auditoria,'d) Las responsabilidades y autoridades involucradas en el proceso de diseño y desarrollo;',169);
					creaPreguntaTablaAuditoria($auditoria,'e) Las necesidades de recursos internos y externos para el diseño y desarrollo de los productos y servicios;',170);
					creaPreguntaTablaAuditoria($auditoria,'f) La necesidad de controlar las interfaces entre las personas que participan activamente en el proceso de diseño y desarrollo;',171);
					creaPreguntaTablaAuditoria($auditoria,'g) la necesidad de la participación activa de los clientes y usuarios en el proceso de diseño y desarrollo;',172);
					creaPreguntaTablaAuditoria($auditoria,'h) Los requisitos para la posterior provisión de productos y servicios;',173);
					creaPreguntaTablaAuditoria($auditoria,'i) El nivel de control del proceso de diseño y desarrollo esperado por los clientes y otras partes interesadas pertinentes;',174);
					creaPreguntaTablaAuditoria($auditoria,'j) La información documentada necesaria para demostrar que se han cumplido los requisitos del diseño y desarrollo.',175);
					

				creaEncabezadoTablaAuditoria('8.3.3 Entradas para el diseño y desarrollo');
					creaEncabezadoTablaAuditoria('La organización debe determinar los requisitos esenciales para los tipos específicos de productos y servicios a diseñar y desarrollar. La organización debe considerar:');
					creaPreguntaTablaAuditoria($auditoria,'a) Los requisitos funcionales y de desempeño;',176);
					creaPreguntaTablaAuditoria($auditoria,'b) La información proveniente de actividades previas de diseño y desarrollo similares;',177);
					creaPreguntaTablaAuditoria($auditoria,'c) Los requisitos legales y reglamentarios;',178);
					creaPreguntaTablaAuditoria($auditoria,'d) Normas o códigos de prácticas que la organización se ha comprometido a implementar;',179);
					creaPreguntaTablaAuditoria($auditoria,'e) Las consecuencias potenciales de fallar debido a la naturaleza de los productos y servicios.',180);
					creaPreguntaTablaAuditoria($auditoria,'Las entradas deben ser adecuadas para los fines del diseño y desarrollo, estar completas y sin ambigüedades.',181);
					creaPreguntaTablaAuditoria($auditoria,'Las entradas del diseño y desarrollo contradictorias deben resolverse.',182);
					creaPreguntaTablaAuditoria($auditoria,'La organización debe conservar la información documentada sobre las entradas del diseño y desarrollo.',183);
					

				creaEncabezadoTablaAuditoria('8.3.4 Controles del diseño y desarrollo');
					creaEncabezadoTablaAuditoria('La organización debe aplicar controles al proceso de diseño y desarrollo para asegurarse de que:');
					creaPreguntaTablaAuditoria($auditoria,'a) Se definen los resultados a lograr;',184);
					creaPreguntaTablaAuditoria($auditoria,'b) Se realizan las revisiones para evaluar la capacidad de los resultados del diseño y desarrollo para cumplir los requisitos;',185);
					creaPreguntaTablaAuditoria($auditoria,'c) Se realizan actividades de verificación para asegurarse de que las salidas del diseño y desarrollo cumplen los requisitos de las entradas;',186);
					creaPreguntaTablaAuditoria($auditoria,'d) Se realizan actividades de validación para asegurarse de que los productos y servicios resultantes satisfacen los requisitos para su aplicación especificada o uso previsto;',187);
					creaPreguntaTablaAuditoria($auditoria,'e) Se toma cualquier acción necesaria sobre los problemas determinados durante las revisiones, o las actividades de verificación y validación;',188);
					creaPreguntaTablaAuditoria($auditoria,'f) Se conserva la información documentada de estas actividades.',189);
					creaEncabezadoTablaAuditoria('NOTA Las revisiones, la verificación y la validación del diseño y desarrollo tienen propósitos distintos. Pueden realizarse de forma separada o en cualquier combinación, según sea idóneo para los productos y servicios de la organización.');
					

				creaEncabezadoTablaAuditoria('8.3.5 Salidas del diseño y desarrollo');
					creaEncabezadoTablaAuditoria('La organización debe asegurarse de que las salidas del diseño y desarrollo:');
					creaPreguntaTablaAuditoria($auditoria,'a) cumplen los requisitos de las entradas;',190);
					creaPreguntaTablaAuditoria($auditoria,'b) son adecuadas para los procesos posteriores para la provisión de productos y servicios;',191);
					creaPreguntaTablaAuditoria($auditoria,'c) incluyen o hacen referencia a los requisitos de seguimiento y medición, cuando sea apropiado, y a los criterios de aceptación;',192);
					creaPreguntaTablaAuditoria($auditoria,'d) especifican las características de los productos y servicios que son esenciales para su propósito previsto y su provisión segura y correcta.',193);
					creaPreguntaTablaAuditoria($auditoria,'La organización debe conservar información documentada sobre las salidas del diseño y desarrollo.',194);

				creaEncabezadoTablaAuditoria('8.3.6 Cambios del diseño y desarrollo');
					creaPreguntaTablaAuditoria($auditoria,'La organización debe identificar, revisar y controlar los cambios hechos durante el diseño y desarrollo de los productos y servicios, o posteriormente en la medida necesaria para asegurarse de que no haya un impacto adverso en la conformidad con los requisitos.',195);
					creaEncabezadoTablaAuditoria('La organización debe conservar la información documentada sobre:');
					creaPreguntaTablaAuditoria($auditoria,'a) los cambios del diseño y desarrollo;',196);
					creaPreguntaTablaAuditoria($auditoria,'b) los resultados de las revisiones;',197);
					creaPreguntaTablaAuditoria($auditoria,'c) la autorización de los cambios;',198);
					creaPreguntaTablaAuditoria($auditoria,'d) las acciones tomadas para prevenir los impactos adversos.',199);

				creaEncabezadoTablaAuditoria('8.4 Control de los procesos, productos y servicios suministrados externamente');
				creaEncabezadoTablaAuditoria('8.4.1 Generalidades');
					creaEncabezadoTablaAuditoria('La organización debe asegurarse de que los procesos, productos y servicios suministrados externamente son conformes a los requisitos. La organización debe determinar los controles a aplicar a los procesos, productos y servicios suministrados externamente cuando:');
					creaPreguntaTablaAuditoria($auditoria,'a) Existen productos y servicios de proveedores externos destinados a incorporarse dentro de los propios productos y servicios de la organización?',200);
					creaPreguntaTablaAuditoria($auditoria,'b) Existen productos y servicios proporcionados directamente a los clientes por proveedores externos en nombre de la organización?',201);
					creaPreguntaTablaAuditoria($auditoria,'c) Existe un proceso, o una parte de un proceso, proporcionado por un proveedor externo como resultado de una decisión de la organización?',202);
					creaPreguntaTablaAuditoria($auditoria,'En caso afirmativo en las preguntas anteriores, se han determinado y aplicado criterios para la evaluación, la selección, el seguimiento del desempeño y la reevaluación de los proveedores externos, basándose en su capacidad para proporcionar procesos o productos y servicios de acuerdo con los requisitos? Existe información documentada de estas actividades y de cualquier acción necesaria que surja de dichas evaluaciones?',203);

				creaEncabezadoTablaAuditoria('8.4.2 Tipo y alcance del control');
					creaPreguntaTablaAuditoria($auditoria,'La organización debe asegurarse de que los procesos, productos y servicios suministrados externamente no afectan de manera adversa a la capacidad de la organización de entregar productos y servicios conformes de manera coherente a sus clientes.',204);
					creaPreguntaTablaAuditoria($auditoria,'a) Se asegura que los procesos suministrados externamente permanecen dentro del control de su sistema de gestión de la calidad?',205);
					creaPreguntaTablaAuditoria($auditoria,'b) Se definen los controles que pretende aplicar a un proveedor externo y los que pretende aplicar a las salidas resultantes?',206);
					creaPreguntaTablaAuditoria($auditoria,'c1) Se tiene en consideración el impacto potencial de los procesos, productos y servicios suministrados externamente en la capacidad de la organización de cumplir regularmente los requisitos del cliente y los legales y reglamentarios aplicables;',207);
					creaPreguntaTablaAuditoria($auditoria,'c2) Se tienen en consideración la eficacia de los controles aplicados por el proveedor externo;',208);
					creaPreguntaTablaAuditoria($auditoria,'d) Se han determinado la verificación u otras actividades necesarias para asegurarse de que los procesos, productos y servicios suministrados externamente cumplen los requisitos?',209);
					

				creaEncabezadoTablaAuditoria('8.4.3 Información para los proveedores externos');
					creaEncabezadoTablaAuditoria('La organización debe asegurarse de la adecuación de los requisitos antes de su comunicación al proveedor externo. La organización debe comunicar a los proveedores externos sus requisitos para:');
					creaPreguntaTablaAuditoria($auditoria,'a) Se comunica a los proveedores externos los requisitos para los procesos, productos y servicios a proporcionar?',210);
					creaPreguntaTablaAuditoria($auditoria,'b1)  Se comunica a los proveedores externos los requisitos para la aprobación de productos y servicios?',211);
					creaPreguntaTablaAuditoria($auditoria,'b2) b)  Se comunica a los proveedores externos los requisitos para los métodos, procesos y equipos?',212);
					creaPreguntaTablaAuditoria($auditoria,'b3) la liberación de productos y servicios;',213);
					creaPreguntaTablaAuditoria($auditoria,'c) Se determina la competencia, incluyendo cualquier calificación requerida de las personas?',214);
					creaPreguntaTablaAuditoria($auditoria,'d) Se han determinado las interacciones del proveedor externo con la organización?',215);
					creaPreguntaTablaAuditoria($auditoria,'e) Se han estrblecidos las medidas para el control y el seguimiento del desempeño del proveedor externo a aplicar por parte de la organización?',216);
					creaPreguntaTablaAuditoria($auditoria,'f) Se han establecidos  las actividades de verificación o validación que la organización, o su cliente, pretende llevar a cabo en las instalaciones del proveedor externo.',217);
					

				creaEncabezadoTablaAuditoria('8.5 Producción y provisión del servicio');
				creaEncabezadoTablaAuditoria('8.5.1 Control de la producción y de la provisión del servicio');
					creaEncabezadoTablaAuditoria('La organización debe implementar la producción y provisión del servicio bajo condiciones controladas. Las condiciones controladas deben incluir, cuando sea aplicable:');
					creaPreguntaTablaAuditoria($auditoria,'a1) Existe información documentada que defina las características de los productos a producir, los servicios a prestar, o las actividades a desempeñar?',218);
					creaPreguntaTablaAuditoria($auditoria,'a2) Existe información documentada que defina los resultados a alcanzar?',219);
					creaPreguntaTablaAuditoria($auditoria,'b) Existe y se usan los recursos de seguimiento y medición adecuados?',220);
					creaPreguntaTablaAuditoria($auditoria,'c) Se han implementado actividades de seguimiento y medición en las etapas apropiadas para verificar que se cumplen los criterios para el control de los procesos o sus salidas, y los criterios de aceptación para los productos y servicios?',221);
					creaPreguntaTablaAuditoria($auditoria,'d) Se determina el uso de la infraestructura y el entorno adecuados para la operación de los procesos?',222);
					creaPreguntaTablaAuditoria($auditoria,'e) Se ha establecido la designación de personas competentes, incluyendo cualquier calificación requerida?',223);
					creaPreguntaTablaAuditoria($auditoria,'f) Se analiza la validación y revalidación periódica de la capacidad para alcanzar los resultados planificados de los procesos de producción y de prestación del servicio, cuando las salidas resultantes no puedan verificarse mediante actividades de seguimiento o medición posteriores?',224);
					creaPreguntaTablaAuditoria($auditoria,'g) Se han implementado acciones para prevenir los errores humanos',225);
					creaPreguntaTablaAuditoria($auditoria,'h) Se han implementado acciones actividades de liberación, entrega y posteriores a la entrega.',226);

				creaEncabezadoTablaAuditoria('8.5.2 Identificación y trazabilidad');
					creaPreguntaTablaAuditoria($auditoria,'Se ha determinado el control necesario para verificar el estado de las salidas con respecto a los requisitos de seguimiento y medición a través de la producción y prestación del servicio?',227);
					creaPreguntaTablaAuditoria($auditoria,'Se ha determinado un sistema para la identificación única de las salidas cuando la trazabilidad sea un requisito, ',228);
					creaPreguntaTablaAuditoria($auditoria,'Se conservar la información documentada necesaria para permitir la trazabilidad?',229);

				creaEncabezadoTablaAuditoria('8.5.3 Propiedad perteneciente a los clientes o proveedores externos');
					creaPreguntaTablaAuditoria($auditoria,'Se identifican, verifican, protegen y salvaguarda la propiedad de los clientes o de los proveedores externos suministrada para su utilización o incorporación dentro de los productos y servicios?',230);
					creaPreguntaTablaAuditoria($auditoria,'Cuando la propiedad de un cliente o de un proveedor externo se pierda, deteriore o de algún otro modo se considere inadecuada para su uso, como se informa de esto al cliente o proveedor externo? ',231);
					creaPreguntaTablaAuditoria($auditoria,'Se conserva la información documentada sobre lo ocurrido?',232);
					creaPreguntaTablaAuditoria($auditoria,'A la hora de definir los requisitos anteriores, se han tenido en cuenta como propiedad de un cliente o de un proveedor externo: materiales, componentes, herramientas y equipos, instalaciones, propiedad intelectual y datos personales?',233);

				creaEncabezadoTablaAuditoria('8.5.4 Preservación');
					creaPreguntaTablaAuditoria($auditoria,'Se considera necesario preservar las salidas durante la producción y prestación del servicio, en la medida necesaria para asegurarse de la conformidad con los requisitos?',234);
					creaPreguntaTablaAuditoria($auditoria,'Que actividades de las siguientes (identificación, la manipulación, el control de la contaminación, el embalaje, el almacenamiento, la transmisión de la información o el transporte, y la protección) se han considerado como incluidas para el control de la preservación?',235);

				creaEncabezadoTablaAuditoria('8.5.5 Actividades posteriores a la entrega');
					creaPreguntaTablaAuditoria($auditoria,'a) Existen requisitos legales y reglamentarios una vez entregado el producto o servicio que la organización deba cumplir?',236);
					creaPreguntaTablaAuditoria($auditoria,'b) Se han definido las consecuencias potenciales no deseadas asociadas a sus productos y servicios y como actúa en caso de que ocurran?',237);
					creaPreguntaTablaAuditoria($auditoria,'c) Se ha tenido en cuenta la naturaleza, el uso y la vida útil prevista de sus productos y servicios para evaluar las posibles actividades posteriores a la entrega?',238);
					creaPreguntaTablaAuditoria($auditoria,'d) Se han tenido en cuenta requisitos del cliente para evaluar las posibles actividades posteriores a la entrega?',239);
					creaPreguntaTablaAuditoria($auditoria,'e) Se ha tenido en cuenta retroalimentación del cliente para evaluar las posibles actividades posteriores a la entrega?',240);
					creaEncabezadoTablaAuditoria('NOTA Las actividades posteriores a la entrega pueden incluir acciones cubiertas por las condiciones de la garantía, obligaciones contractuales como servicios de mantenimiento, y servicios suplementarios como el reciclaje o la disposición final.');

				creaEncabezadoTablaAuditoria('8.5.6 Control de los cambios');
					creaPreguntaTablaAuditoria($auditoria,'En caso de cambios en la producción o la prestación del servicio, como asegura la organización la continuidad en la conformidad con los requisitos?',241);
					creaPreguntaTablaAuditoria($auditoria,'Se conserva información documentada que describa los resultados de la revisión de los cambios, las personas que autorizan el cambio y de cualquier acción necesaria que surja de la revisión?',242);

				creaEncabezadoTablaAuditoria('8.6 Liberación de los productos y servicios');
					creaPreguntaTablaAuditoria($auditoria,'Se establece que la liberación de los productos y servicios al cliente no debe llevarse a cabo hasta que se hayan completado satisfactoriamente las disposiciones planificadas, a menos que sea aprobado de otra manera por una autoridad pertinente y, cuando sea aplicable, por el cliente?',243);
					creaPreguntaTablaAuditoria($auditoria,'a) Se conserva información documentada sobre la liberación que incluya evidencia de la conformidad con los criterios de aceptación?',244);
					creaPreguntaTablaAuditoria($auditoria,'b) Se conserva información documentada sobre la liberación trazabilidad a las personas que autorizan la liberación.',245);

				creaEncabezadoTablaAuditoria('8.7 Control de las salidas no conformes');
				creaEncabezadoTablaAuditoria('8.7.1 La organización debe asegurarse de que las salidas que no sean conformes con sus requisitos se identifican y se controlan para prevenir su uso o entrega no intencionada.');
					creaPreguntaTablaAuditoria($auditoria,'Se han definido las acciones adecuadas a tomar, en caso de un producto o servicio no conforme, basándose en la naturaleza de la no conformidad y en su efecto sobre la conformidad de los productos y servicios? (Esto se debe aplicar también a los productos y servicios no conformes detectados después de la entrega de los productos, durante o después de la provisión de los servicios)',246);
					creaEncabezadoTablaAuditoria('La organización debe tratar las salidas no conformes de una o más de las siguientes maneras:');
					creaPreguntaTablaAuditoria($auditoria,'a) Se establecen correcciones en caso de detectarlas?',247);
					creaPreguntaTablaAuditoria($auditoria,'b) Se establece su separación, contención, devolución o suspensión de provisión de productos y servicios?',248);
					creaPreguntaTablaAuditoria($auditoria,'c) Se informa al cliente? Como?',249);
					creaPreguntaTablaAuditoria($auditoria,'d) Se obtiene una autorización para su aceptación bajo concesión? Como?',250);
					creaPreguntaTablaAuditoria($auditoria,'Se verifica la conformidad con los requisitos cuando se corrigen las salidas no conformes?',251);

				creaEncabezadoTablaAuditoria('8.7.2 La organización debe conservar la información documentada que:');
					creaPreguntaTablaAuditoria($auditoria,'a) Se incluye una descripción de la no conformidad en la información documentada;',253);
					creaPreguntaTablaAuditoria($auditoria,'b) Se incluye una descripción de las acciones tomadas en la información documentada',254);
					creaPreguntaTablaAuditoria($auditoria,'c) Se incluye una descripción de todas las concesiones obtenidas en la información documentada',255);
					creaPreguntaTablaAuditoria($auditoria,'d)En la información documentada, se indica la autoridad que decide la acción con respecto a la no conformidad?',256);
			cierraTablaAuditoria();
		cierraPestaniaAPI();

		abrePestaniaAPI(7);
			creaTablaAuditoria();
				creaEncabezadoTablaAuditoria('9 Evaluación del desempeño');
					creaEncabezadoTablaAuditoria('9.1  Seguimiento, medición, análisis y evaluación');
					creaEncabezadoTablaAuditoria('9.1.1 Generalidades');
					creaEncabezadoTablaAuditoria('La organización debe determinar:');
					creaPreguntaTablaAuditoria($auditoria,'a) Ha determinado la organización que procesos necesitan seguimiento y medición?',257);
					creaPreguntaTablaAuditoria($auditoria,'b) Se han determinado los métodos de seguimiento, medición, análisis y evaluación necesarios para asegurar ',258);
					creaPreguntaTablaAuditoria($auditoria,'c) Se ha determinado la frecuencia para llevar a cabo el seguimiento y la medición?',259);
					creaPreguntaTablaAuditoria($auditoria,'d) Se ha determinado la frecuencia para analizar y evaluar los resultados del seguimiento y la medición?',260);
					creaPreguntaTablaAuditoria($auditoria,'Con los análisis realizados, puede evaluarse el desempeño y la eficacia del sistema de gestión de la calidad? ',261);
				
				creaEncabezadoTablaAuditoria('9.1.2 Satisfacción del cliente');
					creaPreguntaTablaAuditoria($auditoria,'Se han determinado los métodos para obtener, realizar y revisar la información sobre la percepción del cliente del grado en que se cumplen sus necesidades y expectativas? ',262);
					creaPreguntaTablaAuditoria($auditoria,'Se han incluido, además de las encuestas al cliente, otros factores como la retroalimentación del cliente sobre los productos y servicios entregados, las reuniones con los clientes, el análisis de las cuotas de mercado, las felicitaciones, las garantías utilizadas y los informes de agentes comerciales? ',263);

				creaEncabezadoTablaAuditoria('9.1.3 Análisis y evaluación');
					creaEncabezadoTablaAuditoria('La organización debe analizar y evaluar los datos y la información apropiados que surgen por el seguimiento y la medición.');
					creaPreguntaTablaAuditoria($auditoria,'a) Se evalúan los resultados al respecto de la conformidad de los productos y servicios? (por ejemplo, % de productos y servicios no conformes)',264);
					creaPreguntaTablaAuditoria($auditoria,'b) Se evalúa el grado de satisfacción del cliente? (por ejemplo, se toman acciones con clientes cuya satisfacción está por debajo de la media)',265);
					creaPreguntaTablaAuditoria($auditoria,'c) Se evalúa el desempeño y la eficacia del sistema de gestión de la calidad? (por ejemplo, como se actúa en caso de objetivos no conseguidos)',266);
					creaPreguntaTablaAuditoria($auditoria,'d) Se evalúa si lo planificado se ha implementado de forma eficaz (por ejemplo, que se hace con indicadores fuera de rango)?',267);
					creaPreguntaTablaAuditoria($auditoria,'e) Se evalúa la eficacia de las acciones tomadas para abordar los riesgos y oportunidades? (por ejemplo, se ha conseguido minorar o eliminar algún riesgo? Conseguir alguna oportunidad?',268);
					creaPreguntaTablaAuditoria($auditoria,'f) Se evalúa el desempeño de los proveedores externos? (por ejemplo, que acciones se toman con proveedores que incumplen sus compromisos)',269);
					creaPreguntaTablaAuditoria($auditoria,'g) Se evalúa la necesidad de mejoras en el sistema de gestión de la calidad? (Por ejemplo, se fomenta la propuesta de mejoras entre los empleados)',270);
					creaPreguntaTablaAuditoria($auditoria,'h) Se usan técnicas estadísticas para este análisis?',271);
				
				creaEncabezadoTablaAuditoria('9.2 Auditoría interna');
					creaEncabezadoTablaAuditoria('9.2.1 La organización debe llevar a cabo auditorías internas a intervalos planificados para proporcionar ');
				creaEncabezadoTablaAuditoria('a) Es conforme con:');
					creaPreguntaTablaAuditoria($auditoria,'1) Las Auditorias Internas evalúan los requisitos propios de la organización para su sistema de gestión de la calidad?',272);
					creaPreguntaTablaAuditoria($auditoria,'2) Las Auditorias Internas evalúan los requisitos de esta Norma Internacional;',273);
					creaPreguntaTablaAuditoria($auditoria,'b) Las Auditorias Internas evalúan si el sistema se implementa y mantiene eficazmente?',274);

				creaEncabezadoTablaAuditoria('9.2 La organización debe:');
					creaPreguntaTablaAuditoria($auditoria,'a) Existen programas de auditoría que incluyan la frecuencia, los métodos, las responsabilidades, los requisitos de planificación y la elaboración de informes? tienen en consideración la importancia de los procesos involucrados, los cambios que afecten a la organización y los resultados de las auditorías previas? Se han planificado, establecido e implementado?',275);
					creaPreguntaTablaAuditoria($auditoria,'b) Se han definido los criterios de la auditoría y el alcance para cada auditoría?',276);
					creaPreguntaTablaAuditoria($auditoria,'c) Se han establecido como seleccionar los auditores y llevar a cabo auditorías para asegurarse de la objetividad y la imparcialidad del proceso de auditoría?',277);
					creaPreguntaTablaAuditoria($auditoria,'d) Se aseguran de que los resultados de las auditorías se informen a la dirección?',278);
					creaPreguntaTablaAuditoria($auditoria,'e) Se asegura que se realizan las correcciones y se toman las acciones correctivas adecuadas sin demora injustificada?',279);
					creaPreguntaTablaAuditoria($auditoria,'f) Se conserva información documentada como evidencia de la implementación del programa de auditoría y de los resultados de las auditorías?',280);

				creaEncabezadoTablaAuditoria('9.3 Revisión por la dirección');
					creaEncabezadoTablaAuditoria('9.3.1 Generalidades');
					creaPreguntaTablaAuditoria($auditoria,'Ha llevado a cabo la Alta Dirección la revisión del sistema de gestión de la calidad de la organización a intervalos planificados, para asegurarse de su conveniencia, adecuación, eficacia y alineación continuas con la dirección estratégica de la organización? ',281);

				creaEncabezadoTablaAuditoria('9.3.2 Entradas de la revisión por la dirección');
					creaPreguntaTablaAuditoria($auditoria,'a) Incluyen los resultados información sobre el estado de las acciones de las revisiones por la dirección previas?',282);
					creaPreguntaTablaAuditoria($auditoria,'b) Incluyen los resultados información los cambios en las cuestiones externas e internas que sean pertinentes al sistema de gestión de la calidad?',283);
					creaPreguntaTablaAuditoria($auditoria,'c) Incluyen los resultados información la información sobre el desempeño y la eficacia del sistema de gestión de la calidad?',284);
					creaPreguntaTablaAuditoria($auditoria,'1) Incluyen los resultados información sobre las tendencias relativas a la satisfacción del cliente y la retroalimentación de las partes interesadas pertinentes?',285);
					creaPreguntaTablaAuditoria($auditoria,'2) Incluyen los resultados información sobre las tendencias relativas al grado en que se han logrado los objetivos de la calidad?',286);
					creaPreguntaTablaAuditoria($auditoria,'3) Incluyen los resultados información sobre las tendencias relativas al desempeño de los procesos y conformidad de los productos y servicios?',287);
					creaPreguntaTablaAuditoria($auditoria,'4) Incluyen los resultados información sobre las tendencias relativas a las no conformidades y acciones correctivas?',288);
					creaPreguntaTablaAuditoria($auditoria,'5) Incluyen los resultados información sobre las tendencias relativas a los resultados de seguimiento y medición?',289);
					creaPreguntaTablaAuditoria($auditoria,'6) Incluyen los resultados información sobre las tendencias relativas a los resultados de las auditorías?',290);
					creaPreguntaTablaAuditoria($auditoria,'7) Incluyen los resultados información sobre las tendencias relativas al desempeño de los proveedores externos?',291);
					creaPreguntaTablaAuditoria($auditoria,'d) Incluyen los resultados información sobre las tendencias relativas a la adecuación de los recursos?',292);
					creaPreguntaTablaAuditoria($auditoria,'e) Incluyen los resultados información sobre las tendencias relativas a la eficacia de las acciones tomadas para abordar los riesgos y las oportunidades?',293);
					creaPreguntaTablaAuditoria($auditoria,'f) Incluyen los resultados información sobre las tendencias relativas a las oportunidades de mejora?',294);

				creaEncabezadoTablaAuditoria('9.3.3 Salidas de la revisión por la dirección');
					creaEncabezadoTablaAuditoria('Las salidas de la revisión por la dirección deben incluir las decisiones y acciones relacionadas con:');
					creaPreguntaTablaAuditoria($auditoria,'Se han determinado y seleccionado las oportunidades de mejora para implementar las acciones necesarias para cumplir los requisitos del cliente y aumentar su satisfacción? ( estas pueden incluir pueden incluir corrección, acción correctiva, mejora continua, cambio abrupto, innovación y reorganización).',295);
					creaPreguntaTablaAuditoria($auditoria,'a) Incluyen estas medidas mejorar los productos y servicios para cumplir los requisitos, así como considerar las necesidades y expectativas futuras;',296);
					creaPreguntaTablaAuditoria($auditoria,'b) Incluyen estas medidas corregir, prevenir o reducir los efectos no deseados?',297);
					creaPreguntaTablaAuditoria($auditoria,'c) Incluyen estas medidas mejorar el desempeño y la eficacia del sistema de gestión de la calidad?',298);

			cierraTablaAuditoria();
		cierraPestaniaAPI();

		abrePestaniaAPI(8);
			creaTablaAuditoria();
				creaEncabezadoTablaAuditoria('10 Mejora');
					creaEncabezadoTablaAuditoria('10.1 Generalidades');
					creaPreguntaTablaAuditoria($auditoria,'a) Ha determinado la organización que procesos necesitan seguimiento y medición?',299);
					creaPreguntaTablaAuditoria($auditoria,'b) Se han determinado los métodos de seguimiento, medición, análisis y evaluación necesarios para asegurar ',300);
					creaPreguntaTablaAuditoria($auditoria,'c) Se ha determinado la frecuencia para llevar a cabo el seguimiento y la medición?',301);
					creaPreguntaTablaAuditoria($auditoria,'d) Se ha determinado la frecuencia para analizar y evaluar los resultados del seguimiento y la medición?',302);
					creaPreguntaTablaAuditoria($auditoria,'Con los análisis realizados, puede evaluarse el desempeño y la eficacia del sistema de gestión de la calidad? ',303);

				creaEncabezadoTablaAuditoria('10.2 No conformidad y acción correctiva');
					creaPreguntaTablaAuditoria($auditoria,'10.2.1 Se han detectado no conformidades, incluida cualquiera originada por quejas - reclamaciones de clientes?',304);
					creaEncabezadoTablaAuditoria('En caso afirmativo:');
					creaPreguntaTablaAuditoria($auditoria,'a) Como se ha reaccionado ante la no conformidad ?',305);
					creaPreguntaTablaAuditoria($auditoria,'1) Que acciones se han tomado para controlarla y corregirla?',306);
					creaPreguntaTablaAuditoria($auditoria,'1) Incluyen los resultados información sobre las tendencias relativas a la satisfacción del cliente y la retroalimentación de las partes interesadas pertinentes?',307);
					creaPreguntaTablaAuditoria($auditoria,'2) Que acciones se han tomado para hacer frente a las consecuencias?',308);
					creaPreguntaTablaAuditoria($auditoria,'b) Se ha evaluado la necesidad de acciones para eliminar las causas de la no conformidad, con el fin de que no vuelva a ocurrir ni ocurra en otra parte?',309);
					creaPreguntaTablaAuditoria($auditoria,'1) Se ha revisado y analizado la no conformidad?',310);
					creaPreguntaTablaAuditoria($auditoria,'2) Se han determinado las causas de la no conformidad?',311);
					creaPreguntaTablaAuditoria($auditoria,'3) Se ha analizado si existen no conformidades similares, o que potencialmente puedan ocurrir?',312);
					creaPreguntaTablaAuditoria($auditoria,'c) Se han implantado acciones?',313);
					creaPreguntaTablaAuditoria($auditoria,'d) Se ha revisado la eficacia de cualquier de las acciones puestas en marcha?',314);
					creaPreguntaTablaAuditoria($auditoria,'e) se ha analizado la necesidad de actualizar los riesgos y oportunidades determinados durante la planificación?',315);
					creaPreguntaTablaAuditoria($auditoria,'f) Se ha analizado la necesidad de hacer cambios al sistema de gestión de la calidad?',316);
					creaPreguntaTablaAuditoria($auditoria,'Se ha verificado que las acciones correctivas han sido apropiadas a los efectos de las no conformidades encontradas?',317);

				creaEncabezadoTablaAuditoria('10.2.2 La organización debe conservar información documentada como evidencia de:');
					creaPreguntaTablaAuditoria($auditoria,'a) la naturaleza de las no conformidades y cualquier acción tomada posteriormente;',318);
					creaPreguntaTablaAuditoria($auditoria,'b) los resultados de cualquier acción correctiva.',319);

				creaEncabezadoTablaAuditoria('10.3 Mejora continua');
					creaPreguntaTablaAuditoria($auditoria,'Se mejora continuamente la conveniencia, adecuación y eficacia del sistema de gestión de la calidad? Como?',320);
					creaPreguntaTablaAuditoria($auditoria,'Se han considerado los resultados del análisis y la evaluación, y las salidas de la revisión por la dirección, para determinar si hay necesidades u oportunidades que deben considerarse como parte de la mejora continua?',252);
			cierraTablaAuditoria();
		cierraPestaniaAPI();
	
	cierraPestaniasAPI();

	if($datos == false || $datos['enviado'] == 'NO'){
		cierraVentanaGestionAdicional('Registrar auditoría','index.php', true, true, 'Guardar formulario');
	} else {
		cierraVentanaGestion('index.php', true, true, 'Guardar formulario');
	}
}


function imprimeAuditorias(){
	global $_CONFIG;

	$consulta=consultaBD("SELECT * FROM auditorias_internas ORDER BY codigo DESC;", true);
	$iconoC=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');

	while($datos=mysql_fetch_assoc($consulta)){
		$auditoria=datosAuditoria($datos['codigo']);
		$cliente=datosRegistro('clientes',$auditoria['pregunta1']);
		echo "
		<tr>
			<td> ".$cliente['nombre']." </td>
        	<td> ".$auditoria['pregunta3']." </td>
        	<td> ".$auditoria['pregunta5']." </td>
        	<td> ".$auditoria['pregunta6']." </td>
        	<td class='centro'> ".$iconoC[$datos['enviado']]." </td>
        	<td class='centro'>
				<div class='btn-group centro'>
					<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					<ul class='dropdown-menu' role='menu'>
						<li><a href='".$_CONFIG['raiz']."auditorias_internas/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						<li class='divider'></li>
						<li><a href='".$_CONFIG['raiz']."auditorias_internas/generaInforme.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descarga de informe</i></a></li>
					</ul>
				</div>
			</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function generaDatosGraficoAuditoriaInterna(){
	$datos=array('OK'=>0,'NA'=>0,'DESVIACION'=>0,'ODM'=>0);
	
	$consulta="SELECT datos FROM auditorias_internas WHERE enviado='SI';";
	/*if($_SESSION['tipoUsuario']=='TECNICO'){
		$consulta="SELECT cuestionario FROM auditorias_internas INNER JOIN clientes ON auditorias_internas.codigoCliente=clientes.codigo WHERE finalizado='SI' AND empleado='".$_SESSION['codigoU']."';";
	}*/

	conexionBD();

	$consulta=consultaBD($consulta);
	while($reg=mysql_fetch_assoc($consulta)){
		$array=explode('&{}&',$reg['datos']);

		foreach($array as $valor){//Recorro el array de campos
			$par=explode("=>",$valor);//Separo la clave del valor
			if(isset($par[1])){
				if($par[1]=='OK'){
					$datos['OK']++;
				}
				elseif($par[1]=='NA'){
					$datos['NA']++;
				}
				elseif($par[1]=='DESVIACION'){
					$datos['DESVIACION']++;
				}
				if($par[1]=='ODM'){
					$datos['ODM']++;
				}
			}
		}
	}


	cierraBD();

	return $datos;
}

function creaTablaAuditoria($titulo1='Apartado analizado',$titulo2='Resultado',$titulo3='Observaciones, comentarios y evidencias'){
	echo '
	<table class="table table-striped table-bordered tablaAuditoria">
	  <tbody>
		<tr>
	      <th> '.$titulo1.' </th>
	      <th> '.$titulo2.' </th>
	      <th> '.$titulo3.' </th>
	    </tr>';
}

function creaPreguntaTablaAuditoria($datos,$texto,$num,$array=array('OK'=>'','NA'=>'','DESVIACION'=>'','ODM'=>'')){
	$sel=$array;
	if (isset($sel[$datos["pregunta$num"]])){
		$sel[$datos["pregunta$num"]]='selected="selected"';
	}
	echo "
	<tr>
		<td class='justificado'>$texto</td>
		<td>
			<div class='pregunta$num'>
			<select name='pregunta$num' class='selectpicker show-tick span2 '>";
				foreach ($sel as $clave => $valor){
					echo "<option ".$sel[$clave].">".$clave."</option>";
				}
	echo "	</select>

		</td>
		<td>
			<textarea name='observacion$num' id='observacion$num' class='areaTexto ".$class."'>".$datos["observacion$num"]."</textarea>
		</td>
	</tr>";
}

function cierraTablaAuditoria(){
	echo '
	  </tbody>
	</table>
	';
}

function creaEncabezadoTablaAuditoria($texto){
	echo "
	<tr>
		<th colspan='3'>$texto</th>
	</tr>";
}

function generaInforme($PHPWord,$codigoInforme){
	global $_CONFIG;
	$documento=$PHPWord->loadTemplate('../documentos/personal/plantillaAuditoria.docx');

	$datos=datosRegistro('auditorias_internas',$codigoInforme);
	$auditoria=datosAuditoria($datos['codigo']);

	for($i=1;$i<7;$i++){
		$documento->setValue("pregunta".$i,utf8_decode($auditoria['pregunta'.$i]));
	}

	for($i=7;$i<=320;$i++){
		$documento->setValue("pregunta".$i,utf8_decode($auditoria['pregunta'.$i]));
		$documento->setValue("observacion".$i,utf8_decode($auditoria['observacion'.$i]));
	}


	$documento->save('../documentos/personal/Auditoria.docx');

	return "Auditoria.docx";
}

//Fin parte de incidencias