<?php
include_once('../config.php');
header("Content-type: text/css; charset: UTF-8");
?>

div.orgChart {
  margin : 10px;
  padding : 20px;
}

div.orgChart h2 {
  margin : 0px;
  font-size : 12px;
  font-weight: normal;
  min-height: 20px;
}

div.orgChart h2:hover {
  background: <?php echo $_CONFIG['colorSecundario']; ?>;
  cursor: text;
}

div.orgChart ul {
  list-style : none;
  margin : 4px;
  padding : 0px;
  font-size : 0.8em;
  text-align : left;
}

div.orgChart ul.stack,
div.orgChart ul.stack ul { text-align : center; }

div.orgChart table { width : 100%; }

div.orgChart tr.lines td.line {
  width : 1px;
  height : 20px;
}

div.orgChart tr.lines td.top { border-top : 1px solid black; }

div.orgChart tr.lines td.left { border-right : 1px solid black; }

div.orgChart tr.lines td.right { border-left : 0px solid black; }

div.orgChart tr.lines td.half { width : 50%; }

div.orgChart td {
  text-align : center;
  vertical-align : top;
  padding : 0px 2px;
}

div.orgChart div.node {
  color:#FFF;
  cursor : default;
  border : 1px solid <?php echo $_CONFIG['colorSecundario']; ?>;
  display : inline-block;
  padding : 5px;
  width : 100px;
  height : 60px;
  background: <?php echo $_CONFIG['colorPrincipal']; ?>;
  line-height : 1.3em;
  border-radius : 4px;
  -moz-border-radius : 4px;
  -webkit-border-radius : 4px;
  position: relative;
  box-shadow: 1px 1px 0px #ddd;
}

.org-add-button,
.org-del-button,
.org-confirm-del-button {
  position: absolute;
  font-size: 16px;
  cursor:pointer;
}

.org-add-button, .org-del-button {
  font-family: 'FontAwesome';
  src: url('../font/fontawesome-webfont.eot?v=4.4.0');
  src: url('../font/fontawesome-webfont.eot?#iefix&v=4.4.0') format('embedded-opentype'), url('../font/fontawesome-webfont.woff?v=4.4.0') format('woff'), url('../font/fontawesome-webfont.ttf?v=4.4.0') format('truetype'), url('../font/fontawesome-webfont.svg?v=4.4.0#fontawesomeregular') format('svg');
  font-weight: normal;
  font-style: normal;
}
.org-add-button{
  bottom: 3px;
  left: 5px;
}
.org-add-button:before{
  content: "\f055";
}

.org-del-button{
  bottom: 3px;
  right: 5px;
}
.org-del-button:before {
  content: "\f1f8";
}

.org-add-button:hover:before,
.org-del-button:hover:before{
  cursor:pointer;
}

.org-add-button:hover:before{
  color:#7eb216;
}

.org-del-button:hover:before{
  color:#B02B2C;
}

.org-input { width: 90px; }

.org-confirm-del-button { display: none; }

.org-button-centro{
  left:44%;
}