<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionInfraestructuras();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-colorpicker.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.selectpicker').selectpicker();

		$("#tipo").change(function(){
        	if($(this).val() == 'OTROS'){
        		$('.definicionTipo').css('display','block');
        	} else {
        		$('.definicionTipo').css('display','none');
        	}
     	});

     	$("#tipoRevision").change(function(){
        	if($(this).val() == 'OTRO'){
        		$('.definicionRevision').css('display','block');
        	} else {
        		$('.definicionRevision').css('display','none');
        	}
     	});

     	$(".subcontratada input[type=radio]").change(function(){
        	if($(this).val() == 'SI'){
        		$('.proveedor').css('display','block');
        	} else {
        		$('.proveedor').css('display','none');
        	}
     	});
	});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>