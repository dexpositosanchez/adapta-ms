<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales
@include_once('../facturas/funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de facturas

function gestionFacturaRectificativa(){
	operacionesFacturas();

	abreVentanaGestion('Gestión de Facturas Rectificativas','index.php','span5','icon-edit');
	$datos=compruebaDatos('facturacion');
	
	abreColumnaCampos('span11');
	campoSelectConsulta('codigoAlumno','Alumno',"SELECT trabajadores_cliente.codigo, CONCAT(nombre,' ',apellido1,' ',apellido2,' - ',cursos.curso) AS texto FROM trabajadores_cliente
	LEFT JOIN cursos ON trabajadores_cliente.curso=cursos.codigo ORDER BY nombre, apellido1, apellido2;",$datos);
	campoReferenciaFactura($datos,'RECTIFICATIVA');
	campoFecha('fechaEmision','Fecha emisión',$datos);
	campoFecha('fechaVencimiento','Fecha vencimiento',$datos);
	echo "<span id='vencimientosOcultos'></span>";
	campoFormaPago($datos);
	campoRadio('enviada','Enviada a cliente',$datos);
	campoRadio('cobrada','Rectificada',$datos);
	campoOculto('RECTIFICATIVA','tipoFactura');
	cierraColumnaCampos();
	
	cierraVentanaGestion('index.php',true);
	return $datos;
}

//Fin parte de facturas