<?php
  $seccionActiva=9;
  include_once("../cabecera.php");
?>
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-paste"></i><i class="icon-chevron-right"></i><i class="icon-filter"></i>
              <h3>Informe de facturación</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">	
				<div class="tab-pane" id="formcontrols">
					<form id="edit-profile" class="form-horizontal" action='gestion.php' method='post'>
						<fieldset class='span5'>
						
							<?php								
								campoTexto('nombreAlumno','Nombre');
								campoTexto('apellido1','Apellido 1');
								campoTexto('apellido2','Apellido 2');
								campoTexto('poblacion','Población');
								
							?>
						
						</fieldset>
						
						<fieldset class='span5'>
							
							<?php
								campoFecha('fechaUno','Inicio del curso del');
								campoFecha('fechaDos','Hasta el');
								campoSelectConsulta('codigoCurso','Curso',"SELECT codigo, curso AS texto FROM cursos ORDER BY curso;");
								campoTextoSimbolo('importe','Importe','€');
								campoFecha('fechaVencimiento','Vencimiento');
							?>
							
						</fieldset>

						<br /><br />
						<fieldset class="sinFlotar">
							<div class="form-actions">
							  <button type="submit" class="btn btn-primary"><i class="icon-arrow-right"></i> Ver coincidencias</button> 
							</div> <!-- /form-actions -->
						</fieldset>
					</form>
				</div>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../../api/js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>