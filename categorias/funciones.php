<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores

function operacionesCategorias(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('categorias');
	}
	elseif(isset($_POST['categoria'])){
		$res=insertaDatos('categorias');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('categorias');
	}

	mensajeResultado('categoria',$res,'Categoría');
    mensajeResultado('elimina',$res,'Categoría', true);
}

function listadoCategorias(){
	global $_CONFIG;

	$columnas=array('id','categoria','id','id');
	$having=obtieneWhereListado("WHERE 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT * FROM categorias $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT * FROM categorias $having;");
	cierraBD();
	
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['id'],
			$datos['categoria'],
			creaBotonDetalles("categorias/gestion.php?codigo=".$datos['codigo']),
        	"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionCategorias(){
	operacionesCategorias();

	abreVentanaGestion('Gestión de Categorías','index.php','span3');
	$datos=compruebaDatos('categorias');
	if($datos){
		$id=$datos['id'];
	} else {
		$id=generaNumeroReferencia('categorias','id');
	}
	abreColumnaCampos('span11');
		campoTexto('id','ID',$id,'input-small');
		campoTexto('categoria','Categoría',$datos);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}



function filtroCursos(){
	abreCajaBusqueda();
		campoTexto(0,'ID');
		campoTexto(1,'Categoría');
	cierraCajaBusqueda();
}