<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores

function operacionesCursos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('cursos');
	}
	elseif(isset($_POST['curso'])){
		$res=insertaDatos('cursos');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('cursos');
	}

	mensajeResultado('curso',$res,'Curso');
    mensajeResultado('elimina',$res,'Curso', true);
}

function listadoCursos(){
	global $_CONFIG;

	$columnas=array('curso','precio','plazos','codigo');
	$having=obtieneWhereListado("WHERE 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, curso, precio, plazos, tipoPlazo FROM cursos $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, curso, precio, plazos FROM cursos $having;");
	cierraBD();
	
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$plazos = $datos['tipoPlazo'] == 'SI' ? 'Total ('.$datos['plazos'].')' : 'Mensual';
		$fila=array(
			$datos['curso'],
			$datos['precio']." €",
			$plazos,
			botonAcciones(array('Detalles'),array('cursos/gestion.php?codigo='.$datos['codigo']),array("icon-search-plus")),
        	"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionCursos(){
	operacionesCursos();

	abreVentanaGestion('Gestión de Módulos','index.php','span3');
	$datos=compruebaDatos('cursos');
	
	abreColumnaCampos('span11');
	campoTexto('curso','Módulo',$datos);
	campoTextoSimbolo('precio','Precio mensual',"<i class='icon-eur'></i>",$datos);
	campoRadio('tipoPlazo','Precio total definido',$datos);
	echo '<div class="divTipoPlazo">';
		campoTexto('plazos','Plazos',$datos,'input-mini');
	echo '</div>';
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}



function filtroCursos(){
	abreCajaBusqueda();

	campoTexto(0,'Módulo');
	campoTextoSimbolo(1,'Precio','<i class="icon-eur"></i>');
	campoTexto(2,'Plazos','','input-mini');
	cierraCajaBusqueda();
}