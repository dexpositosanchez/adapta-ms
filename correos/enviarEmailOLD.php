<?php
  $seccionActiva=$_GET['seccion'];
  include_once('../cabecera.php');

  if($seccionActiva==1){
    $destinatarios=obtieneMails('contactos');
  }
  elseif($seccionActiva==8){
	$destinatarios=obtieneMails('proveedores','correo');
  }
  elseif($seccionActiva==9){
	$destinatarios=obtieneMails('colaboradores');
  }
  elseif($seccionActiva==16){
	$destinatarios=obtieneMails('usuarios');
  }
  else{
    $destinatarios=obtieneMails('clientes');
  }

  $destinoFormu=destinoFormulario($_GET['seccion']);
  $firma=firmaUsuario();
?>

<!-- /subnavbar -->
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-envelope"></i>
              <h3>Nuevo mensaje de correo electrónico</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="<?php echo $destinoFormu; ?>" method="post" enctype="multipart/form-data">
                  <fieldset>
                    
                    <div class="control-group">                     
                      <label class="control-label" for="detinatarios">Destinatario/s:</label>
                      <div class="controls">
                        <input type="text" class="span5" id="destinatarios" name="destinatarios" value="<?php echo $destinatarios; ?>">
						<!--<a class="btn btn-propio" id="posibles"><i class="icon-exclamation-circle"></i> Posibles Clientes</a>
						<a class="btn btn-propio" id="clientes"><i class="icon-group"></i> Clientes</a>-->
						<?php
							/*$emails=emailsClientes();
							campoOculto($emails,'emails');
							$emailsClientes=emailsClientes('SI');
							campoOculto($emailsClientes,'emailsClientes');*/
						?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="asunto">Asunto:</label>
                      <div class="controls">
                        <input type="text" class="span5" id="asunto" name="asunto">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                     

                    <div class="control-group">                     
                      <label class="control-label" for="mensajeCorreo"></label>
                      <div class="controls">
                        <textarea name="mensajeCorreo" id="mensajeCorreo">Mensaje...<br><?php echo $firma; ?></textarea>
                      </div>
                    </div>


                    <div class="control-group">                     
                      <label class="control-label" for="adjunto">Adjunto:</label>
                      <div class="controls">
                        <input type="file" name="adjunto" id="adjunto"><div class="tip">Solo se admiten ficheros de 20 MB como máximo</div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                  
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-share-alt"></i> Enviar correo</button> 
                      <a href="<?php echo $destinoFormu; ?>" class="btn btn-default"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                  
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-filestyle.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#mensajeCorreo').wysihtml5({locale: "es-ES"});
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
	$('#posibles').click(function(){
      $('#destinatarios').val($('input[name=emails]').val());
	  $('#posibles').hide(300);
	  $('#clientes').css('display','inline');
    });
	$('#clientes').click(function(){
      $('#destinatarios').val($('input[name=emailsClientes]').val());
	  $('#clientes').hide(300);
	  $('#posibles').css('display','inline');
    });
  });
</script>