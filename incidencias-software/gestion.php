<?php
  $seccionActiva=5;
  include_once("../cabecera.php");  
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

	  
	  
      <div class="span12 margenAb">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-laptop"></i><i class="icon-chevron-right"></i><i class="icon-plus-circle"></i>
              <h3>Notificación de incidencia en software</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="index.php" method="post">
                  <fieldset>

                    <?php
                      campoOculto($_CONFIG['codigoIncidenciasSoftware'],'codigoCliente');
          					  campoOculto('SI','creadoCliente');
          					  campoOculto('','textoOtros');

                      campoSelect('tipoTarea','Tipo de tarea',array('Modificación/Mejora','Error de funcionamiento'),array('MODIFICACION','INCIDENCIA'),false,'selectpicker span3 show-tick','');
          					  campoOculto('NULL','codigoUsuario');
          					  campoOculto('1','realizado');
          					  campoOculto('','fechaFinalizacion');
          					  campoOculto('','tiempoEmpleado');
                      areaTexto('observaciones','Indique su incidencia','','areaInforme');
                      campoOculto('','comentariosProgramacion');
                    ?>
                    
                    <br />                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-propio"><i class="icon-check"></i> Notificar incidencia</button> 
					            <a href="index.php" class="btn btn-default"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('select').selectpicker();
  });
</script>

<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>