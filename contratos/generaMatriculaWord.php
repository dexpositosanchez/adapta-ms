<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común


  	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('contratos',$_GET['codigo']);
	$datosCliente=datosRegistro('trabajadores_cliente',$datos['codigoAlumno']);
	$datosPrograma=datosRegistro('programas',$datos['codigoPrograma']);
	
	//Carga de la librería PHPWord
	require_once '../../api/phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('../documentos/matriculas/plantillaMatricula.docx');

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	
	$document->setValue("apellidos",utf8_decode($datosCliente['apellido1']));
	$document->setValue("nombre",utf8_decode($datosCliente['nombre']));
	$document->setValue("domicilio",utf8_decode($datosCliente['direccion']));
	$document->setValue("fecha",utf8_decode(formateaFechaWeb($datosPrograma['previstaFecha'])));
	$document->setValue("cp",utf8_decode($datosCliente['cp']));
	$document->setValue("provincia",utf8_decode($datosCliente['provincia']));
	$document->setValue("localidad",utf8_decode($datosCliente['poblacion']));
	$document->setValue("fijo",utf8_decode($datosCliente['telefono']));

	$document->setValue("movil",utf8_decode($datosCliente['movil']));
	$document->setValue("dni",utf8_decode($datosCliente['nif']));
	$document->setValue("email",utf8_decode($datosCliente['email']));

	$document->setValue("programa",utf8_decode($datosPrograma['nombre']));
	
	$document->setValue("precio",utf8_decode($datosPrograma['precio']));

	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$document->save('../documentos/matriculas/Matricula'.$datos['codigo'].'.docx');


	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Matricula".$datos['codigo'].".docx");
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('../documentos/matriculas/Matricula'.$datos['codigo'].'.docx');


?>