-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-04-2017 a las 17:27:12
-- Versión del servidor: 5.1.73
-- Versión de PHP: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `adaptams`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesos`
--

CREATE TABLE IF NOT EXISTS `accesos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoUsuario` int(255) NOT NULL,
  `fecha` datetime NOT NULL,
  `ip` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `navegador` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `sistema` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=512 ;

--
-- Volcado de datos para la tabla `accesos`
--

INSERT INTO `accesos` (`codigo`, `codigoUsuario`, `fecha`, `ip`, `navegador`, `sistema`) VALUES
(450, 8, '2017-01-09 10:51:56', '81.36.79.110', 'Mozilla', 'Mac'),
(451, 8, '2017-01-09 10:55:23', '92.186.181.215', 'Chrome', 'Windows'),
(452, 8, '2017-01-09 17:12:21', '213.143.60.46', 'Chrome', 'Windows'),
(453, 8, '2017-01-11 13:02:05', '92.186.181.215', 'Chrome', 'Windows'),
(454, 8, '2017-01-11 13:27:10', '213.143.60.92', 'Chrome', 'Windows'),
(455, 8, '2017-01-11 13:30:20', '213.143.60.92', 'Chrome', 'Windows'),
(456, 8, '2017-01-12 12:06:27', '83.61.242.152', 'Chrome', 'Windows'),
(457, 8, '2017-01-12 13:21:02', '83.61.242.152', 'Chrome', 'Windows'),
(458, 8, '2017-01-12 13:21:15', '83.61.242.152', 'Chrome', 'Windows'),
(459, 8, '2017-01-20 09:44:15', '83.61.242.152', 'Chrome', 'Windows'),
(460, 10, '2017-01-20 12:41:03', '83.61.242.152', 'Chrome', 'Windows'),
(461, 10, '2017-01-20 12:45:32', '83.61.242.152', 'Chrome', 'Windows'),
(462, 10, '2017-01-20 12:46:31', '83.61.242.152', 'Chrome', 'Windows'),
(463, 10, '2017-01-20 12:48:16', '83.61.242.152', 'Chrome', 'Windows'),
(464, 8, '2017-01-20 12:52:08', '83.61.242.152', 'Chrome', 'Windows'),
(465, 10, '2017-01-23 09:16:16', '83.61.242.152', 'Chrome', 'Windows'),
(466, 10, '2017-01-23 09:16:52', '83.61.242.152', 'Chrome', 'Windows'),
(467, 10, '2017-01-23 09:17:37', '83.61.242.152', 'Chrome', 'Windows'),
(468, 8, '2017-01-23 09:47:59', '83.61.242.152', 'Chrome', 'Windows'),
(469, 8, '2017-01-23 12:49:11', '83.61.242.152', 'Chrome', 'Windows'),
(470, 8, '2017-01-25 09:18:17', '213.143.60.16', 'Chrome', 'Windows'),
(471, 8, '2017-01-25 13:44:59', '213.143.60.16', 'Chrome', 'Windows'),
(472, 8, '2017-02-01 16:02:45', '83.61.242.152', 'Chrome', 'Windows'),
(473, 8, '2017-02-03 17:49:14', '213.143.60.94', 'Chrome', 'Windows'),
(474, 8, '2017-02-04 16:36:46', '80.28.255.80', 'Chrome', 'Windows'),
(475, 8, '2017-02-05 17:09:34', '80.28.255.80', 'Chrome', 'Windows'),
(476, 8, '2017-02-05 18:07:56', '80.28.255.80', 'Chrome', 'Windows'),
(477, 8, '2017-02-06 13:50:09', '83.61.242.152', 'Chrome', 'Windows'),
(478, 8, '2017-02-07 09:31:12', '213.143.60.49', 'Chrome', 'Windows'),
(479, 8, '2017-02-07 13:39:49', '83.61.242.152', 'Chrome', 'Windows'),
(480, 8, '2017-02-14 10:00:26', '83.61.242.152', 'Chrome', 'Windows'),
(481, 8, '2017-02-14 19:36:31', '213.143.60.101', 'Chrome', 'Windows'),
(482, 8, '2017-02-15 10:12:30', '213.143.60.101', 'Chrome', 'Windows'),
(483, 8, '2017-02-15 15:57:46', '185.178.136.107', 'Chrome', 'Windows'),
(484, 8, '2017-02-16 09:43:07', '92.186.181.215', 'Chrome', 'Windows'),
(485, 8, '2017-02-21 11:10:20', '83.61.242.152', 'Chrome', 'Windows'),
(486, 8, '2017-02-21 13:44:18', '92.186.181.215', 'Chrome', 'Windows'),
(487, 8, '2017-02-21 14:12:51', '83.61.242.152', 'Chrome', 'Windows'),
(488, 8, '2017-02-27 13:56:18', '83.61.242.152', 'Chrome', 'Windows'),
(489, 8, '2017-02-27 14:10:05', '83.61.242.152', 'Chrome', 'Windows'),
(490, 8, '2017-02-27 18:21:28', '185.178.136.107', 'Chrome', 'Windows'),
(491, 8, '2017-02-28 23:53:29', '80.28.255.80', 'Chrome', 'Windows'),
(492, 8, '2017-03-03 09:17:16', '83.61.242.152', 'Chrome', 'Windows'),
(493, 8, '2017-03-03 19:02:47', '213.143.60.30', 'Chrome', 'Windows'),
(494, 8, '2017-03-04 08:08:31', '185.178.136.107', 'Chrome', 'Windows'),
(495, 8, '2017-03-05 12:50:36', '80.28.255.80', 'Chrome', 'Windows'),
(496, 8, '2017-03-07 01:16:45', '185.178.136.107', 'Chrome', 'Windows'),
(497, 8, '2017-03-08 09:34:41', '83.61.242.152', 'Chrome', 'Windows'),
(498, 8, '2017-03-09 17:20:03', '83.48.11.50', 'Chrome', 'Windows'),
(499, 8, '2017-03-13 13:31:28', '81.44.98.241', 'Chrome', 'Windows'),
(500, 8, '2017-03-14 13:18:47', '213.143.60.2', 'Chrome', 'Windows'),
(501, 8, '2017-03-17 11:11:07', '92.186.181.215', 'Chrome', 'Windows'),
(502, 8, '2017-03-17 12:39:26', '81.44.98.241', 'Chrome', 'Windows'),
(503, 8, '2017-03-17 12:40:26', '213.143.60.51', 'Chrome', 'Windows'),
(504, 8, '2017-03-22 09:54:48', '81.44.98.241', 'Chrome', 'Windows'),
(505, 8, '2017-03-22 11:40:26', '92.186.181.215', 'Chrome', 'Windows'),
(506, 8, '2017-03-23 10:32:13', '213.143.60.75', 'Chrome', 'Windows'),
(507, 8, '2017-03-23 10:33:35', '213.143.50.85', 'Chrome', 'Windows'),
(508, 8, '2017-03-24 09:54:37', '92.186.181.215', 'Chrome', 'Windows'),
(509, 8, '2017-03-27 12:53:26', '213.143.60.60', 'Chrome', 'Windows'),
(510, 8, '2017-03-31 13:42:37', '81.44.98.241', 'Chrome', 'Windows'),
(511, 8, '2017-04-06 16:14:06', '2.138.192.124', 'Chrome', 'Windows');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditorias_internas`
--

CREATE TABLE IF NOT EXISTS `auditorias_internas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `datos` longtext COLLATE latin1_spanish_ci NOT NULL,
  `enviado` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `id` int(255) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`codigo`, `id`, `categoria`) VALUES
(1, 1, 'Cursos Oficiales Autodesk'),
(3, 2, 'Cursos Certificaciones'),
(4, 3, 'Cursos Project Management');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `activo` enum('SI','NO') NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `nif` varchar(20) NOT NULL,
  `numeroEmpleados` varchar(20) NOT NULL,
  `importe` varchar(20) NOT NULL,
  `denominacionSocial` varchar(255) NOT NULL,
  `sector` varchar(255) NOT NULL,
  `importancia` varchar(255) NOT NULL,
  `programa` varchar(255) NOT NULL,
  `email1` varchar(255) NOT NULL,
  `telefono1` varchar(20) NOT NULL,
  `direccion1` varchar(255) NOT NULL,
  `website1` varchar(255) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `email2` varchar(255) NOT NULL,
  `telefono2` varchar(20) NOT NULL,
  `direccion2` varchar(255) NOT NULL,
  `website2` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `observaciones` text NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86 ;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`codigo`, `activo`, `nombre`, `nif`, `numeroEmpleados`, `importe`, `denominacionSocial`, `sector`, `importancia`, `programa`, `email1`, `telefono1`, `direccion1`, `website1`, `fax`, `linkedin`, `email2`, `telefono2`, `direccion2`, `website2`, `facebook`, `twitter`, `descripcion`, `observaciones`) VALUES
(2, 'SI', 'Mdos Ingenieros', 'B92535905', '8', '', 'Mdos Consulting Ingenieros, S. L. ', 'IngenierÃ­a Proyectos', 'Alta', 'PFIBIM Manager', 'info@m2ingenieros.com', '670066079', 'Calle Margarita, nÃºm. 63 - local 2 - Bloque 2', 'www.m2ingenieros.com', '952585113', '', 'lobato@m2ingenieros.com', '', '', '', '', '', 'Empresa de ingenierÃ­a que desarrollo proyecto a la empresa LIDL', 'CONTRATO COMPLETO CONSULTARÃA FORMACIÃ“N Y ASESORAMIENTO PROYECTO PILOTO'),
(3, 'SI', 'GonzÃ¡lez & Jacobson Arquitectura', 'B29798816', '10', '', 'GonzÃ¡lez & Jacobson Arquitectura, S.L.', 'Arquitectura', 'Alta', 'PFIBIM Manager', 'rodolfo@gjarquitectura.com', '607191436', 'UrbanizaciÃ³n Las Chapas, S/N, CN CÃ¡diz 340, km 193, 29604 - Marbella', 'www.gjarquitectura.com', '952832859', '', 'diego@gjarquitectura.com', '', '', '', '', '', 'Empresa de arquitectura que desarrollo proyectos de edificaciÃ³n', ''),
(4, 'SI', 'CONSTRUCCIONES VERA - Antonio Navas', 'A29013711', '', '', 'CONSTRUCCIONES VERA, S.A.', 'ConstrucciÃ³n', 'Alta', 'MÃ³dulo 1: LOD 100/200/300/350', 'antonio.navas@grupovera.es', '952318250', 'Calle Cerrajeros, nÃºm. 10, 29006, MÃ¡laga', 'www.grupovera.es', '952326822', '', 'fuensanta.hurtado@construccionesvera.es', '', '', '', '', '', 'CONSTRUCCIONES VERA, S.A.\r\nCIF: A-29013711\r\nCalle Cerrajeros, nÃºm. 10\r\n29006 - MÃ¡laga\r\n[M] +34 952 31 82 50 [F] +34 952 32 68 22\r\n[E] antonio.navas@grupovera.es - www.grupovera.es\r\n', ''),
(6, 'SI', 'ALFONSO VALENCIA ARQUITECTURA, S.L.P.', 'B-92285527', '', '', 'ALFONSO VALENCIA', '', '', '', ' alfonsovalencia05@hotmail.com', '', 'Plaza de la Solidaridad, Edificio La Araucaria, nÃƒÂºm. 7, portal 4, 9Ã‚Âº A', '', '', '', '', '666 442 432', '', '', '', '', '', ''),
(7, 'SI', 'ALONDRA COMÃƒÂšN, S.L.', ' B-92819812', '', '', 'ALONDRA', '', '', '', '', '', 'Ctra. de la Subzona, A7077, KM 4.410', '', '', '', '', '', '', '', '', '', '', ''),
(8, 'SI', 'ALTRA CORPORACIÃƒÂ“N', 'B92611102', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 'SI', 'ASENJO Y ASOCIADOS', '', '', '', 'ASENJO', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10, 'SI', 'AVANSIS, S.L.', 'B80802374', '', '', 'AVANSIS', '', '', '', '', '915768484', 'C/. Enrique Granados, 6 - Centro Empresarial', '', '', '', '', '914311975', '', '', '', '', '', ''),
(11, 'SI', 'CAI CONSULTORES', '', '', '', '', '', '', '', 'cai@caiconsultores.com', '952808718', 'Av. del Carmen, Edificio Puertosol, Of. 5 ', '', '', '', '', '', '', '', '', '', '', ''),
(12, 'SI', 'COAATIE DE ALMERIA', 'Q-0475002-B', '', '', 'COAATIE ', '', '', '', '', '', 'Antonio GonzÃƒÂ¡lez Egea, nÃƒÂºm. 11', '', '', '', '', '', '', '', '', '', '', ''),
(13, 'SI', 'COAATIE DE MÃƒÂLAGA', 'Q-2975002-C', '', '', 'COAATIE', '', '', '', '', '', 'Paseo del Limonar, nÃƒÂºm. 41', '', '', '', '', '', '', '', '', '', '', ''),
(14, 'SI', 'CONFORMAS REHABILITACIÃƒÂ“N Y OBRA NUEVA, S.L.', 'B-92084714', '', '', 'CONFORMAS ', '', '', '', 'direccion@conformas.es', '952 34 24 06', 'Calle Ayala, nÃƒÂºm. 3, 1Ã‚Âº - E.', '', '', '', '', '952 34 23 58', '', '', '', '', '', ''),
(15, 'SI', 'CONSTRUCCIONES ELISABETH Y VICTORIA, S.L.', 'B-92397512', '', '', 'CONSTRUCCIONES', '', '', '', 'construcciones.elisabeth@gamil.com', '', 'C/Idelfonso Marzo, nÃƒÂºm. 6, Piso 4, Puerta E', '', '', '', '', '665 942 034', '', '', '', '', '', ''),
(16, 'SI', 'CONSTRUCCIONES SANCHEZ DOMINGUEZ, S.A.', 'A-29.029.428', '', '', 'CONSTRUCCIONES', '', '', '', '', '', 'Avda. Ortega y Gasset, nÃƒÂºm. 112', '', '', '', '', '', '', '', '', '', '', ''),
(17, 'SI', 'CONSTRUCCIONES T. ARJONA, S.L.', 'B-29.178.910', '', '', 'ARJONA', '', '', '', 'gema@t-arjona.com', '952071196', 'Avda. de AndalucÃƒÂ­a, nÃƒÂºm. 30, 3Ã‚Âº A', '', '', '', '', '636966812', '', '', '', '', '', ''),
(18, 'SI', 'CONSTRUCCIONES Y MANTENIMIENTO BILBA, S.L.L.', ' B-92.349.760', '', '', 'BILBA', '', '', '', 'rtecnico@bilba.es', '666442415', 'Calle SUSPIROS, nÃƒÂºm. 68', '', '', '', '', '659784110', '', '', '', '', '', ''),
(19, 'SI', 'CORPORACION EMPRESARIAL ALTRA', 'B-92611102', '', '', 'ALTRA', '', '', '', 'mgonzalez@altracorporacion.es', '902 400 445', 'C/Marie Curie, nÃƒÂºm. 21', '', '', '', '', '902 400 446', '', '', '', '', '', ''),
(20, 'SI', 'COTRACOM, S. COOP. AND', 'F-29.397.957', '', '', 'COTRACOM', '', '', '', 'msantaella@cotracom.com', '952 04 06 30', 'Calle Jaen, nÃƒÂºm. 9, oficina 114', '', '', '', '', '952 31 87 54', '', '', '', '', '', ''),
(21, 'SI', 'DFB PROYDITEC, S.L.', 'B-92260983', '', '', 'DFB PROYDITEC', '', '', '', '', '', 'UrbanizaciÃƒÂ³n Montepiedra, nÃƒÂºm. 29', '', '', '', '', '659782519', '', '', '', '', '', ''),
(22, 'SI', 'EDIPSA', '', '', '', 'EDIPSA', '', '', '', 'edipsa@edipsa.es', '952215053', 'Puerta del Mar 20 1Ã‚Âº Edificio Edipsa', '', '', '', '', '952215053', '', '', '', '', '', ''),
(23, 'SI', 'ENERGA EFICIENCIA, S.L.P.U.', 'B-54565015', '', '', 'ENERGIA EFICIENCIA', '', '', '', ' info@energaeficiencia.es', '', 'Avda. Condomina, nÃƒÂºm. 69, 7Ã‚Âº D', '', '', '', '', '696 309 407', '', '', '', '', '', ''),
(24, 'SI', 'ESCUELA SUPERIOR DE ESTUDIOS DE EMPRESA, S.A.', 'A-29268109', '', '', 'ESTUDIOS EMPRESA', '', '', '', ' irico@esesa.eu', '952 07 14 51', 'Edificio Tabacalera, Avenida Sor Teresa Prat, 15, MÃƒÂ³d. E-0', '', '', '', '', '', '', '', '', '', '', ''),
(25, 'SI', 'Estudio Arkimd, SLP', 'B-92799378', '', '', 'Arkimd', '', '', '', 'pepemesa@coamalaga.es', '', 'Calle Victoria nÃƒÂºm. 32, 3Ã‚Âº C', '', '', '', '', '666 442 428', '', '', '', '', '', ''),
(26, 'SI', 'FABEM DIRECCIONES Y PROYECTOS, S.L.', 'B-92420496', '', '', 'FABEM', '', '', '', 'fabu2308@coaat.es', '952 775 566', 'UrbanizaciÃƒÂ³n Los Pinos de NagÃƒÂ¼eles, Bloque 1, 3Ã‚Âº-A', '', '', '', '', '649 418 874', '', '', '', '', '', ''),
(27, 'SI', 'FABEM DIRECCIONES Y PROYECTOS, S.L.', 'B-92420496', '', '', 'FABEM', '', '', '', ' fabu2308@coaat.es', '952 775 566', 'UrbanizaciÃƒÂ³n Los Pinos de NagÃƒÂ¼eles, Bloque 1, 3Ã‚Âº-A', '', '', '', '', ' 649 418 874', '', '', '', '', '', ''),
(28, 'SI', 'GRUPO SANDO', '', '', '', 'GRUPO SANDO', '', '', '', '', '', 'Avda. Ortega y Gasset, 112', '', '', '', '', '952045252', '', '', '', '', '', ''),
(29, 'SI', 'GRUPO VERA', 'B-92807452Ã‚Â ', '', '', 'GRUPO VERA', '', '', '', 'grupovera@grupovera.es', '', 'C/ Cerrajeros 10', '', '', '', '', '952318250', '', '', '', '', '', ''),
(30, 'SI', 'GUAMAR', '', '', '', 'Guamar', '', '', '', 'info@guamar.es', '952220721', 'Calle Puerto, 14 - 3Ã‚Âª', '', '', '', '', '', '', '', '', '', '', ''),
(31, 'SI', 'HCP Arquitectos', '', '', '', 'HCP ', '', '', '', 'hcp@hcpmalaga.com', '952227707', 'Paseo MarÃƒÂ­timo Ciudad de Melilla. ', '', '', '', '', '', '', '', '', '', '', ''),
(32, 'SI', 'Human Resources Consulting Services & MARM Consultores, S.L.', 'B92707728', '', '', 'Human Resources Services & MARM ', '', '', '', '', '', 'Graham Bell, 6 - 2Ã‚Âª Planta', '', '', '', '', '', '', '', '', '', '', ''),
(33, 'SI', 'IELCO', '', '', '', 'IELCO', '', '', '', '', '', 'Carretera Azucarera Ã¢Â€Â“ Intelhorce nÃƒÂºm. 4', '', '', '', '', '', '', '', '', '', '', ''),
(34, 'SI', 'INGHO FACILITY MANAGEMENT, S.L.', 'B-92.586.742', '', '', 'INGHO', '', '', '', ' inghofm@inghofm.com', '952 020 199', 'IVÃƒÂN PAVLOV, 2-4 2Ã‚Âº 13. EDIFICIO HEVIMAR II', '', '', '', '', '', '', '', '', '', '', ''),
(35, 'SI', 'ITELMA15 INGENIERIA & ARQUITECTURA S.L', 'B-93428167', '', '', 'ITELMA', '', '', '', 'mlopeztur@gmail.com', '', 'Calle Fernando el CatÃƒÂ³lico, nÃƒÂºm. 31, ÃƒÂ¡tico I', '', '', '', '', '661 790 646 ', '', '', '', '', '', ''),
(36, 'SI', 'JFG IngenierÃƒÂ­a y DiseÃƒÂ±o de Estructuras S.L.P.', 'B-93390573', '', '', 'JFG ', '', '', '', ' jfayos@ciccp.es', '', 'C/Vela, nÃ‚Âº2. Bloque 5. Bajo A.', '', '', '', '', '696 749 964', '', '', '', '', '', ''),
(37, 'SI', 'LEM3A ARQUITECTURA AVANZADADE ANDALUCÃƒÂA, S.L.', 'B-93133742', '', '', 'LEM3A', '', '', '', 'rafaelroa@lem3a.es', '', 'Alameda Principal, nÃƒÂºm. 20, planta 8. 29005 - MÃƒÂ¡laga', '', '', '', '', '606 562 744', '', '', '', '', '', ''),
(38, 'SI', 'MONCOBRA SA', 'A78990413', '', '', 'MONCOBRA SA', '', '', '', 'golivera@grupocobra.com', '956 61 24 20 ', 'P. I. GUADARRANQUE PARC.- 12B', '', '', '', '', '956 61 23 90', '', '', '', '', '', ''),
(39, 'SI', 'MYRAMAR', '', '', '', 'Myramar', '', '', '', 'info@myramar.com', '952323200', '', '', '', '', '', '', '', '', '', '', '', ''),
(40, 'SI', 'OLUZ, S.L.P.', 'B-92468172', '', '', 'OLUZ', '', '', '', 'info@oluz.es', ' 952 25 03 08', 'Plaza Lex Flavia Malacitana, nÃƒÂºm. 10', '', '', '', '', '677 441 716', '', '', '', '', '', ''),
(41, 'SI', 'PARQUE MÃƒÂLAGA', '', '', '', 'PARQUE MÃƒÂLAGA', '', '', '', '', '952343431', 'Alameda Principal, 16, 4Ã‚Âª planta, oficina 1', '', '', '', '', '666452249', '', '', '', '', '', ''),
(42, 'SI', 'PHD Modular Access Services Ltd', '579405114', '', '', 'PHD', '', '', '', '', '01895 822322', '54 Oxford Road, Denham', '', '', '', '', '1895822292', '', '', '', '', '', ''),
(43, 'SI', 'RIVERVIAL', '', '', '', 'RIVERVIAL GRUPO', '', '', '', 'info@rivervial.com ', '952104400', 'C/Armengual de la Mota 21, 1Ã‚ÂºD-E', '', '', '', '', '659207637', '', '', '', '', '', ''),
(44, 'SI', 'SGS', '', '', '', 'SGS', '', '', '', '', '', 'Calle Diderot, 28', '', '', '', '', '', '', '', '', '', '', ''),
(45, 'SI', 'SOLCAISUR, S.L.', ' B-72.076.417', '', '', 'SOLCAISUR', '', '', '', 'hector@solcaisur.es', '956 487 739', 'Calle Colombianas, nÃƒÂºm. 5, local 5', '', '', '', '', '', '', '', '', '', '', ''),
(46, 'SI', 'ALFONSO VALENCIA ARQUITECTURA, S.L.P.', 'B-92285527', '', '', 'ALFONSO VALENCIA', '', '', '', ' alfonsovalencia05@hotmail.com', '', 'Plaza de la Solidaridad, Edificio La Araucaria, nÃƒÂºm. 7, portal 4, 9Ã‚Âº A', '', '', '', '', '666 442 432', '', '', '', '', '', ''),
(47, 'SI', 'ALONDRA COMÃƒÂšN, S.L.', ' B-92819812', '', '', 'ALONDRA', '', '', '', '', '', 'Ctra. de la Subzona, A7077, KM 4.410', '', '', '', '', '', '', '', '', '', '', ''),
(48, 'SI', 'ALTRA CORPORACIÃƒÂ“N', 'B92611102', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(49, 'SI', 'ASENJO Y ASOCIADOS', '', '', '', 'ASENJO', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(50, 'SI', 'AVANSIS, S.L.', 'B80802374', '', '', 'AVANSIS', '', '', '', '', '915768484', 'C/. Enrique Granados, 6 - Centro Empresarial', '', '', '', '', '914311975', '', '', '', '', '', ''),
(51, 'SI', 'CAI CONSULTORES', '', '', '', '', '', '', '', 'cai@caiconsultores.com', '952808718', 'Av. del Carmen, Edificio Puertosol, Of. 5 ', '', '', '', '', '', '', '', '', '', '', ''),
(52, 'SI', 'COAATIE DE ALMERIA', 'Q-0475002-B', '', '', 'COAATIE ', '', '', '', '', '', 'Antonio GonzÃƒÂ¡lez Egea, nÃƒÂºm. 11', '', '', '', '', '', '', '', '', '', '', ''),
(53, 'SI', 'COAATIE DE MÃƒÂLAGA', 'Q-2975002-C', '', '', 'COAATIE', '', '', '', '', '', 'Paseo del Limonar, nÃƒÂºm. 41', '', '', '', '', '', '', '', '', '', '', ''),
(54, 'SI', 'CONFORMAS REHABILITACIÃƒÂ“N Y OBRA NUEVA, S.L.', 'B-92084714', '', '', 'CONFORMAS ', '', '', '', 'direccion@conformas.es', '952 34 24 06', 'Calle Ayala, nÃƒÂºm. 3, 1Ã‚Âº - E.', '', '', '', '', '952 34 23 58', '', '', '', '', '', ''),
(55, 'SI', 'CONSTRUCCIONES ELISABETH Y VICTORIA, S.L.', 'B-92397512', '', '', 'CONSTRUCCIONES', '', '', '', 'construcciones.elisabeth@gamil.com', '', 'C/Idelfonso Marzo, nÃƒÂºm. 6, Piso 4, Puerta E', '', '', '', '', '665 942 034', '', '', '', '', '', ''),
(56, 'SI', 'CONSTRUCCIONES SANCHEZ DOMINGUEZ, S.A.', 'A-29.029.428', '', '', 'CONSTRUCCIONES', '', '', '', '', '', 'Avda. Ortega y Gasset, nÃƒÂºm. 112', '', '', '', '', '', '', '', '', '', '', ''),
(57, 'SI', 'CONSTRUCCIONES T. ARJONA, S.L.', 'B-29.178.910', '', '', 'ARJONA', '', '', '', 'gema@t-arjona.com', '952071196', 'Avda. de AndalucÃƒÂ­a, nÃƒÂºm. 30, 3Ã‚Âº A', '', '', '', '', '636966812', '', '', '', '', '', ''),
(58, 'SI', 'CONSTRUCCIONES Y MANTENIMIENTO BILBA, S.L.L.', ' B-92.349.760', '', '', 'BILBA', '', '', '', 'rtecnico@bilba.es', '666442415', 'Calle SUSPIROS, nÃƒÂºm. 68', '', '', '', '', '659784110', '', '', '', '', '', ''),
(59, 'SI', 'CORPORACION EMPRESARIAL ALTRA', 'B-92611102', '', '', 'ALTRA', '', '', '', 'mgonzalez@altracorporacion.es', '902 400 445', 'C/Marie Curie, nÃƒÂºm. 21', '', '', '', '', '902 400 446', '', '', '', '', '', ''),
(60, 'SI', 'COTRACOM, S. COOP. AND', 'F-29.397.957', '', '', 'COTRACOM', '', '', '', 'msantaella@cotracom.com', '952 04 06 30', 'Calle Jaen, nÃƒÂºm. 9, oficina 114', '', '', '', '', '952 31 87 54', '', '', '', '', '', ''),
(61, 'SI', 'DFB PROYDITEC, S.L.', 'B-92260983', '', '', 'DFB PROYDITEC', '', '', '', '', '', 'UrbanizaciÃƒÂ³n Montepiedra, nÃƒÂºm. 29', '', '', '', '', '659782519', '', '', '', '', '', ''),
(62, 'SI', 'EDIPSA', '', '', '', 'EDIPSA', '', '', '', 'edipsa@edipsa.es', '952215053', 'Puerta del Mar 20 1Ã‚Âº Edificio Edipsa', '', '', '', '', '952215053', '', '', '', '', '', ''),
(63, 'SI', 'ENERGA EFICIENCIA, S.L.P.U.', 'B-54565015', '', '', 'ENERGIA EFICIENCIA', '', '', '', ' info@energaeficiencia.es', '', 'Avda. Condomina, nÃƒÂºm. 69, 7Ã‚Âº D', '', '', '', '', '696 309 407', '', '', '', '', '', ''),
(64, 'SI', 'ESCUELA SUPERIOR DE ESTUDIOS DE EMPRESA, S.A.', 'A-29268109', '', '', 'ESTUDIOS EMPRESA', '', '', '', ' irico@esesa.eu', '952 07 14 51', 'Edificio Tabacalera, Avenida Sor Teresa Prat, 15, MÃƒÂ³d. E-0', '', '', '', '', '', '', '', '', '', '', ''),
(65, 'SI', 'Estudio Arkimd, SLP', 'B-92799378', '', '', 'Arkimd', '', '', '', 'pepemesa@coamalaga.es', '', 'Calle Victoria nÃƒÂºm. 32, 3Ã‚Âº C', '', '', '', '', '666 442 428', '', '', '', '', '', ''),
(66, 'SI', 'FABEM DIRECCIONES Y PROYECTOS, S.L.', 'B-92420496', '', '', 'FABEM', '', '', '', 'fabu2308@coaat.es', '952 775 566', 'UrbanizaciÃƒÂ³n Los Pinos de NagÃƒÂ¼eles, Bloque 1, 3Ã‚Âº-A', '', '', '', '', '649 418 874', '', '', '', '', '', ''),
(67, 'SI', 'FABEM DIRECCIONES Y PROYECTOS, S.L.', 'B-92420496', '', '', 'FABEM', '', '', '', ' fabu2308@coaat.es', '952 775 566', 'UrbanizaciÃƒÂ³n Los Pinos de NagÃƒÂ¼eles, Bloque 1, 3Ã‚Âº-A', '', '', '', '', ' 649 418 874', '', '', '', '', '', ''),
(68, 'SI', 'GRUPO SANDO', '', '', '', 'GRUPO SANDO', '', '', '', '', '', 'Avda. Ortega y Gasset, 112', '', '', '', '', '952045252', '', '', '', '', '', ''),
(69, 'SI', 'GRUPO VERA', 'B-92807452Ã‚Â ', '', '', 'GRUPO VERA', '', '', '', 'grupovera@grupovera.es', '', 'C/ Cerrajeros 10', '', '', '', '', '952318250', '', '', '', '', '', ''),
(70, 'SI', 'GUAMAR', '', '', '', 'Guamar', '', '', '', 'info@guamar.es', '952220721', 'Calle Puerto, 14 - 3Ã‚Âª', '', '', '', '', '', '', '', '', '', '', ''),
(71, 'SI', 'HCP Arquitectos', '', '', '', 'HCP ', '', '', '', 'hcp@hcpmalaga.com', '952227707', 'Paseo MarÃƒÂ­timo Ciudad de Melilla. ', '', '', '', '', '', '', '', '', '', '', ''),
(72, 'SI', 'Human Resources Consulting Services & MARM Consultores, S.L.', 'B92707728', '', '', 'Human Resources Services & MARM ', '', '', '', '', '', 'Graham Bell, 6 - 2Ã‚Âª Planta', '', '', '', '', '', '', '', '', '', '', ''),
(73, 'SI', 'IELCO', '', '', '', 'IELCO', '', '', '', '', '', 'Carretera Azucarera Ã¢Â€Â“ Intelhorce nÃƒÂºm. 4', '', '', '', '', '', '', '', '', '', '', ''),
(74, 'SI', 'INGHO FACILITY MANAGEMENT, S.L.', 'B-92.586.742', '', '', 'INGHO', '', '', '', ' inghofm@inghofm.com', '952 020 199', 'IVÃƒÂN PAVLOV, 2-4 2Ã‚Âº 13. EDIFICIO HEVIMAR II', '', '', '', '', '', '', '', '', '', '', ''),
(75, 'SI', 'ITELMA15 INGENIERIA & ARQUITECTURA S.L', 'B-93428167', '', '', 'ITELMA', '', '', '', 'mlopeztur@gmail.com', '', 'Calle Fernando el CatÃƒÂ³lico, nÃƒÂºm. 31, ÃƒÂ¡tico I', '', '', '', '', '661 790 646 ', '', '', '', '', '', ''),
(76, 'SI', 'JFG IngenierÃƒÂ­a y DiseÃƒÂ±o de Estructuras S.L.P.', 'B-93390573', '', '', 'JFG ', '', '', '', ' jfayos@ciccp.es', '', 'C/Vela, nÃ‚Âº2. Bloque 5. Bajo A.', '', '', '', '', '696 749 964', '', '', '', '', '', ''),
(77, 'SI', 'LEM3A ARQUITECTURA AVANZADADE ANDALUCÃƒÂA, S.L.', 'B-93133742', '', '', 'LEM3A', '', '', '', 'rafaelroa@lem3a.es', '', 'Alameda Principal, nÃƒÂºm. 20, planta 8. 29005 - MÃƒÂ¡laga', '', '', '', '', '606 562 744', '', '', '', '', '', ''),
(78, 'SI', 'MONCOBRA SA', 'A78990413', '', '', 'MONCOBRA SA', '', '', '', 'golivera@grupocobra.com', '956 61 24 20 ', 'P. I. GUADARRANQUE PARC.- 12B', '', '', '', '', '956 61 23 90', '', '', '', '', '', ''),
(79, 'SI', 'MYRAMAR', '', '', '', 'Myramar', '', '', '', 'info@myramar.com', '952323200', '', '', '', '', '', '', '', '', '', '', '', ''),
(80, 'SI', 'OLUZ, S.L.P.', 'B-92468172', '', '', 'OLUZ', '', '', '', 'info@oluz.es', ' 952 25 03 08', 'Plaza Lex Flavia Malacitana, nÃƒÂºm. 10', '', '', '', '', '677 441 716', '', '', '', '', '', ''),
(81, 'SI', 'PARQUE MÃƒÂLAGA', '', '', '', 'PARQUE MÃƒÂLAGA', '', '', '', '', '952343431', 'Alameda Principal, 16, 4Ã‚Âª planta, oficina 1', '', '', '', '', '666452249', '', '', '', '', '', ''),
(82, 'SI', 'PHD Modular Access Services Ltd', '579405114', '', '', 'PHD', '', '', '', '', '01895 822322', '54 Oxford Road, Denham', '', '', '', '', '1895822292', '', '', '', '', '', ''),
(83, 'SI', 'RIVERVIAL', '', '', '', 'RIVERVIAL GRUPO', '', '', '', 'info@rivervial.com ', '952104400', 'C/Armengual de la Mota 21, 1Ã‚ÂºD-E', '', '', '', '', '659207637', '', '', '', '', '', ''),
(84, 'SI', 'SGS', '', '', '', 'SGS', '', '', '', '', '', 'Calle Diderot, 28', '', '', '', '', '', '', '', '', '', '', ''),
(85, 'SI', 'SOLCAISUR, S.L.', ' B-72.076.417', '', '', 'SOLCAISUR', '', '', '', 'hector@solcaisur.es', '956 487 739', 'Calle Colombianas, nÃƒÂºm. 5, local 5', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientestareas`
--

CREATE TABLE IF NOT EXISTS `clientestareas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `localidad` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `contacto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `mail` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `codigoTarea` int(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoTarea` (`codigoTarea`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE IF NOT EXISTS `contactos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `empresa` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `origen` varchar(255) NOT NULL,
  `cargo` varchar(255) NOT NULL,
  `importancia` varchar(255) NOT NULL,
  `programa` varchar(255) NOT NULL,
  `email1` varchar(255) NOT NULL,
  `email2` varchar(255) NOT NULL,
  `direccion1` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `telefono1` varchar(20) NOT NULL,
  `telefono2` varchar(20) NOT NULL,
  `website1` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `observaciones` text NOT NULL,
  `activo` enum('SI','NO') NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=401 ;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`codigo`, `nombre`, `fechaNacimiento`, `empresa`, `tipo`, `apellidos`, `origen`, `cargo`, `importancia`, `programa`, `email1`, `email2`, `direccion1`, `provincia`, `linkedin`, `telefono1`, `telefono2`, `website1`, `facebook`, `twitter`, `skype`, `descripcion`, `observaciones`, `activo`) VALUES
(1, 'JosÃ© Antonio', '0000-00-00', '', '', 'Lares', '', '', '', 'PFIBIM Manager / BIM Revit Structure', 'joseantoniolares@gmail.com', '', '', '', '', '629526581', '', '', '', '', '', 'De: JOSÃ‰ ANTONIO LARES \r\nTelefono:629526581\r\nEmail: joseantoniolares@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(2, 'Inmaculada', '0000-00-00', '', '', 'SÃ¡nchez', '', '', '', 'PFIBIM Manager', 'inmasanchezp@hotmail.es', '', '', '', '', '', '', '', '', '', '', 'Buenas tardes Jose MarÃ­a, \r\n\r\nCristian Iborra me ha pasado su contacto, le escribo por si me podrÃ­a enviar la informaciÃ³n relativa al curso de Revit.\r\n\r\nGracias, un saludo\r\n', 'PFIBIM Manager', 'NO'),
(3, 'Demys ', '0000-00-00', '', '', 'HernÃ¡ndez GonzÃ¡lez ', '', '', '', 'Solicitud informaciÃ³n', 'demys.hernandezg@gmail.com', '', '', '', '', '', '', '', '', '', '', 'De: Demys HernÃ¡ndez GonzÃ¡lez \r\nTelefono:+5353620476\r\nEmail: demys.hernandezg@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', 'demys.hernandezg@gmail.com', 'NO'),
(4, 'Carmen M.', '0000-00-00', 'Tablinum', 'LicitaciÃ³n obras', 'Alvarez', 'web adaptams', 'Responsable TÃ©cnico de Estudios y Proyectos.', 'Alta', 'PFIBIM Manager', 'cmalvarez@tablinum.es', 'cmalvarez@ingepac.es', 'C/ Plaza de la Solidaridad 12 Edificio Indocar Planta 5Âª Ofic.538 29002', 'MÃ¡laga', '', '606637671', '952004388', 'Web: www.tablinum.es ', '', '', '', 'Buenas tardes.\r\nQuerÃ­amos consultar si desarrollan cursos de formaciÃ³n sobre BIM y si es asÃ­, conocer el temario, horas del curso, posibilidades de calendario/horarios, certificaciÃ³n y precios.\r\nGracias por la atenciÃ³n.\r\nSaludos,\r\nCARMEN M. ALVAREZ\r\nResponsable TÃ©cnico de Estudios y Proyectos.\r\nIngeniera de Caminos, Canales y Puertos.\r\nMÃ³vil:606637671 / Telf:952004388 \r\nE-mail:cmalvarez@tablinum.es / cmalvarez@ingepac.es\r\nWeb: www.tablinum.es \r\nC/ Plaza de la Solidaridad 12\r\nEdificio Indocar Planta 5Âª Ofic.538\r\n29002 MÃLAGA\r\n', '', 'SI'),
(5, 'Enrique', '0000-00-00', '', '', 'Suarez Tapiador', '', '', '', 'PMP Profesional', 'logan629@gmail.com', '', '', '', '', '650804120', '', '', '', '', '', 'De: Enrique \r\nTelefono: 650804120\r\nEmail: logan629@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(6, 'Emmanuel ', '0000-00-00', '', '', '', '', '', '', 'Solicitud informaciÃ³n', 'emmanuel.ingara@gmail.com', '', '', '', '', '', '', '', '', '', '', 'De: Emmanuel \r\nTelefono:619502998\r\nEmail: emmanuel.ingara@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', 'El mÃ³vil no lo tiene bien', 'NO'),
(7, 'ÃLvaro  ', '0000-00-00', '', '', 'Sampedro', '', '', '', 'REVIT  Structure / Revit MEP', 'alvaro@sampedroingenieria.com', '', '', '', '', '', '', '', '', '', '', 'Hola,\r\n\r\nQuerÃ­a solicitar informaciÃ³n y precio de los cursos: \r\n\r\nREVIT ESTRUCTURAS\r\nREVIT INSTALACIONES â€“ FAM II\r\n\r\nSaludos,\r\nÃlvaro\r\n', '', 'NO'),
(8, 'VÃ­ctor', '0000-00-00', '', '', 'Roda', '', '', '', 'AutoCAD Civil 3D', 'victormroda@hotmail.com', '', '', '', '', '676649943', '', '', '', '', '', 'De: VÃ­ctor Roda \r\nEmail: victormroda@hotmail.com\r\nTelÃ©fono: 676649943\r\nAsunto: FormaciÃ³n\r\n\r\nCuerpo del mensaje:\r\nsolicito informaciÃ³n sobre curso en AutoCAD Civil 3D. Precios , horarios, programa, certificaciÃ³n...\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(9, 'Santiago', '0000-00-00', '', '', 'Rayo GarcÃ­a ', '', '', '', 'Solicitud informaciÃ³n', 'srayogarcia@gmail.com', '', '', '', '', '606781491', '', '', '', '', '', 'De: Santiago Rayo GarcÃ­a \r\nTelefono:606781491\r\nEmail: srayogarcia@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(10, 'Miguel', '0000-00-00', 'COA MÃ¡laga', '', 'Lobon', '', '', '', 'Solicitud informaciÃ³n', 'mlobcan987@gmail.com', '', '', '', '', '605405048', '', '', '', '', '', 'De: Miguel Lobon \r\nTelefono:605405048\r\nEmail: mlobcan987@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(11, 'Juan', '0000-00-00', '', 'Linkedin', 'Campos', '', '', '', 'PFIBIM Manager', 'juan_campos@coaat.es', '', '', '', '', '660296016', '', '', '', '', '', 'Buenos dÃ­as JosÃ© MarÃ­a, estoy interesado por lo que me puedes enviar el temario. Aunque para esta ediciÃ³n lo tengo complicado estarÃ© pendiente para la 6 ediciÃ³n (prÃ³xima). Gracias por contactar conmigo y espero que nos volvamos a ver pronto.\r\n\r\nAtentamente.\r\n\r\nJuan Campos', 'Perfil de Juan\r\nhttps://www.linkedin.com/in/juan-campos-546b05105\r\nnÃºmero de telÃ©fono\r\n660296016 (MÃ³vil)\r\nCorreo electrÃ³nico\r\njuan_campos@coaat.es', 'NO'),
(12, 'MÂª Teresa', '0000-00-00', '', '', 'Labrador Villanueva ', '', '', '', 'CERTIFICACIÃ“N PROFESSIONAL AUTODESK REVIT', 'teresa.labrador.imbim@gmail.com', '', '', '', '', '627938091', '', '', '', '', '', 'De: MÂª Teresa Labrador Villanueva \r\nEmail: teresa.labrador.imbim@gmail.com\r\nTelÃ©fono: 627938091\r\nAsunto: FormaciÃ³n\r\n\r\nCuerpo del mensaje:\r\nBuenos dÃ­as, estoy interesada en la CertificaciÃ³n profesional de Revit Arquitecture.\r\n\r\nMe gustarÃ­a conocer el procedimiento para la preparaciÃ³n y la realizaciÃ³n del examen correspondiente, asÃ­ como los costes del mismo.\r\n\r\nMuchas gracias\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(13, 'Salvador', '0000-00-00', '', '', 'FernÃ¡ndez Rebollo ', '', '', '', 'Solicitud informaciÃ³n', 'salvador.fernandez.rebollo@gmail.com', '', '', '', '', '635532193', '', '', '', '', '', 'De: Salvador FernÃ¡ndez Rebollo \r\nTelefono:635532193\r\nEmail: salvador.fernandez.rebollo@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(14, 'JosÃ© Manuel', '0000-00-00', '', '', 'Torres Torres ', '', '', '', 'PFIBIM Manager', 'gesvipro@gmail.com', '', '', '', '', '693019655', '', '', '', '', '', 'De: Jose Manuel Torres Torres \r\nEmail: gesvipro@gmail.com\r\nTelÃ©fono: 693019655\r\nAsunto: FormaciÃ³n\r\n\r\nCuerpo del mensaje:\r\nBuenas. Estoy interesado en la formaciÃ³n que ofrecÃ©is y me gustarÃ­a recibir mas informaciÃ³n. Gracias.\r\n', '', 'NO'),
(15, 'Carmen M.', '0000-00-00', 'TABLINUM', '', 'Alvarez', '', 'Responsable TÃ©cnico de Estudios y Proyectos.', '', 'PFIBIM Manager', 'cmalvarez@tablinum.es', 'cmalvarez@ingepac.es', 'C/ Plaza de la Solidaridad 12, Edificio Indocar Planta 5Âª Ofic.538 29002', 'MÃ¡laga', '', '606637671', '952004388', 'www.tablinum.es', '', '', '', 'CARMEN M. ALVAREZ\r\nResponsable TÃ©cnico de Estudios y Proyectos.\r\nIngeniera de Caminos, Canales y Puertos.\r\nMÃ³vil:606 637 671 / Telf: 952004388 \r\nE-mail:cmalvarez@tablinum.es / cmalvarez@ingepac.es\r\nWeb: www.tablinum.es \r\nC/ Plaza de la Solidaridad 12\r\nEdificio Indocar Planta 5Âª Ofic.538\r\n29002 MÃLAGA\r\n\r\nBuenos dÃ­as JosÃ© Manuel.\r\nTe agradecerÃ­amos que nos enviases el temario o Ã­ndice de contenidos del curso de 210h que estuvimos hablando ayer.\r\nPor otra parte, nos interesa conocer la forma de pago y, si en algÃºn caso, ofrecÃ©is la posibilidad de fraccionarlo.\r\nMuchas gracias.\r\n', '', 'SI'),
(16, 'JesÃºs ', '0000-00-00', '', '', '', '', '', '', 'CERTIFICACIÃ“N PROFESSIONAL AUTODESK REVIT', 'jesusgarciaarquitecto@gmail.com', '', '', '', '', '686941777', '', '', '', '', '', 'De: JesÃºs \r\nTelefono:686941777\r\nEmail: jesusgarciaarquitecto@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(17, 'Serafin', '0000-00-00', 'CONSTRUCCIONES VERA, S.A.', 'ConstrucciÃ³n', 'FernÃ¡ndez Rebollo ', '', 'Jefe de Obra', 'Alta', 'MÃ³dulo 1: LOD 100/200/300/350', 'antonio.navas@grupovera.es', '', 'Calle Cerrajeros, nÃºm. 10, 29006, MÃ¡laga', 'MÃ¡laga', '', '952318250', '', 'www.grupovera.es', '', '', '', 'CONSTRUCCIONES VERA, S.A.\r\nCIF: A-29013711\r\nCalle Cerrajeros, nÃºm. 10\r\n29006 - MÃ¡laga\r\n[M] +34 952 31 82 50 [F] +34 952 32 68 22\r\n[E] antonio.navas@grupovera.es - www.grupovera.es', '', 'SI'),
(18, 'Miguel', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'mlobcan987@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(19, 'JosÃƒÂ© Miguel', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jgarcia@teodorocabrilla.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(20, 'L. Alba', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'lalba@teodorocabrilla.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(21, 'M.', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'mlobcan@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(22, 'Carmen', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'baeza.vocal@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(23, 'Francisco', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'infocat@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(24, 'Naiara', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'naiara@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(25, 'JosÃƒÂ© Manuel', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jmgarrido@arquigrafproyectos.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(26, 'Vicente', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'vicenteprados@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(27, 'JoaquÃƒÂ­n', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'joaquinbonet@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(28, 'Fernando', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'gzhuete@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(29, 'Guillermo', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'gdelasheras@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(30, 'JosÃƒÂ© Carlos', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'josecarlos@jccifuentes.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(31, 'Francisco Manuel', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'lopezchacon@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(32, 'Enrique', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'info@ldcarquitectura.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(33, 'Fran', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'c2a@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(34, 'Arquitectura Domingo Corpas', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'arquitectura@domingocorpas.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(35, 'Beatriz', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'beatrizaybaromero@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(36, 'Arteisur', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'info@arteisur.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(37, 'Francisco', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'madyo@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(38, 'Javier', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'javiercantos@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(39, 'B. Marquez', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'bmarquez@teodorocabrilla.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(40, 'Julia', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jcastellano@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(41, 'Carlos', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'carlos@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(42, 'Santiago', '0000-00-00', 'COA MÃƒÂLAGA 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'santidorao@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(43, 'Rafael', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'rcottacrooke@ggarquitectura.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(44, 'Francisco', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'javiermoreno@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(45, 'Rafael', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'rmaciasgil@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(46, 'Juan Antonio', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'ruizgor@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(47, 'Carmen', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'cbaeza@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(48, 'Eduardo', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'equero@qgproyectos.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(49, 'JosÃƒÂ© Francisco', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'josefrancisco@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(50, 'David', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'dcarquitec@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(51, 'JesÃƒÂºs', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jesuslopezorozco@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(52, 'Carolina', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'carolinablanes@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(53, 'Francisco Javier', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'miruchav@arquired.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(54, 'Jon', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jontxo@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(55, 'Antonio', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'estudiopalma@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(56, 'Angeles', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'angeles_alonso@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(57, 'Juan Manuel', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'tautres@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(58, 'Ezequiel', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'ezequiel@construccioncreativa.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(59, 'Juan Ignacio', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'juan_calvo@infonegocio.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(60, 'Pablo', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'pabloalba@arcs.com.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(61, 'Elisa', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'elicepedano@madarquitectura.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(62, 'Susana', '0000-00-00', 'COA MÃƒÂLAGA - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'susanavera@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(63, 'Jordi', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'jordi.royo@guamar.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(64, 'Manuel', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'manuelrios@guamar.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(65, 'Juan M.', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'juanma.gomez.martin@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(66, 'Francisco', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'froman@deltagrupo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(67, 'SerafÃƒÂ­n', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'serafin.fernandez@construccionesvera.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(68, 'Daniel', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'tecnico@lasor.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(69, 'Virginia', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'virginiasegura@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(70, 'Pedro JosÃƒÂ©', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'pedro.rueda@ielco.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(71, 'CÃƒÂ©sar', '0000-00-00', 'ACP MÃƒÂLAGA', '', '', '', '', '', '', 'cesar.martinez@construccionesvera.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(72, 'Kenny', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'kennygerardo@yahoo.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(73, 'Francisco', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'burgos@m2ingenieros.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(74, 'Matias', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'matias@m2ingenieros.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(75, 'Ana', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'ana@m2ingenieros.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(76, 'Juan', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'garcia@m2ingenieros.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(77, 'Daniel', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'dmadronal@m2ingenieros.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(78, 'Alberto', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'alberto.aguilar@m2ingenieros.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(79, 'Carlos', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'clobato@m2ingenieros.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(80, 'Antonio', '0000-00-00', 'M2 INGENIEROS', '', '', '', '', '', '', 'lobato@m2ingenieros.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(81, 'Alberto', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'albertogm@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(82, 'Raquel', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'rbarco@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(83, 'Alberto', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'algamor@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(84, 'Manuel', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'mrmedina@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(85, 'Carlos', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'cjrosa@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(86, 'CristÃƒÂ³bal', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'cristobal@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(87, 'Pablo', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'psepulveda@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(88, 'MarÃƒÂ­a JosÃƒÂ©', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'mjmarquez@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(89, 'ÃƒÂ“scar', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'director.smart@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(90, 'Jorge', '0000-00-00', 'VICERRECTORADO MÃƒÂLAGA', '', '', '', '', '', '', 'jbarrios@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(91, 'Miguel', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'miguel_lopezmunoz@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(92, 'JesÃƒÂºs', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'lolo0947@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(93, 'Alberto', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'apinel@maiaconsultores.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(94, 'Adriano', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'adrianoperezlopez@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(95, 'Ricardo', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'urlo1475@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(96, 'Laura', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'lauramartin3719@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(97, 'Alejandro', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'alarcon@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(98, 'Carmen', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'cperez@arksur.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(99, 'Manuel', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'manolocastillo@alborans.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(100, 'Mike', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'mikeezlop@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(101, 'Juan Luis', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'roga3177@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(102, 'Ricardo', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'rpascua@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(103, 'Alfredo', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'roiz1678@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(104, 'JosÃƒÂ© Antonio', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'joseantoniolares@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(105, 'Oscar', '0000-00-00', '1Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA ', '', '', '', '', '', '', 'oscar.taboada.rabaneda@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(106, 'Alberto', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'aquesad@alumni.unav.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(107, 'JosÃƒÂ© Antonio', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'ojalaseasi@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(108, 'Alejandro', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'aruizlagos@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(109, 'A', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'aj.janeiro@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(110, 'Q', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'qar3225@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(111, 'Antonio', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'azorrillagutierrez@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(112, 'Justo', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'justoruizg@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(113, 'Salvador', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'salvador0983@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(114, 'Juan', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'jleiva_arqtec@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(115, 'Francisco', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'flopezclavijo@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(116, 'Teresa', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'torozco100@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(117, 'JosÃƒÂ©', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'jcasanae@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(118, 'JosÃƒÂ© Luis', '0000-00-00', ' 2Ã‚Âª EDICIÃƒÂ“N COAATIE MÃƒÂLAGA', '', '', '', '', '', '', 'jlbaltanas@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(119, 'Francisco', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'infoarquinova@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(120, 'Carlos', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'jcarlosmoncayo@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(121, 'Almudena', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'almutc91@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(122, 'Francisco', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'fd.conejomarquez@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(123, 'JosÃƒÂ© Carlos', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'jc.arquitecto...nico@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(124, 'JosÃƒÂ©', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'jrubinog@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(125, 'Ana', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'anapelaezm@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(126, 'Carmen', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'carmenariza@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(127, 'Alberto', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'jica2202@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(128, 'Noelia', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'noelia@arquitectura2tc.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(129, 'Francisco', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'buvi2343@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(130, 'JosÃƒÂ© Antonio', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'pepeconde@bsolis.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(131, 'CÃƒÂ©sar', '0000-00-00', '5Ã‚Âª EdiciÃƒÂ³n BIM MÃƒÂ¡laga', '', '', '', '', '', '', 'cesarposadas@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(132, 'Fran', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'franarquitectotec@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(133, 'JesÃƒÂºs', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'marbella321@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(134, 'Pepe', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'sana3539@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(135, 'Alvaro', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'alvaro@arqui-tec.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(136, 'JosÃƒÂ©', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'morito.jose@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(137, 'BelÃƒÂ©n', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'bpizarromarvel@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(138, 'Alberto', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'alberto@exclusiveronda.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(139, 'Javier', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'info@progestec.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(140, 'Elena', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'e.navazon@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(141, 'Vicente', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'vbejarano@beteca.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(142, 'Salvador', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'sama2063@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(143, 'JosÃƒÂ© Antonio', '0000-00-00', 'COAATIE Marbella - 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'roca1566@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(144, 'Pedro', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'corrales_pedro@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(145, 'Juan Manuel', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jmluque@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(146, 'G', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'gara2095@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(147, 'Migeul', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'miguelcasero@bsolis.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(148, 'Viky', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'vserrano@bsolis.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(149, 'Antonio', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'antoniocarrascogil@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(150, 'Miguel ÃƒÂngel', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'miguelyebra@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(151, 'AgustÃƒÂ­n', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'agustin@tuttoarquitectura.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(152, 'Manuel', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'manuel.mftecnico@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(153, 'AdriÃƒÂ¡n', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'j.a.gutierrez@live.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(154, 'Francisco', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'ffmaparejador@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(155, 'Guillermo', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'gpmr.at@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(156, 'JosÃƒÂ© Carlos', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'josecarlosgil1989@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(157, 'JosÃƒÂ© MarÃƒÂ­a', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'chemajaime@bsolis.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(158, 'Miguel', '0000-00-00', 'COAATIE Marbella - 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'miguelcarrasco@bsolis.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(159, 'ÃƒÂlvaro', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'alvaro@arqui-tec.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(160, 'Javier', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'olra3074@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(161, 'Francisco', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'curro@llodilu.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(162, 'Juan', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'cejuni2@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(163, 'Rosa', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'rosa2001es@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(164, 'Eduardo', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'fena2801@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(165, 'MarÃƒÂ­a Adela', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'urto2352@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(166, 'JosÃƒÂ© Antonio', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jacanadas@bsolis.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(167, 'Rafael', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'rafabracero@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(168, 'Beatriz', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'beatriz@arqui-tec.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(169, 'Fernando Javier', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'fernandoartimez@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(170, 'Fernando', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'nandogr2002@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(171, 'Emilio', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'fabu2308@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(172, 'Francisco', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'pacopasdan@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(173, 'JosÃƒÂ© Antonio', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'josea.macias.mbim@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(174, 'Juan Francisco', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'silvamediano@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(175, 'Edward', '0000-00-00', 'COAAT Marbella - 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'efnavas@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(176, 'JosÃƒÂ© Antonio', '0000-00-00', 'BLOCKFAST', '', '', '', '', '', '', 'jaesparraga@blockfast.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(177, 'Antonio', '0000-00-00', 'BLOCKFAST', '', '', '', '', '', '', 'amedina@blockfast.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(178, 'Francisco', '0000-00-00', 'CONSTRUCCIONES ARJONA', '', '', '', '', '', '', 'f.fernandez@t-arjona.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(179, 'Victor', '0000-00-00', 'CONSTRUCCIONES ARJONA', '', '', '', '', '', '', 'victormlpedrosa@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(180, 'Salvador', '0000-00-00', 'CONSTRUCCIONES ARJONA', '', '', '', '', '', '', 'srdeburgos@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(181, 'Paolo', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'pdodi@letteringenieros.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(182, 'Silvia', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'silviacasero@bilba.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(183, 'Miguel', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'mcg@ait-ingenieros.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(184, 'JosÃƒÂ©', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'ingenieria@tuinsa.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(185, 'Juan', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'juandiaz@bilba.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(186, 'IvÃƒÂ¡n', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'imolina@letteringenieros.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(187, 'Ana', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'asiles@letteringenieros.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(188, 'Silvia', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'silvialuna@bilba.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(189, 'Sonia', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'sonia.eccmurcia@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(190, 'Ana', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'larr.arquitectos@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(191, 'Alberto', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'albertorios@bilba.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(192, 'OTM IngenierÃƒÂ­a', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'info@otmingenieria.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(193, 'Jorge', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'jorgeborrego@bilba.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(194, 'Francisco', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'gerencia@alesotoarquitectos.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(195, 'Lidia', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'tecnologia@alesotoarquitectos.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(196, 'Crisanto', '0000-00-00', 'HOTEL COSTA DEL SOL', '', '', '', '', '', '', 'proyectos@alesotoarquitectos.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(197, 'Yolanda', '0000-00-00', 'INGHO', '', '', '', '', '', '', 'yolanditi@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(198, 'Manuel Gallardo', '0000-00-00', 'INGHO', '', '', '', '', '', '', 'gallardo@inghofm.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(199, 'JosÃƒÂ© ÃƒÂngel', '0000-00-00', 'IMV', '', '', '', '', '', '', 'japuche@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(200, 'JM', '0000-00-00', 'IMV', '', '', '', '', '', '', 'jmcerezo@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(201, 'Consuelo', '0000-00-00', 'IMV', '', '', '', '', '', '', 'cpcornejo@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(202, 'Luis MarÃƒÂ­a', '0000-00-00', 'IMV', '', '', '', '', '', '', 'lmagarcia@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(203, 'Luis Miguel', '0000-00-00', 'IMV', '', '', '', '', '', '', 'lmnavidad@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(204, 'J', '0000-00-00', 'IMV', '', '', '', '', '', '', 'jpuche@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(205, 'Rosa', '0000-00-00', 'IMV', '', '', '', '', '', '', 'rsegui@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(206, 'Eduardo', '0000-00-00', 'IMV', '', '', '', '', '', '', 'ecruz@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(207, 'VÃƒÂ­ctor', '0000-00-00', 'IMV', '', '', '', '', '', '', 'vherrero@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(208, 'JosÃƒÂ© Luis', '0000-00-00', 'IMV', '', '', '', '', '', '', 'jlheredia@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(209, 'Carlos', '0000-00-00', 'IMV', '', '', '', '', '', '', 'cariza@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(210, 'Salvador', '0000-00-00', 'IMV', '', '', '', '', '', '', 'smtrujillo@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(211, 'Antonio', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'el_ochoa1@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(212, 'Ana', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'anaavilesr@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(213, 'Almudena', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'acrucescuesta@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(214, 'Antonio', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'wsiless@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(215, 'Amabel', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'amabel_quintero@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(216, 'JosÃƒÂ© Luis', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'j.l.arevaloarevalo@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(217, 'JosÃƒÂ©', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', '90joseruiz@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(218, 'Antonio', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'antonioaoc86@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(219, 'Miguel', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'miguelbraung@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(220, 'P.', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'p.castillog88@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(221, 'Santiago', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'santiagorc_89@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(222, 'Manuel', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'manolo.diaz.sanchez@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(223, 'Diego', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'corpasgonzalezdiego@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(224, 'Yanire', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'yanireortizdiaz@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(225, 'Teresa', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'teresanietofs@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(226, 'Antonio', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'tonomalaga@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(227, 'SiranÃƒÂºs', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'siranushm@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(228, 'SofÃƒÂ­a', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'sofiadominguezm@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(229, 'Sergio', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'sergiosanpan@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(230, 'Gara', '0000-00-00', 'MASTER BIM 3DCUBE', '', '', '', '', '', '', 'genesis.r.a@hotmail.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(231, 'M.', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'mjvera@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(232, 'JosÃƒÂ© Luis', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'gore2336@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(233, 'L.', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'lcalerocuenca@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(234, 'ÃƒÂngel', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'c2503pr@copitima.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(235, 'Suso', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'skycarm400@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(236, 'JosÃƒÂ© Antonio', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'joseaguera30@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(237, '', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'sirmaky@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(238, 'Juan Francisco', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'c2611md@copitima.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(239, 'Francisco', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'francisco@comedap.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(240, 'Salvador', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'smainger10@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(241, 'Marina', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'mamvh@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(242, 'Natalia', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'nataliacd1972@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(243, 'Carmen', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'info@ecocerter.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(244, 'Miguel', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'tortomi@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(245, 'JosÃƒÂ© Luis', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'jluis1976@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(246, 'Aitor', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'aitorherant@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(247, 'Juan Miguel', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'juanmi.garcia@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(248, '', '0000-00-00', 'COPITIMA 1Ã‚Âª EDICIÃƒÂ“N', '', '', '', '', '', '', 'macondobuendia67@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(249, 'Miguel', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'lepe@alora.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(250, 'Marta', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'martajimenezdiaz@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO');
INSERT INTO `contactos` (`codigo`, `nombre`, `fechaNacimiento`, `empresa`, `tipo`, `apellidos`, `origen`, `cargo`, `importancia`, `programa`, `email1`, `email2`, `direccion1`, `provincia`, `linkedin`, `telefono1`, `telefono2`, `website1`, `facebook`, `twitter`, `skype`, `descripcion`, `observaciones`, `activo`) VALUES
(251, 'JosÃƒÂ© Antonio', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'revit12062015@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(252, 'Luis', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'lmoreno.at@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(253, 'Guillermo', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'guille.mf@hotmail.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(254, 'Tudela', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'tudelacintado@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(255, 'Ana', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'ana.cisnerosruiz@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(256, 'Mikel', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'mikelbenito@telefonica.net', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(257, 'Eva', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'itempmalaga@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(258, 'Ana', '0000-00-00', 'PFIBIM 1Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'ambo2266@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(259, 'Domingo', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'adelagijont@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(260, 'EncarnaciÃƒÂ³n', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'cardenasencarni@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(261, 'Jacob', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jacobbadillo41@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(262, 'Antonio', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'antoniocamachobaena@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(263, 'Jaime', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'jfo73.iti@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(264, 'ÃƒÂlvaro', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'alvarocabreja@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(265, 'C', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'cotizajm@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(266, 'Mauel', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'mlopeztur@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(267, 'Francisco', '0000-00-00', 'PFIBIM 2Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'franxiskorando@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(268, 'Marta', '0000-00-00', 'PFIBIM 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'marta_ruiz_bernet@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(269, 'Francisco', '0000-00-00', 'PFIBIM 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'frangomezf@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(270, 'Juan', '0000-00-00', 'PFIBIM 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'juaniokm@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(271, 'Rafael', '0000-00-00', 'PFIBIM 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'rafamv10@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(272, 'Luis', '0000-00-00', 'PFIBIM 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'luismrbcg@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(273, 'Cristina', '0000-00-00', 'PFIBIM 3Ã‚Âª EdiciÃƒÂ³n', '', '', '', '', '', '', 'criss_benitez88@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(274, 'Sergio', '0000-00-00', 'AVE MARÃƒÂA', '', '', '', '', '', '', 'sergiocano96@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(275, 'Francisco', '0000-00-00', 'INICIACIÃƒÂ“N BIM', '', '', '', '', '', '', 'f.fernandez@t-arjona.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(276, 'TomÃƒÂ¡s', '0000-00-00', 'INICIACIÃƒÂ“N BIM', '', '', '', '', '', '', 'arjonarey@icloud.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(277, 'A', '0000-00-00', 'INICIACIÃƒÂ“N BIM', '', '', '', '', '', '', 'adelagijont@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(278, 'Francisco', '0000-00-00', 'INICIACIÃƒÂ“N BIM', '', '', '', '', '', '', 'fjdimo@portablebunkerhouse.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(279, 'Salvador', '0000-00-00', 'INICIACIÃƒÂ“N BIM', '', '', '', '', '', '', 'salvador@t-arjona.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(280, 'Sergio', '0000-00-00', 'INICIACIÃƒÂ“N BIM', '', '', '', '', '', '', 'sernoa2009@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(281, 'Francisco', '0000-00-00', 'R2', '', '', '', '', '', '', 'rentalpacifico@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(282, 'Juan Manuel', '0000-00-00', 'R2', '', '', '', '', '', '', 'estudio@sauraarquitectos.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(283, 'Alfonso', '0000-00-00', 'R2', '', '', '', '', '', '', 'alfonsovalencia05@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(284, 'Pepe', '0000-00-00', 'R2', '', '', '', '', '', '', 'pepemesa@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(285, 'Giovanna', '0000-00-00', 'R2', '', '', '', '', '', '', 'gso.giovanna@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(286, 'JosÃƒÂ© Manuel', '0000-00-00', 'R2', '', '', '', '', '', '', 'jmlzarco@arksur.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(287, 'Francisco', '0000-00-00', 'R2', '', '', '', '', '', '', 'gucr2776@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(288, 'Luis', '0000-00-00', 'ESTUDIO7', '', '', '', '', '', '', 'malaga@estudio7.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(289, 'Juan Pablo', '0000-00-00', 'ESTUDIO7', '', '', '', '', '', '', 'jperez@apba.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(290, 'Oscar', '0000-00-00', 'ARQUIRED', '', '', '', '', '', '', 'osagrui@arquired.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(291, 'JosÃƒÂ© Antonio', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'c3141as@copitima.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(292, 'Juan', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'jlsferdinand@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(293, 'Fernando', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'fermansa59@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(294, 'MarÃƒÂ­a del Mar', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'marimarcarrion@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(295, 'F. Esteban', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'festeban64festeban64@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(296, 'Rosa MarÃƒÂ­a', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', '0610265453@alu.uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(297, 'RocÃƒÂ­o', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'rocionievesmedina@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(298, 'C', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'c3339mm@copitima.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(299, 'Marta', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'maruegat@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(300, 'Francisco', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'jfernandezquintana@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(301, 'Rodriguez', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'aojsmzmz@telefonica.net', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(302, 'JO', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'calo1170@coaat.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(303, 'SALVADOR', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'salvagod@outlook.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(304, 'JM', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'jmmj32000@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(305, 'JesÃƒÂºs  Manuel', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'jemagosa72@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(306, 'David', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'davi1244@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(307, 'I', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'ideo@ideoingenieria.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(308, 'JosÃƒÂ© Antonio', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'jaadalidsoldado@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(309, 'MarÃƒÂ­a Luisa', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'mlescudier@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(310, 'ÃƒÂngel Fernando', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'angelmatamartin2016@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(311, 'Marta', '0000-00-00', 'DIPUTACIÃƒÂ“N MÃƒÂLAGA - 1Ã‚Âª Fase', '', '', '', '', '', '', 'ortigalan@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(312, 'Alejandro', '0000-00-00', 'SMASSA', '', '', '', '', '', '', 'abarrio@smassa.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(313, 'Trinidad', '0000-00-00', 'SMASSA', '', '', '', '', '', '', 'trinidad@smassa.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(314, 'MrarÃƒÂ­a JosÃƒÂ©', '0000-00-00', 'SMASSA', '', '', '', '', '', '', 'lobatoc5598@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(315, 'ÃƒÂlvaro', '0000-00-00', 'SMASSA', '', '', '', '', '', '', 'alvarogarciacampos@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(316, 'Juan', '0000-00-00', 'SMASSA', '', '', '', '', '', '', 'juangarciavil...obos@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(317, 'Leonardo', '0000-00-00', 'SMASSA', '', '', '', '', '', '', 'ldiazales@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(318, 'Juan', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'jammalave@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(319, 'JosÃƒÂ© Luis', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'jlrcuadra@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(320, 'Manuela', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'mfernandezp@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(321, 'JosÃƒÂ© Luis', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'jlrcuadra@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(322, 'Francisco', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'feguilior@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(323, 'Lola', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'mdpraxedes@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(324, 'Tenti', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'vccazalilla@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(325, 'Lola', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'aptcmarbella@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(326, 'Juan Antonio', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'jamarin@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(327, 'Francisco', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'feguilior@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(328, 'Tenti', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'cartenticar@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(329, 'AgustÃƒÂ­n', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'avaleroarce@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(330, 'F', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'fjperez@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(331, 'Manuela', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'mfernandezp888@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(332, 'Juan Antonio', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'juanantonio.macias@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(333, 'Juan Antonio', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'jampatino@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(334, 'Fernando', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'fernando.guerrero@coamalaga.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(335, 'Fernando', '0000-00-00', 'Urbanismo - Arquitectura', '', '', '', '', '', '', 'arquifgd@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(336, 'Antonio', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'amartins@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(337, 'Pedro JosÃƒÂ©', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'pnavas@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(338, 'Pedro JosÃƒÂ©', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'pnavasq@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(339, 'A', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'admerida@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(340, 'R', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'rvidsan@ciccp.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(341, 'S', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'smolina@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(342, 'Antonio', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'amartinsdv@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(343, 'Pilar', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'pilarvilah@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(344, 'Pilar', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'pvila@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(345, 'Alberto', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'albertaco@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(346, 'Silvana', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', '71silvanamolina@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(347, 'Paloma', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'palomaml62@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(348, 'Carmen', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'mcmlopez@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(349, 'R', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'rvidal@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(350, 'Paloma', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'palomadelcarmen80@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(351, 'David', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'dmguerrero@malaga.eu', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(352, 'David', '0000-00-00', 'Urbanismo - Infraestructuras', '', '', '', '', '', '', 'dacarlu80@yahoo.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(353, 'JosÃƒÂ© M', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'josem.murillo@soligue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(354, 'JosÃƒÂ© M.', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'j.murillo@sorigue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(355, 'F.', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'f.munof@sorigue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(356, 'Antonio', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'a.mohedano@sorigue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(357, 'Ana', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'a.maldonado@sorigue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(358, 'Victor', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'victor.duque@sorigue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(359, 'Dionisio', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'd.godoy@sorigue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(360, 'Santo', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'santo.ruiz@sorigue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(361, 'Victor', '0000-00-00', 'ACSA', '', '', '', '', '', '', 'vm.duque@sorigue.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(362, 'Rodolfo', '0000-00-00', 'GONZÃƒÂLEZ & JACOBSON ARQUITECTURA, S. L.', '', '', '', '', '', '', 'rodolfo@gjarquitectura.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(363, 'Miguel', '0000-00-00', 'COAAT Granada', '', '', '', '', '', '', 'mcm@coaagtr.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(364, 'JosÃƒÂ© Luis', '0000-00-00', 'COAAT CÃƒÂ³rdoba', '', '', '', '', '', '', 'luqueruiz@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(365, 'MarÃƒÂ­a', '0000-00-00', 'EJECUCIÃƒÂ“N DEL PLANEAMIENTO, S.L.', '', '', '', '', '', '', 'maria.garcia@edp-sl.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(366, 'Francisco', '0000-00-00', 'FAMASER', '', '', '', '', '', '', 'central@famaser.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(367, 'Juan', '0000-00-00', 'Promociones y Propiedades Inmobiliarias Espacio, S.L.U.', '', '', '', '', '', '', 'jsannoda@ie-sa.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(368, 'Roger', '0000-00-00', 'TORRAS Y SIERRA ARQUITECTOS', '', '', '', '', '', '', 'roger@torrasysierra.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(369, 'JesÃƒÂºs', '0000-00-00', 'Ayuntamiento Torremolinos', '', '', '', '', '', '', 'jesus.gonzalez.pgou@ayto-torremolinos.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(370, 'Ana', '0000-00-00', 'BYNOK, S.L.', '', '', '', '', '', '', 'ana@bynok.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(371, 'JosÃƒÂ© Carlos', '0000-00-00', 'URBITECH', '', '', '', '', '', '', 'jcmelero@urbitech.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(372, 'Miguel', '0000-00-00', 'REYMALASA XXI, S.L.', '', '', '', '', '', '', 'arquitecto@gruporeymalasa.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(373, 'Rafael', '0000-00-00', 'UNIVERSIDAD DE MÃƒÂLAGA', '', '', '', '', '', '', 'rguzman@uma.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(374, 'Francisco', '0000-00-00', 'TEMPRADO 10 Arquitectos', '', '', '', '', '', '', 'bs@temprado10.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(375, 'Eduardo', '0000-00-00', 'FERROVIAL', '', '', '', '', '', '', 'eduardo.mancha@ferrovial.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(376, 'Miguel', '0000-00-00', 'DORRONSORO ARQUITECTOS', '', '', '', '', '', '', 'info@dorronsoroarquitectos.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(377, 'Sergio', '0000-00-00', 'SINGLEHOME', '', '', '', '', '', '', 'sergio.barragan@singlehome.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(378, 'IvÃƒÂ¡n', '0000-00-00', 'BIM LEVEL', '', '', '', '', '', '', 'ivanguerra@bimlevel.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(379, 'David', '0000-00-00', 'Arquiox. ', '', '', '', '', '', '', 'davidrodriguez@arquiox.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(380, 'Antonio', '0000-00-00', 'SGS MÃƒÂ¡laga ', '', '', '', '', '', '', 'antonio.martinez@sgs.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(381, 'Daniel', '0000-00-00', 'CHACÃƒÂ“N ARQUITECTOS', '', '', '', '', '', '', 'daniel@chaconarquitectos.com ', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(382, 'Angeles', '0000-00-00', 'METACENTRO CONSTRUCCIONES, S.L.', '', '', '', '', '', '', 'angeles.mena@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(383, 'Marisa ', '0000-00-00', 'COAATIE DE ALMERÃƒÂA', '', '', '', '', '', '', 'formacion@coaat-al.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(384, 'Abilio', '0000-00-00', 'UNICAJA', '', '', '', '', '', '', 'asanzrus@unicaja.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(385, 'JosÃƒÂ© Antonio', '0000-00-00', 'CONSULTING DE INGENIERÃƒÂA OLUZ', '', '', '', '', '', '', 'info@oluz.es', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(386, 'MarÃƒÂ­a Isabel', '0000-00-00', 'CENTRO DE EDUCACUÃƒÂ“N SECUNDARIA Ã¢Â€ÂœSAN JOSÃƒÂ‰Ã¢Â€Â ', '', '', '', '', '', '', 'sanjosecicloconstruccion@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(387, 'JosÃ© ', '0000-00-00', '', '', 'Moreno GalÃ¡n', '', '', 'Alta', 'PFIBIM Manager', 'c3654mg@copitima.com', '', '', 'MÃ¡laga', '', '615060562', '', '', '', '', '', 'De: JOSE \r\nTelefono:615060562\r\nEmail: c3654mg@copitima.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n', '', 'NO'),
(388, 'FRANCISCO', '0000-00-00', '', '', 'GUTIERREZ CRUZ ', '', '', '', 'PFIBIM Manager', 'gucr2776@gmail.com', '', '', '', '', '647929121', '', '', '', '', '', 'De: FRANCISCO GUTIERREZ CRUZ \r\nTelefono:647929121\r\nEmail: gucr2776@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n', '', 'NO'),
(389, 'Pedro', '0000-00-00', '', '', '', '', '', 'Alta', 'CERTIFICACIÃ“N PROFESSIONAL AUTODESK REVIT', 'pedro81ss@hotmail.com', '', '', '', '', '635566715', '', '', '', '', '', 'De: Pedro \r\nEmail: pedro81ss@hotmail.com\r\nTelÃ©fono: 635566715\r\nAsunto: FormaciÃ³n\r\n\r\nCuerpo del mensaje:\r\nBuenos dÃ­as. Mi nombres es Pedro y estoy interesado en el examen de certificaciÃ³n de Revit. He visto que desde este mes de Febrero las condiciones han cambiado y necesitarÃ­a todo la informaciÃ³n relativa a dicho examen (fechas, precios, cursos, etc). Actualmente vivo en Almeria, por lo que necesito saber si se pueden hacer los cursos a distancia. Espero su respuesta con toda la informaciÃ³n posible sobre este tema. Gracias. Pedro.\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(390, 'Pedro', '0000-00-00', '', '', 'Naranjo', 'Web', '', 'Alta', 'PFIBIM Manager', 'orantx@gmail.com', '', '', '', '', '662068434', '', '', '', '', '', 'De: Pedro Naranjo \r\nTelefono:662068434\r\nEmail: orantx@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(391, 'JosÃ© Luis', '0000-00-00', 'QMA Consultores', '', 'DÃ¼rstelers Esteban', 'Referencia', 'Socio', 'Media', 'AECO.01', 'jldursteler@qmaconsultores.com', '', 'Pasaje Saturno, 2', 'MÃ¡laga', '', '622255008', '', 'www.qmamalaga.com', '', '', '', '', '', 'SI'),
(392, '', '0000-00-00', '', '', '', '', '', '', 'AECO.01', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(393, 'Diana', '0000-00-00', '', '', 'Romero', '', '', '', '', 'diva870521@hotmail.com', '', '', '', '', '662012048', '', '', '', '', '', 'De: diana romero \r\nTelefono:662012048\r\nEmail: diva870521@hotmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(394, 'Oscar', '0000-00-00', '', '', 'GutiÃ©rrez', '', '', 'Alta', '', 'oscar.gutierrez@grupolaquinta.com', '', '', '', '', '627568626', '', '', '', '', '', 'De: OSCAR GUTIERREZ \r\nEmail: oscar.gutierrez@grupolaquinta.com\r\nTelÃ©fono: 627568626\r\nAsunto: FormaciÃ³n\r\n\r\nCuerpo del mensaje:\r\nDesearia informacioon sobre sus cursos en BIM (REVIT, NAVISWORK,..)\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)', '', 'NO'),
(395, 'Acerina', '0000-00-00', 'INSTITUTO TECNOLOGICO DE CANARIAS', 'IngenierÃ­a BiomÃ©dica', 'MonzÃ³n Ramos ', 'Web', 'DiseÃ±adora', 'Alta', 'FUSION 360', 'acerinamonzon@gmail.com', '', '', '', '', '646944845', '', '', '', '', '', 'De: Acerina MonzÃ³n Ramos \r\nTelefono:646944845\r\nEmail: acerinamonzon@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(396, 'Miguel Ãngel', '0000-00-00', '', '', 'LÃ³pez Santos ', '', '', 'Alta', 'BIM PFIBIM Manager', 'miguelals@hotmail.com', '', '', '', '', '666024219', '', '', '', '', '', 'De: Miguel Ãngel LÃ³pez Santos \r\nEmail: miguelals@hotmail.com\r\nTelÃ©fono: 666024219\r\nAsunto: FormaciÃ³n\r\n\r\nCuerpo del mensaje:\r\nSoy arquitecto y estoy empezando como profesional liberal. Estoy interesado en tener informaciÃ³n sobre el CURSO BIM MANAGER y  BIM REVIT BASICO. TambiÃ©n sobre prÃ³ximas fechas y coste de los mismos.\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(397, 'Jorge', '0000-00-00', '', '', 'Padilla', '', '', 'Alta', 'PFIBIM Manager', 'jpadilla86@gmail.com', '', '', '', '', '661892284', '', '', '', '', '', 'De: Jorge \r\nEmail: jpadilla86@gmail.com\r\nTelÃ©fono: 661892284\r\nAsunto: FormaciÃ³n\r\n\r\nCuerpo del mensaje:\r\nInformaciÃ³n, precios y horarios de todos los cursos de bim revit relacionados con la ingenierÃ­a industrial.\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(398, 'PEPE', '0000-00-00', '', '', 'PRUEBA', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NO'),
(399, 'Carlos Alejandro ', '0000-00-00', 'CAI Consultores', '', 'GarcÃ­a LÃ³pez', 'Mail', 'ICCP ', 'Alta', 'PFIBIM Manager', 'carlosalejandro.ingenieria@gmail.com', '', 'Avenida del Carmen s/nÃºm. Edificio Puerto Sol, oficina nÃºm. 5 Estepona', 'MÃ¡laga', '', '619831581', '952808718', '', '', '', '', 'De: Carlos Alejandro \r\nTelefono:619831581\r\nEmail: carlosalejandro.ingenieria@gmail.com\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO'),
(400, 'SILVIA ', '0000-00-00', '', '', '', '', '', '', '', 'info@lopadoarquitectura.coom', '', '', '', '', '620963472', '', '', '', '', '', 'De: SILVIA \r\nTelefono:620963472\r\nEmail: info@lopadoarquitectura.coom\r\nAsunto: Adapta MS | Autodesk BIM MÃ¡laga "Contacto"\r\n\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en Adapta MS | Autodesk BIM MÃ¡laga (http://adaptams.com)\r\n', '', 'NO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratos`
--

CREATE TABLE IF NOT EXISTS `contratos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoAlumno` int(255) NOT NULL,
  `codigoPrograma` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `importeCurso` varchar(100) NOT NULL,
  `pagoInicial` varchar(100) NOT NULL,
  `formaPago` enum('Cuenta corriente','Paypal','Transferencia','Recibo domiciliado') NOT NULL,
  `baja` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `fechaBaja` date NOT NULL,
  `motivoBaja` varchar(255) NOT NULL,
  `facturaEmpresa` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `denominacionSocial` varchar(255) NOT NULL,
  `cif` varchar(20) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `cp` varchar(20) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoAlumno` (`codigoAlumno`),
  KEY `codigoCurso` (`codigoPrograma`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `contratos`
--

INSERT INTO `contratos` (`codigo`, `codigoAlumno`, `codigoPrograma`, `fecha`, `importeCurso`, `pagoInicial`, `formaPago`, `baja`, `fechaBaja`, `motivoBaja`, `facturaEmpresa`, `denominacionSocial`, `cif`, `direccion`, `cp`, `provincia`) VALUES
(2, 2, NULL, '2017-02-06', '2150', '150', 'Recibo domiciliado', 'NO', '0000-00-00', '', 'NO', '', '', '', '', ''),
(3, 83, NULL, '2017-03-03', '2150', '150', 'Recibo domiciliado', 'NO', '0000-00-00', '', 'NO', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratos_facturas`
--

CREATE TABLE IF NOT EXISTS `contratos_facturas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoContrato` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `importe` double NOT NULL,
  `facturado` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoContrato` (`codigoContrato`),
  KEY `facturado` (`facturado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratos_gastos`
--

CREATE TABLE IF NOT EXISTS `contratos_gastos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoContrato` int(255) NOT NULL,
  `codigoGasto` int(255) DEFAULT NULL,
  `precio` double NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoContrato` (`codigoContrato`),
  KEY `codigoGasto` (`codigoGasto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conversaciones`
--

CREATE TABLE IF NOT EXISTS `conversaciones` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `asunto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos`
--

CREATE TABLE IF NOT EXISTS `correos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `destinatarios` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `asunto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` text COLLATE latin1_spanish_ci NOT NULL,
  `adjunto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `correos`
--

INSERT INTO `correos` (`codigo`, `destinatarios`, `fecha`, `hora`, `asunto`, `mensaje`, `adjunto`, `codigoUsuario`) VALUES
(1, 'jldursteler@qmaconsultores.com', '2017-03-03', '10:35:00', 'PROGRAMA SOLICITADO E INSCRIPCIÃ“N', 'Buenos a todos:\r\n\r\nSoy JosÃ© MarÃ­a GutiÃ©rrez, responsable del Ã¡rea de formaciÃ³n\r\ny gerente del centro&nbsp; AUTODESK\r\nATC/AAP/CACC BIM MÃLAGA â€“ Adapta Management Services.\r\n\r\n<span>Contacto contigo en relaciÃ³n a la convocatoria del â€œPrograma\r\nPFIBIM Manager 5Âª EdiciÃ³n" y creo que podrÃ­a interesarte. Esta formaciÃ³n\r\nes Oficial de Autodesk y estÃ¡ homologada por la ACP Profesional. Si lo deseas,\r\npuedes tener mÃ¡s informaciÃ³n de nuestra formaciÃ³n en <a href="http://www.adaptams.com" target="" rel="">www.adaptams.com</a></span>\r\n\r\nSi deseas recibir el temario del Programa PFIBIM Manager\r\npara la convocatoria vigente Febrero de 2017 indÃ­camelo y te lo hago llegar por\r\nmail. La formaciÃ³n se imparte de manera presencial y prÃ³ximamente online.\r\nNuestra meta como ATC/AAP/CACC de Autodesk, es atenderte personalmente, y\r\nofrecerte la mejor propuesta formativa.\r\n\r\n&nbsp;\r\n\r\nEn cuanto a fechas, el programa estÃ¡ planificado para el mes\r\nde Febrero, pendiente de concretar fecha. En cuanto a los dÃ­as, disponemos de\r\nvarias opciones, por la tarde, los Lunes y MiÃ©rcoles en horario de 16:00 a\r\n21:00 horas, o la opciÃ³n de turno de maÃ±ana, los Lunes, Martes y Viernes, en\r\nvarios horarios, 8:45 a 11:45 horas y 12:00 a 15:00 horas. Y en fines de\r\nsemana, los viernes tarde de 16:00 a 21:00 horas y la maÃ±ana de los sÃ¡bados en\r\nhorario de 9:00 a 14:00 horas.\r\n\r\n&nbsp;\r\n\r\nEn cuanto al precio del programa asciende a 2150 â‚¬. Si\r\nquisieras financiarlo, te pondremos en contacto con la financiera con la que\r\ncolaboramos.\r\n\r\n&nbsp;\r\n\r\nPara cualquier consulta o duda puedes contactar con nosotros\r\na travÃ©s de este mail o llamÃ¡ndonos al mÃ³vil 637 474 372 o al telÃ©fono fijo 952\r\n341 007.\r\n\r\n&nbsp;\r\n\r\nADAPTA Management Services:\r\n\r\nï€­&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Centro\r\nde FormaciÃ³n Oficial de Autodesk ATC/APP. Ver aquÃ­ \r\n\r\nï€­&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Centro\r\nde FormaciÃ³n Oficial de BIM Iberica.\r\n\r\nï€­&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Instructores\r\nATC Autodesk autorizados.\r\n\r\nï€­&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Centro\r\nOficial Certificador de Autodesk ACC.\r\n\r\nï€­&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CertificaciÃ³n\r\nde Autodesk Revit Profesional NO incluido en el coste del programa. RealizaciÃ³n\r\ndel Examen en las instalaciones de ADAPTA MS. CertificaciÃ³n de validez\r\ninternacional, Ãºnica reconocida.\r\n\r\nï€­&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Entrega\r\nal alumno de la Licencia Educacional de Medit.', '', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `curso` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `precio` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `tipoPlazo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'SI',
  `plazos` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`codigo`, `curso`, `precio`, `tipoPlazo`, `plazos`) VALUES
(1, 'BIM MANAGER â€“ PFIBIM', '2150', 'SI', '1'),
(2, 'BIM REVIT ESTRUCTURES', '515', 'SI', '1'),
(3, 'BIM REVIT BÃSICO', '', 'SI', '1'),
(4, 'BIM REVIT PARA ARQUITECTOS', '', 'SI', '1'),
(5, 'BIM REVIT PARA JEFES DE OBRAS', '', 'SI', '1'),
(6, 'BIM REVIT PARA ARQUITECTOS TÃ‰CNICOS / INGENIEROS EDIFICACIÃ“N', '', 'SI', '1'),
(7, 'BIM REVIT MEP', '', 'SI', '1'),
(8, 'BIM AUTODESK REVIT', '', 'SI', '1'),
(9, 'BIM AUTODESK NAVISWORKS ', '', 'SI', '1'),
(10, 'AUTODESK BIM 360', '', 'SI', '1'),
(11, 'FLUJOS DE TRABAJOS BIM CON AUTODESK', '', 'SI', '1'),
(12, 'INFRAWORKS ', '', 'SI', '1'),
(13, 'AUTODESK AUTOCAD CIVIL 3D ', '', 'SI', '1'),
(14, 'FUSION 360', '', 'SI', '1'),
(15, 'AUTODESK INVENTOR', '', 'SI', '1'),
(16, 'AUTODESK REVIT & API ARCHIBUS', '', 'SI', '1'),
(17, 'AUTODESK BIM 360 GLUE', '', 'SI', '1'),
(18, 'AUTODESK VAULT', '', 'SI', '1'),
(19, 'BIM REVIT INFRAESTRUCTURAS', '', 'SI', '1'),
(20, 'AUTODESK INFRAWORKS', '', 'SI', '1'),
(21, 'AUTODESK AUTOCAD CIVIL 3D', '', 'SI', '1'),
(22, 'AUTODESK AUTOCAD MAP 3D', '', 'SI', '1'),
(23, 'AUTODESK RECAP & REVIT', '', 'SI', '1'),
(24, 'MEDIT', '', 'SI', '1'),
(25, 'MICROSOFT PROJECT', '', 'SI', '1'),
(26, 'PRIMAVERA P6', '', 'SI', '1'),
(27, 'PROJECT MANAGEMENT', '', 'SI', '1'),
(28, 'CONSTRUCTION MANAGEMENT', '', 'SI', '1'),
(29, 'NAVISWORKS', '', 'SI', '1'),
(30, 'SYNCHRO', '', 'SI', '1'),
(31, 'SINNAPS', '', 'SI', '1'),
(32, 'CERTIFICACION Autodesk Revit Architecture', '', 'SI', '1'),
(33, 'CERTIFICACION Autodesk Revit Structure', '', 'SI', '1'),
(34, 'CERTIFICACION Autodesk Revit MEP Mechanical', '', 'SI', '1'),
(35, 'CERTIFICACION Autodesk Revit MEP Electrical', '', 'SI', '1'),
(36, 'CERTIFICACION Autodesk Autocad Civil 3D', '', 'SI', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinatarios_conversacion`
--

CREATE TABLE IF NOT EXISTS `destinatarios_conversacion` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoConversacion` int(255) DEFAULT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoMensaje` (`codigoConversacion`,`codigoUsuario`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinatarios_mensajes`
--

CREATE TABLE IF NOT EXISTS `destinatarios_mensajes` (
  `codigoMensaje` int(255) NOT NULL,
  `codigoUsuario` int(255) NOT NULL,
  `leido` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  KEY `codigoMensaje` (`codigoMensaje`,`codigoUsuario`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentosDistribucion`
--

CREATE TABLE IF NOT EXISTS `documentosDistribucion` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `nombreDocumento` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `ubicacion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `copia` enum('ORIGINAL','CONTROLADO','NOCONTROLADO') COLLATE latin1_spanish_ci NOT NULL,
  `ficheroDistribucion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentosExternos`
--

CREATE TABLE IF NOT EXISTS `documentosExternos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoDocumento` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `nombreDocumento` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `ubicacion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `ficheroExterno` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentosInternos`
--

CREATE TABLE IF NOT EXISTS `documentosInternos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoDocumento` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `nombreDocumento` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaRevision` date NOT NULL,
  `fecha` date NOT NULL,
  `ficheroInterno` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentosRegistros`
--

CREATE TABLE IF NOT EXISTS `documentosRegistros` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoDocumento` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `nombreDocumento` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `responsable` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `ubicacion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `tiempoConservacion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `ficheroRegistro` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion`
--

CREATE TABLE IF NOT EXISTS `facturacion` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `referencia` int(255) NOT NULL,
  `fechaEmision` date NOT NULL,
  `fechaVencimiento` date NOT NULL,
  `subtotal` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `iva` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `coste` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `formaPago` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `cobrada` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `enviada` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `concepto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoFactura` enum('GENERAL','RECTIFICATIVA') COLLATE latin1_spanish_ci NOT NULL,
  `origen` enum('CONTRATO','VENTA') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'CONTRATO',
  `codigoContrato` int(255) DEFAULT NULL,
  `codigoVenta` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoTrabajo` (`codigoContrato`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `facturacion`
--

INSERT INTO `facturacion` (`codigo`, `referencia`, `fechaEmision`, `fechaVencimiento`, `subtotal`, `iva`, `coste`, `formaPago`, `cobrada`, `enviada`, `concepto`, `tipoFactura`, `origen`, `codigoContrato`, `codigoVenta`) VALUES
(2, 1, '2017-02-06', '2017-03-02', '2150', '0', '2150', 'TRANSFERENCIA', 'SI', 'NO', 'PFIBIM Manager', 'GENERAL', 'CONTRATO', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historico_clientes`
--

CREATE TABLE IF NOT EXISTS `historico_clientes` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `gestion` varchar(255) NOT NULL,
  `observaciones` text NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoCliente` (`codigoCliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `historico_clientes`
--

INSERT INTO `historico_clientes` (`codigo`, `fecha`, `gestion`, `observaciones`, `codigoCliente`) VALUES
(1, '2017-02-14', 'email', 'FormaciÃ³n ACP', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historico_contactos`
--

CREATE TABLE IF NOT EXISTS `historico_contactos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `gestion` varchar(255) NOT NULL,
  `observaciones` text NOT NULL,
  `codigoContacto` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoContacto` (`codigoContacto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `historico_contactos`
--

INSERT INTO `historico_contactos` (`codigo`, `fecha`, `gestion`, `observaciones`, `codigoContacto`) VALUES
(3, '2017-01-20', 'email', 'Intentar llamarla a su mÃ³vil', 2),
(5, '2017-01-12', 'email', 'Pendiente de quedar en la oficina la semana 23 de enero', 4),
(6, '2017-01-20', 'email', 'demys.hernandezg@gmail.com', 3),
(7, '2017-01-20', 'email', 'Llamada mÃ³vil', 5),
(8, '2017-01-20', 'email', 'Nuevo mail enviado', 6),
(9, '2017-01-09', 'llamada', 'Avisar cuando se convoque un curso AutoCAD Civil 3D', 8),
(11, '2017-01-20', 'llamada', 'Enviarle programa PFIBIM Manager y BIM Structure', 1),
(12, '2017-01-20', 'email', 'EnvÃ­o de programas PFIBIM Manager y BIM Revit Structure', 1),
(13, '2017-01-23', 'email', 'Solicitud informaciÃ³n + temario PFIBIM Manager', 11),
(18, '2017-02-01', 'email', 'Enviar solicitud informaciÃ³n PFIBIM Manager', 13),
(21, '2017-02-04', 'email', 'Enviado CONTRATO INSCRIPCIÃ“N - MATRÃCULA', 15),
(23, '2017-02-07', 'llamada', 'Pendiente enviar CERTIFICACIÃ“N AUTODESK REVIT ARCHITECTURE', 16),
(24, '2017-02-01', 'email', 'Pendiente de enviar email con solicitud de informaiÃ³n en certificaciÃ³n Revit Architecture', 12),
(25, '2017-02-14', 'email', 'Asistente a curso', 17),
(26, '2017-02-21', 'email', 'PFIBIM Manager', 387),
(27, '2017-02-21', 'email', 'EnvÃ­o PFIBIM', 388),
(28, '2017-02-28', 'email', 'CertifiaciÃ³n Autodesk Revit Professional', 389),
(29, '2017-03-03', 'email', 'PFIBIM Manager y CERTIFICACIÃ“N Autodesk Revit', 390),
(30, '2017-03-03', 'email', 'EnvÃ­o ACO.01', 391),
(31, '2017-03-05', 'email', 'PFIBIM Manager', 393),
(32, '2017-03-08', 'email', 'PFIBIM Manager - EMPRESA', 394),
(34, '2017-03-13', 'email', 'iNFORMACIÃ“N FUSION 360', 395),
(35, '2017-03-13', 'email', 'Solicitud informaciÃ³n PFIBIM Manager ', 396),
(36, '2017-03-13', 'email', 'Solicitud informaciÃ³n PFIBIM manager', 397),
(37, '2017-03-31', 'email', 'PFIBIM Manager', 400);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicadores`
--

CREATE TABLE IF NOT EXISTS `indicadores` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipo` enum('MISIONALES','ESTRATEGICOS','APOYO','') COLLATE latin1_spanish_ci NOT NULL,
  `valor_real` enum('ESTATICO','FORMULA') COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `referencias_documentales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `proveedores` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `entradas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `responsable` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `recursos` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `clientes` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `salidas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `registros` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `inspecciones` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infraestructuras`
--

CREATE TABLE IF NOT EXISTS `infraestructuras` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `fechaCompra` date NOT NULL,
  `identificacion` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `tipo` enum('VEHICULO','MAQUINARIA','ORDENADOR','OTROS') COLLATE latin1_spanish_ci NOT NULL,
  `definicionTipo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoRevision` enum('VISUAL','PRUEBA','OTRO') COLLATE latin1_spanish_ci NOT NULL,
  `definicionRevision` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `subcontratada` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `proveedor` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fechaUltimaRevision` date NOT NULL,
  `fechaProximaRevision` date NOT NULL,
  `seguro` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `fechaVencimiento` date NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mapaProcesos`
--

CREATE TABLE IF NOT EXISTS `mapaProcesos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `indicador` int(255) NOT NULL,
  `descripcion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `valorFinal` int(255) NOT NULL,
  `numerador` int(255) NOT NULL,
  `denominador` int(255) NOT NULL,
  `condicion` enum('==','>=','>','<=','<') COLLATE latin1_spanish_ci NOT NULL,
  `tolerancia` int(255) NOT NULL,
  `frecuencia` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `mapa_procesos_ibfk_1` (`indicador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mejoracontinua`
--

CREATE TABLE IF NOT EXISTS `mejoracontinua` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `tipo` enum('nc','incidencia') CHARACTER SET latin1 NOT NULL,
  `fecha` date NOT NULL,
  `area` varchar(255) CHARACTER SET latin1 NOT NULL,
  `responsable` varchar(255) CHARACTER SET latin1 NOT NULL,
  `emailResponsable` varchar(255) CHARACTER SET latin1 NOT NULL,
  `descripcion` text CHARACTER SET latin1 NOT NULL,
  `causas` text CHARACTER SET latin1 NOT NULL,
  `actuaciones` text CHARACTER SET latin1 NOT NULL,
  `correccion` text CHARACTER SET latin1 NOT NULL,
  `responsableImplantacion` varchar(255) CHARACTER SET latin1 NOT NULL,
  `emailResponsableImplantacion` varchar(255) CHARACTER SET latin1 NOT NULL,
  `fechaPrevista` date NOT NULL,
  `resuelta` enum('si','no') CHARACTER SET latin1 NOT NULL,
  `fechaResolucion` date NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE IF NOT EXISTS `mensajes` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `mensaje` text COLLATE latin1_spanish_ci NOT NULL,
  `ficheroAdjunto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `codigoConversacion` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoUsuario` (`codigoUsuario`),
  KEY `codigoConversacion` (`codigoConversacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metas`
--

CREATE TABLE IF NOT EXISTS `metas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `definicionMeta` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fechaLimiteMeta` date NOT NULL,
  `responsableMeta` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `recursosMeta` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `conseguidaMeta` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoObjetivo` int(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoObjetivo` (`codigoObjetivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `objetivos`
--

CREATE TABLE IF NOT EXISTS `objetivos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `area` enum('CALIDAD','MEDIO','PREVENCION','COMERCIAL') COLLATE latin1_spanish_ci NOT NULL,
  `definicion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `valorPartida` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `valorEsperado` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `formula` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaLimite` date NOT NULL,
  `conseguido` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otros_gastos`
--

CREATE TABLE IF NOT EXISTS `otros_gastos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `gasto` varchar(255) NOT NULL,
  `importe` double NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `codigoPuestoTrabajo` int(255) DEFAULT NULL,
  `fechaAlta` date NOT NULL,
  `fechaDocumentacion` date NOT NULL,
  `observaciones` text NOT NULL,
  `ficheroAdjunto` varchar(255) NOT NULL,
  `codigoUsuario` int(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoPuestoTrabajo` (`codigoPuestoTrabajo`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas_plan`
--

CREATE TABLE IF NOT EXISTS `personas_plan` (
  `codigoPlan` int(255) NOT NULL,
  `codigoEmpleado` int(255) NOT NULL,
  KEY `codigoPlan` (`codigoPlan`,`codigoEmpleado`),
  KEY `codigoEmpleado` (`codigoEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planesFormacion`
--

CREATE TABLE IF NOT EXISTS `planesFormacion` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `contenido` tinytext COLLATE latin1_spanish_ci NOT NULL,
  `fechaPrevista` date NOT NULL,
  `docentes` enum('INTERNOS','EXTERNOS') COLLATE latin1_spanish_ci NOT NULL,
  `indicar` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fechaReal` date NOT NULL,
  `evaluacion` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuestos`
--

CREATE TABLE IF NOT EXISTS `presupuestos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `empresa` int(255) NOT NULL,
  `comercial` int(255) DEFAULT NULL,
  `fase` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaValidez` date NOT NULL,
  `subtotal` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `referenciaPresupuesto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `aceptado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `empresa` (`empresa`),
  KEY `comercial` (`comercial`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `presupuestos`
--

INSERT INTO `presupuestos` (`codigo`, `empresa`, `comercial`, `fase`, `fechaValidez`, `subtotal`, `descripcion`, `referenciaPresupuesto`, `aceptado`) VALUES
(1, 3, 10, 'ganada', '2017-01-30', '16800', '<p style="margin-left:53.4pt"><em>M&Oacute;DULO INTRODUCCI&Oacute;N CV-BIM &ndash; <strong>(10h) - </strong>800 &euro;</em></p>\r\n\r\n<p style="margin-left:53.4pt"><em>(PROJECT MANAGEMENT Y LA METODOLOG&Iacute;A BIM)</em></p>\r\n\r\n<p style="margin-left:53.4pt">&nbsp;</p>\r\n\r\n<p style="margin-left:53.4pt"><em>M&Oacute;DULO CREATIVIDAD CON BIM &ndash; LOD 100/200 ARQUITECTURA <strong>(70 h) &ndash; </strong>5.600 &euro;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </em></p>\r\n\r\n<p style="margin-left:53.4pt"><em>(Estudio Previos, Anteproyecto y Proyecto B&aacute;sico)</em></p>\r\n\r\n<p style="margin-left:53.45pt">R1&nbsp;&nbsp;&nbsp; REVIT B&Aacute;SICO ENFOCADO A LA GESTI&Oacute;N BIM - 20h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt">R2&nbsp;&nbsp;&nbsp; REVIT ARQUITECTURA - 30h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt">R3&nbsp;&nbsp;&nbsp; REVIT FAMILIAS - 10h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt">R4&nbsp;&nbsp;&nbsp; REVIT VISUALIZACI&Oacute;N - 10h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:18.0pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.4pt"><em>M&Oacute;DULO PRODUCTIVIDAD CON BIM &ndash; LOD 300 ARQUITECTURA <strong>(30h) &ndash; </strong>2.400 &euro;</em></p>\r\n\r\n<p style="margin-left:53.4pt"><em>(Proyecto Ejecuci&oacute;n Arquitect&oacute;nico y Coordinaci&oacute;n)</em></p>\r\n\r\n<p style="margin-left:53.45pt">R5&nbsp;&nbsp;&nbsp; REVIT EN EQUIPO &ndash; 10h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt">R6&nbsp;&nbsp;&nbsp; REVIT MEDICIONES Y PRESUPUESTO CON MEDIT -20h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt">&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt"><em>M&Oacute;DULO ESPECIALIZACI&Oacute;N &ndash; LOD 300 INSTALACIONES / ESTRUCTURAS <strong>(50 h) &ndash; </strong>4.000 &euro;</em></p>\r\n\r\n<p style="margin-left:53.45pt"><em>(Proyecto Ejecuci&oacute;n Estructuras e Instalaciones)</em></p>\r\n\r\n<p style="margin-left:53.45pt">R7&nbsp;&nbsp;&nbsp; REVIT ESTRUCTURAS -20h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.4pt">R8&nbsp;&nbsp;&nbsp; REVIT INSTALACIONES &ndash; 30h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.4pt">&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt"><em>M&Oacute;DULO </em><em>GESTI&Oacute;N BIM LOD 400/4D Y 5D <strong>(</strong></em><strong><em>50 h)</em></strong><em> &ndash; 4.000 &euro;</em></p>\r\n\r\n<p style="margin-left:53.45pt"><em>(Calidad, gesti&oacute;n del coste y tiempo del Proyecto y Obra)</em></p>\r\n\r\n<p style="margin-left:53.45pt">R9&nbsp;&nbsp;&nbsp; DWG, IFC, DESIGN REVIEW, NAVISWORKS - 25h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt">R10&nbsp; BIM EN LA NUBE 360 GLUE - 5h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style="margin-left:53.45pt">R11&nbsp; DATA MANAGEMENT (VAULT) &ndash; 10h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>R12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; FLUJOS BIM AUTODESK A360 &ndash; 10h</p>\r\n', '1', 'SI'),
(2, 2, 10, 'ganada', '2017-02-05', '4125', '', '2', 'SI'),
(3, 4, 10, 'ganada', '2017-02-14', '445.46', '', '3', 'SI'),
(4, 4, 10, 'ganada', '2017-02-14', '445.45', '', '4', 'SI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuestos_facturas`
--

CREATE TABLE IF NOT EXISTS `presupuestos_facturas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoPresupuesto` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `importe` double NOT NULL,
  `facturado` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoPresupuesto` (`codigoPresupuesto`),
  KEY `facturado` (`facturado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_presupuestos`
--

CREATE TABLE IF NOT EXISTS `productos_presupuestos` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoPresupuesto` int(255) NOT NULL,
  `codigoProducto` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cantidad` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `precioUnitario` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descuento` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `totalDescuento` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `impuesto` int(255) DEFAULT NULL,
  `neto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoPresupuesto` (`codigoPresupuesto`,`codigoProducto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=107 ;

--
-- Volcado de datos para la tabla `productos_presupuestos`
--

INSERT INTO `productos_presupuestos` (`codigo`, `codigoPresupuesto`, `codigoProducto`, `cantidad`, `precioUnitario`, `descuento`, `totalDescuento`, `impuesto`, `neto`) VALUES
(60, 1, 'CV_BIM0_IntroducciÃ³n_BIM_Management', '1', '800', '0', '800', 0, '800'),
(61, 1, 'CV_BIM10_BIM360_Glue', '1', '400', '0', '400', 0, '400'),
(62, 1, 'CV_BIM11_Data_Management', '1', '800', '0', '800', 0, '800'),
(63, 1, 'CV_BIM12_BIM360_Field', '1', '800', '0', '800', 0, '800'),
(64, 1, 'CV_BIM1_Revit_Iniciacion', '1', '1600', '0', '1600', 0, '1600'),
(65, 1, 'CV_BIM2_Revit_Arquitectura', '1', '2400', '0', '2400', 0, '2400'),
(66, 1, 'CV_BIM3_Revit_Familias', '1', '800', '0', '800', 0, '800'),
(67, 1, 'CV_BIM4_Revit_Visualizacion', '1', '800', '0', '800', 0, '800'),
(68, 1, 'CV_BIM5_Revit_Equipos', '1', '800', '0', '800', 0, '800'),
(69, 1, 'CV_BIM6_Revit_Mediciones', '1', '1600', '0', '1600', 0, '1600'),
(70, 1, 'CV_BIM7_Revit_Structures', '1', '1600', '0', '1600', 0, '1600'),
(71, 1, 'CV_BIM8_Revit_MEP', '1', '2400', '0', '2400', 0, '2400'),
(72, 1, 'CV_BIM9_Gestion_BIM', '1', '2000', '0', '2000', 0, '2000'),
(78, 2, 'CV_BIM7_Revit_Structures', '1', '1100', '0', '1100', 0, '1100'),
(79, 2, 'CV_BIM8_Revit_MEP', '1', '1650', '0', '1650', 0, '1650'),
(80, 2, 'CV_BIM9_Gestion_BIM', '1', '1375', '0', '1375', 0, '1375'),
(97, 3, 'Costes OrganizaciÃ³n FormaciÃ³n MÃ³dulo 1 ACP MÃ¡laga', '1', '40.49', '0', '0', 0, '40.49'),
(98, 3, 'FormaciÃ³n MÃ³dulo 1 ACP MÃ¡laga', '1', '404.96', '0', '404.96', 0, '404.96'),
(99, 4, 'MÃ³dulo 1: LOD 100/200/300/350', '1', '404.96', '0', '404.96', 0, '404.96'),
(100, 4, 'Gastos organizaciÃ³n', '1', '40.49', '0', '40.49', 0, '40.49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programas`
--

CREATE TABLE IF NOT EXISTS `programas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoCategoria` int(255) DEFAULT NULL,
  `codigoSubcategoria` int(255) DEFAULT NULL,
  `codigoCurso` int(255) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `codigoInterno` varchar(255) NOT NULL,
  `precio` double NOT NULL,
  `incluyeMatricula` enum('SI','NO') NOT NULL,
  `importeMatricula` double NOT NULL,
  `observaciones` text NOT NULL,
  `lugar` varchar(255) NOT NULL,
  `horasLectivas` varchar(255) NOT NULL,
  `diasPrevistos` varchar(255) NOT NULL,
  `horario` varchar(255) NOT NULL,
  `modalidad` enum('PRESENCIAL','SEMIPRESENCIAL','DISTANCIA') NOT NULL,
  `previstaFecha` varchar(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoCategoria` (`codigoCategoria`),
  KEY `codigoSubcategoria` (`codigoSubcategoria`),
  KEY `codigoCurso` (`codigoCurso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `programas`
--

INSERT INTO `programas` (`codigo`, `codigoCategoria`, `codigoSubcategoria`, `codigoCurso`, `nombre`, `codigoInterno`, `precio`, `incluyeMatricula`, `importeMatricula`, `observaciones`, `lugar`, `horasLectivas`, `diasPrevistos`, `horario`, `modalidad`, `previstaFecha`) VALUES
(6, 1, 1, 1, 'Plan FormaciÃ³n Integral BIM Manager', 'AECO.01', 2150, 'SI', 150, 'FormaciÃ³n prÃ¡ctica en el dominio de las siguientes herramientas informÃ¡ticas BIM de Autodesk: Revit Architecture, Structure, MEP, Navisworks, BIM 360, Vault y las variadas y potentes extensiones de Autodesk Revit como Medit, ademÃ¡s de las perifÃ©ricas MS Project o Primavera P6.\r\n\r\nConocer las Ã¡reas de conocimiento de la gestiÃ³n de proyectos (Project & Construction Management) y su aplicaciÃ³n en un entorno BIM para el desarrollo de la construcciÃ³n virtual VC/BIM de proyectos de edificaciÃ³n a travÃ©s de las herramientas informÃ¡ticas de Autodesk y sus extensiones.', 'ATC BIM MÃ¡laga Training Centre Adapta MS', '210', 'Lunes y MiÃ©rcoles', '16:00 a 21:00', 'PRESENCIAL', '01/03/2017'),
(7, 1, 1, 3, 'BIM Revit BÃ¡sico', 'AECO.02', 550, 'SI', 150, 'FormaciÃ³n prÃ¡ctica en el dominio de las siguientes herramientas informÃ¡ticas BIM de Autodesk: Revit Architecture 2017. Mediante ejercicios estrictamente prÃ¡cticos se establecen los conceptos bÃ¡sicos del diseÃ±o paramÃ©trico y los entornos de trabajo BIM con Autodesk Revit.', 'ATC BIM MÃ¡laga Training Centre Adapta MS', '40', 'Martes y Jueves', '16:00 a 21:00', 'PRESENCIAL', ''),
(8, 1, 1, 4, 'CURSO BIM REVIT ARCHITECTURE', 'AECO.03', 950, 'NO', 150, 'FormaciÃ³n prÃ¡ctica en el dominio de las siguientes herramientas informÃ¡ticas BIM de Autodesk: Revit Architecture 2017. Al final del aprendizaje del empleo de esta herramienta se realiza un ejercicio prÃ¡ctico real donde el asistente puede elegir entre 2 tipos edificatorios facilitado por el centro con lo cual el asistente pone en prÃ¡ctica los conocimientos adquiridos.', 'ATC BIM MÃ¡laga Training Centre Adapta MS', '70', 'Lunes y MiÃ©rcoles', '16:00 a 21:00', 'PRESENCIAL', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programas_sesiones`
--

CREATE TABLE IF NOT EXISTS `programas_sesiones` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoPrograma` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `actividad` varchar(255) NOT NULL,
  `codigoDocente` int(255) DEFAULT NULL,
  `horas` varchar(255) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoDocente` (`codigoDocente`),
  KEY `codigoPrograma` (`codigoPrograma`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Volcado de datos para la tabla `programas_sesiones`
--

INSERT INTO `programas_sesiones` (`codigo`, `codigoPrograma`, `fecha`, `actividad`, `codigoDocente`, `horas`, `id`) VALUES
(17, 7, '2017-03-03', 'CV/BIM1 â€“ REVIT INICIACIÃ“N', 10, '20', 0),
(18, 7, '2017-03-03', 'CV/BIM2 â€“ REVIT ARQUITECTURA', 10, '20', 0),
(19, 8, '2017-03-03', 'CV/BIM1 â€“ REVIT INICIACIÃ“N', 10, '20', 0),
(20, 8, '2017-03-03', 'CV/BIM2 â€“ REVIT ARQUITECTURA', 10, '30', 0),
(21, 8, '2017-03-03', 'CV/BIM3 â€“ REVIT FAMILIAS', 10, '10', 0),
(22, 8, '2017-03-03', 'CV/BIM4 â€“ REVIT VISUALIZACIÃ“N', 10, '10', 0),
(36, 6, '2017-03-03', 'CV BIM', 10, '10', 0),
(37, 6, '2017-03-03', 'CV/BIM1 â€“ REVIT BÃSICO', 10, '20', 0),
(38, 6, '2017-03-03', 'CV/BIM2 â€“ REVIT ARQUITECTURA', 10, '30', 0),
(39, 6, '2017-03-03', 'CV/BIM3 â€“ REVIT FAMILIAS I', 10, '10', 0),
(40, 6, '2017-03-03', 'CV/BIM4 â€“ REVIT VISUALIZACIÃ“N', 10, '10', 0),
(41, 6, '2017-03-03', 'CV/BIM5 â€“ REVIT EN EQUIPO: SUBPROYECTOS Y PROYECTOS VINCULADOS', 10, '10', 0),
(42, 6, '2017-03-03', 'CV/BIM6 â€“ REVIT MEDICIONES: MEDIT', 10, '20', 0),
(43, 6, '2017-03-03', 'CV/BIM7 â€“ REVIT ESTRUCTURAS', 10, '20', 0),
(44, 6, '2017-03-03', 'CV/BIM8 â€“ REVIT INSTALACIONES + FAMILIAS II', 10, '30', 0),
(45, 6, '2017-03-03', 'CV/BIM9 â€“ DWG, IFC, DESIGN REVIEW, NAVISWORKS', 10, '25', 0),
(46, 6, '2017-03-03', 'CV/BIM10 â€“ BIM EN LA NUBE: AUTODESK 360', 10, '5', 0),
(47, 6, '2017-03-03', 'CV/BIM11 â€“ BIM DATA MANAGEMENT: VAULT', 10, '10', 0),
(48, 6, '2017-03-03', 'CV/BIM12 â€“ FLUJOS DE TRABAJO BIM', 10, '10', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puestosTrabajo`
--

CREATE TABLE IF NOT EXISTS `puestosTrabajo` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `funcion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `actividades` tinytext COLLATE latin1_spanish_ci NOT NULL,
  `cualificacion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `experiencia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fechaActualizacion` date NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `remesas`
--

CREATE TABLE IF NOT EXISTS `remesas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `tipoSepa` enum('CORE','B2B') COLLATE latin1_spanish_ci NOT NULL,
  `enviadaBanco` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `satisfaccion`
--

CREATE TABLE IF NOT EXISTS `satisfaccion` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `cliente` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `pregunta1` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta2` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta3` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta4` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta5` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta6` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta7` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `comentarios` varchar(500) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `software`
--

CREATE TABLE IF NOT EXISTS `software` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `fechaSolicitud` date NOT NULL,
  `fechaPrevista` date NOT NULL,
  `tipoTarea` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `textoOtros` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaFinalizacion` date NOT NULL,
  `tiempoEmpleado` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `realizado` enum('1','2','3') COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `comentariosProgramacion` text COLLATE latin1_spanish_ci NOT NULL,
  `creadoCliente` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `codigoPresupuesto` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoCliente` (`codigoCliente`,`codigoUsuario`),
  KEY `codigoPresupuesto` (`codigoPresupuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE IF NOT EXISTS `subcategorias` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `id` int(255) NOT NULL,
  `subcategoria` varchar(255) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `subcategorias`
--

INSERT INTO `subcategorias` (`codigo`, `id`, `subcategoria`) VALUES
(1, 1, 'Arquitectura, IngenierÃ­a y ConstrucciÃ³n'),
(2, 2, 'DiseÃ±o y Productos'),
(3, 3, 'Facility Managers'),
(4, 4, 'Project Managers'),
(5, 5, 'Infraestructuras'),
(6, 6, 'Certificaciones Autodesk'),
(7, 7, 'Certificacion BIM Manager ACP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE IF NOT EXISTS `tareas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `tarea` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFin` time NOT NULL,
  `todoDia` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `estado` enum('PENDIENTE','REALIZADA') COLLATE latin1_spanish_ci NOT NULL,
  `prioridad` enum('ALTA','NORMAL','BAJA') COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoCliente` (`codigoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareasSoftware`
--

CREATE TABLE IF NOT EXISTS `tareasSoftware` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `actividad` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaInicio` date NOT NULL,
  `horaInicio` time NOT NULL,
  `fechaPrevista` date NOT NULL,
  `horaPrevista` time NOT NULL,
  `fechaFin` date NOT NULL,
  `horaFin` time NOT NULL,
  `tiempo` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `codigoSoftware` int(255) NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoSofware` (`codigoSoftware`),
  KEY `codigoUsuario` (`codigoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadores_cliente`
--

CREATE TABLE IF NOT EXISTS `trabajadores_cliente` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `apellido1` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `apellido2` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `nif` varchar(12) COLLATE latin1_spanish_ci NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `telefono` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `movil` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `numCuenta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `bic` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `cp` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesTrabajador` text COLLATE latin1_spanish_ci NOT NULL,
  `medioConocimiento` enum('Google','Facebook','Twitter','Foro del Guardia Civil','Amigo','Antiguo alumno','Otro') COLLATE latin1_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `poblacion` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `provincia` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=84 ;

--
-- Volcado de datos para la tabla `trabajadores_cliente`
--

INSERT INTO `trabajadores_cliente` (`codigo`, `nombre`, `apellido1`, `apellido2`, `nif`, `fechaNacimiento`, `telefono`, `movil`, `email`, `activo`, `numCuenta`, `bic`, `cp`, `observacionesTrabajador`, `medioConocimiento`, `direccion`, `poblacion`, `provincia`) VALUES
(1, 'Carmen M.', 'Alvarez', '', '33390117M', '0000-00-00', '606637671', '', 'cmalvarez@tablinum.es', 'NO', '', 'BAEMESM1XXX', '29002', '', 'Otro', 'C/ Plaza de la Solidaridad 12 Edificio Indocar Planta 5Âª Ofic.538', 'MÃ¡laga', 'MÃ¡laga'),
(2, 'JosÃ© Carlos', 'GarcÃ­a', 'Alegre', '26224042V', '0000-00-00', '952004388', '626460182', 'jcgarcia@tablinum.es', 'SI', '', 'BAEMESM1XXX', '29002', '', 'Otro', 'Calle Cuarteles 7, planta 2, oficina 6', 'MÃ¡laga', '<Seleccionar>'),
(3, 'Serafin', 'FernÃ¡ndez Rebollo ', '', '', '0000-00-00', '952318250', '', 'antonio.navas@grupovera.es', 'SI', '', '', '', '', '', 'Calle Cerrajeros, nÃºm. 10, 29006, MÃ¡laga', '', 'MÃ¡laga'),
(4, 'Alberto', 'RÃƒÂ­os', 'Naranjo', '74.905.257-F', '0000-00-00', '952498875', '666442415', 'albertorios@bilba.es', 'SI', '', '', '29501', '', '', 'Calle Suspiros 69', 'ÃƒÂlora', 'MÃƒÂ¡laga'),
(5, 'ALBERTO ', 'ARIAS', 'GALINDO', '33374204-P', '0000-00-00', '', '646 108 837 ', 'aariasg@movistar.es', 'SI', '', '', '29730', '', '', 'Calle Clara Campoamor, nÃƒÂºm. 1, Portal 1, escalera 4, 4Ã‚Âº C', 'RincÃƒÂ³n de la Victoria', 'MÃƒÂ¡laga'),
(6, 'AMPARO  ', 'FERNÃƒÂNDEZ', 'FERNÃƒÂNDEZ', '71851057-T', '0000-00-00', '', '', '', 'SI', '', '', '29017', '', '', 'Avda. Juan SebastiÃƒÂ¡n Elcano, nÃƒÂºm. 20 - 3Ã‚Âº - 1', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(7, 'Ana', 'Cisneros', '', '', '0000-00-00', '', '654120486', 'ana.cisnerosruiz@gmail.com', 'SI', '', '', '', '', '', '', '', ''),
(8, 'Ana', 'MuÃƒÂ±oz', '', '', '0000-00-00', '952808718', '', 'administracion@caiconsultores.com', 'SI', '', '', '29681', '', '', 'Av. del Carmen, Edificio Puertosol, Of. 5 ', 'Estepona', 'MÃƒÂ¡laga'),
(9, 'ANA ISABEL ', 'GARCÃƒÂA ', 'REAL', '74885900Q', '0000-00-00', '', '617 344 865', 'anagr86@gmail.com', 'SI', '', '', '29010', '', '', 'Calle Fiscal Enrique BeltrÃƒÂ¡n, nÃƒÂºm. 3, 1D', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(10, 'Antonio', 'MartÃƒÂ­nez', 'GarcÃƒÂ­a', '', '0000-00-00', '', '661408353', 'antonio.martinez@sgs.com', 'SI', '', '', '', '', '', 'Calle Diderot, 28', '', ''),
(11, 'Antonio', 'Navas', '', '', '0000-00-00', '', '952318250', 'antonio.navas@grupovera.com', 'SI', '', '', '29006', '', '', 'C/ Cerrajeros 10', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(12, 'Antonio', 'Chaparro', '', '', '0000-00-00', '902400445', '', 'ACHAPARRO@ALTRACORPORACION.ES', 'SI', '', '', '29590', '', '', 'CALLE MARIE CURIE, 22', 'Campanillas', 'MÃƒÂ¡laga'),
(13, 'ANTONIO', 'ROZAS', 'SERRANO', '25690962P', '0000-00-00', '', '', '', 'SI', '', '', '29590', '', '', 'Calle Almojarifazgo, nÃƒÂºm. 4', 'Campanillas', 'MÃƒÂ¡laga'),
(14, 'Bernardo', 'Callejas', '', '', '0000-00-00', '', '952318250', 'bernardo.callejas@construccionesvera.es', 'SI', '', '', '29006', '', '', 'C/ Cerrajeros 10', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(15, 'Cecilio', 'LÃƒÂ³pez', 'Moreno', 'Y-25329331', '0000-00-00', '952700926', '627900318', 'lomo2260@coaat.es', 'SI', '', '', '29200', '', '', 'Calle Camberos, nÃƒÂºm.17-1Ã‚Âº', 'Antequera', 'MÃƒÂ¡laga'),
(16, 'CECILIO MANUEL ', 'FERNÃƒÂNDEZ ', 'PABÃƒÂ“N', '79014850Y', '0000-00-00', '', '608 599 250', ' fepa3581@coaat.es', 'SI', '', '', '29650', '', '', 'Avda. de MÃƒÂ©jico, nÃƒÂºm. 19', 'Mijas', 'MÃƒÂ¡laga'),
(17, 'CRISTIAN', 'IBORRA', 'VALADEZ', '09.056.585-J', '0000-00-00', '', '662 42 19 27', 'cristianibor@gmail.com', 'SI', '', '', '29680', '', '', 'Calle Arcos de PeÃƒÂ±as Blancas, nÃƒÂºm. 5, 5Ã‚Âº-D', 'Estepona', 'MÃƒÂ¡laga'),
(18, 'CRISTÃƒÂ“BAL ', 'GALEOTE', 'PADILLA', '74808942Q', '0000-00-00', '', '635 192 855', 'cristobalgaleote@yahoo.es', 'SI', '', '', '29017', '', '', 'Alonso Carrillo de Albornoz, nÃƒÂºm. 11, 5A', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(19, 'DAVID', 'TRUJILLO', 'TORRES', '44590515-R', '0000-00-00', '', '619 084 712', 'yoquese_8054@hotmail.com', 'SI', '', '', '29018', '', '', 'Calle Escritor Manuel Solano, nÃƒÂºm. 117', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(20, 'DAVID', 'LÃƒÂ“PEZ', 'MORENO', '08.920.369-A', '0000-00-00', '', '615 86 02 88', 'atd@coaat.es', 'SI', '', '', '29680', '', '', 'Avda. Puerta del Mar, nÃƒÂºm. , 3Ã‚Âº - 6', 'Estepona', 'MÃƒÂ¡laga'),
(21, 'DAVID HUGO ', 'GIL', 'MARTEL', ' 77473086-R', '0000-00-00', '', '657 064 917', 'martell.dh@gmail.com', 'SI', '', '', '29700', '', '', 'Calle RÃƒÂ­o Guadalete, nÃƒÂºm. 3 - 1Ã‚Âº D', 'VÃƒÂ©lez MÃƒÂ¡lga', 'MÃƒÂ¡laga'),
(22, 'Diego', 'PeÃƒÂ±a', 'Ortega', '', '0000-00-00', '952215053', '952215053', 'dpo@edipsa.es', 'SI', '', '', '29005', '', '', 'Puerta del Mar 20 1Ã‚Âº Edificio Edipsa', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(23, 'DIEGO', 'CASERO', 'RUBIO', '25313708 - T', '0000-00-00', '', '670 569 516', 'c1447cr@copitima.com', 'SI', '', '', '29200', '', '', 'C/Torre del Hacho, nÃƒÂºm. 14, PolÃƒÂ­gono Industrial', 'Antequera', 'MÃƒÂ¡laga'),
(24, 'DOMINGO', 'MÃƒÂ‰RIDA', 'SALCEDO', '33.375.440-W', '0000-00-00', '', '607 95 30 24', 'domingofotografo@gmail.com', 'SI', '', '', '29002', '', '', 'Calle Salitre nÃƒÂºm. 28, 6Ã‚Âº-C', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(25, 'Elena ', 'SÃƒÂ¡nchez', '', '', '0000-00-00', '', '952224225', 'asenjo@asenjo.net', 'SI', '', '', '29016', '', '', 'Plaza de la Malagueta, nÃ‚Âº 3', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(26, 'Elisa', 'Cepedano', '', '', '0000-00-00', '952770374', '', 'elicepedano@madarquitectura.com', 'SI', '', '', '29601', '', '', 'C/Maria Auxiliadora nÃ‚Âº2 ofi 35', 'Marbella', 'MÃƒÂ¡laga'),
(27, 'Emad', 'Fikry', '', '', '0000-00-00', '952227707', '635384229', 'emad.fikry@hcparquitectos.com', 'SI', '', '', '29018', '', '', 'Paseo MarÃƒÂ­timo Ciudad de Melilla. ', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(28, 'Emilio', 'LÃƒÂ³pez', 'CaparrÃƒÂ³s', '', '0000-00-00', '952245151', '952245151', 'emiliolopez@ielco.es', 'SI', '', '', '29005', '', '', 'Carretera Azucarera Ã¢Â€Â“ Intelhorce nÃƒÂºm. 4', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(29, 'EUGENIO ', 'CANO', 'MACÃƒÂAS', ' 25669100-L', '0000-00-00', '', '650 910 133', 'eugeniocano3@gmail.com', 'SI', '', '', '29640', '', '', 'Calle Lanzarote, nÃƒÂºm. 6, 4Ã‚Âº D', 'Fuengirola', 'MÃƒÂ¡laga'),
(30, 'FERNANDO', 'Osorio', '', '', '0000-00-00', '639851848', '', 'ferosogar@terra.com', 'SI', '', '', '', '', '', '', '', ''),
(31, 'FERNANDO ', 'BALLESTEROS', 'BLANCA', '33361826G', '0000-00-00', '', '619 074 251', 'f.ballesteros@movistar.es', 'SI', '', '', '29620', '', '', 'Calle Sardinero, 16-7G ', 'Torremolinos', 'MÃƒÂ¡laga'),
(32, 'FERNANDO ', 'SOLA', 'ALAMEDA', '44579439 -B', '0000-00-00', '', '626 876 582', ' fernandosola@gmail.com', 'SI', '', '', '29018', '', '', 'C/ Reino de LeÃƒÂ³n, nÃƒÂºm. 23', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(33, 'FRANCISCO CLAUDIO', 'RANDO', 'RANDO', '44.589.003-F', '0000-00-00', '', '687 968 009', 'franrando81@hotmail.com', 'SI', '', '', '29738', '', '', 'BÃ‚Âº Los FernÃƒÂ¡ndez, nÃƒÂºm. 66, bloque 1-2Ã‚ÂºA', 'RincÃƒÂ³n de la Victoria', 'MÃƒÂ¡laga'),
(34, 'FRANCISCO JAVIER ', 'ALES', 'SOTO', '33369855-Y', '0000-00-00', '', '619 084 712', 'gerencia@alesotoarquitectos.com', 'SI', '', '', '29140', '', '', 'Calle Maestro TomÃƒÂ¡s BretÃƒÂ³n, nÃƒÂºm. 8, Local 6.', 'Churriana', 'MÃƒÂ¡laga'),
(35, 'FRANCISCO JAVIER ', 'GUTIÃƒÂ‰RREZ', 'CRUZ', '75017617-N', '0000-00-00', '', '647 929 121', 'gucr2776@gmail.com', 'SI', '', '', '29004', '', '', 'C/PacÃƒÂ­fico, nÃƒÂºm. 19, P 3-5F', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(36, 'FRANCISCO JOSÃƒÂ‰', 'DÃƒÂAZ', 'MONTILLA', '52.585.755-G', '0000-00-00', '', '685 095 922', 'dimo2219@coaat.es', 'SI', '', '', '29009', '', '', 'Plaza Castillejos, nÃƒÂºm. 4, 4Ã‚Âº-A', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(37, 'GERMÃƒÂN', 'GONZALEZ', 'BORGES', ' 26807991-L', '0000-00-00', '', '677 717 865', 'germangonbor@gmail.com', 'SI', '', '', '29014', '', '', 'Camino de la Adelfilla, nÃƒÂºm. 7A', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(38, 'ISABEL', 'DE LOS RÃƒÂOS', 'BUENO', '74919592-J', '0000-00-00', '', '652 134 700', 'isabeldelosrios9@gmail.com', 'SI', '', '', '29170', '', '', 'AxarquÃƒÂ­a, nÃƒÂºm. 9', 'Colmenar', 'MÃƒÂ¡laga'),
(39, 'JACOB', 'BADILLO', 'RUIZ', '77186450Z', '0000-00-00', '', '625358900', 'jacobbadillo41@gmail.com', 'SI', '', '', '29010', '', '', 'Calle Magistrado Salvador BarberÃƒÂ¡, nÃƒÂºm. 3, 5-B', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(40, 'Javier', 'FernÃƒÂ¡ndez', '', '', '0000-00-00', '952342406', '664424274', 'jfernandez@conformas.es', 'SI', '', '', '29003', '', '', 'Calle de Ayala, 4', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(41, 'JAVIER ', 'MUÃƒÂ‘OZ', 'GUILLÃƒÂ‰N', '18436747Q', '0000-00-00', '', '687 64 65 84', ' javiermg2014@gmail.com', 'SI', '', '', '29009', '', '', 'C/AndalucÃƒÂ­a, 9 2Ã‚ÂºB', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(42, 'JOAQUÃƒÂN', 'ANDRADE', 'CASQUERO', '33356844 - J', '0000-00-00', '', '687 869 581', 'joaquin.andrade.c@gmail.com', 'SI', '', '', '29004', '', '', 'Avda. Imperio Argentina, nÃƒÂºm. 5, Portal 8, Planta 3, Puerta C', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(43, 'Jordi ', 'Arroyo', 'Florenza', '', '0000-00-00', '952220721', '665569432', 'jordi.royo@guamar.es', 'SI', '', '', '29017', '', '', 'Calle Puerto, 14 - 3Ã‚Âª', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(44, 'Jorge', 'Borrego', 'ÃƒÂlvarez', 'E-53682758', '0000-00-00', '952498875', '660399362', 'jorgeborrego@bilba.es', 'SI', '', '', '29501', '', '', 'Calle Suspiros 69', 'ÃƒÂlora', 'MÃƒÂ¡laga'),
(45, 'JosÃƒÂ©', 'RamÃƒÂ³n', '', '', '0000-00-00', '952343431', '666452249', 'joseramon@parquemalaga.com', 'SI', '', '', '29005', '', '', 'Alameda Principal, 16, 4Ã‚Âª planta, oficina 1', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(46, 'Jose Antonio', 'Rivera', 'Pedraza', '', '0000-00-00', '952104400', '659207637', 'jrivera@rivervial.com', 'SI', '', '', '29007', '', '', 'C/Armengual de la Mota 21, 1Ã‚ÂºD-E', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(47, 'JOSE ANTONIO', 'BAUTISTA', 'FERNÃƒÂNDEZ', '25664235 - F', '0000-00-00', '952 225 152', '677 412 157', 'estudio@jbarquitectura.com', 'SI', '', '', '29001', '', '', 'C/ PinzÃƒÂ³n, nÃƒÂºm. 8, Planta 4, Puerta 7', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(48, 'JosÃƒÂ© Antonio', 'Jaimez', 'MuÃƒÂ±oz', '', '0000-00-00', '952250308', '677441716', 'jajaimez@oluz.es', 'SI', '', '', '29004', '', '', 'Calle PacÃƒÂ­fico, nÃ‚Âº 72', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(49, 'JOSE ANTONIO ', 'ROMERO', 'HENESTROSA', '25319011-J', '0000-00-00', '', '650 910 133', 'rohe2402@hotmail.com', 'SI', '', '', '29560', '', '', 'Calle Suecia, nÃƒÂºm. 22', 'Pizarra', 'MÃƒÂ¡laga'),
(50, 'JOSE CARLOS ', 'MELERO ', 'MASEGOSA', '44.298.467-F', '0000-00-00', '', '637 891 801', 'cmelero@urbitech.com', 'SI', '', '', '29793', '', '', 'URB. LAGUNA BEACH, BL. COMERCIAL B, LOCAL 14', 'Torrox', 'MÃƒÂ¡laga'),
(51, 'JosÃƒÂ© Manuel', 'Rodriguez', '', '', '0000-00-00', '', '952040630', 'jmrodriguez@cotracom.com', 'SI', '', '', '29004', '', '', 'Calle JaÃƒÂ©n, 9', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(52, 'Juan', 'Ramirez', 'Mateos', '', '0000-00-00', '952342406', '600550035', 'direccion@conformas.es', 'SI', '', '', '29002', '', '', 'Calle de Ayala, 3', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(53, 'Juan Antonio', 'Baez', 'DurÃƒÂ¡n', '', '0000-00-00', '630087010', '952045252', 'jabaez@sando.com', 'SI', '', '', '29006', '', '', 'Avda. Ortega y Gasset, 112', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(54, 'Juan Ignacio', 'Soriano', 'Bueno', '', '0000-00-00', '952214991', '654549027', 'juan@arquitecturadeguardia.com', 'SI', '', '', '29012', '', '', 'Plaza Santa MarÃƒÂ­a, 0 Bloque 10', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(55, 'JUAN JOSÃƒÂ‰', 'ORTEGA', 'DURÃƒÂN', ' 25705753X', '0000-00-00', '', '609 566 927', 'jjotinsa@gmail.com', 'SI', '', '', '29016', '', '', 'Calle Sierra del CÃƒÂ³, nÃƒÂºm. 18, BLOQUE A-2 - 2Ã‚Âº F', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(56, 'JUAN MANUEL', 'DE LA ', 'TORRE', ' 25669752-G', '0000-00-00', '', '629 660 136', 'jmdelatorre@movistar.es', 'SI', '', '', '29790', '', '', 'Calle Ubio, nÃƒÂºm. 4, casa 53', 'Chilches', 'MÃƒÂ¡laga'),
(57, 'JUAN MANUEL', 'SAURA', 'CHENU', 'V-25048627', '0000-00-00', '', '', '', 'SI', '', '', '29007', '', '', 'C/Don Cristian nÃ‚Âº2, oficina 1Ã¢Â€Â32', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(58, 'JUAN MANUEL ', 'DE LA ', 'TORRE', '25669752-G', '0000-00-00', '', '629 660 136', ' jmdelatorre@movistar.es', 'SI', '', '', '29790', '', '', 'Calle Ubio, nÃƒÂºm. 4, casa 53', 'Chilches', 'MÃƒÂ¡laga'),
(59, 'JUAN MIGUEL ', 'GONZALEZ', 'ALVAREZ', '25.677.434-G', '0000-00-00', '', '678 470 035', 'gajuanmiguel@gmail.com', 'SI', '', '', '29738', '', '', 'Calle Eugenio Onegin, nÃƒÂºm. 12, 4Ã‚Âº-D', 'RincÃƒÂ³n de la Victoria', 'MÃƒÂ¡laga'),
(60, 'Julio ', 'Cardenete', 'Pascual', '', '0000-00-00', '952214991', '654549033', 'julio@arquitecturadeguardia.com', 'SI', '', '', '29013', '', '', 'Plaza Santa MarÃƒÂ­a, 0 Bloque 11', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(61, 'JUSTO', 'RUIZ', 'GARCÃƒÂA', '74893825-Y', '0000-00-00', '', '680937371', 'justoruizg@hotmail.com', 'SI', '', '', '29560', '', '', 'C/ Vicente Aleixandre, nÃƒÂºm. 32', 'Pizarra', 'MÃƒÂ¡laga'),
(62, 'JUSTO FERMÃƒÂN ', 'GÃƒÂ“MEZ ', 'RUIZ', '74539425-D', '0000-00-00', '', '625 717 450', 'fermin.gomez.at@gmail.com', 'SI', '', '', '18300', '', '', 'Calle Real, nÃƒÂºm. 7, 6Ã‚Âº-J', 'Loja', 'Granada'),
(63, 'LUCAS', 'BIONDI', '', ' X1114448y', '0000-00-00', '', ' 637 592 415', 'liruk01@gmail.com', 'SI', '', '', '29620', '', '', 'Calle los Tres Caballos nÃ‚Âº22', 'Torremolinos', 'MÃƒÂ¡laga'),
(64, 'MÃ‚Âª DE LOS ANGELES ', 'SANCHEZ', 'DE LA TORRE', 'H-24297173L', '0000-00-00', '', '636 458 328', ' angelessanchez4@hotmail.com', 'SI', '', '', '29016', '', '', 'Calle Monte Miramar, nÃƒÂºm. 27', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(65, 'MANUEL', 'MARTÃƒÂNEZ ', ' JIMENEZ', 'H-25305331', '0000-00-00', '', '636 458 327', ' maji1523@coaat.es', 'SI', '', '', '29016', '', '', 'Calle Monte Miramar, nÃƒÂºm. 27', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(66, 'MANUEL', 'LÃƒÂ“PEZ', 'TUR', '74.828.401 -', '0000-00-00', '', '661 790 646 ', 'mlopeztur@gmail.com', 'SI', '', '', '29013', '', '', 'C/Fernando El CatÃƒÂ³lico, nÃƒÂºm. 31, ÃƒÂ¡tico I', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(67, 'MANUEL ', 'MARTÃƒÂNEZ ', 'JIMENEZ', 'H-25305331', '0000-00-00', '', '636 458 327', 'maji1523@coaat.es', 'SI', '', '', '29016', '', '', 'Calle Monte Miramar, nÃƒÂºm. 27', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(68, 'MARGARITA', 'JIMENEZ ', 'MORENO', '44575282 - V', '0000-00-00', '', '666 440 095', 'margarita@coaat.es', 'SI', '', '', '29016', '', '', 'C/RocÃƒÂ­o, nÃƒÂºm. 39', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(69, 'MarÃƒÂ­a Carmen', 'GonzÃƒÂ¡lez ', 'Muriano', '', '0000-00-00', '902400445', '684008179', 'MCGONZALEZ@ALTRACORPORACION.ES', 'SI', '', '', '29590', '', '', 'CALLE MARIE CURIE, 21', 'Campanillas', 'MÃƒÂ¡laga'),
(70, 'MARIA ISABEL', 'BERMEJO', 'BERENGUER', '74831755 - J', '0000-00-00', '', '607 112 843', 'isaberbe@gmail.com', 'SI', '', '', '29009', '', '', 'C/ Juan de Herrera, nÃƒÂºm. 10-12', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(71, 'MARÃƒÂA ROSARIO ', 'CANTUDO', 'NARVÃƒÂEZ', '77455141L', '0000-00-00', '', '', '', 'SI', '', '', '29660', '', '', 'Urb. Guadalmina Alta, Unidad 18, Cjto. Villas y Golf, Casa 33 A-3', 'San Pedro de AlcÃƒÂ¡ntara', 'MÃƒÂ¡laga'),
(72, 'MARIANO', 'LÃƒÂ“PEZ', 'ORDOÃƒÂ‘EZ', '44576151N', '0000-00-00', '', '619 790 022', 'marianomanilva@hotmail.com', 'SI', '', '', '29017', '', '', 'Avda. Juan SebastiÃƒÂ¡n Elcano, nÃƒÂºm. 88 - 1Ã‚Âº D', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(73, 'Mario', 'Romero', '', '', '0000-00-00', '952227707', '', 'mario.romero@hcparquitectos.com', 'SI', '', '', '29017', '', '', 'Paseo MarÃƒÂ­timo Ciudad de Melilla. ', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(74, 'Miguel ', 'Rodriguez', 'Porras', '', '0000-00-00', '952323200', '', 'miguelrp@myramar.com', 'SI', '', '', '29002', '', '', 'Avda. Andalucia, 21. Edif. Jardines Picasso Planta de Oficinas.', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(75, 'MIGUEL ANGEL ', 'LUNA', 'GUERRERO', '44576472-B', '0000-00-00', '', '652 94 87 73', 'c2961lg@copitima.com', 'SI', '', '', '29649', '', '', 'C/EquitaciÃƒÂ³n, nÃƒÂºm. 3, casa 13', 'Mijas Costa', 'MÃƒÂ¡laga'),
(76, 'PABLO', 'VAZQUEZ', 'PUERTAS', '74880511D', '0000-00-00', '', '637 881 754', 'pablovaz84@gmail.com', 'SI', '', '', '29190', '', '', 'Calle Musgo, nÃƒÂºm. 8', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(77, 'Roberto', 'Barrios', '', '', '0000-00-00', '952770375', '', 'elicepedano@madarquitectura.com', 'SI', '', '', '29602', '', '', 'C/Maria Auxiliadora nÃ‚Âº2 ofi 35', 'Marbella', 'MÃƒÂ¡laga'),
(78, 'Santiago ', 'Rayo GarcÃƒÂ­a', '', '', '0000-00-00', '606781491', '', ' srayogarcia@gmail.com', 'SI', '', '', '', '', '', '', '', 'Extramadura'),
(79, 'SERGIO', 'CUBO', 'CLEMENTE', '33.367.232-M', '0000-00-00', '', '638 052 471', 'cucl3178@coaat.es', 'SI', '', '', '29002', '', '', 'Pasaje La Peira, nÃƒÂºm. 1, 4Ã‚Âº-2', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(80, 'SILVIA', 'GARCÃƒÂA ', 'FERNÃƒÂNDEZ', '74.694.094-F', '0000-00-00', '', '679 063 422', 'adm.tecnica12@gmail.com', 'SI', '', '', '29009', '', '', 'Calle Luchana, nÃƒÂºm. 29, 9Ã‚Âº-1', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(81, 'SIRO', 'ALTAMIRANO', 'MACARRÃƒÂ“N', '24654376 - D', '0000-00-00', '', '629 584 289', 'alma0307@coaat.es', 'SI', '', '', '29190', '', '', 'C/ Sumatra, nÃƒÂºm. 51', 'MÃƒÂ¡laga', 'MÃƒÂ¡laga'),
(82, 'Victor', 'Roda', '', '', '0000-00-00', '676649943', '', 'victormroda@hotmail.com', 'SI', '', '', '', '', '', '', '', 'MÃƒÂ¡laga'),
(83, 'JosÃ© Luis', 'DÃ¼rsteler', 'Esteban', '36965247S', '0000-00-00', '622255008', '622255008', 'jldursteler@qmaconsultores.com', 'SI', '', 'BAEMESM1XXX', '29011', '', 'Otro', 'Pasaje Saturno, 2', 'MÃ¡laga', 'MÃ¡laga');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `dni` varchar(12) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `colorTareas` varchar(7) COLLATE latin1_spanish_ci NOT NULL,
  `usuario` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `clave` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `tipo` enum('ADMIN','ADMINISTRACION1','ADMINISTRACION2','CONTABILIDAD','TUTOR','DIRECTOR','DELEGADO','COMERCIAL','TELEMARKETING','DOCENTE') COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `sesion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `usuario` (`usuario`),
  KEY `directorAsociado` (`codigoUsuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`codigo`, `nombre`, `apellidos`, `dni`, `email`, `telefono`, `colorTareas`, `usuario`, `clave`, `activo`, `tipo`, `codigoUsuario`, `sesion`) VALUES
(8, 'Invitado', 'QMA', '', '', '', '', 'invitado', 'demo', 'SI', 'ADMIN', NULL, '53c960eb66135b9f13956c7fce50cf26'),
(9, '', '', '', '', '', '', 'soporte', 'soporte15', 'SI', 'ADMIN', NULL, ''),
(10, 'JosÃ© MarÃ­a ', 'GutiÃ©rrez', '33390117M', 'info@adaptams.com', '637474372', '', 'adapta', 'invitado', 'SI', 'DOCENTE', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores_mapa`
--

CREATE TABLE IF NOT EXISTS `valores_mapa` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `codigoMapa` int(255) NOT NULL,
  `valor` int(255) NOT NULL,
  `valorN` int(255) NOT NULL,
  `valorD` int(255) NOT NULL,
  `checkValor` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoMapa` (`codigoMapa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vencimientos_facturas`
--

CREATE TABLE IF NOT EXISTS `vencimientos_facturas` (
  `codigo` int(255) NOT NULL AUTO_INCREMENT,
  `importe` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `cobrado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `codigoFactura` int(255) NOT NULL,
  `codigoGasto` int(255) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codigoFactura` (`codigoFactura`),
  KEY `codigoGasto` (`codigoGasto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `vencimientos_facturas`
--

INSERT INTO `vencimientos_facturas` (`codigo`, `importe`, `fecha`, `cobrado`, `codigoFactura`, `codigoGasto`) VALUES
(1, '666.66666666667', '2017-03-02', 'NO', 2, NULL),
(2, '666.66666666667', '2017-04-02', 'NO', 2, NULL),
(3, '666.66666666667', '2017-05-02', 'NO', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vencimientos_remesas`
--

CREATE TABLE IF NOT EXISTS `vencimientos_remesas` (
  `codigoVencimiento` int(255) NOT NULL,
  `codigoRemesa` int(255) NOT NULL,
  KEY `codigoFactura` (`codigoVencimiento`,`codigoRemesa`),
  KEY `codigoRemesa` (`codigoRemesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clientestareas`
--
ALTER TABLE `clientestareas`
  ADD CONSTRAINT `clientesTareas_ibfk_1` FOREIGN KEY (`codigoTarea`) REFERENCES `tareas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `contratos`
--
ALTER TABLE `contratos`
  ADD CONSTRAINT `contratos_programas_ibfk` FOREIGN KEY (`codigoPrograma`) REFERENCES `programas` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `contactos_clientes_ibfk` FOREIGN KEY (`codigoAlumno`) REFERENCES `trabajadores_cliente` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `contratos_facturas`
--
ALTER TABLE `contratos_facturas`
  ADD CONSTRAINT `contratos_facturas_ibfk` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contratos_facturas_ibfk_1` FOREIGN KEY (`facturado`) REFERENCES `facturacion` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `contratos_gastos`
--
ALTER TABLE `contratos_gastos`
  ADD CONSTRAINT `contratos_gastos_ibfk` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contratos_gastos_ibfk2` FOREIGN KEY (`codigoGasto`) REFERENCES `otros_gastos` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `conversaciones`
--
ALTER TABLE `conversaciones`
  ADD CONSTRAINT `res_conversaciones_usuarios` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `correos`
--
ALTER TABLE `correos`
  ADD CONSTRAINT `correos_ibfk_1` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `destinatarios_conversacion`
--
ALTER TABLE `destinatarios_conversacion`
  ADD CONSTRAINT `destinatarios_conversacion_ibfk_2` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `destinatarios_conversacion_ibfk_3` FOREIGN KEY (`codigoConversacion`) REFERENCES `conversaciones` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `destinatarios_mensajes`
--
ALTER TABLE `destinatarios_mensajes`
  ADD CONSTRAINT `destinatarios_mensajes_ibfk_1` FOREIGN KEY (`codigoMensaje`) REFERENCES `mensajes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `destinatarios_mensajes_ibfk_2` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `facturacion`
--
ALTER TABLE `facturacion`
  ADD CONSTRAINT `facturas_contratos_ibfk` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `historico_clientes`
--
ALTER TABLE `historico_clientes`
  ADD CONSTRAINT `historico_clientes_ibfk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `historico_contactos`
--
ALTER TABLE `historico_contactos`
  ADD CONSTRAINT `historico_contactos_ibfk` FOREIGN KEY (`codigoContacto`) REFERENCES `contactos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mapaProcesos`
--
ALTER TABLE `mapaProcesos`
  ADD CONSTRAINT `mapa_procesos_ibfk_1` FOREIGN KEY (`indicador`) REFERENCES `indicadores` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD CONSTRAINT `mensajes_ibfk_1` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `mensajes_ibfk_2` FOREIGN KEY (`codigoConversacion`) REFERENCES `conversaciones` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `metas`
--
ALTER TABLE `metas`
  ADD CONSTRAINT `metas_ibfk_1` FOREIGN KEY (`codigoObjetivo`) REFERENCES `objetivos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `presupuestos`
--
ALTER TABLE `presupuestos`
  ADD CONSTRAINT `presupuestos_ibfk_4` FOREIGN KEY (`empresa`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `presupuestos_ibfk_5` FOREIGN KEY (`comercial`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `presupuestos_facturas`
--
ALTER TABLE `presupuestos_facturas`
  ADD CONSTRAINT `presupuestos_facturas_ibfk` FOREIGN KEY (`codigoPresupuesto`) REFERENCES `presupuestos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `presupuestos_facturas_ibfk_1` FOREIGN KEY (`facturado`) REFERENCES `facturacion` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `productos_presupuestos`
--
ALTER TABLE `productos_presupuestos`
  ADD CONSTRAINT `productos_presupuestos_ibfk_1` FOREIGN KEY (`codigoPresupuesto`) REFERENCES `presupuestos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `programas`
--
ALTER TABLE `programas`
  ADD CONSTRAINT `programas_ibfk_1` FOREIGN KEY (`codigoCategoria`) REFERENCES `categorias` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `programas_ibfk_2` FOREIGN KEY (`codigoSubcategoria`) REFERENCES `subcategorias` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `programas_ibfk_3` FOREIGN KEY (`codigoCurso`) REFERENCES `cursos` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `programas_sesiones`
--
ALTER TABLE `programas_sesiones`
  ADD CONSTRAINT `sesiones_docentes_ibfk` FOREIGN KEY (`codigoDocente`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `sesiones_programas_ibfk` FOREIGN KEY (`codigoPrograma`) REFERENCES `programas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `valores_mapa`
--
ALTER TABLE `valores_mapa`
  ADD CONSTRAINT `valores_mapa_ibfk` FOREIGN KEY (`codigoMapa`) REFERENCES `mapaProcesos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vencimientos_facturas`
--
ALTER TABLE `vencimientos_facturas`
  ADD CONSTRAINT `vencimientos_facturas_ibfk_1` FOREIGN KEY (`codigoFactura`) REFERENCES `facturacion` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vencimientos_facturas_ibfk_2` FOREIGN KEY (`codigoGasto`) REFERENCES `otros_gastos` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `vencimientos_remesas`
--
ALTER TABLE `vencimientos_remesas`
  ADD CONSTRAINT `vencimientos_remesas_ibfk_2` FOREIGN KEY (`codigoRemesa`) REFERENCES `remesas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vencimientos_remesas_ibfk_3` FOREIGN KEY (`codigoVencimiento`) REFERENCES `vencimientos_facturas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
