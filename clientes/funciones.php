<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de clientes

function operacionesClientes(){
	$res=true;

	if(isset($_GET['posibleCliente'])){
		$res=pasarCliente($_GET['posibleCliente']);
	}
	if(isset($_POST['codigo'])){
		$res=actualizaCliente();
	}
	elseif(isset($_POST['nombre'])){
		$res=creaCliente();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('clientes');
	}

	mensajeResultado('nombre',$res,'Cliente');
    mensajeResultado('elimina',$res,'Cliente', true);
}

function pasarCliente($codigo){
	$res=consultaBD('UPDATE clientes SET activo="SI" WHERE codigo='.$codigo,true);
	return $res;
}

function creaCliente(){
	$res=insertaDatos('clientes');

	return $res;
}

function actualizaCliente(){
	$datos=arrayFormulario();
	$res=actualizaDatos('clientes');

	return $res;
}

function eliminaDatosCliente($tabla,$codigoCliente,$actualizacion){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM $tabla WHERE codigoCliente=$codigoCliente");
	}

	return $res;
}

function listadoClientes(){
	global $_CONFIG;

	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');

	//fechaAlta lo utilizo para el filtro por ejercicios
	$columnas=array('nombre','denominacionSocial','nif','telefono1','email1','programa','nombre','nombre');
	$where=obtieneWhereListado("WHERE 1=1",$columnas,true,true);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, denominacionSocial, nif, telefono1, email1, programa FROM clientes $where AND activo='SI' $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, nombre, denominacionSocial, nif, telefono1, email1, programa FROM clientes $where AND activo='SI';");

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){

		$fila=array(
			"<a href='gestion.php?codigo=".$datos['codigo']."'>".$datos['nombre']."</a>",
			$datos['denominacionSocial'],
			$datos['nif'],
			"<a href='tel:".$datos['telefono1']."' class='nowrap'>".formateaTelefono($datos['telefono1'])."</a>",
			"<a href='mailto:".$datos['email1']."'>".$datos['email1']."</a>",
			$datos['programa'],
			botonAcciones(array('Detalles'),array('clientes/gestion.php?codigo='.$datos['codigo']),array("icon-search-plus"),array(0,0,0),false,''),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}
	cierraBD();

	echo json_encode($res);
}

function gestionCliente(){
	operacionesClientes();

	abreVentanaGestion('Gestión de Clientes','index.php');
	$datos=compruebaDatos('clientes');

	compruebaMensajeConfirmarCliente();
	
	echo "<h4 class='apartadoFormulario sinFlotar'>DATOS DEL CLIENTE</h4>";
	abreColumnaCampos();
		campoOculto($datos,'activo','SI');
		campoTexto('nombre','Nombre',$datos);
		campoTexto('nif','NIF',$datos,'input-small');
		campoTexto('numeroEmpleados','Número de empleados',$datos);
		campoTexto('importe','Importe oportunidades ganadas',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('denominacionSocial','Denominación social',$datos);
		campoTexto('sector','Sector',$datos);
		campoTexto('importancia','Importancia',$datos);
		campoTexto('programa','Programa',$datos);
	cierraColumnaCampos();

	echo "<h4 class='apartadoFormulario sinFlotar'>DATOS DE CONTACTO</h4>";
	abreColumnaCampos();
		campoTexto('personaContacto','Contacto',$datos);
		campoTextoSimbolo('email1','Email 1','<i class="icon-envelope"></i>',$datos,'input-large');
		campoTextoSimbolo('telefono1','Teléfono 1','<i class="icon-phone"></i>',$datos,'input-large');
		campoTexto('direccion1','Dirección 1',$datos);
		campoTexto('website1','Website 1',$datos);
		campoTexto('fax','Fax',$datos);
		campoTextoSimbolo('linkedin','URL LinkedIn','<i class="icon-linkedin"></i>',$datos,'input-large');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTextoSimbolo('email2','Email 2','<i class="icon-envelope"></i>',$datos,'input-large');
		campoTextoSimbolo('telefono2','Teléfono 2','<i class="icon-phone"></i>',$datos,'input-large');
		campoTexto('direccion2','Dirección 2',$datos);
		campoTexto('website2','Website 2',$datos);
		campoTextoSimbolo('facebook','URL Facebook','<i class="icon-facebook"></i>',$datos,'input-large');
		campoTextoSimbolo('twitter','URL Twitter','<i class="icon-twitter"></i>',$datos,'input-large');
	cierraColumnaCampos();
	
	echo "<h4 class='apartadoFormulario sinFlotar'>OTROS</h4>";
	areaTexto('descripcion','Descripción',$datos,'areaInforme');
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');
	
	cierraVentanaGestion('index.php',true);
}

function compruebaMensajeConfirmarCliente(){
	if(isset($_GET['confirmar'])){
		mensajeAdvertencia("complete los campos obligatorios para confirmar el cliente.");
	}
}


function filtroClientes(){
	abreCajaBusqueda();
	abreColumnaCampos();

	$columnas=array('nombre','denominacionSocial','nif','telefono1','email1','programa');

	campoTexto(0,'Nombre');
	campoTexto(1,'Denominación social');
	campoTexto(2,'NIF','','input-small');
	

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(3,'eMail');
	campoTexto(4,'Teléfono',false,'input-small pagination-right');
	campoTexto(5,'Programa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}



function obtieneNombreCliente($codigoCliente){
	$datos=consultaBD("SELECT razonSocial FROM clientes WHERE codigo=$codigoCliente",true,true);

	return "<a href='gestion.php?codigo=$codigoCliente'>".$datos['razonSocial']."</a>";
}


function creaRegistroExtra($query){
	$res='fallo';

	conexionBD();
	
	$consulta=consultaBD($query);

	if($consulta){
		$res=mysql_insert_id();
	}

	cierraBD();

	echo $res;
}

function campoCuentaBancaria($valor){
	$valor=compruebaValorCampo($valor,'numCuenta');
	echo '<div class="control-group">                     
	  <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
	  <div class="controls numSS">
		<div class="input-prepend input-append">
		  <input type="text" class="input-mini" id="iban" name="iban" maxlength="4" size="4" value="'.substr($valor, 0, 4).'">
		  <span class="add-on">/</span>
		  <input type="text" class="input-medium" id="numCuenta" name="numCuenta" maxlength="20" size="20" value="'.substr($valor, 4, 24).'">
		</div>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->';
}

//Fin parte de clientes