$(document).ready(function(){
  $('select').selectpicker();//La inicialización del selectpicker debe ir siempre antes

  $('select.selectAjax').each(function(){
    consultaSelectAjax($(this));

    oyenteBotonAjax($(this));
    $(this).change(function(){
      oyenteBotonAjax($(this));
    });
  });
});

function consultaSelectAjax(select){
  select.next().find('.input-block-level').keyup(function(e){
    var consulta=select.attr('consulta');//Obtengo la consulta dentro del oyente keyup para los casos en los que la modifico vía JS (como en el caso de los alumnos en /inicios-grupos)

    if(e.which!=13 && e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40){//Se comprueba que las teclas pulsadas no son intro ni los cursores
      var busqueda=$(this).val();
	  
      if(parseInt(busqueda.length)>=3){//Han requerido que la búsqueda comience en 3 dígitos
        var datos=new FormData();
        datos.append('busqueda',busqueda);
        datos.append('consulta',consulta);

        $.ajax({
          url: "../listadoAjax.php?funcion=cargaBusquedaSelectAjax();",
          type: "POST",
          data: datos,
          processData: false,
          contentType: false,
          success: function (options) {
            select.html(options);
            select.selectpicker('refresh');
          }
        });
      }
    }
  });
}

function oyenteBotonAjax(campo){
  var id=campo.attr('id');
  var codigo=campo.val();
  
  if(codigo!='NULL'){
    var enlace=$('#boton-'+id).attr('enlace');
    $('#boton-'+id).attr('href',enlace+codigo);
  }
  else{
    $('#boton-'+id).attr('href','#'); 
  }
}