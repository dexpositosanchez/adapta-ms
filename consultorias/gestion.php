<?php
  $seccionActiva=12;
  include_once("../cabecera.php");
  $datos=gestionProyectos();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectpicker').selectpicker();
	$('select[name=tipoTarea]').change(function(){
      if($(this).val()=='OTROS'){
        $('#cajaOtros').css('display','block');
      }
      else{
        $('#cajaOtros').css('display','none');
      }
    });

    <?php
      if($datos['tipoTarea']!='OTROS'){
        echo "$('#cajaOtros').css('display','none');";
      }

      if($datos['realizado']=='3'){
        echo "$('#cajaRealizado').css('display','block');";
      }
      else{
        echo "$('#cajaRealizado').css('display','none');";
      }
    ?>

    $('select[name=realizado]').change(function(){
      if($(this).val()=='3'){
        $('#cajaRealizado').css('display','block');
      }
      else{
        $('#cajaRealizado').css('display','none');
      }
    });
	<?php if(isset($datos['codigo'])){ ?>
		var codigoCliente=$('#codigoCliente').val();
		var codigo=$('#codigoPresupuestoSeleccionado').val();
		cargaPresupuestos(codigoCliente,codigo);
	<?php }else{ ?>
		var codigoCliente=$('#codigoCliente').val();
		cargaPresupuestos(codigoCliente);
	<?php } ?>
	$('#codigoCliente').change(function(){
		var codigoCliente=$('#codigoCliente').val();
		cargaPresupuestos(codigoCliente);
	});	
  });
  
  function cargaPresupuestos(codigoCliente, codigo){
	$.ajax({
         type: "POST",
         url: "listadoPresupuestos.php",
         data: "codigoCliente=" + codigoCliente + "&codigo=" +codigo,
         success: function(response){
               $("#resultadoDos").html(response);
         }
    });
  }
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>