<?php
  $seccionActiva=12;
  include_once('../cabecera.php');
  
  $estadisticas=estadisticasTiempos();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas sobre Proyectos</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-clock-o"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Actividades realizadas</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Actividades</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="index.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Activiades para cada Proyecto registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th class='nowrap'> Cliente </th>
					<th class='nowrap'> Presupuesto </th>
                    <th class='nowrap'> Tipo Tarea </th>
                    <th class='nowrap'> Técnico </th>
                    <th class='nowrap'> Actividad </th>
                    <th class='nowrap'> Inicio </th>
                    <th class='nowrap'> Hora </th>
                    <th class='nowrap'> Fin </th>
                    <th class='nowrap'> Hora </th>
                    <th class='nowrap'> Tiempo </th>
                    <th class="centro"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeTiempos();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('../pie.php'); ?>

<script src="../../api/js/jquery.dataTables.js"></script>
<script src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
