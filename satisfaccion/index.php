<?php
  $seccionActiva=15;
  include_once('../cabecera.php');
    
  if(isset($_POST['codigo'])){
    $res=actualizaDatos('satisfaccion');
  }

  $estadisticas=creaDatosEstadisticasSatisfaccion();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Resultados de las encuestas de satisfacción realizadas:</h6>
                  <div id="big_stats" class="cf">
                  
                    <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
                  
                    <div class="leyenda" id="leyenda"></div>
                    
                    <!-- .stat --> 
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-edit"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Encuestas registradas</div>
                  </div>
				  
				   <h6 class="bigstats"></h6>
				          <div class="shortcuts">
                    <a href="<?php echo $_CONFIG['raiz']; ?>satisfaccion/estadisticas.php" class="shortcut"><i class="shortcut-icon icon-bar-chart"></i><span class="shortcut-label">Ver estadísticas</span> </a>
                  </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>


        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-link"></i>
              <h3>Enlace a Cuestionario</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content cajaEnlace">
              <div class="widget big-stats-container">
                Proporcione el siguiente enlace a sus clientes para que accedan al cuestionario:<br>
                <a href="<?php echo obtieneURLCuestionario(); ?>" target="_blank"><?php echo obtieneURLCuestionario(); ?></a>
              </div>
            </div>
          </div>
         
        </div>


      <div class="span12">
        
        <?php
        if(isset($_POST['codigo'])){
          if($res){
            mensajeOk("Encuesta actualizada correctamente");
          }
          else{
            mensajeError("no se han podido actualizar la Encuesta. Compruebe los datos introducidos.");
          }
        }
  			elseif(isset($_POST['puntuacion1'])){
  				if($res){
  					mensajeOk("Encuesta registrada correctamente");
  				}
  				else{
  					mensajeError("no se han podido registrar la Encuesta. Compruebe los datos introducidos.");
  				}
  			}
		
        ?>
        
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Encuestas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Cliente </th>
                    <th> Fecha </th>
                    <th class="td-actions"> </th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeEncuestasSatisfaccion();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">
    <?php
      $datos=generaDatosGraficoEncuestasSatisfaccion();
    ?>
    var pieData = [
        {
            value: <?php echo $datos['1']; ?>,
            color: "#B02B2C",
			label: "1 estrella"
        },
        {
            value: <?php echo $datos['2']; ?>,
            color: "#F37D01",
			label: "2 estrellas"
        },
        {
            value: <?php echo $datos['3']; ?>,
            color: "#f0ad4e",
			label: "3 estrellas"
        },
        {
            value: <?php echo $datos['4']; ?>,
            color: "#6BBA70",
			label: "4 estrellas"
        },
		{
            value: <?php echo $datos['5']; ?>,
            color: "#0061B4",
			label: "5 estrellas"
        }
      ];

    var grafico = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
</script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/eventoGrafico.js"></script>

