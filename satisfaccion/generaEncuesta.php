<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común


  	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('satisfaccion',$_GET['codigo']);

	//Carga de la librería PHPWord
	require_once '../phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('../ficheros/plantilla.docx');

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	
	
	$document->setValue("cliente",utf8_decode($datos['cliente']));
	$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fecha'])));
	$document->setValue("pregunta1",utf8_decode($datos['pregunta1']));
	$document->setValue("pregunta2",utf8_decode($datos['pregunta2']));
	$document->setValue("pregunta3",utf8_decode($datos['pregunta3']));
	$document->setValue("pregunta4",utf8_decode($datos['pregunta4']));
	$document->setValue("pregunta5",utf8_decode($datos['pregunta5']));
	$document->setValue("pregunta6",utf8_decode($datos['pregunta6']));
	$document->setValue("pregunta7",utf8_decode($datos['pregunta7']));
	$document->setValue("comentarios",utf8_decode($datos['comentarios']));


	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$document->save('../ficheros/Encuesta-'.$datos['cliente'].'.docx');


	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Encuesta-".$datos['cliente'].".docx");
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('../ficheros/Encuesta-'.$datos['cliente'].'.docx');


?>