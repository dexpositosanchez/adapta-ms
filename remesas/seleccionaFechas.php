<?php
  $seccionActiva=2;
  include_once("../cabecera.php");
?>
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-paste"></i><i class="icon-chevron-right"></i><i class="icon-plus-sign"></i>
              <h3>Generación de remesas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
				<form action='gestion.php' method='post'>

					Vencimiento del: <input type='text' class='input-small datepicker hasDatepicker' id='fechaUno' name='fechaUno' value='<?php echo imprimeFecha(); ?>'> 
					al: <input type='text' class='input-small datepicker hasDatepicker' id='fechaDos' name='fechaDos' value='<?php echo imprimeFecha(); ?>'>
					
					<br /><br />
					Tipo de remesa:<br />
					<?php
						//Las remesas SEPA pueden ser CORE o B2B, que llevan los códigos asociados 19143 y 19445 respectivamente.
						campoSelect('tipoSepa','',array('CORE','B2B'),array('CORE','B2B'),false,'selectpicker span3 show-tick','',2);
					?>

					<br /><br />
					<a href="index.php" class="btn btn-default"><i class="icon-arrow-left"></i> Volver</a>
					<button type="submit" class="btn btn-propio">Continuar <i class="icon-arrow-right"></i></button>
				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../../api/js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>