<?php
  $seccionActiva=8;
  require_once('../cabecera.php');
  
  operacionesAlumnos();
  $estadisticas=creaEstadisticasAlumnos();
  borrarDuplicados();
  //listadoAlumnosRevisar();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas sobre alumnos de baja:</h6>
                  <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-industry"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Alumno/s de baja</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Alumnos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="../alumnos/index.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Alumnos registrados</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroAlumnos();//El div utilizado para fnFilter se encuentra en esta función
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaAlumnos">
                <thead>
                  <tr>
					          <th> Apellido 1 </th>
                    <th> Apellido 2 </th>
                    <th> Nombre </th>
                    <th> DNI </th>
                    <th> Fecha de nacimiento </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>

<script src="../js/selectAjax.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaAlumnos','../listadoAjax.php?include=alumnos_baja&funcion=listadoAlumnos();');
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaAlumnos');

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    var filtroEmpresa=$('#filtroEmpresa').text();
    if(filtroEmpresa!=''){
      $('#tablaAlumnos').dataTable().fnFilter(filtroEmpresa);
    }
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>