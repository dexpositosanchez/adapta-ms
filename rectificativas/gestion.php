<?php
  $seccionActiva=2;
  include_once("../cabecera.php");
  $datos=gestionFacturaRectificativa();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#fechaEmision').datepicker({format:'dd/mm/yyyy',weekStart:1});
		$('#fechaVencimiento').datepicker({format:'dd/mm/yyyy',weekStart:1});
		$('.selectpicker').selectpicker();
		<?php if(isset($datos['codigo'])){ 
				echo "var alumno=$('#codigoAlumno').val();
				var codigo=$('#codigo').val();
				listadoVencimientos(alumno,codigo);";
			}
		?>
		$('#codigoAlumno').change(function(){
			var alumno=$('#codigoAlumno').val();
			listadoVencimientos(alumno);
		});
	});
	
	function listadoVencimientos(codigoAlumno,codigo){
		var parametros = {
                "alumno" : codigoAlumno,
				"codigo" : codigo
        };
		$.ajax({
			 type: "POST",
			 url: "../listadoAjax.php?include=facturas&funcion=listadoVencimientos('SI');",
			 data: parametros,
			  beforeSend: function () {
				   $("#vencimientosOcultos").html('<center><i class="icon-refresh icon-spin"></i></center>');
			  },
			 success: function(response){
				   $("#vencimientosOcultos").html(response);
			 }
		});
	}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>