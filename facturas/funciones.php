<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de facturas

function operacionesFacturas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('facturacion',$_POST['codigo']);
		//$res=insertaGastos($_POST['codigo']);
	}
	elseif(isset($_POST['formaPago'])){
		$res=creaFactura();
	}
	elseif(isset($_POST['cobrar']) && $_POST['cobrar']=='SI'){
		$res=cobraFacturas();
	}
	elseif(isset($_POST['generacionMasiva']) && $_POST['generacionMasiva']=='SI'){
		$res=generacionMasiva();
	}
	elseif(isset($_GET['codigoEnviar'])){
		$res=enviaEmailFactura($_GET['codigoEnviar']);
  	}	
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('facturacion');
	}

	mensajeResultado('formaPago',$res,'factura');
	mensajeResultado('cobrar',$res,'facturas');
	mensajeResultadoEnvioFactura('codigoEnviar',$res,'Factura');
	mensajeResultado('elimina',$res,'factura', true);
}

function actualizaFactura(){
	if($_POST['origen']=='CONTRATO'){
		$res=consultaBD('UPDATE contratos_facturas SET facturado=NULL WHERE facturado='.$_POST['codigo'],true);
		$res=consultaBD('UPDATE contratos_facturas SET facturado="'.$_POST['codigo'].'" WHERE codigo='.$_POST['plazo'],true);
		$plazo=datosRegistro('contratos_facturas',$_POST['plazo']);
		$_POST['subtotal']=$plazo['importe'];
		$iva=($_POST['subtotal']*21)/100;
		$_POST['iva']=$iva;
		$_POST['coste']=$_POST['subtotal']+$iva;
		$_POST['importe']=$_POST['coste'];
		$res=actualizaDatos('facturacion');
		$res=actualizaDatos('vencimientos_facturas','','',' WHERE codigoFactura='.$_POST['codigo']);
	} else {
		$factura=datosRegistro('facturacion',$_POST['codigo']);
		$venta=datosRegistro('presupuestos',$factura['codigoVenta']);
		$res=consultaBD('UPDATE presupuestos_facturas SET facturado=NULL WHERE facturado='.$_POST['codigo'],true);
		$res=consultaBD('UPDATE presupuestos_facturas SET facturado="'.$_POST['codigo'].'" WHERE codigo='.$_POST['plazo'],true);
		$plazo=datosRegistro('presupuestos_facturas',$_POST['plazo']);
		$_POST['iva']=($_POST['subtotal']*21)/100;
		$_POST['coste']=$_POST['subtotal']+$_POST['iva'];
		$_POST['importe']=$_POST['coste'];
		$res=actualizaDatos('facturacion');
		$res=actualizaDatos('vencimientos_facturas','','',' WHERE codigoFactura='.$_POST['codigo']);

	}
	return $res;
}

function creaFactura(){
	$res = true ;
	$datos=arrayFormulario();
	
	if($_POST['origen']=='CONTRATO'){
		$alumno = datosRegistro('contratos',$_POST['codigoContrato']);
		$plazo=datosRegistro('contratos_facturas',$_POST['plazo']);
		$_POST['subtotal']=$plazo['importe'];
		$iva=($_POST['subtotal']*21)/100;
		$_POST['iva']=$iva;
		$_POST['coste']=$_POST['subtotal']+$iva;
		$programa = datosRegistro('programas',$alumno['codigoPrograma']);
		$_POST['concepto']=$programa['nombre'];
		$_POST['fechaVencimiento'] = formateaFechaWeb($plazo['fecha']);
		$res=insertaDatos('facturacion');
		$codigoFactura=$res;
		$res=consultaBD('UPDATE contratos_facturas SET facturado="'.$codigoFactura.'" WHERE codigo='.$_POST['plazo'],true);
		$res = insertaVencimientosFacturas($codigoFactura,$_POST['coste'],$_POST['fechaVencimiento']);
	} else {
		$venta=datosRegistro('presupuestos',$_POST['codigoVenta']);
		$plazo=datosRegistro('presupuestos_facturas',$_POST['plazo']);
		$_POST['subtotal']=$plazo['importe'];
		$_POST['iva']=($_POST['subtotal']*21)/100;
		$_POST['coste']=$_POST['subtotal']+$_POST['iva'];
		$fecha=explode('-', $venta['fechaValidez']);
		$referencia=str_pad($venta['referenciaPresupuesto'], 3, "0", STR_PAD_LEFT).'/'.$fecha[1].'-'.$fecha[0];
		$_POST['concepto']='Venta '.$referencia;
		$_POST['fechaVencimiento'] = formateaFechaWeb($plazo['fecha']);
		$res=insertaDatos('facturacion');
		$codigoFactura=$res;
		$res=consultaBD('UPDATE presupuestos_facturas SET facturado="'.$codigoFactura.'" WHERE codigo='.$_POST['plazo'],true);
		$res = insertaVencimientosFacturas($codigoFactura,$_POST['coste'],$_POST['fechaVencimiento']);
	}

	if($_POST['bonificada']=='SI'){
		insertaParticipantes($datos, $codigoFactura);
	}
	
	//$res=insertaGastos($codigoFactura);
	return $res;
}

function operacionesRecibos(){
	$res=true;

	if(isset($_POST['cobrar']) && $_POST['cobrar']=='SI'){
		$res=cobraRecibos();
	}

	mensajeResultado('cobrar',$res,'recibos');
}

function gestionFactura(){
	operacionesFacturas();

	abreVentanaGestion('Gestión de Facturas','index.php','span5','icon-edit');
	$datos=compruebaDatos('facturacion');
	
	abreColumnaCampos('span11');
	if($datos){
		$contrato=datosRegistro('contratos',$datos['codigoContrato']);
		$alumno=datosRegistro('trabajadores_cliente',$contrato['codigoAlumno']);
		$curso=datosRegistro('cursos',$contrato['codigoCurso']);
		$venta=datosRegistro('presupuestos',$datos['codigoVenta']);
		if($datos['origen']=='CONTRATO'){
			campoDato('Matrícula',$alumno['nombre'].' '.$alumno['apellido1'].' '.$alumno['apellido2'].' - '.$curso['curso'],'codigoContrato');
			campoDato('Fecha emisión de matrícula',formateaFechaWeb($contrato['fecha']));
		} else {
			$fecha=explode('-', $venta['fechaValidez']);
			$referencia=str_pad($venta['referenciaPresupuesto'], 3, "0", STR_PAD_LEFT).'/'.$fecha[1].'-'.$fecha[0].' - '.$venta['nombre'];
			campoDato('Presupuesto',$referencia,'codigoVenta');
			campoDato('Fecha emisión de presupuesto',formateaFechaWeb($venta['fechaValidez']));
		}
		campoOculto($datos['origen'],'origen');
		campoDato('Referencia',$datos['referencia'],'referencia');
		campoDato('Fecha emisión de factura',formateaFechaWeb($datos['fechaEmision']),'fechaEmision');
		campoDato('Fecha vencimiento',formateaFechaWeb($datos['fechaVencimiento']),'fechaVencimiento');
		campoDato('Forma de pago',$datos['formaPago'],'formaPago');
	} else {
		if(isset($_GET['codigoPresupuesto'])){
			$datos['origen']='VENTA';
			$datos['codigoVenta']=$_GET['codigoPresupuesto'];
			$datos['referencia']=false;
			$datos['formaPago']=false;
			$datos['enviada']='NO';
			$datos['cobrada']='NO';
			$datos['fechaVencimiento']=false;
		}
		campoRadio('origen','Origen de factura',$datos,'CONTRATO',array('Matrícula','Venta'),array('CONTRATO','VENTA'));
		campoRadio('bonificada','¿Bonificada?',$datos,'NO',array('SI','NO'),array('SI','NO'));

		echo "<div id='divCONTRATO' class='origen hide'>";
		campoSelectConsulta('codigoContrato','Matrícula',"SELECT contratos.codigo, CONCAT(trabajadores_cliente.nombre,' ',apellido1,' ',apellido2,' - ',programas.nombre) AS texto FROM contratos INNER JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo
		LEFT JOIN programas ON contratos.codigoPrograma=programas.codigo ORDER BY trabajadores_cliente.nombre, apellido1, apellido2;",'','selectpicker span8 show-tick');
		echo "</div>";

		echo "<div id='divVENTA' class='origen hide'>";
		$ventas=consultaBD('SELECT presupuestos.codigo,referenciaPresupuesto,fechaValidez,clientes.nombre FROM presupuestos INNER JOIN clientes ON presupuestos.empresa=clientes.codigo WHERE aceptado="SI"',true);
		$nombres=array();
		$valores=array();
		while($venta=mysql_fetch_assoc($ventas)){
			array_push($valores, $venta['codigo']);
			$fecha=explode('-', $venta['fechaValidez']);
			$referencia=str_pad($venta['referenciaPresupuesto'], 3, "0", STR_PAD_LEFT).'/'.$fecha[1].'-'.$fecha[0].' - '.$venta['nombre'];
			array_push($nombres, $referencia);
		}
		campoSelect('codigoVenta','Venta',$nombres,$valores,$datos,'selectpicker span8 show-tick');
		echo "</div>";
		campoSelect('plazo','Plazo',array(),array());
		campoReferenciaFactura(false);
		campoDato('Fecha emisión de factura',fecha());
		campoOculto(fecha(),'fechaEmision');
		if($datos && $datos['fechaVencimiento']!=false){
			campoFecha('fechaVencimiento','Fecha vencimiento primera o única factura',$datos);
		}
		//echo "<span id='vencimientosOcultos'></span>";
		campoFormaPago($datos);
	}
	campoRadio('enviada','Enviada a cliente',$datos);
	campoRadio('cobrada','Cobrada',$datos);
	campoOculto('GENERAL','tipoFactura');

	echo "<div id='participantes' class='origen hide'>";
		tablaParticipantes();
	echo "</div>";
	cierraColumnaCampos();
	
	
	cierraVentanaGestion('index.php',true);
	return $datos;
}

function actualizaReferencia($codigo){						//MODIFICADO JOSE LUIS 14/04/15
	conexionBD();
	$datos=arrayFormulario();
	
	$referencia=consultaBD("SELECT referencia FROM facturacion WHERE fechaEmision LIKE'".date('Y')."-%' ORDER BY referencia DESC LIMIT 1;");
	$referencia=mysql_fetch_assoc($referencia);
	
	$referencia=$referencia['referencia']+1;
	
	if(strlen($referencia)<3){
		while(strlen($referencia)<3){	
			$referencia='0'.$referencia;
		}
	}
	
	$consulta=consultaBD("UPDATE facturacion SET referencia='$referencia' WHERE codigo='$codigo';");
	
	cierraBD();
}

function cobraFacturas(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("UPDATE facturacion SET cobrada='SI', enviada='SI' WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}else{
			$consulta=consultaBD("UPDATE vencimientos_facturas SET cobrado='SI' WHERE codigoFactura='".$datos['codigo'.$i]."';");
		}
	}
	cierraBD();

	return $res;
}

function creaEstadisticasFacturas($where='',$comprobacionCurso=true, $tipoFactura="AND tipoFactura='GENERAL'"){
	$datos=array();

	conexionBD();
	
	$anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos' && $comprobacionCurso){
		$anio=$_SESSION['ejercicio'];
		$anio="AND (facturacion.fechaEmision LIKE '".$anio."-%' OR facturacion.codigo IN (SELECT codigoFactura FROM vencimientos_facturas WHERE fecha LIKE '".$anio."-%'))";
	}

	$consulta=consultaBD("SELECT COUNT(facturacion.codigo) AS codigo FROM facturacion LEFT JOIN contratos ON facturacion.codigoContrato=contratos.codigo WHERE 1=1 $tipoFactura $where $anio;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['facturacion']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(facturacion.codigo) AS codigo FROM facturacion LEFT JOIN contratos ON facturacion.codigoContrato=contratos.codigo WHERE cobrada='SI' $tipoFactura $where $anio;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['cobradas']=$consulta['codigo'];
	
	$consulta=consultaBD("SELECT SUM(vencimientos_facturas.importe) AS cobrado FROM vencimientos_facturas INNER JOIN facturacion ON vencimientos_facturas.codigoFactura=facturacion.codigo INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo WHERE cobrado='SI' $tipoFactura $where $anio;",false,true);
	$consultaDos=consultaBD("SELECT SUM(replace(contratos.pagoInicial,',','.')) AS cobrado FROM facturacion INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo WHERE 1=1 $tipoFactura $where $anio;",false,true);
	$consultaTres=consultaBD("SELECT SUM(facturacion.coste) AS cobrado FROM facturacion INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo WHERE cobrada='SI' AND (facturacion.codigo NOT IN(SELECT codigoFactura FROM vencimientos_facturas) OR facturacion.codigo IN(SELECT codigoFactura FROM vencimientos_facturas GROUP BY codigoFactura HAVING SUM(importe)=0)) $tipoFactura $where $anio;",false,true);
	if($consulta['cobrado']<0){
		$consultaDos['cobrado']=$consultaDos['cobrado']*(-1);
	}
	$datos['cobrado']=number_format($consulta['cobrado']+$consultaDos['cobrado']+$consultaTres['cobrado'],2,',','');

	$consulta=consultaBD("SELECT SUM(coste) AS total FROM facturacion INNER JOIN contratos ON facturacion.codigoContrato = contratos.codigo WHERE cobrada='SI' $tipoFactura $where $anio;",false,true);
	$datos['cobrado']=number_format($consulta['total']);
	
	$consulta=consultaBD("SELECT SUM(vencimientos_facturas.importe) AS pendiente FROM vencimientos_facturas INNER JOIN facturacion ON vencimientos_facturas.codigoFactura=facturacion.codigo INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo WHERE cobrado='NO' $tipoFactura $where $anio;",false,true);
	$consultaDos=consultaBD("SELECT SUM(facturacion.coste) AS pendiente FROM facturacion INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo WHERE cobrada='NO' AND (facturacion.codigo NOT IN(SELECT codigoFactura FROM vencimientos_facturas) OR facturacion.codigo IN(SELECT codigoFactura FROM vencimientos_facturas GROUP BY codigoFactura HAVING SUM(importe)=0)) $tipoFactura $where $anio;",false,true);
	$datos['pendiente']=number_format($consulta['pendiente']+$consultaDos['pendiente'],2,',','');

	$consulta=consultaBD("SELECT SUM(coste) AS total FROM facturacion INNER JOIN contratos ON facturacion.codigoContrato = contratos.codigo WHERE cobrada='NO' $tipoFactura $where $anio;",false,true);
	$datos['pendiente']=number_format($consulta['total'],2,',','');

	cierraBD();

	return $datos;
}

function imprimefacturas($where='',$comprobacionCurso=true, $tipoFactura="AND tipoFactura='GENERAL'"){
	global $_CONFIG;
	conexionBD();
	$anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos' && $comprobacionCurso){
		$anio=$_SESSION['ejercicio'];
		$anio="AND (facturacion.fechaEmision LIKE '".$anio."-%' OR facturacion.codigo IN (SELECT codigoFactura FROM vencimientos_facturas WHERE fecha LIKE '".$anio."-%'))";
	}
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.concepto, CONCAT(trabajadores_cliente.nombre,' ',trabajadores_cliente.apellido1,' ',trabajadores_cliente.apellido2) AS alumno, facturacion.fechaEmision, facturacion.cobrada, facturacion.referencia, coste, facturacion.enviada, facturacion.tipoFactura, contratos.importeCurso, contratos.pagoInicial, clientes.nombre AS nombreEmpresa, facturacion.origen	
		FROM facturacion LEFT JOIN contratos ON facturacion.codigoContrato=contratos.codigo LEFT JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo LEFT JOIN presupuestos ON facturacion.codigoVenta=presupuestos.codigo LEFT JOIN clientes ON presupuestos.empresa=clientes.codigo WHERE 1=1 $tipoFactura $where $anio ORDER BY referencia DESC;");
	
	$datos=mysql_fetch_assoc($consulta);
	$iconoC=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove iconoFactura icon-danger"></i>');
	$destinoFactura=array('GENERAL'=>'facturas','RECTIFICATIVA'=>'rectificativas');
	$rectificacion=array('GENERAL'=>'','RECTIFICATIVA'=>'R - ');
	while($datos!=0){
		$totalPendiente=consultaBD("SELECT SUM(vencimientos_facturas.importe) AS importe FROM vencimientos_facturas WHERE codigoFactura ='".$datos['codigo']."' AND vencimientos_facturas.cobrado='NO';",true,true);
		if(!isset($totalPendiente['importe'])){
			$totalPendiente['importe']=$datos['importeCurso']-$datos['pagoInicial'];
		}
		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$fechaPartida=explode('/',$fecha);
		if($datos['origen']=='CONTRATO'){
			$alumno = $datos['alumno'];
		} else {
			$alumno = $datos['nombreEmpresa'];
		}
		echo "
		<tr>
			<td> ".$rectificacion[$datos['tipoFactura']].$datos['referencia']." / ".substr($fechaPartida[2],2,2)."</td>
			<td> ".$datos['concepto']." </td>
        	<td> ".$alumno." </td>
        	<td> $fecha </td>
			<td> ".$datos['coste']." </td>
			<td> ".number_format((float)$totalPendiente['importe'], 2, ',', '').''."</td>
			<td class='centro'> ".$iconoC[$datos['enviada']]." </td>
			<td class='centro'> ".$iconoC[$datos['cobrada']]." </td>
        	<td class='centro'>
				<div class='btn-group'>
		                <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
		                <ul class='dropdown-menu' role='menu'>
							<li><a href='".$_CONFIG['raiz'].$destinoFactura[$datos['tipoFactura']]."/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Ver datos</i></a> </li>
							<li class='divider'></li>
							<li><a href='".$_CONFIG['raiz'].$destinoFactura[$datos['tipoFactura']]."/generaFacturaWord.php?codigo=".$datos['codigo']."' class='noAjax'><i class='icon-download'></i> Descargar</i></a> </li>
							<li class='divider'></li>
							<li><a href='".$_CONFIG['raiz'].$destinoFactura[$datos['tipoFactura']]."/controlRecibosFactura.php?codigoFactura=".$datos['codigo']."'><i class='icon-file-text'></i> Ver recibos</i></a> </li>
							<li class='divider'></li>							
							<li><a href='index.php?codigoEnviar=".$datos['codigo']."'><i class='icon-envelope'></i> Enviar Email</i></a> </li>							
						</ul>
				    </div>
        	</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function listadoVencimientos($rectificada='NO'){
	$datos=arrayFormulario();
	$rectificacion=array('NO'=>'','SI'=>'-');
	if(isset($datos['codigo'])){
		$datosFactura=datosRegistro('facturacion',$datos['codigo']);
		$datosAlumno=datosRegistro('contratos',$datosFactura['codigoContrato']);
		campoDato('Importe del curso',$datosAlumno['importeCurso'].' €','importeCurso');
		campoDato('Pago inicial',$datosAlumno['pagoInicial'] == '' ? '0 €':$datosAlumno['pagoInicial'].' €','pagoInicial');
		echo "<span id='otrosGastos'></span>";
		creaTablaVencimientos();
		$consultaVencimientos=consultaBD("SELECT * FROM vencimientos_facturas WHERE codigoFactura='".$datos['codigo']."' ORDER BY fecha ASC;",true);
		$i=0;
		$total=0;
		$recibos=1;
		while($datosVencimientos=mysql_fetch_assoc($consultaVencimientos)){
			if($datosVencimientos['codigoGasto'] == NULL){
				$concepto='Recibo '.$recibos;
				$recibos++;
			} else {
				$gasto=datosRegistro('otros_gastos',$datosVencimientos['codigoGasto']);
				$concepto=$gasto['gasto'];
			}
			creaLineaTablaVencimientos($datosVencimientos['importe'],$datosVencimientos['fecha'],$concepto,$datosVencimientos['cobrado']);
			$total= $total + $datosVencimientos['importe'];
			$i++;
		}
		cierraTablaVencimientos();
		campoDato('Subtotal',$total.' €','subtotal');
		campoDato('I.V.A',$datosFactura['iva'].' €','iva');
		campoDato('Total',$total.' €','coste');
		campoDato('Concepto',$datosFactura['concepto'],'concepto');
	}elseif(isset($datos['alumno']) && $datos['alumno']!='NULL'){
		$datosAlumno=datosRegistro('contratos',$datos['codigoContrato']);
		$curso=consultaBD("SELECT curso, precio, plazos FROM cursos WHERE codigo=(SELECT curso FROM trabajadores_cliente WHERE codigo='".$datos['alumno']."')",true,true);
		$contador=$datosAlumno['plazosCurso'];
		$precio=str_replace(',','.',$datosAlumno['importeCurso'])-str_replace(',','.',$datosAlumno['pagoInicial']);
		campoDato('Importe del curso',$datosAlumno['importeCurso'].' €','importeCurso');
		campoDato('Pago inicial',$datosAlumno['pagoInicial'] == '' ? '0 €':$datosAlumno['pagoInicial'].' €','pagoInicial');
		creaTablaVencimientos();
		$i=		$total=0;
		while($contador>0){
			$mes=1*$i;
			$fecha=strtotime ( "+$mes month" , strtotime ( $datosAlumno['fechaAlta'] ) ) ;
			$fecha = date ( 'Y-m-d' , $fecha );
			$importe = $rectificacion[$rectificada].$precio/$datosAlumno['plazosCurso'];
			creaLineaTablaVencimientos($importe,$fecha,$i);
			$total = $total + $importe;
			$contador--;
			$i++;
		}
		cierraTablaVencimientos();
		campoDato('Subtotal',$total.' €','subtotal');
		campoDato('I.V.A','0 €','iva');
		campoDato('Total',$total.' €','coste');
		campoDato('Concepto',$datosFactura['concepto'],'concepto');
	}
}

function creaTablaVencimientos(){
	echo"<h3 class='apartadoFormulario sinFlotar'>Vencimientos de pagos</h3>
	<center>
		<div class='table-responsive'>
			<table class='table' id='tablaVencimientos'>
				<thead>
					<tr class='apartadoTablaFormularioEvaluacion'>
						<th> Concepto </th>
						<th> Importe </th>
						<th> Fecha Vencimiento </th>
						<th> Cobrado </th>
					</tr>
				</thead>
				<tbody>";
}

function creaLineaTablaVencimientos($importe,$fecha,$concepto,$cobrado){
	$iconoC=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove iconoFactura icon-danger"></i>');
	echo "<tr>
	<td>".$concepto."</td>
	<td>".number_format((float) $importe,2,'.','')."</td>
	<td>".formateaFechaWeb($fecha)."</td>
	<td class='centro'> ".$iconoC[$cobrado]." </td>

	</tr>";
			/*campoTextoTabla('importe'.$posicion,number_format((float) $importe,2,'.',''));
			campoFechaTabla('fecha'.$posicion,$fecha);
		echo"
			</tr>";*/
}

function cierraTablaVencimientos(){
	echo "</tbody>
		</table>
	</div>
	</center>
	<br>";
}

function otrosGastos(){
	camposPreciosProductos();
	echo"<h3 class='apartadoFormulario sinFlotar'>Otros Gastos</h3>
	<center>
		<div class='table-responsive'>
			<table class='table' id='tablaGastos'>
				<thead>
					<tr class='apartadoTablaFormularioEvaluacion'>
						<th> Gasto </th>
						<th> Importe </th>
						<th> Fecha </th>
					</tr>
				</thead>
				<tbody>";
				$i=0;
				if($_POST['codigo'] != 'false'){
					$gastos=consultaBD('SELECT * FROM contratos_gastos WHERE codigoContrato='.$_POST['codigo'],true);
					while($gasto=mysql_fetch_assoc($gastos)){
						echo "<tr>";
							campoSelectConsulta('gasto'.$i,'','SELECT codigo,gasto AS texto FROM otros_gastos ORDER BY gasto',$gasto['codigoGasto'],'selectpicker span3 show-tick',"data-live-search='true'",'',1);
							campoTextoSimbolo('precio'.$i,'','€',$gasto['precio'],'input-mini pagination-right',1);
							campoFechaTabla('fecha'.$i);
						echo"</tr>";
						$i++;
					}}
						echo "<tr>";
							campoSelectConsulta('gasto'.$i,'','SELECT codigo,gasto AS texto FROM otros_gastos ORDER BY gasto','','selectpicker span3 show-tick',"data-live-search='true'",'',1);
							campoTextoSimbolo('precio'.$i,'','€','','input-mini pagination-right',1);
							campoFechaTabla('fecha'.$i);
						echo"</tr>";

	echo "</tbody>
		</table>
	</div>
	</center>
	<center>
	<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaGastos\");'><i class='icon-plus'></i> Añadir gasto</button> 
				</center><br/><br/>";
}

function camposPreciosProductos(){
	$consulta=consultaBD("SELECT codigo, importe FROM otros_gastos", true);
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto($datos['importe'],'codProducto'.$datos['codigo']);
	}
}


function cobraRecibos(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("UPDATE vencimientos_facturas SET cobrado='SI' WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}
	}
	$consultaAuxiliar=consultaBD("UPDATE facturacion SET cobrada='SI', enviada='SI' WHERE codigo NOT IN(SELECT codigoFactura FROM vencimientos_facturas WHERE cobrado='NO');");
	cierraBD();

	return $res;
}

function creaEstadisticasRecibos($where='',$comprobacionCurso=true,$tipoFactura='GENERAL'){
	$datos=array();

	conexionBD();
	
	$anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos' && $comprobacionCurso){
		$anio=$_SESSION['ejercicio'];
		$anio="AND (fecha LIKE '".$anio."-%')";
	}

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM vencimientos_facturas WHERE 1=1 AND codigoFactura IN(SELECT codigo FROM facturacion WHERE tipoFactura='$tipoFactura') $where $anio;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['facturacion']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM vencimientos_facturas WHERE cobrado='SI' AND codigoFactura IN(SELECT codigo FROM facturacion WHERE tipoFactura='$tipoFactura') $where $anio;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['cobradas']=$consulta['codigo'];
	
	$consulta=consultaBD("SELECT SUM(importe) AS cobrado FROM vencimientos_facturas WHERE cobrado='SI' AND codigoFactura IN(SELECT codigo FROM facturacion WHERE tipoFactura='$tipoFactura') $where $anio;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['cobrado']=number_format($consulta['cobrado'],2,',','');
	
	$consulta=consultaBD("SELECT SUM(importe) AS pendiente FROM vencimientos_facturas WHERE cobrado='NO' AND codigoFactura IN(SELECT codigo FROM facturacion WHERE tipoFactura='$tipoFactura') $where $anio;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['pendiente']=number_format($consulta['pendiente'],2,',','');

	cierraBD();

	return $datos;
}

function imprimeRecibos($where='',$comprobacionCurso=true,$tipoFactura='GENERAL'){
	global $_CONFIG;
	conexionBD();
	$anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos' && $comprobacionCurso){
		$anio=$_SESSION['ejercicio'];
		$anio="AND (fecha LIKE '".$anio."-%')";
	}
	$consulta=consultaBD("SELECT codigo, fecha, importe, cobrado, codigoFactura FROM vencimientos_facturas WHERE 1=1 AND codigoFactura IN(SELECT codigo FROM facturacion WHERE tipoFactura='$tipoFactura') $where $anio;");
	cierraBD();
	
	$datos=mysql_fetch_assoc($consulta);
	$iconoC=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove iconoFactura icon-danger"></i>');
	while($datos!=0){
		$datosFactura=consultaBD("SELECT facturacion.codigo, facturacion.referencia, CONCAT(trabajadores_cliente.nombre,' ',trabajadores_cliente.apellido1,' ',trabajadores_cliente.apellido2) AS alumno 
		FROM facturacion INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo INNER JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo WHERE facturacion.codigo='".$datos['codigoFactura']."';",true,true);
		$fecha=formateaFechaWeb($datos['fecha']);
		$fechaPartida=explode('/',$fecha);
		echo "
		<tr>
			<td> <a href='".$_CONFIG['raiz']."facturas/gestion.php?codigo=".$datosFactura['codigo']."'>".$datosFactura['referencia']." / ".substr($fechaPartida[2],2,2)."</a></td>
        	<td> ".$datosFactura['alumno']." </td>
			<td> $fecha </td>
			<td> ".number_format($datos['importe'],2)." € </td>
			<td class='centro'> ".$iconoC[$datos['cobrado']]." </td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function generaPDFFacturaNuevo($codigoFactura){
	$datos=datosRegistro('facturacion',$codigoFactura);
	$datosContrato=datosRegistro('contratos',$datos['codigoContrato']);
	$datosVenta=datosRegistro('presupuestos',$datos['codigoVenta']);
	$cliente=datosRegistro('clientes',$datosVenta['empresa']);
	//$gastos=consultaBD('SELECT * FROM contratos_gastos WHERE codigoContrato='.$datos['codigoContrato'],true);
	$datosAlumno=datosRegistro('trabajadores_cliente',$datosContrato['codigoAlumno']);
	$fecha=$datos['fechaEmision'];
	$fechaPartida=explode('-',$fecha);
	$mes=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	$rectificacion=array('GENERAL'=>'','RECTIFICATIVA'=>'R - ');

	$fecha=formateaFechaWeb($datos['fechaEmision']);
	$fechaPartida=explode('/',$fecha);
	$anio=substr($fechaPartida[2], 2);
	$referencia=str_pad($datos['referencia'], 3, "0", STR_PAD_LEFT);
	$referencia=$anio.$referencia;
	
		$porCientoIVA=21;
		$textoPorCiento='I.V.A.';
		$valorPorCiento=number_format((float)str_replace(',','.',$datos['iva']), 2, ',', '');
		$pie='';

	if($datos['codigoContrato']!=NULL){
		$importe=number_format((float)str_replace(',','.',$datosContrato['importeCurso']), 2, ',', '');
	} else {
		$importe=number_format((float)str_replace(',','.',$datosVenta['subtotal']), 2, ',', '');
	}
	$concepto=$datos['concepto'];
	if($datos['origen']=='VENTA'){
		$cajaCliente=$cliente['nombre']."<br />
			".$cliente['direccion1']."<br>
			CIF: ".$cliente['nif']."<br>";
	} else {
		if($datosContrato['facturaEmpresa']=='SI'){
			$cajaCliente=$datosContrato['denominacionSocial']."<br />
			".$datosContrato['direccion']."<br>
			".$datosContrato['cp']." - ".$datosContrato['provincia']."<br>
			CIF: ".$datosContrato['cif']."<br>";
			$concepto.=' - '.$datosAlumno['nombre']." ".$datosAlumno['apellido1']." ".$datosAlumno['apellido2'].' - DNI: '.$datosAlumno['nif'];
		} else {
			$cajaCliente=$datosAlumno['nombre']." ".$datosAlumno['apellido1']." ".$datosAlumno['apellido2']."<br />
				".$datosAlumno['direccion'].", ".$datosAlumno['poblacion']."<br>
				".$datosAlumno['cp']." - ".$datosAlumno['provincia']."<br>
				NIF: ".$datosAlumno['nif']."<br>";
		}
	}
    $contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:12px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }
	        table td, .tablaConceptos td{
	            font-size: 12px;
	        }

	        .logo{
	            float:left;
	            vertical-align: bottom;
	            margin-right: 15px;
	        }
	        .caja{
	            border:1px solid #009AA3;
	            padding:20px;
	        }
	        .cajaCliente, .cajaCentro{
	            line-height: 18px;
	            margin-left:300px;
	            margin-bottom:10px;
	        }
	        .cajaCliente{
	            position:absolute;
	            right:10px;
	            width:350px;
				top:80px;
	        }
	        .cajaTabla{
	            clear: both;
	        }
	        .tablaSemiBorde{
	            border-collapse: collapse;
	        }

	        .tablaSemiBorde .ultimaFila td{
	            border-bottom:1px solid #009AA3;
	        }

	        .tablaSemiBorde td{
	            border-right: 1px solid #009AA3;
	            padding:5px;
	        }
	        .tablaSemiBorde .ultimaCelda{
	            border-right:none;
	        }
	        
	        .ancho100{
	            width:100%;
	        }
	        .margenAr{
	            margin-top:15px;
	        }
	        .ancho25{
	            width: 25%;
	        }

	        .cajaConceptos{
	            padding:0px 0px;
	        }
	        .tablaConceptos{
	            border-bottom:none;
	        }
	        .tablaConceptos thead td{
	            text-align: center;
	        }
	        .tablaConceptos td{
	            text-align: center;
	        }
	        .tablaConceptos td.descripcion{
	            text-align: justify;
	        }

	        .ancho10{
	            width:10.5%;
	        }
	        .ancho26{
	            width: 26%;
	        }
	        .ancho50{
	            width: 50%;
	            text-align:left;
	        }
	        .ancho5{
	            width:5%;
	        }
	        .total{
	            display:block;
	            position:relative;
	            left:70%;
	        }
	        .cajaObservaciones{
	            border-bottom:1px solid #009AA3;
	            padding:20px;
	            margin-top:20px;
	        }

	        .cajaPie{
	            width:100%;
	            margin-top:20px;
	            color:#333;
	            padding:10px;
	            font-weight: bold;
	            font-size:10px;
	            text-align:center;
				color:#009AA3;
	        }

	        .cajaDocumento{
	            position:relative;
				top:20px;
	        }

	        .ancho11{
	            width:12%;
	        }
	        
	        .ancho70{
	            width:70%;
	        }
	        .celdaImporte{
	        	text-align:right;
	        }
			.ancho74{
	            width:74%;
	        }
			.azul{
				color:#009AA3;
			}
			.divisor{
				width: 750px;
				height: 25px;
				border-bottom: 3px solid #009AA3;
				clear:both;
				margin-top:-30px;
			}
			
			.cajaGarzon{
	            line-height: 18px;
	            margin-bottom:10px;
				position:relative;
	            width:350px;
				color:#009AA3;
				margin-top:10px;
	        }
			.anchoEnunciado{
				width:130px;
			}
			.altoGrande{
				height:500px;
			}
			.bordeAbajo{
				border-bottom: 1px solid #009AA3;
			}
			.ancho33{
				width:24.66%;
			}
			.ancho20{
				width:16.66%;
			}
			.blanco{
				color:#FFF;
			}
			.pequenio{
				font-size:10px;
			}
			#cajaLogo{
				width:100%;
			}
		}
	-->
	</style>
	<page footer='page'>
		<div id='cajaLogo'>
	    <img src='../img/logo3.png' class='logo' />
	    </div>
		

	    <div class='cajaGarzon'>
	    	<b>Adapta management services</b><br/>
	    	CIF: B-93171973<br/>
			Calle Cuarteles, 7, 29002 Málaga Planta <br>
			2,Oficina 6. (Edificio Guadalmedina)<br>
			Tlfno: (+34) 952 34 10 07 - 637 474 372<br>
			correo-e: info@adaptams.es<br>
			www.adaptams.com
		</div>
	    <div class='cajaCliente'>
	        ".$cajaCliente."
	    </div>

	    <div class='cajaTabla cajaDocumento'>
	        <table>
	            <tr>
	                <td class='anchoEnunciado'><b class='azul'>Número de Factura:</b></td>
					<td>".$referencia."</td>
				</tr>
				<tr>
	                <td><b class='azul'>Fecha:</b></td>
					<td>".formateaFechaWeb($datos['fechaEmision'])."</td>
				</tr>
				<tr>
	                <td><b class='azul'>Forma de Pago:</b></td>
					<td>".$datos['formaPago']."</td>
				</tr>
	        </table>
	    </div>

	    <div class='caja cajaConceptos margenAr'>
	        <table class='tablaSemiBorde tablaConceptos ancho100 altoGrande'>
	            <thead>
	                <tr>
	                    <td class='ancho74 azul bordeAbajo' align='center' colspan='3'>CONCEPTO</td>
	                    <td class='ancho26 azul ultimaCelda bordeAbajo' align='center'>IMPORTE TOTAL</td>
	                </tr>
	            </thead>
	            <tbody>
					<tr>
						<td class='ancho74 bordeAbajo' align='left' colspan='3'>".$concepto."</td>
						<td class='ancho12 ultimaCelda celdaImporte bordeAbajo'>".$importe." €</td>
					</tr>
					<br>
					<tr>
						<td class='ancho74 azul bordeAbajo' align='center' colspan='3'>FORMA DE PAGO</td>
						<td class='ancho12 ultimaCelda celdaImporte bordeAbajo azul' align='center'>IMPORTE RECIBO</td>
					</tr>
					<br>";
					$contenido.=creaRecibosImpreso($datos['codigo']);
					$ancho='ancho33';
					$ancho='ancho20';
					$contenido.="<tr>
						<td colspan='3' class='bordeAbajo'> </td>
						<td class='bordeAbajo ultimaCelda'> </td>
					</tr>
					<tr>	
						<td class='azul bordeAbajo ".$ancho."'>BASE IMPONIBLE</td>
						<td class='azul bordeAbajo ".$ancho."'>% ".$textoPorCiento."</td>
						<td class='azul bordeAbajo ".$ancho."'>IMPORTE ".$textoPorCiento."</td>
						<td class='ultimaCelda azul bordeAbajo ".$ancho."'>TOTAL FACTURA €</td>
					</tr>
					<tr>	
						<td>".number_format((float)str_replace(',','.',$datos['subtotal']), 2, ',', '')." €</td>
						<td>".$porCientoIVA." %</td>
						<td>".$valorPorCiento." €</td>
						<td class='ultimaCelda'>".number_format((float)str_replace(',','.',$datos['coste']), 2, ',', '')." €</td>
					</tr>
	            </tbody>
	        </table>
	    </div>
		<page_footer>

		</page_footer>
	</page>";

	return $contenido;
}

function creaRecibosImpreso($codigoFactura){
	$consulta=consultaBD("SELECT * FROM vencimientos_facturas WHERE codigoFactura='$codigoFactura' ORDER BY fecha ASC;",true);
	$res="";
	$i=0;
	$recibos=1;
	while($datosRecibos=mysql_fetch_assoc($consulta)){
		if($datosRecibos['codigoGasto'] == NULL){
			$concepto='Recibo '.$recibos;
			$recibos++;
		} else {
			$gasto=datosRegistro('otros_gastos',$datosRecibos['codigoGasto']);
			$concepto=$gasto['gasto'];
		}
		$res.="<tr>
			<td class='ancho74' align='left' colspan='3'>".$concepto." ".formateaFechaWeb($datosRecibos['fecha']).": </td>
			<td class='ancho12 ultimaCelda celdaImporte'>".number_format((float)str_replace(',','.',$datosRecibos['importe']), 2, ',', '')." €</td>
		</tr>";
		$i++;
	}
	
	while($i<23){
		$res.="<tr>
			<td class='ancho74 blanco' colspan='3'>A</td>
			<td class='ancho12 ultimaCelda celdaImporte blanco'>A</td>
		</tr>";
		$i++;
	}
	$i++;
	$res.="<tr>
			<td class='ancho74' colspan='3' align='center'></td>
			<td class='ancho12 ultimaCelda celdaImporte'></td>
		</tr>";
	return $res;
}

function creaGastosPDF($gastos){
	$res='';
	while($gasto=mysql_fetch_assoc($gastos)){
		$gasto2=datosRegistro('otros_gastos',$gasto['codigoGasto']);
		$res.="<tr>
				<td class='ancho74 bordeAbajo' align='left' colspan='3'>".$gasto2['gasto']."</td>
				<td class='ancho12 ultimaCelda celdaImporte bordeAbajo'>".number_format((float)str_replace(',','.',$gasto['precio']), 2, ',', '')." €</td>
			</tr>";
	}
	return $res;
}

function generacionMasiva(){
	$datos=arrayFormulario();
	$res=true;
	if($datos['origen']=='CONTRATO'){
		$consulta=consultaBD("SELECT * FROM contratos_facturas WHERE fecha>='".$datos['fechaUno']."' AND fecha<='".$datos['fechaDos']."' AND facturado IS NULL",true);
	} else {
		$consulta=consultaBD("SELECT * FROM presupuestos_facturas WHERE fecha>='".$datos['fechaUno']."' AND fecha<='".$datos['fechaDos']."' AND facturado IS NULL",true);
	} 

	$referencia=consultaBD('SELECT MAX(referencia) AS referencia FROM facturacion WHERE  YEAR(fechaVencimiento) = "'.date('Y').'" AND tipoFactura="GENERAL"',true,true);
	$referencia=$referencia['referencia']+1;
	
	$_POST['fechaEmision']=fecha();
	$_POST['formaPago']='CUENTA CORRIENTE';
	$_POST['cobrada']='NO';
	$_POST['enviada']='NO';
	$_POST['tipoFactura']='GENERAL';

	while($datos=mysql_fetch_assoc($consulta)){
		$_POST['referencia']=$referencia;
		if($_POST['origen']=='CONTRATO'){
			$_POST['codigoContrato']=$datos['codigoContrato'];
			$_POST['codigoVenta']='NULL';
			$alumno = datosRegistro('contratos',$_POST['codigoContrato']);
			$_POST['subtotal']=$datos['importe'];
			$iva=($_POST['subtotal']*21)/100;
			$_POST['iva']=$iva;
			$_POST['coste']=$_POST['subtotal']+$iva;
			$programa = datosRegistro('programas',$alumno['codigoPrograma']);
			$_POST['concepto']=$programa['nombre'];
			$_POST['fechaVencimiento'] = formateaFechaWeb($datos['fecha']);
			$res=insertaDatos('facturacion');
			$codigoFactura=$res;
			$res=consultaBD('UPDATE contratos_facturas SET facturado="'.$codigoFactura.'" WHERE codigo='.$datos['codigo'],true);
			$res = insertaVencimientosFacturas($codigoFactura,$_POST['coste'],$_POST['fechaVencimiento']);
		} else {
			$_POST['codigoContrato']='NULL';
			$_POST['codigoVenta']=$datos['codigoPresupuesto'];
			$venta=datosRegistro('presupuestos',$_POST['codigoVenta']);
			$_POST['subtotal']=$datos['importe'];
			$_POST['iva']=($_POST['subtotal']*21)/100;
			$_POST['coste']=$_POST['subtotal']+$_POST['iva'];
			$fecha=explode('-', $venta['fechaValidez']);
			$referencia=str_pad($venta['referenciaPresupuesto'], 3, "0", STR_PAD_LEFT).'/'.$fecha[1].'-'.$fecha[0];
			$_POST['concepto']='Venta '.$referencia;
			$_POST['fechaVencimiento'] = formateaFechaWeb($datos['fecha']);
			$res=insertaDatos('facturacion');
			$codigoFactura=$res;
			$res=consultaBD('UPDATE presupuestos_facturas SET facturado="'.$codigoFactura.'" WHERE codigo='.$datos['codigo'],true);
			$res = insertaVencimientosFacturas($codigoFactura,$_POST['coste'],$_POST['fechaVencimiento']);
		}
		$referencia++;
	}
	
	return $res;	
}

function insertaVencimientosFacturas($codigoFactura,$importe,$fechaVencimiento){
	$res=true;
	conexionBD();
	$fecha = formateaFechaBD($fechaVencimiento);
	$res=consultaBD("INSERT INTO vencimientos_facturas VALUES(NULL, '".$importe."', '".$fecha."', 'NO', '$codigoFactura',NULL);",true);
	cierraBD();
	return $res;
}

function insertaVencimientosFacturasMensual($codigoFactura,$alumno,$fechaVencimiento){
	$res=true;
	conexionBD();
	$fecha = $fechaVencimiento;
	$gastos=consultaBD('SELECT SUM(precio) AS total FROM contratos_gastos WHERE codigoContrato='.$alumno['codigo'],true,true);
	$alumno['importeCurso']=$alumno['importeCurso']+$gastos['total'];
	$res=consultaBD("INSERT INTO vencimientos_facturas VALUES(NULL, '".$alumno['importeCurso']."', '".$fecha."', 'NO', '$codigoFactura',NULL);");
	cierraBD();
	return $res;
}


function campoReferenciaFactura($datos,$tipoFactura='GENERAL'){
	if($datos){
		$valor=$datos['referencia'];
	}
	else{
		$factura=consultaBD("SELECT MAX(referencia) AS referencia FROM facturacion WHERE tipoFactura='$tipoFactura' AND YEAR(fechaEmision) = '".date('Y')."'",true,true);
		$valor=$factura['referencia']+1;
	}

	$valor=str_pad($valor,3,'0',STR_PAD_LEFT);

	campoTexto('referencia','Nº factura',$valor,'input-small pagination-right',true);
}

function nuevaMetodologia(){
	$facturas=consultaBD('SELECT * FROM facturacion WHERE codigoAlumno IS NOT NULL',true);
	while($factura=mysql_fetch_assoc($facturas)){
		$contrato=consultaBD('SELECT * FROM contratos WHERE codigoAlumno='.$factura['codigoAlumno'].' AND importeCurso="'.$factura['coste'].'";',true,true);
		echo "UPDATE facturacion SET codigoContrato=".$contrato['codigo']." WHERE codigo=".$factura['codigo'].";<br/>";
	}
}

function datosContrato(){
	$contrato=datosRegistro('contratos',$_POST['codigo']);
	echo json_encode($contrato);
}

function insertaGastos($codigoFactura){
	$res=true;
	$i=0;
	$suma=0;
	//$datos=arrayFormulario();
	while(isset($_POST['gasto'.$i])){
		if($_POST['gasto'.$i] != 'NULL'){
			$res=consultaBD("INSERT INTO vencimientos_facturas VALUES(NULL, '".$_POST['precio'.$i]."', '".formateaFechaBD($_POST['fecha'.$i])."', 'NO', '$codigoFactura','".$_POST['gasto'.$i]."');",true);
			$suma=$suma+$_POST['precio'.$i];
		}

		$i++;
	}
	$factura=datosRegistro('facturacion',$codigoFactura);
	$suma=$suma+$factura['coste'];
	$res=consultaBD("UPDATE facturacion SET coste='".$suma."',subtotal='".$suma."' WHERE codigo=".$codigoFactura,true);
	return $res;
}

function insertaGastos2($codigoFactura,$codigoContrato){
	$res=true;
	$i=0;
	$suma=0;
	$gastos=consultaBD('SELECT * FROM contratos_gastos WHERE codigoContrato='.$codigoContrato,true);
	while($gasto=mysql_fetch_assoc($gastos)){
			$res=consultaBD("INSERT INTO vencimientos_facturas VALUES(NULL, '".$gasto['precio']."', '".date('Y-m-d')."', 'NO', '$codigoFactura','".$gasto['codigoGasto']."');",true);
			$suma=$suma+$gasto['precio'];
		$i++;
	}
	$factura=datosRegistro('facturacion',$codigoFactura);
	$suma=$suma+$factura['coste'];
	$res=consultaBD("UPDATE facturacion SET coste='".$suma."',subtotal='".$suma."' WHERE codigo=".$codigoFactura,true);
	return $res;
}

function reasignacionReferencias(){
	$facturas=consultaBD('SELECT * FROM facturacion WHERE tipoFactura="GENERAL" ORDER BY referencia',true);
	$i=1;
	while($factura=mysql_fetch_assoc($facturas)){
		$referencia=$factura['referencia'];
		if($i != $referencia){
			$consulta=consultaBD('UPDATE facturacion SET referencia='.$i.' WHERE codigo='.$factura['codigo'],true);
			echo '----------------------<br/>';
		}
		echo $factura['referencia'].'<br/>';
		$i++;
	}
}

function editarFechas(){
	$vtos=consultaBD('SELECT * FROM vencimientos_facturas WHERE fecha >= "2016-12-01" ORDER BY fecha',true);
	while($vto=mysql_fetch_assoc($vtos)){
		$fecha=$vto['fecha'];
		$arrayFecha=explode('-', $fecha);
		$nuevaFecha=$arrayFecha[0].'-'.$arrayFecha[1].'-02';
		$sql='UPDATE vencimientos_facturas SET fecha="'.$nuevaFecha.'" WHERE codigo='.$vto['codigo'];
		$consulta=consultaBD($sql,true);
		echo '<br/>'.$vto['fecha'].' - '.$nuevaFecha.'<br/>';
	}
}

function marcarCobrados(){
	$recibos=consultaBD('SELECT * FROM vencimientos_remesas ORDER BY codigoVencimiento',true);
	$i=1;
	while($recibo=mysql_fetch_assoc($recibos)){
		$rec=datosRegistro('vencimientos_facturas',$recibo['codigoVencimiento']);
		$remesa=datosRegistro('remesas',$recibo['codigoRemesa']);
		echo $i.' - '.$recibo['codigoVencimiento'].' - '.$rec['cobrado'].' - '.$remesa['codigo'].'<br/>';
		if($remesa['enviadaBanco']=='SI'){
			$consulta=consultaBD('UPDATE vencimientos_facturas SET cobrado="SI" WHERE codigo='.$recibo['codigoVencimiento']);
		}
		$i++;
	}
}

function recogePlazos(){
	$datos=arrayFormulario();
	if($datos['tabla']=='contratos_facturas'){
		$campo='codigoContrato';
	} else {
		$campo='codigoPresupuesto';
	}
	$res='';
	$plazos=consultaBD('SELECT * FROM '.$datos['tabla'].' WHERE '.$campo.'='.$datos['codigo'].' AND facturado IS NULL',true);
	while($plazo=mysql_fetch_assoc($plazos)){
		$res.='<option value="'.$plazo['codigo'].'">'.formateaFechaWeb($plazo['fecha']).' - '.$plazo['importe'].' €</option>';
	}
	echo $res;
}

function enviaEmailFactura($codigo){

	$res=true;

	$datos=datosRegistro('facturacion',$codigo);
	$datosContratos=datosRegistro('contratos',$datos['codigoContrato']);

	if($datos['origen']=='CONTRATO'){
		$datosAlumnos=datosRegistro('trabajadores_cliente',$datosContratos['codigoAlumno']);
	}else{

	}


	$factura='Factura'.$codigo.'.docx';
	
	/*Parte de envío por correo al cliente de la factura*/
	
	$mensajeEnvio="Buenos Días,
	<br /><br />Adjunto remito su Factura.<br /><br />";
	
	$mensajeEnvio.="<img src='http://crmparapymes.com.es/adapta-ms/img/logo2.png'/><br /><br />";
	
	$mensajeEnvio.="<br /><br /><strong>Adapta Management Services</strong><br />
	<strong><i>Ventas</i></strong><br /><br />
	CP 29002 Málaga<br />
	C/ Cuarteles, 7- Planta 2,Oficina 6. (Edificio Guadalmedina)<br /><br />
	+34 952 34 10 07<br />
	+34 637 474 372<br />
	www.adaptams.com<br /><br />";
	
	

	$mensajeEnvio.="
		Este mensaje se dirige exclusivamente a su destinatario y puede contener información privilegiada o confidencial. Si no es vd. el destinatario indicado, queda notificado de que la utilización, divulgación y/o copia sin autorización está prohibida en virtud de la legislación vigente. Si ha recibido este mensaje por error, le rogamos que nos lo comunique inmediatamente por esta misma vía y proceda a su destrucción.
	";
	
	$aleatorio=md5(time());
	$limiteMime="==TecniBoundary_x{$aleatorio}x";
	$headers="From: info@adaptams.com\r\n";
	$headers.= "MIME-Version: 1.0\r\n";
	$headers.="Content-Type: multipart/mixed;" . "boundary=\"{$limiteMime}\"";

	$mensaje="--{$limiteMime}\r\n"."Content-Type: text/html; charset=\"utf-8\"\r\n"."Content-Transfer-Encoding: 7bit\n\n".$mensajeEnvio."\n\n";

	$fp=fopen('../documentos/facturas/'.$factura, "r");
	$tam=filesize('../documentos/facturas/'.$factura);
	$fichero=fread($fp,$tam);
	$ficheroAdjunto=chunk_split(base64_encode($fichero));
	fclose($fp);
	
	$mensaje.="--{$limiteMime}\r\n";
	$mensaje.="Content-Type: application/octet-stream; name=\"".basename('../documentos/facturas/'.$factura)."\"\r\n"."Content-Description:".basename('../documentos/facturas/'.$factura)."\r\n"."Content-Disposition: attachment;filename=\"".basename('../documentos/facturas/'.$factura)."\";size=".$tam."\r\n"."Content-Transfer-Encoding:base64\r\n\r\n".$ficheroAdjunto."\r\n\r\n";

	$mensaje .= "--{$limiteMime}--";
	
    
    if (!mail($datosAlumnos['email'], 'Envío de Factura', $mensaje, $headers)) {
		$res=false;
    } 
	
	/* FIN PARTE ENVIO AL CLIENTE*/
	
	return $res;
}

function mensajeResultadoEnvioFactura($indice,$res,$campo='Factura'){
	if(isset($_GET[$indice])){
		if($res){
		  mensajeOk("$campo enviada correctamente."); 
		}
		else{
		  mensajeError("se ha producido un error al intentar enviar. Comprueba la dirección de correo."); 
		}
	}
}

function tablaParticipantes(){

  echo'   <h3 class="apartadoFormulario">Participantes</h3>
      <table class="table table-striped table-bordered anchoAuto" id="tablaAlumnos">
        <thead>
          <tr>
            <th> Nombre </th>
            <th> Apellidos </th>
            <th> DNI </th>
            <th> eMail </th>
            <th> Teléfono </th>
          </tr>
        </thead>
        <tbody>
        
        <tr>
          <td><input type="text" class="input-large" id="nombre0" name="nombre0"></td>
          <td><input type="text" class="input-large" id="apellidos0" name="apellidos0"></td>
          <td><input type="text" class="input-small" id="dni0" name="dni0"></td>
          <td><input type="text" class="input-large" id="mail0" name="mail0"></td>
          <td><input type="text" class="input-small" id="tlf0" name="tlf0"></td>
        </tr>

        </tbody>
      </table>
      <br>
      <center>
        <button type="button" class="btn btn-success" onclick="insertaFila();"><i class="icon-plus"></i> Añadir Participante</button> 
        <button type="button" class="btn btn-danger" onclick="eliminaFila();"><i class="icon-minus"></i> Eliminar Participante</button> 
      </center>';

}

function insertaParticipantes($datos, $codigoFactura){

	conexionBD();

	$i=0;
	//NUEVO PARA VENTA DIRECTA DE CURSO
	while(isset($datos['nombre'.$i])){
		$consulta=consultaBD("INSERT INTO participantes_factura(codigo,nombre,apellidos,dni,mail,telefono,codigoFactura) VALUES(NULL,'".$datos['nombre'.$i]."','".$datos['apellidos'.$i]."','".$datos['dni'.$i]."','".$datos['mail'.$i]."','".$datos['tlf'.$i]."','".$codigoFactura."');");
		$codigoAlumno=mysql_insert_id();

		$i++;
	}

	cierraBD();

}

//Fin parte de facturas