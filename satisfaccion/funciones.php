<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de satisfacción
function creaDatosEstadisticasSatisfaccion(){
	$datos=array();

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM satisfaccion;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function obtieneURLCuestionario(){
	return "http://".$_SERVER['HTTP_HOST']."/adapta-ms/cuestionario-de-satisfaccion";
}

function imprimeEncuestasSatisfaccion(){
	global $_CONFIG;
	conexionBD();
	$codigoS=$_SESSION['codigoU'];
	$consulta=consultaBD("SELECT codigo, fecha, cliente FROM satisfaccion ORDER BY fecha;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){

		$fecha=formateaFechaWeb($datos['fecha']);
		echo "
		<tr>        	
        	<td> ".$datos['cliente']." </td>
			<td> $fecha </td>
        	<td class='centro'>
        		<a href='".$_CONFIG['raiz']."satisfaccion/gestion.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-search-plus'></i> Ver datos</i></a>
				<a href='".$_CONFIG['raiz']."satisfaccion/estadisticasIndividual.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-bar-chart'></i> Estadísticas</i></a>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function generaDatosGraficoEncuestasSatisfaccion(){
	$datos=array('1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0);

	conexionBD();

	$consulta=consultaBD("SELECT * FROM satisfaccion;");
	$datosConsulta=mysql_fetch_assoc($consulta);
	
	while($datosConsulta!=0){
		$datos[$datosConsulta['pregunta1']]++;
		$datos[$datosConsulta['pregunta2']]++;
		$datos[$datosConsulta['pregunta3']]++;
		$datos[$datosConsulta['pregunta4']]++;
		$datos[$datosConsulta['pregunta5']]++;
		$datos[$datosConsulta['pregunta6']]++;
		$datos[$datosConsulta['pregunta7']]++;
		$datosConsulta=mysql_fetch_assoc($consulta);
	}

	cierraBD();

	return $datos;
}

function creaEncuestaSatisfaccion(){
	$res=true;
	
	$datos=arrayFormulario();
	
	conexionBD();
	$consulta=consultaBD("INSERT INTO satisfaccion VALUES(NULL, '".$datos['cliente']."', '".$datos['fecha']."', '".$datos['puntuacion1']."', '".$datos['puntuacion2']."', '".$datos['puntuacion3']."', 
	'".$datos['puntuacion4']."', '".$datos['puntuacion5']."', '".$datos['puntuacion6']."', '".$datos['puntuacion7']."', '".$datos['comentarios']."');");

	if(!$consulta){
		$res=false;
	}
	return $res;
}

function datosEncuesta($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM satisfaccion WHERE codigo='$codigo';");
	cierraBD();

	return mysql_fetch_assoc($consulta);
}

function actualizaEncuestaSatisfaccion(){
	$codigoU=$_SESSION['codigoS'];
	$res=true;
	
	$datos=arrayFormulario();
	
	conexionBD();
	$consulta=consultaBD("UPDATE satisfaccion SET cliente='".$datos['cliente']."', fecha='".$datos['fecha']."', pregunta1='".$datos['puntuacion1']."', pregunta2='".$datos['puntuacion2']."', pregunta3='".$datos['puntuacion3']."', 
	pregunta4='".$datos['puntuacion4']."', pregunta5='".$datos['puntuacion5']."', pregunta6='".$datos['puntuacion6']."', pregunta7='".$datos['puntuacion7']."', comentarios='".$datos['comentarios']."'
	WHERE codigo='".$datos['codigo']."';");

	if(!$consulta){
		$res=false;
	}
	return $res;
}

function datosPorcentajes(){
		conexionBD();
		$consulta=consultaBD("SELECT * FROM satisfaccion");
		cierraBD();
	return mysql_fetch_assoc($consulta);
}

function creaPreguntaPorcentajesClientes($num,$texto,$datos){
	
	conexionBD();
		$totalpregunta=consultaBD("SELECT COUNT(pregunta$num) AS totalpregunta FROM satisfaccion;");
		$porcentajeTotalDesacuerdo=consultaBD("SELECT COUNT(pregunta$num) AS porcentajeTotalDesacuerdo FROM satisfaccion WHERE pregunta$num='1';");
		$porcentajeMuyPoco=consultaBD("SELECT COUNT(pregunta$num) AS porcentajeMuyPoco FROM satisfaccion WHERE pregunta$num='2';");
		$porcentajeNormal=consultaBD("SELECT COUNT(pregunta$num) AS porcentajeNormal FROM satisfaccion WHERE pregunta$num='3';");
		$porcentajeBastante=consultaBD("SELECT COUNT(pregunta$num) AS porcentajeBastante FROM satisfaccion WHERE pregunta$num='4';");
		$porcentajeMucho=consultaBD("SELECT COUNT(pregunta$num) AS porcentajeMucho FROM satisfaccion WHERE pregunta$num='5';");
		
		$totalpregunta=mysql_fetch_assoc($totalpregunta);
		$porcentajeTotalDesacuerdo=mysql_fetch_assoc($porcentajeTotalDesacuerdo);
		$porcentajeMuyPoco=mysql_fetch_assoc($porcentajeMuyPoco);
		$porcentajeNormal=mysql_fetch_assoc($porcentajeNormal);
		$porcentajeBastante=mysql_fetch_assoc($porcentajeBastante);
		$porcentajeMucho=mysql_fetch_assoc($porcentajeMucho);
		
		cierraBD();
		
		if($totalpregunta['totalpregunta']!=0){
			$nsnc=round((($porcentajeTotalDesacuerdo['porcentajeTotalDesacuerdo']*100)/$totalpregunta['totalpregunta']),1);
			$muypoco=round((($porcentajeMuyPoco['porcentajeMuyPoco']*100)/$totalpregunta['totalpregunta']),1);
			$poco=round((($porcentajeNormal['porcentajeNormal']*100)/$totalpregunta['totalpregunta']),1);
			$bastante=round((($porcentajeBastante['porcentajeBastante']*100)/$totalpregunta['totalpregunta']),1);
			$mucho=round((($porcentajeMucho['porcentajeMucho']*100)/$totalpregunta['totalpregunta']),1);
		}else{
			$nsnc=0;
			$muypoco=0;
			$poco=0;
			$bastante=0;
			$mucho=0;
		}
		
		$positivo=($porcentajeBastante['porcentajeBastante']+$porcentajeMucho['porcentajeMucho'])*100;
		$total=$porcentajeBastante['porcentajeBastante']+$porcentajeMucho['porcentajeMucho']+$porcentajeNormal['porcentajeNormal']+$porcentajeMuyPoco['porcentajeMuyPoco']+$porcentajeTotalDesacuerdo['porcentajeTotalDesacuerdo'];
		if($total!=0){
			$global=round($positivo/$total,1);
		}else{
			$global=0;
		}
		if($global<60){
			$valoracion='MUY BAJA';
		}
		elseif($global<80){
			$valoracion='BAJA';
		}
		else{
			$valoracion='ALTA';
		}

		echo "
		<tr>
			<td rowspan='7'>$texto</td>
		</tr>
		<tr>
			<td><span class='label label-muyP'>Totalmente en desacuerdo</span></td><td>$nsnc %</td>
		</tr>
		<tr>
			<td><span class='label label-poc'>En desacuerdo</span></td><td>$muypoco %</td>
		</tr>
		<tr>
			<td><span class='label label-ns'>Ni de acuerdo ni en desacuerdo</span></td><td>$poco %</td>
		</tr>
		<tr>
			<td><span class='label label-sa'>De acuerdo</span></td><td>$bastante %</td>
		</tr>
		<tr>
			<td><span class='label label-muyS'>Totalmente de acuerdo</span></td><td>$mucho %</td>
		</tr>
		<tr>
			<th class='asterisco separadorTabla'>VALORACIÓN GLOBAL:</th><th class='asterisco separadorTabla'><span class='valorGlobal'>$global</span> % ($valoracion)</th>
		</tr>";

}

function datosPorcentajesIndividual($codigo){
	
	conexionBD();
	
	$consulta=consultaBD("SELECT pregunta1, pregunta2, pregunta3, pregunta4, pregunta5, pregunta6, pregunta7 
	FROM satisfaccion WHERE codigo=$codigo;");
	
	$consulta=mysql_fetch_assoc($consulta);
	
	cierraBD();
	
	$total=7;
	$totalSatisfechos=0;
	
	$i=1;
	
	while($i<=7){
		if($consulta['pregunta'.$i]=='5'||$consulta['pregunta'.$i]=='4'){
			$totalSatisfechos++;
		}
		$i++;
	}
	
	$totalSatisfechos=$totalSatisfechos*100;
	
	$global=round($totalSatisfechos/$total,1);
	
	return $global;
	
}

function operacionesEncuesta(){	
  if(isset($_POST['codigo'])){
    $res=actualizaDatos('satisfaccion');
  }
  
  if(isset($_POST['codigo'])){
	  if($res){
		mensajeOk("Encuesta actualizada correctamente");
	  }
	  else{
		mensajeError("no se han podido actualizar la Encuesta. Compruebe los datos introducidos.");
	  }
	}
	elseif(isset($_POST['puntuacion1'])){
		if($res){
			mensajeOk("Encuesta registrada correctamente");
		}
		else{
			mensajeError("no se han podido registrar la Encuesta. Compruebe los datos introducidos.");
		}
	}
}

function gestionEncuesta(){
	operacionesEncuesta();
	
	abreVentanaGestion('Gestión de Encuestas','?');
	$datos=compruebaDatos('satisfaccion');
	
	echo '
	<div class="control-group" id="cliente">                     
	  <label class="control-label" for="cliente">Cliente:</label>
	  <div class="controls">
		<input type="text" class="input-large" id="cliente" name="cliente" value="'. $datos['cliente'] .'">
		<input type="hidden" name="codigo" value="<?php echo $codigo ?>">
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->
	

	<div class="control-group">                     
	  <label class="control-label" for="fecha">Fecha:</label>
	  <div class="controls">
		<input type="text" class="input-small datepicker hasDatepicker" id="fecha" name="fecha" value="'.formateaFechaWeb($datos['fecha']).'">
	  </div> <!-- /controls -->       
	</div> <!-- /control-group --> <br />
	
	<p class="justificado parrafo-margenes" > <strong>Estimado Cliente.</strong> En Avefor nos preocupa mucho su satisfacción con los servicios

		contratados con nuestra entidad y la calidad final de los proyectos ejecutados. Por ello, le agradecemos que pueda 

		hacernos llegar la presente encuesta de satisfacción, indicando además en el apartado observaciones cualquier 

		comentario que crea que pueda ayudarnos a mejorar la calidad de nuestros servicios o a ajustar los mismos a sus 

		necesidades. Sin duda, creemos firmemente que en sus opiniones y sugerencias se encuentran nuestras mayores 

		oportunidades de mejorar. Gracias por anticipado.</p><br /><br />';
	
	
		creaTablaSatisfaccion(); 
		creaPreguntaTablaEncuesta("Asistencia Previa a la contratación de nuestra empresa (Atención, solicitud de información inicial, facilidad de contacto).",1,$datos['pregunta1']);
		creaPreguntaTablaEncuesta("Documentación Recibida ( Propuestas, Ofertas, Facturas Proformas) y explicaciones /atenciones recibidas sobre la misma.",2,$datos['pregunta2']);
		creaPreguntaTablaEncuesta("Atención Telefónica / Administrativa.",3,$datos['pregunta3']);
		creaPreguntaTablaEncuesta("Cumplimientos de plazos",4,$datos['pregunta4']);
		creaPreguntaTablaEncuesta("Asistencia ante incidencias.",5,$datos['pregunta5']);
		creaPreguntaTablaEncuesta("Valoración de los Servicios Prestados / Productos Adquiridos",6,$datos['pregunta6']);
		creaPreguntaTablaEncuesta("En general, su satisfacción con los nuestra organización es",7,$datos['pregunta7']);

		echo '
		<td colspan="2">A continuación, incluya cuantos comentarios o sugerencias estime oportuno para aclarar o ampliar las cuestiones anteriores:</td>	
		<tr>
			<td colspan="2"><textarea rows="10" class="textarea-amplia" name="comentarios" id="comentarios">'.$datos['comentarios'].'</textarea></td>	
		</tr>';
		
		cierraTablaSatisfaccion();
	
	cierraVentanaGestion('index.php');
}

//Fin parte de satisfacción