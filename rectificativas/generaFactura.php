<?php
    session_start();
    include_once("funciones.php");
    compruebaSesion();

    $contenido=generaPDFFacturaNuevo($_GET['codigo']);

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Factura.pdf');
?>