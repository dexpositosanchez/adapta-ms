<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores

function operacionesSubcategorias(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('subcategorias');
	}
	elseif(isset($_POST['subcategoria'])){
		$res=insertaDatos('subcategorias');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('subcategorias');
	}

	mensajeResultado('subcategoria',$res,'Subcategoría');
    mensajeResultado('elimina',$res,'Subategoría', true);
}

function listadoSubcategorias(){
	global $_CONFIG;

	$columnas=array('id','subcategoria','id','id');
	$having=obtieneWhereListado("WHERE 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT * FROM subcategorias $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT * FROM subcategorias $having;");
	cierraBD();
	
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['id'],
			$datos['subcategoria'],
			creaBotonDetalles("subcategorias/gestion.php?codigo=".$datos['codigo']),
        	"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionSubcategorias(){
	operacionesSubcategorias();

	abreVentanaGestion('Gestión de Subcategorías','index.php','span3');
	$datos=compruebaDatos('subcategorias');
	if($datos){
		$id=$datos['id'];
	} else {
		$id=generaNumeroReferencia('subcategorias','id');
	}
	abreColumnaCampos('span11');
		campoTexto('id','ID',$id,'input-small');
		campoTexto('subcategoria','Subcategoría',$datos);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}



function filtroCursos(){
	abreCajaBusqueda();
		campoTexto(0,'ID');
		campoTexto(1,'Subcategoría');
	cierraCajaBusqueda();
}