<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de clientes

function operacionesClientes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaCliente();
	}
	elseif(isset($_POST['nombre'])){
		$res=creaCliente();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('clientes');
	}

	mensajeResultado('nombre',$res,'Cliente');
    mensajeResultado('elimina',$res,'Cliente', true);
}

function creaCliente(){
	$res=true;
	$res=insertaDatos('clientes');
	$_POST['codigo']=mysql_insert_id();
	$res=insertaHistorico();	
	return $res;
}

function actualizaCliente(){
	$res=true;
	$res=actualizaDatos('clientes');
	$res=insertaHistorico();
	return $res;
}

function insertaHistorico(){
	$res=true;
	$i=1;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM historico_clientes WHERE codigoCliente='".$_POST['codigo']."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['observaciones'.$i]) && $datos['observaciones'.$i]!=""){
		$res = $res && consultaBD("INSERT INTO historico_clientes VALUES (NULL,'".$datos['fecha'.$i]."',
			'".$datos['gestion'.$i]."','".$datos['observaciones'.$i]."','".$_POST['codigo']."');");

		$i++;
	}

	return $res;
}

function eliminaDatosCliente($tabla,$codigoCliente,$actualizacion){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM $tabla WHERE codigoCliente=$codigoCliente");
	}

	return $res;
}

function listadoClientes(){
	global $_CONFIG;

	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');

	//fechaAlta lo utilizo para el filtro por ejercicios
	$columnas=array('nombre','denominacionSocial','nif','telefono1','email1','programa','nombre','nombre');
	$where=obtieneWhereListado("WHERE 1=1",$columnas,true,true);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, denominacionSocial, nif, telefono1, email1, programa FROM clientes $where AND activo='NO' $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, nombre, denominacionSocial, nif, telefono1, email1, programa FROM clientes $where AND activo='NO';");

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){

		$fila=array(
			"<a href='gestion.php?codigo=".$datos['codigo']."'>".$datos['nombre']."</a>",
			$datos['denominacionSocial'],
			$datos['nif'],
			"<a href='tel:".$datos['telefono1']."' class='nowrap'>".formateaTelefono($datos['telefono1'])."</a>",
			"<a href='mailto:".$datos['email1']."'>".$datos['email1']."</a>",
			$datos['programa'],
			botonAcciones(array('Detalles','Pasar a cliente'),array('posibles_clientes/gestion.php?codigo='.$datos['codigo'],'clientes/index.php?posibleCliente='.$datos['codigo']),array("icon-search-plus",'icon-check'),array(0,0,0),false,''),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}
	cierraBD();

	echo json_encode($res);
}

function gestionClientes(){
	operacionesClientes();

	abreVentanaGestion('Gestión de Clientes','index.php');
	$datos=compruebaDatos('clientes');

	compruebaMensajeConfirmarCliente();
	
	echo "<h4 class='apartadoFormulario sinFlotar'>DATOS DEL CLIENTE</h4>";
	abreColumnaCampos();
		campoOculto($datos,'activo','NO');
		campoTexto('nombre','Nombre',$datos);
		campoTexto('nif','NIF',$datos,'input-small');
		campoTexto('numeroEmpleados','Número de empleados',$datos);
		campoTexto('importe','Importe oportunidades ganadas',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('denominacionSocial','Denominación social',$datos);
		campoTexto('sector','Sector',$datos);
		campoTexto('importancia','Importancia',$datos);
		campoTexto('programa','Programa',$datos);
	cierraColumnaCampos();

	echo "<h4 class='apartadoFormulario sinFlotar'>DATOS DE CONTACTO</h4>";
	abreColumnaCampos();
		campoTextoSimbolo('email1','Email 1','<i class="icon-envelope"></i>',$datos,'input-large');
		campoTextoSimbolo('telefono1','Teléfono 1','<i class="icon-phone"></i>',$datos,'input-large');
		campoTexto('direccion1','Dirección 1',$datos);
		campoTexto('website1','Website 1',$datos);
		campoTexto('fax','Fax',$datos);
		campoTextoSimbolo('linkedin','URL LinkedIn','<i class="icon-linkedin"></i>',$datos,'input-large');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTextoSimbolo('email2','Email 2','<i class="icon-envelope"></i>',$datos,'input-large');
		campoTextoSimbolo('telefono2','Teléfono 2','<i class="icon-phone"></i>',$datos,'input-large');
		campoTexto('direccion2','Dirección 2',$datos);
		campoTexto('website2','Website 2',$datos);
		campoTextoSimbolo('facebook','URL Facebook','<i class="icon-facebook"></i>',$datos,'input-large');
		campoTextoSimbolo('twitter','URL Twitter','<i class="icon-twitter"></i>',$datos,'input-large');
	cierraColumnaCampos();
	
	echo "<h4 class='apartadoFormulario sinFlotar'>OTROS</h4>";
	areaTexto('descripcion','Descripción',$datos,'areaInforme');
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');

	echo "<h4 class='apartadoFormulario sinFlotar'>HISTÓRICO</h4>";
	abreColumnaCampos('span11');
		tablaHistorico($datos);
	cierraColumnaCampos();
	
	cierraVentanaGestion('index.php',true);
}

function compruebaMensajeConfirmarCliente(){
	if(isset($_GET['confirmar'])){
		mensajeAdvertencia("complete los campos obligatorios para confirmar el cliente.");
	}
}


function filtroClientes(){
	abreCajaBusqueda();
	abreColumnaCampos();

	$columnas=array('nombre','denominacionSocial','nif','telefono1','email1','programa');

	campoTexto(0,'Nombre');
	campoTexto(1,'Denominación social');
	campoTexto(2,'NIF','','input-small');
	

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(3,'eMail');
	campoTexto(4,'Teléfono',false,'input-small pagination-right');
	campoTexto(5,'Programa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}



function obtieneNombreCliente($codigoCliente){
	$datos=consultaBD("SELECT razonSocial FROM clientes WHERE codigo=$codigoCliente",true,true);

	return "<a href='gestion.php?codigo=$codigoCliente'>".$datos['razonSocial']."</a>";
}


function creaRegistroExtra($query){
	$res='fallo';

	conexionBD();
	
	$consulta=consultaBD($query);

	if($consulta){
		$res=mysql_insert_id();
	}

	cierraBD();

	echo $res;
}

function campoCuentaBancaria($valor){
	$valor=compruebaValorCampo($valor,'numCuenta');
	echo '<div class="control-group">                     
	  <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
	  <div class="controls numSS">
		<div class="input-prepend input-append">
		  <input type="text" class="input-mini" id="iban" name="iban" maxlength="4" size="4" value="'.substr($valor, 0, 4).'">
		  <span class="add-on">/</span>
		  <input type="text" class="input-medium" id="numCuenta" name="numCuenta" maxlength="20" size="20" value="'.substr($valor, 4, 24).'">
		</div>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->';
}

function tablaHistorico($datos){
	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaHistorico'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Fecha </th>
							<th> Gestión </th>
							<th> Observaciones </th>
							<th> </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=1;
	if($datos){
		$consulta=consultaBD("SELECT * FROM historico_clientes WHERE codigoCliente='".$datos['codigo']."';", true);				  
		while($datosHistorico=mysql_fetch_assoc($consulta)){
		 	echo "<tr>";
				campoFechaTabla('fecha'.$i,$datosHistorico['fecha']);
				campoSelect('gestion'.$i, '', array('Email', 'Llamada', 'Visita'), array('email','llamada','visita'), $datosHistorico['gestion'], '', "", 1);
				areaTextoTabla('observaciones'.$i,$datosHistorico['observaciones']);
			echo"
					<td>
						<input type='checkbox' name='filasTabla[]' value='$i'>
		        	</td>
				</tr>";
			$i++;
		}
	}

	if($i==1 || !$datos){
	 	echo "<tr>";
			campoFechaTabla('fecha1');
			campoSelect('gestion1', '', array('Email', 'Llamada', 'Visita'), array('email','llamada','visita'), false, '', "", 1);
			areaTextoTabla('observaciones1','','areaTextoTabla');
		echo"
				<td>
					<input type='checkbox' name='filasTabla[]' value='$i'>
	        	</td>
			</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaHistorico\");'><i class='icon-plus'></i> Añadir</button> 
	              <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaHistorico\");'><i class='icon-minus'></i> Eliminar</button> 
	            </center>
		 	";
}

//Fin parte de clientes