        function CalcularIBAN(numerocuenta, codigopais) {
            //Conversi�n de letras por n�meros
            //A=10 B=11 C=12 D=13 E=14
            //F=15 G=16 H=17 I=18 J=19
            //K=20 L=21 M=22 N=23 O=24
            //P=25 Q=26 R=27 S=28 T=29
            //U=30 V=31 W=32 X=33 Y=34
            //Z=35
            
            if (codigopais.length != 2)
                return "";
            else {
                var Aux;
                var CaracteresSiguientes;
                var TmpInt;
                var CaracteresSiguientes;

                numerocuenta = numerocuenta + (codigopais.charCodeAt(0) - 55).toString() + (codigopais.charCodeAt(1) - 55).toString() + "00";

                //Hay que calcular el m�dulo 97 del valor contenido en n�mero de cuenta
                //Como el n�mero es muy grande vamos calculando m�dulos 97 de 9 en 9 d�gitos
                //Lo que se hace es calcular el m�dulo 97 y al resto se le a�aden 7 u 8 d�gitos en funci�n de que el resto sea de 1 � 2 d�gitos
                //Y as� sucesivamente hasta tratar todos los d�gitos

                TmpInt = parseInt(numerocuenta.substring(0, 9), 10) % 97;

                if (TmpInt < 10)
                    Aux = "0";
                else
                    Aux = "";

                Aux=Aux + TmpInt.toString();
                numerocuenta = numerocuenta.substring(9);

                while (numerocuenta!="") {
                    if (parseInt(Aux, 10) < 10)
                        CaracteresSiguientes = 8;
                    else
                        CaracteresSiguientes = 7;

                    if (numerocuenta.length<CaracteresSiguientes) {
                        Aux=Aux + numerocuenta;
                        numerocuenta="";
                    }
                    else {
                        Aux=Aux + numerocuenta.substring(0, CaracteresSiguientes);
                        numerocuenta=numerocuenta.substring(CaracteresSiguientes);
                    }

                    TmpInt = parseInt(Aux, 10) % 97;

                    if (TmpInt < 10)
                        Aux = "0";
                    else
                        Aux = "";

                    Aux=Aux + TmpInt.toString();
                }

                TmpInt = 98 - parseInt(Aux, 10);

                if (TmpInt<10)
                    return codigopais + "0" + TmpInt.toString();
                else
                    return codigopais + TmpInt.toString();

            }
        }