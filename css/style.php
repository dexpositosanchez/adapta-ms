<?php
include_once('../config.php');
header("Content-type: text/css; charset: UTF-8");
?>

/*------------------------------------------------------------------
Estilos específicos de la interfaz
------------------------------------------------------------------*/



/*------------------------------------------------------------------
[1. Global]
*/

html{
	height:100%;/* Para que el pie se quede siempre abajo */
}

body {
	background: #fAfAfA;
	font: 13px/1.7em 'Open Sans';
/* Las siguientes 2 líneas sirven para que el pie se quede siempre abajo */
	min-height:100%;
	position:relative;
}

#contenido{
	padding-bottom:70px;/* Para que el pie se quede siempre abajo */
}
    
p { 
	font: 13px/1.7em 'Open Sans'; 	
}
    
input,
button,
select,
textarea {
  font-family: 'Open Sans';
}

.dropdown .dropdown-menu {
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
}

.btn-icon-only {
	padding-right: 3px;
	padding-left: 3px;
}

.table td {
	vertical-align: middle;
}

.table-bordered th {
	background: #F5F5F5;

	
	font-size: 10px;
	color: #444;
	text-transform: uppercase;
}




.logo{
	margin-top:0;
	margin-bottom: 0;
	text-align: center;
}


/*------------------------------------------------------------------
[2. Navbar / .navbar]
*/

.navbar .container {
	position: relative;
}

.navbar-inner {
	padding: 7px 0;
	
	background: <?php echo $_CONFIG['colorPrincipal']; ?> !important;
	
	-moz-border-radius: 0;
	-webkit-border-radius: 0;
	border-radius: 0;
}

.navbar-fixed-top {
	position: static;
}

.navbar .nav a {
	font-size: 11px;
}
.navbar .nav>li>a { color:#fff !important;}
.navbar .brand {
	font-weight: 600;
	position: relative;
	top: 2px;
}

.navbar .search-query {
	background-color: #444;
	width: 150px;
	font-size: 11px;
	font-weight: bold;
}

.navbar .search-query::-webkit-input-placeholder {
    color: #666;
}

.navbar .search-query:-moz-placeholder {
    color: #666;
}

.navbar-search .search-query { background:#008866; border:0; color:#fff; line-height:normal;}


/*------------------------------------------------------------------
[3. Subnavbar / .subnavbar]
*/

.subnavbar {
    	margin-bottom: 2.5em;
    }
    
.subnavbar-inner {
	height: 60px;
	background: #fff;
	border-bottom: 1px solid #d6d6d6;
}

.subnavbar .container > ul {
	display: inline-block;
	
	height: 80px;
	padding: 0;
	margin: 0;
	
}

.subnavbar .container > ul > li {
	float: left;
	
	min-width: 85px;
    height: 60px;
	padding: 0;
	margin: 0;
	
	text-align: center;
	list-style: none;
	
	border-left: 1px solid #d9d9d9;
	
	
}

.subnavbar .container > ul > li > a {
	display: block;
	
	height: 100%;
	padding: 0 5px;
	
	font-size: 11px;
	font-weight: bold;
	color: #888;
}

.subnavbar .container > ul > li > a:hover {
	color: #444;
	text-decoration: none;
}

.subnavbar .container > ul > li > a > i {
	display: inline-block;
	
	width: 24px;
	height: 24px;
	margin-top: 11px;
	margin-bottom: -3px;
	font-size: 20px;
}

.subnavbar .container > ul > li > a > span {
	display: block;
	
}


.subnavbar .container > ul > li.active > a {
	
	border-bottom:3px solid <?php echo $_CONFIG['colorPrincipal']; ?>;
	color: #383838;
}
    

.subnavbar .dropdown .dropdown-menu a {
	font-size: 12px;
}

    
.subnavbar .dropdown .dropdown-menu {
    	text-align: left;
    	
		-webkit-border-top-left-radius: 0;
		-webkit-border-top-right-radius: 0;
		-moz-border-radius-topleft: 0;
		-moz-border-radius-topright: 0;
		border-top-left-radius: 0;
		border-top-right-radius: 0;
    }
    
    
    
.subnavbar .dropdown-menu::before {
	content: '';
	display: inline-block;
	border-left: 7px solid transparent;
	border-right: 7px solid transparent;
	border-bottom: 7px solid #CCC;
	border-bottom-color: rgba(0, 0, 0, 0.2);
	position: absolute;
	top: -7px;
	left: 9px;
}

.subnavbar .dropdown-menu::after {
	content: '';
	display: inline-block;
	border-left: 6px solid transparent;
	border-right: 6px solid transparent;
	border-bottom: 6px solid white;
	position: absolute;
	top: -6px;
	left: 10px;
}


.subnavbar .caret {
	margin-top: 4px;
	
	border-top-color: white;
	border-bottom-color: white;
}

.subnavbar .dropdown.open .caret {
	display: none;
}





/*------------------------------------------------------------------
[4. Main / .main]
*/

.main {
	padding-bottom: 2em;
}



/*------------------------------------------------------------------
[5. Extra / .extra]
*/

.extra {

	border-top: 1px solid #585858;
	border-bottom: 1px solid #000;

}

.extra-inner {
	padding: 20px 0;
	
	font-size: 11px;
	color: #BBB;
	
	background: #1A1A1A;
}

.extra a {
	color: #666;
}

.extra h4 {
	margin-bottom: 1em;
	
	font-weight: 400;
}

.extra ul {
	padding: 0;
	margin: 0;
}

.extra li {
	margin-bottom: .6em;
	
	list-style: none;
}




/*------------------------------------------------------------------
[6. Footer/ .footer]
*/

.footer {/* Para que el pie se quede siempre abajo */
	text-align: center;
	width:100%;
	height:52px;
	position:absolute;
	bottom:0;
	left:0;
	border-top: 1px solid <?php echo $_CONFIG['colorSecundario']; ?>;
}

.footer-inner {

	padding: 15px 0;
	
	font-size: 12px;
	background: <?php echo $_CONFIG['colorPrincipal']; ?>;
	color: #FFF;
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}

.footer a {
	color: #FFF;
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	font-weight: bold;
}

.footer a:hover {
	color: #FFF;
	text-decoration: underline;
}


/*------------------------------------------------------------------
[6. Widget / .widget]
*/

.widget, .modal-body {

	position: relative;
	clear: both;
	
	width: auto;
	
	margin-bottom: 2em;
		
	overflow: visible !important;
}

.modal{
	overflow: visible !important;	
}
	
.widget-header {
	
	position: relative;
	
	height: 40px;
	line-height: 40px;
	
	background: <?php echo $_CONFIG['colorPrincipal']; ?>;	
}	
	
	.widget-header h3 {
		
		position: relative;
		top: 2px;
		left: 10px;
		
		display: inline-block;
		margin-right: 3em;
		
		font-size: 14px;
		font-weight: 800;
		color: #FFF;
		line-height: 18px;
		
		
	}
	
		.widget-header [class^="icon-"], .widget-header [class*=" icon-"] {
			
			display: inline-block;
			margin-left: 13px;
			margin-right: -2px;
			
			font-size: 16px;
			color: #FFF;
			vertical-align: middle;
			
			
			
		}




.widget-content {
	padding: 20px 15px 15px;
	
	background: #FFF;
	
	
	border: 1px solid <?php echo $_CONFIG['colorPrincipal']; ?>;
	
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}

.widget-header+.widget-content {
	border-top: none;
	
	-webkit-border-top-left-radius: 0;
	-webkit-border-top-right-radius: 0;
	-moz-border-radius-topleft: 0;
	-moz-border-radius-topright: 0;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
}

.widget-nopad .widget-content {
	padding: 0;
}

/* Widget Content Clearfix */	
.widget-content:before,
.widget-content:after {
    content:"";
    display:table;
}

.widget-content:after {
    clear:both;
}

/* For IE 6/7 (trigger hasLayout) */
.widget-content {
    zoom:1;
}

/* Widget Table */

.widget-table .widget-content {
	padding: 0;
}

.widget-table .table {
	margin-bottom: 0;
	
	border: none;
}

.widget-table .table tr td:first-child {
	border-left: none;
}

.widget-table .table tr th:first-child {
	border-left: none;
}


/* Widget Plain */

.widget-plain {
	
	background: transparent;
	
	border: none;
}

.widget-plain .widget-content {
	padding: 0;
	
	background: transparent;
	
	border: none;
}


/* Widget Box */

.widget-box {	
	
}

.widget-box .widget-content {	
	background: #E3E3E3;	
	background: #FFF;
}




/*------------------------------------------------------------------
[7. Error / .error-container]
*/

.error-container {
	margin-top: 4em;
	margin-bottom: 4em;
	text-align: center;
}

.error-container h1 {
	margin-bottom: .5em;
	
	font-size: 120px;
	line-height: 1em;
}

.error-container h2 {
	margin-bottom: .75em;
	font-size: 28px;
}

.error-container .error-details {
	margin-bottom: 1.5em;
	
	font-size: 16px;
}

.error-container .error-actions a {
	margin: 0 .5em;
}



/* Message layout */


ul.messages_layout {
	position: relative;
	margin: 0;
	padding: 0
}
ul.messages_layout li {
	float: left;
	list-style: none;
	position: relative
}
ul.messages_layout li.left {
	padding-left: 75px
}
ul.messages_layout li.right {
	padding-right: 75px
}
ul.messages_layout li.right .avatar {
	right: 0;
	left: auto
}
ul.messages_layout li.right .message_wrap .arrow {
	right: -12px;
	left: auto;
	background-position: 0 -213px;
	height: 15px;
	width: 12px
}
ul.messages_layout li.by_myself .message_wrap {
	border: 1px solid #b3cdf8
}
ul.messages_layout li.by_myself .message_wrap .info a.name {
	color: #4a8cf7
}
ul.messages_layout li a.avatar {
	position: absolute;
	left: 0;
	top: 0
}
ul.messages_layout li a.avatar img {
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px
}
ul.messages_layout li .message_wrap {
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	position: relative;
	border: 1px solid #e9e9e9;
	padding: 10px;
	border: 1px solid #cbcbcb;
	margin-bottom: 20px;
	float: left;
	background: #fefefe;
	-webkit-box-shadow: rgba(0,0,0,0.1) 0 1px 0px;
	-moz-box-shadow: rgba(0,0,0,0.1) 0 1px 0px;
	box-shadow: rgba(0,0,0,0.1) 0 1px 0px
}
ul.messages_layout li .message_wrap .arrow {
	background-position: 0 -228px;
	height: 15px;
	width: 12px;
	height: 15px;
	width: 12px;
	position: absolute;
	left: -12px;
	top: 13px
}
ul.messages_layout li .message_wrap .info {
	float: left;
	width: 100%;
	border-bottom: 1px solid #fff;
	line-height: 23px
}
ul.messages_layout li .message_wrap .info .name {
	float: left;
	font-weight: bold;
	color: #483734
}
ul.messages_layout li .message_wrap .info .time {
	float: left;
	font-size: 11px;
	margin-left: 6px
}
ul.messages_layout li .message_wrap .text {
	float: left;
	width: 100%;
	border-top: 1px solid #cfcfcf;
	padding-top: 5px
}

ul.messages_layout .dropdown-menu  li{ width:100%; font-size:11px;}


/* Full Calendar */

.fc {
	direction: ltr;
	text-align: left;
	position: relative
}
.fc table {
	border-collapse: collapse;
	border-spacing: 0
}
html .fc, .fc table {
	font-size: 1em
}
.fc td, .fc th {
	padding: 0;
	vertical-align: top
}
.fc-header td {
	white-space: nowrap;
	background: none
}
.fc-header-left {
	width: 100%;
	text-align: left;
	position: absolute;
	left: 0;
	top: 6px
}
.fc-header-left .fc-button {
	margin: 0;
	position: relative
}
.fc-header-left .fc-button-prev, .fc-header-left .fc-button-next {
	float: left;
	border: none;
	padding: 14px 10px;
	opacity: 0.5
}
.fc-header-left .fc-button-prev .fc-button-inner, .fc-header-left .fc-button-next .fc-button-inner {
	border: none
}
.fc-header-left .fc-button-prev .fc-button-inner .fc-button-content, .fc-header-left .fc-button-next .fc-button-inner .fc-button-content {
	display: none
}
.fc-header-left .fc-button-prev.fc-state-hover, .fc-header-left .fc-button-next.fc-state-hover {
	opacity: 1
}
.fc-header-left .fc-button-prev.fc-state-down, .fc-header-left .fc-button-next.fc-state-down {
	background: none !important;
	margin-top: -1px
}
.fc-header-left .fc-button-prev .fc-button-inner:after{
	font-family: FontAwesome;
	color:<?php echo $_CONFIG['colorPrincipal']; ?>;
	content:"\f053";
	font-size:16px;
}

.fc-header-left .fc-button-next {
	float: right
}

.fc-header-left .fc-button-next:after{
	font-family: FontAwesome;
	color:<?php echo $_CONFIG['colorPrincipal']; ?>;
	content:"\f054";
	font-size:16px;
}

.fc-header-center {
	text-align: center
}
.fc-header-right {
	text-align: right;
	position: absolute;
	top: -34px;
	right: 10px
}
.fc-header-title {
	display: inline-block;
	vertical-align: top
}
.fc-header-title h2 {
	margin-top: 0;
	white-space: nowrap;
	font-size: 1.1rem;
	color: <?php echo $_CONFIG['colorPrincipal']; ?>;
	line-height: 55px;
}
.fc .fc-header-space {
	padding-left: 10px
}
.fc-header .fc-button {
	margin-bottom: 1em;
	vertical-align: top
}
.fc-header .fc-button {
	margin-right: -1px
}
.fc-header .fc-corner-right {
	margin-right: 1px
}
.fc-header .ui-corner-right {
	margin-right: 0
}
.fc-header .fc-state-hover, .fc-header .ui-state-hover {
	z-index: 2
}
.fc-header .fc-state-down {
	z-index: 3
}
.fc-header .fc-state-active, .fc-header .ui-state-active {
	z-index: 4
}
.fc-content {
	clear: both;
	background: #f9f9f9
}
.fc-view {
	width: 100%;
	overflow: hidden
}
.fc-view thead {
	background:#e9ecf1;
	line-height: 35px
}
.fc-widget-header, .fc-widget-content {
	border: 1px solid #ccc
}
.fc-state-highlight {
	background: #F4F3E6
}
.fc-cell-overlay {
	background: #9cf;
	opacity: .2;
	filter: alpha(opacity=20)
}
.fc-button {
	position: relative;
	display: inline-block;
	cursor: pointer
}
.fc-button-today{
	margin-top: 8px !important;
}
.fc-button-today .fc-button-inner{
	background-color: <?php echo $_CONFIG['colorPrincipal']; ?>;
}
.fc-button-today.fc-state-hover .fc-button-inner{
	background-color: <?php echo $_CONFIG['colorSecundario']; ?>;
	border-color: <?php echo $_CONFIG['colorSecundario']; ?> !important;
}

.fc-state-default {
	border-style: solid;
	border-width: 1px 0
}
.fc-button-inner {
	position: relative;
	float: left;
	overflow: hidden
}
.fc-state-default .fc-button-inner {
	border-style: solid;
	border-width: 0 1px
}
.fc-button-content {
	position: relative;
	float: left;
	height: 1.9em;
	line-height: 1.9em;
	padding: 0 .6em;
	white-space: nowrap
}
.fc-button-content .fc-icon-wrap {
	position: relative;
	float: left;
	top: 50%
}
.fc-button-content .ui-icon {
	position: relative;
	float: left;
	margin-top: -50%;
	*margin-top:0;
	*top:-50%
}
.fc-state-default .fc-button-effect {
	position: absolute;
	top: 50%;
	left: 0
}
.fc-state-default .fc-button-effect span {
	position: absolute;
	top: -100px;
	left: 0;
	width: 500px;
	height: 100px;
	border-width: 100px 0 0 1px;
	border-style: solid;
	border-color: #fff;
	background: #fff;
	opacity: .09;
	filter: alpha(opacity=9)
}
.fc-state-default, .fc-state-default .fc-button-inner {
	border-style: solid;
	border-color: <?php echo $_CONFIG['colorSecundario']; ?>;
	color: #fff;
}
.fc-state-hover, .fc-state-hover .fc-button-inner {
	border-color: #fff;
}
.fc-state-down {
	border-color: #fff;
	background: <?php echo $_CONFIG['colorSecundario']; ?>;
}
.fc-state-active, .fc-state-active .fc-button-inner {
	border-color: <?php echo $_CONFIG['colorSecundario']; ?>;
	background: #FFF;
	color: <?php echo $_CONFIG['colorPrincipal']; ?>;
}
.fc-state-disabled, .fc-state-disabled .fc-button-inner {
	color: #999;
	border-color: #ddd;
	background-color:#FFF;
}
.fc-state-disabled {
	cursor: default
}
.fc-state-disabled .fc-button-effect {
	display: none
}
.fc-event {
	border-style: solid;
	border-width: 0;
	font-size: .85em;
	cursor: default
}
a.fc-event, .fc-event-draggable {
	cursor: pointer
}
a.fc-event {
	text-decoration: none
}
.fc-rtl .fc-event {
	text-align: right
}
.fc-event-skin {
	border-color: #3f85f5;
	background-color: #5e96ea;
	color: #fff
}

.fc-event-inner {
	position: relative;
	width: 100%;
	height: 100%;
	border-style: solid;
	border-width: 0;
	overflow: hidden
}
.fc-event-time, .fc-event-title {
	padding: 0 1px
}
.fc .ui-resizable-handle {
	display: block;
	position: absolute;
	z-index: 99999;
	overflow: hidden;
	font-size: 300%;
	line-height: 50%
}
.fc-event-hori {
	border-width: 1px 0;
	margin-bottom: 1px
}
.fc-event-hori .ui-resizable-e {
	top: 0 !important;
	right: -3px !important;
	width: 7px !important;
	height: 100% !important;
	cursor: e-resize
}
.fc-event-hori .ui-resizable-w {
	top: 0 !important;
	left: -3px !important;
	width: 7px !important;
	height: 100% !important;
	cursor: w-resize
}
.fc-event-hori .ui-resizable-handle {
	_padding-bottom: 14px
}
.fc-corner-left {
	margin-left: 1px
}
.fc-corner-left .fc-button-inner, .fc-corner-left .fc-event-inner {
	margin-left: -1px
}
.fc-corner-right {
	margin-right: 1px
}
.fc-corner-right .fc-button-inner, .fc-corner-right .fc-event-inner {
	margin-right: -1px
}
.fc-corner-top {
	margin-top: 1px
}
.fc-corner-top .fc-event-inner {
	margin-top: -1px
}
.fc-corner-bottom {
	margin-bottom: 1px
}
.fc-corner-bottom .fc-event-inner {
	margin-bottom: -1px
}
.fc-corner-left .fc-event-inner {
	border-left-width: 1px
}
.fc-corner-right .fc-event-inner {
	border-right-width: 1px
}
.fc-corner-top .fc-event-inner {
	border-top-width: 1px
}
.fc-corner-bottom .fc-event-inner {
	border-bottom-width: 1px
}
table.fc-border-separate {
	border-collapse: separate
}
.fc-border-separate th, .fc-border-separate td {
	border-width: 1px 0 0 1px
}
.fc-border-separate th.fc-last, .fc-border-separate td.fc-last {
	border-right-width: 1px
}
.fc-border-separate tr.fc-last th, .fc-border-separate tr.fc-last td {
	border-bottom-width: 0px
}
.fc-first {
	border-left-width: 0 !important
}
.fc-last {
	border-right-width: 0 !important
}
.fc-grid th {
	text-align: center
}
.fc-grid .fc-day-number {
	float: right;
	padding: 0 2px
}
.fc-grid .fc-other-month .fc-day-number {
	opacity: 0.3;
	filter: alpha(opacity=30)
}
.fc-grid .fc-day-content {
	clear: both;
	padding: 2px 2px 1px
}
.fc-grid .fc-event-time {
	font-weight: bold
}
.fc-rtl .fc-grid .fc-day-number {
	float: left
}
.fc-rtl .fc-grid .fc-event-time {
	float: right
}
.fc-agenda table {
	border-collapse: separate
}
.fc-agenda-days th {
	text-align: center
}
.fc-agenda .fc-agenda-axis {
	width: 60px !important;
	padding: 0 4px;
	vertical-align: middle;
	text-align: right;
	white-space: nowrap;
	font-weight: normal
}
.fc-agenda .fc-day-content {
	padding: 2px 2px 1px
}
.fc-agenda-days .fc-agenda-axis {
	border-right-width: 1px
}
.fc-agenda-days .fc-col0 {
	border-left-width: 0
}
.fc-agenda-allday th {
	border-width: 0 1px
}
.fc-agenda-allday .fc-day-content {
	min-height: 34px;
	_height: 34px
}
.fc-agenda-divider-inner {
	height: 2px;
	overflow: hidden
}
.fc-widget-header .fc-agenda-divider-inner {
	background: #eee
}
.fc-agenda-slots th {
	border-width: 1px 1px 0
}
.fc-agenda-slots td {
	border-width: 1px 0 0;
	background: none
}
.fc-agenda-slots td div {
	height: 20px
}
.fc-agenda-slots tr.fc-slot0 th, .fc-agenda-slots tr.fc-slot0 td {
	border-top-width: 0
}
.fc-agenda-slots tr.fc-minor th, .fc-agenda-slots tr.fc-minor td {
	border-top-style: dotted
}
.fc-agenda-slots tr.fc-minor th.ui-widget-header {
*border-top-style:solid
}
.fc-event-vert {
	border-width: 0 1px
}
.fc-event-vert .fc-event-head, .fc-event-vert .fc-event-content {
	position: relative;
	z-index: 2;
	width: 100%;
	overflow: hidden
}
.fc-event-vert .fc-event-time {
	white-space: nowrap;
	font-size: 10px
}
.fc-event-vert .fc-event-bg {
	position: absolute;
	z-index: 1;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #fff;
	opacity: .3;
	filter: alpha(opacity=30)
}
.fc .ui-draggable-dragging .fc-event-bg, .fc-select-helper .fc-event-bg {
	display: none\9
}
.fc-event-vert .ui-resizable-s {
	bottom: 0 !important;
	width: 100% !important;
	height: 8px !important;
	overflow: hidden !important;
	line-height: 8px !important;
	font-size: 11px !important;
	font-family: monospace;
	text-align: center;
	cursor: s-resize
}
.fc-agenda .ui-resizable-resizing {
	_overflow: hidden
}

/*------------------------------------------------------------------
[8. Miscellaneous]
*/

.chart-holder {
	width: 100%;
	height: 250px;
}

.dropdown-menu li>a:hover, .dropdown-menu .active>a, .dropdown-menu .active>a:hover { background:<?php echo $_CONFIG['colorPrincipal']; ?>;}

.accordion-heading { background:#e5e5e5;  }
.accordion-heading a { color:#545454; text-decoration:none; font-weight:bold; }

.btn-facebook-alt i {
color: #23386a;
}
.btn-twitter-alt i {
color: #0098d0;
}
.btn-google-alt i {
color: #b6362d;
}
.btn-linkedin-alt i {
color: #0073b2;
}
.btn-pinterest-alt i {
color: #ab171e;
}
.btn-github-alt i {
color: #333;
}

.all-icons li { list-style:none;}

.ML0 { margin-left:0}
.MR0 { margin-right:0;}



/*------------------------------------------------------------------
[1. Max Width: 480px]
*/

@media (max-width: 480px) {
	
	.error-container h1 {
		font-size: 72px;
	}
	
}





/*------------------------------------------------------------------
[1. Max Width: 767px]
*/

@media (max-width: 767px) {
    	
	#main {
		padding: 0 10px;
		margin-right: -20px;
		margin-left: -20px;	
	}
	
	
	.subnavbar {
		margin-left: -20px;
		margin-right: -20px;	
	}
	
	
	.subnavbar-inner {
		height: auto;
	}
	
	.subnavbar .container > ul {
		width: 100%;
		height: auto;
		
		border: none;
	}
	
	.subnavbar .container > ul > li {
		width: 33%;
		height: 70px;
		margin-bottom: 0;
		
		border: none;
	}
	
    
    
    .subnavbar .container > ul > li.active > a {
		font-size: 11px;
    	background: transparent;
    }
	
	.subnavbar .container > ul > li > a > i {	
		display: inline-block;	
		margin-bottom: 0;
		
		font-size: 20px;
	}
	
	
	.subnavbar-open-right .dropdown-menu {
		left: auto;
		right: 0;
	}
	
	.subnavbar-open-right .dropdown-menu:before {
		left: auto;
		right: 12px;
	}
	.subnavbar-open-right .dropdown-menu:after {
		left: auto;
		right: 13px;
	}
	
	.extra {
		margin-right: -20px;
		margin-left: -20px;
	}
	
	.extra .container {
		padding: 0 20px;
	}
	
	.footer {
		margin-right: -20px;
		margin-left: -20px;
	}
	
	.footer .container {
		padding: 0 20px;
	}
	
	.footer .footer-terms {
		text-align: left;
	}
	
	.footer .footer-terms a {
		margin-left: 0;
		margin-right: 1em;
	}

}
    




/*------------------------------------------------------------------
[3. Max Width: 979px]
*/

@media (max-width: 979px) {
	
	.navbar-fixed-top {
		position: static;
		
		margin-bottom: 0;
	}
	
	.subnavbar {
	}
	
	.subnavbar .container {		
		width: auto;
	}
}






/*------------------------------------------------------------------
[2. Max Width: 1200px]
*/

@media (min-width: 1200px) {
	.navbar .search-query {
		width: 200px;
	}
	
}



/* Estilos propios ---------------------------------------------------- */

.errorLogin{
	width:450px;
	position: fixed;
	z-index: 999;
	right:10px;
	top:70px;
}

.sClientes{
	margin-left:20px;
}

.centro{
	text-align: center !important;
	white-space:nowrap !important;
}

.centro .btn-group{
	display:inline-block !important;
}

.centro .dropdown-menu li{
	text-align: left !important;
}

.centro table{
	margin:0 auto 10px auto !important;
}


.grafico{
	width:20px;
	height:20px;
	display: block;
	float: left;
	margin-right: 10px;
}

.grafico1{background-color:#aa291e;}
.grafico2{background-color:#ACACAC;}
.grafico3{background-color:#ff8c00;}

.grafico6{background-color:#ff8c00;}
.grafico7{background-color:#f89406;}
.grafico8{background-color:#468847;}

.grafico9{background-color:#6BBA70;}
.grafico10{background-color:#356AA0;}
.grafico11{background-color:#F37D01;}
.grafico12{background-color:#B02B2C;}


.leyenda3{
	width: auto !important;
	position: absolute;
	bottom:120px;
	left:80px;
	text-align: left !important;
}

.leyenda3.derecha{
	bottom:150px;
	left:665px;
}

.leyenda2{
	position: absolute;
	bottom:-10px;
	float:left;
	left:0px;
	width:100px;
	padding:1px;
}


.leyenda{
	position: absolute;
	bottom:5px;
	left:4px;
	padding:5px;
}


.grafico-2{
	width:13px;
	height:13px;
	display: inline-block;
	margin-right: 5px;
	margin-left:15px;
	position: relative;
	top:3px;
}

.grafico4{background-color:rgba(220,220,220,0.5);border:1px solid rgba(220,220,220,1);}
.grafico5{background-color:rgba(151,187,205,0.5);border:1px solid rgba(151,187,205,1);}
.grafico-2.grafico1{background-color:#ff8c00;border:1px solid #ff8c00;}
.grafico-2.grafico2{background-color:#ACACAC;border:1px solid #ACACAC;}





.datepicker .btn-primary:hover{
	background-color: #1399dc !important;
}

.mitadAncho{
	width: 50%;
}

.ancho100{
	width:100% !important;
}


.peque{
	width:75px !important;
}

.peque select{
	width:65px !important;
}

.mediano{
	width:130px !important;
}

.mediano select{
	width:120px !important;
}

.areaInforme{
	width:480px !important;
	height:200px;
}

.margenAb{
	margin-bottom:30%;
}

.margenAbPeque{
	margin-bottom: 15px !important;
}

.table.margenAbPeque{
	margin-bottom:5px !important;
}

.margenMapa{
	margin-bottom:3%;
}

.margenIzq{
	margin-left:200px;
}

.margenDer{
	margin-right: 100px;
}



.btn-propio{
	background-color: <?php echo $_CONFIG['colorPrincipal']; ?>;
	border-color: <?php echo $_CONFIG['colorSecundario']; ?>;
}

.btn-propio:hover, .btn-propio:active, .btn-propio.active, .btn-propio.disabled, .btn-propio[disabled] {
	background-color: <?php echo $_CONFIG['colorSecundario']; ?> !important;
  	color: white;
  	*background-color: <?php echo $_CONFIG['colorSecundario']; ?>;
}


.btn-propio:active, .btn-propio.active {
	background-color: <?php echo $_CONFIG['colorSecundario']; ?> \9;
}

.btn-propio .caret{
	border-top-color: #ffffff;
	border-bottom-color: #ffffff;
	opacity: 0.75;
	filter: alpha(opacity=75);
}


.btn-propio, .btn-propio:hover{
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	color: #ffffff;
}






.logo2{
	float:left;
	color:#FFF;
	font-weight: bold;
	font-size: 16px;
}

.logo2 span{
	position: relative;
	top:5px;
	display: inline-block;
}


.dropdown-menu i{
	margin-right: 5px;
}

.resaltado{
	font-weight:bold;
	font-size:18px;
	vertical-align: middle;
}

.filaFecha{
	vertical-align: middle;
}

.filaFecha input{
	margin-top:0px !important;
}



.action-table .td-actions { 
	width: 300px; 	
	text-align: center !important; 
}

.action-table .td-large { 
	width: 450px !important;
	text-align: center;
}


.action-table .td-actions .btn { 
	margin-right: .5em;		
}

.action-table .td-actions .btn:last-child { 
	margin-right: 0; 		
}

.alert i{
	font-size:16px;
	vertical-align:absmiddle;
	margin-right:10px;
}

td .label{
	vertical-align: baseline !important;
	margin-left: 10px;
}

.textoDetalles{
	padding-top: 5px;
	line-height: 18px;
}

.iconoFactura{
	font-size:18px;
}

.icon-success{
	color: #7eb216;
}

.icon-danger{
	color: #B02B2C;
}


/************* DataTables ****************/
.dataTables_length select{
	float:left;
	width: auto;
	margin-right:10px;
	margin-left:15px;
}

.dataTables_filter{
	text-align: right;
}

.dataTables_filter label{
	line-height: 28px;
}

.dataTables_filter input{
	/*float:right;*/ /* Modificado */
	margin-right:15px;
	margin-left:10px;
}

.dataTables_info{
	margin-left:15px;
	line-height: 72px;
}

.dataTables_wrapper .row-fluid{
	padding-top:10px;
	background-color:#f8f8f8;
}

.pagination{
	text-align: right;
	margin-right:15px;
}

.row-fluid.arriba{
	border-bottom:1px solid #EEE;
}

.row-fluid.abajo{
	border-top:1px solid #EEE;
}

.pagination a{
	background-color: #FAFAFA;
}

.letraPeque{
	font-size:10px !important;
}

.input-micro{
	width: 15px !important;
	font-size:10px !important;
}

.sinPadding td{
	padding:10px 1px !important;
	text-align: center;
}

.separador{
	border-top: 1px solid #eee;
	margin:20px;
	padding-top:20px;
}

/*.separadorLeyenda{
	border-top: 1px solid #eee;
	margin:0px 2px 10px 2px;
}*/



/* Estilos mapa procesos */


th{
	text-align: center;
}

.tablaFormularioIndicadores{
	width: 100%;
}

.tablaFormularioIndicadores td{
	padding-bottom: 3%;
}

.tablaFormularioIndicadores td .descripcion{
	width:100%;
}

.tablaFormularioIndicadores td .valor{
	text-align: right;
}

.modal-example-content h4{
	color:#5cabd3;
}

fieldset .dataTables_wrapper{
	border:1px solid #ccc;
}




.tituloMapaProcesos{
	color:#444;
}

.tablaInterna{
	border-collapse: separate;
	width:100%;
	background-color:#b6e0fb;
	color:#444;
	border:1px solid #9dc1d9;
	text-align: center;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;

	text-shadow: 0 1px #FFF;
	box-shadow: 0px 0px 3px 0px #AAA;
}

.tablaBorde, .tablaBorde tr, .tablaBorde th, .tablaBorde td{
	border:1px solid red;
	border-spacing:0;
}

.celdaVertical{
	background-color:#4b85c1;
	color:#FFF;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	width: 50px !important;
	max-width: 50px !important;
	vertical-align: middle;

	box-shadow: 0px 0px 3px 0px #AAA;	
}

.celdaFlecha{
	vertical-align: middle;
	padding:0 10px;
}

.celdaVertical p{
	margin-top:180px;
	-webkit-transform: rotate(270deg);
	-moz-transform: rotate(270deg);
	-o-transform: rotate(270deg);
	writing-mode: lr-tb;
	white-space:nowrap;
	font-size:20px;
}

.cajasContenedor{
	width:300px;
	background-color: #8cc9ec;
	border:1px solid #7bb0d0;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	font-size:12px;
	margin:0 10px;
	font-weight: normal;
	text-align: center;
	height:50px;
	text-shadow:none;
	line-height: 52px;
}

.cajasContenedor img{
	vertical-align: middle;
	margin-right: 10px;
}

.cajasContenedor:hover{
	background-color:#C3D9FF;
	cursor:pointer;
}

/* Fin mapa procesos */


@media (max-width: 1025px) {
	
	.subnavbar .container > ul > li > a{
		font-size: 11px;
		padding:0 8px;
	}
	
}


.apartadoFormulario{
	border-bottom: 1px solid #EEE;
	color:<?php echo $_CONFIG['colorPrincipal']; ?>;
	margin-bottom:40px;
}


.sinFlotar{
	clear: both;
	padding-top:20px !important;
}


.margenAr{
	margin-top: 20px;
	margin-bottom:20px;
}


.areaTexto{
	width:300px !important;
	height:85px;
}

#cajaAlumnos table{
	margin: auto;
}

.anchoAuto{
	width: auto !important;
}


 #mensajeCorreo {
 	overflow:scroll; 
 	height:300px !important;
 	max-height: 300px;
 	width:70% !important;
 }


fieldset .datatable{
	margin-bottom:0px !important;
	-webkit-border-radius: 0px !important;
	-moz-border-radius: 0px !important;
	border-radius: 0px !important;	
}

.conBorde{
	border:1px solid #ccc;
}


.big-stats-container.tareas{
	margin-bottom:5px !important;
}

.bigstats.tareas{
	margin-top:10px !important;
	margin-bottom: 22px !important;
	padding-bottom:12px !important;
}

.selectpicker .label{
	width:94%;
	display: inline-block;
	height:20px;
	line-height: 20px;
	font-size: 14px;
	margin-left: 0;
}

.span1 .selectpicker .label{
	width:50%;
}

.label-morado{
	background-color:#b083d4;
}

.label-morado:hover{
	background-color:#956bba;
}

.sinMargenAb{
	margin-bottom:0px !important;
	border:none;
}

.sinMargenAb th{
	border-left:none;
}

.sinMargenAr{
	margin-top:0px !important;
}

.input-supermini{
	width: 20px;
}

.numSS .add-on{
	margin-right:-4px;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;
	border-right:none;
	padding-left:0;
	padding-right:0;
}

.numSS #numSS1{
	-webkit-border-top-left-radius: 3px;
	-moz-border-radius-topleft: 3px;
	border-top-left-radius: 3px;
	-webkit-border-bottom-left-radius: 3px;
	-moz-border-radius-bottomleft: 3px;
	border-bottom-left-radius: 3px;
}
.numSS #numSS2{
	-webkit-border-top-right-radius: 3px;
	-moz-border-radius-topright: 3px;
	border-top-right-radius: 3px;
	-webkit-border-bottom-right-radius: 3px;
	-moz-border-radius-bottomright: 3px;
	border-bottom-right-radius: 3px;
}

.cajaObjetivo{
	padding-top:30px;
	padding-left:20px;
	padding-right:20px;
	padding-bottom: 0;
}

.cajaObjetivo div{
	margin-bottom: 0;
}

.marginAbPeque{
	margin-bottom:7px !important;
}

.bootstrap-filestyle span{
	margin-left:0px !important;
}

.nowrap{
	white-space: nowrap;
}

.tip{
	font-size:10px;
	white-space: nowrap;
	padding-top:4px;
	margin-left:140px;
}

.enlaceForm{
	padding-top:3px !important;
}

.aliIzq{
	text-align: left !important;
}

.aliDer{
	text-align: right !important;
}

.encabezadoInforme{
	margin-top:0px;
	padding:20px;
	text-transform: uppercase;
	text-align: center;
	margin-bottom:20px;
}

.encabezadoInforme span{
	font-weight: bold;
	color:#ff8c00;
}

.graficoInforme{
	width: 550px;
	clear: none !important;
	display: inline-block !important;
	min-height:432px;
}

.tablaGraficosInforme{
	width: 100%;
}

.tablaGraficosInforme td{
	text-align: center;
	width: 570px;
}

.porcentajeInforme{
	position:relative;
	font-size:40px;
	bottom:160px;
}

.subPorcentaje{
	margin-top:10px;
	font-size:12px;
	color:#333;
	line-height: 12px !important;
}

#donut-chart{
	margin-top:100px;
}

fieldset .dataTables_info{
	margin-left:15px;
	line-height: 18px;
	padding-top:24px;
}

.formSeguimiento input:disabled, .formSeguimiento textarea:disabled{
	background-color:#EFEFEF !important;
}

.areaTextoAmplia{
	width: 95%;
	text-align: justify;
	padding:10px;
	border:1px solid #AAA;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;

	background-color:#f8e0ff;
}

.tabbable form{
	margin-bottom:0px;
}

table.centro th, table.centro td{
	text-align: center !important;
}

/***** Parte firma ****/
.pad{
	border:1px solid #CCC;
}

.clearButton{
	text-align: right;
	width: 1070px;
}

/***** Fin parte firma *****/

.datoSinInput{
	margin-top:3px;
}

.celda10{
	width: 10%;
}

.pLados{
	padding:0 20px;
}


embed{
	border-top:1px solid #565656;
	border-bottom:5px solid #565656;
	border-left:1px solid #565656;
	border-right:4px solid #565656;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
}

.vistaPrevia{
	width: 400px;
	height:500px;
	position: absolute;
	right:50px;
}

.vistaPreviaAdjunto{
	width:200;
	height: 300px;
	margin-right:20px;
}

.btn-derecha{
	float:right;
	display: block;
	margin-right: 150px;
}
.todo{
	padding-left:20px;
	width:97.6% !important;
}

.widget-header .icon-chevron-right{
	font-size:10px;
	font-weight: normal;
}


.leyenda3 td {
	position: relative;
	margin-bottom: 10px;
	border-radius: 5px;
	padding: 5px 8px 5px 28px;
	font-size: 10px;
	line-height: 10px;
	cursor: default;
	-webkit-transition: background-color 200ms ease-in-out;
	-moz-transition: background-color 200ms ease-in-out;
	-o-transition: background-color 200ms ease-in-out;
	transition: background-color 200ms ease-in-out;
}

.leyenda3 td:hover {
	background-color: #eee;
}

.leyenda3 td span{
	display: block;
	position: absolute;
	left: 0;
	top: 0;
	width: 14px;
	height: 14px;
	border-radius: 3px;
	margin-top:4px;
	margin-left: 10px;
}

.leyenda2 li {
  display: block;
  position: relative;
  margin-bottom: 4px;
  border-radius: 5px;
  padding: 2px 8px 2px 28px;
  font-size: 10px;
  line-height: 10px;
  cursor: default;
  -webkit-transition: background-color 200ms ease-in-out;
  -moz-transition: background-color 200ms ease-in-out;
  -o-transition: background-color 200ms ease-in-out;
  transition: background-color 200ms ease-in-out;
}

.leyenda2 li:hover {
  background-color: #eee;
}

.leyenda2 li span{
  display: block;
  position: absolute;
  left: 0;
  top: 0;
  width: 14px;
  height: 14px;
  border-radius: 3px;
}



.leyenda li {
  display: block;
  position: relative;
  margin-bottom: 4px;
  border-radius: 5px;
  padding: 2px 8px 2px 28px;
  font-size: 14px;
  line-height: 14px;
  cursor: default;
  -webkit-transition: background-color 200ms ease-in-out;
  -moz-transition: background-color 200ms ease-in-out;
  -o-transition: background-color 200ms ease-in-out;
  transition: background-color 200ms ease-in-out;
}

.leyenda li:hover {
  background-color: #eee;
}

.leyenda li span{
  display: block;
  position: absolute;
  left: 0;
  top: 0;
  width: 20px;
  height: 100%;
  border-radius: 6px;
}


/* Fin leyenda para gráfico circular */

.justificado{
	text-align: justify !important;
}

.cajaEnlace{
	padding:10px 10px 0px 10px !important;
}

.textarea-amplia{
	width:99%;
	height:150px;
}

.cabeceraCuestionario{
	position:fixed;
}

#graficoBarras{
	margin-bottom: 60px;
	padding-left: 20px;
	padding-right: 20px;
}

input:disabled, textarea:disabled{
	background-color: #fefefe !important;
}

#avisoSaldo{
	margin-left: 10px;
	position: absolute;
	margin-top:10px;
	float:right;
}

.btn-group.bootstrap-select.span3.show-tick{
	clear: none !important;
}

#area-chart{
	margin-left:10px;
}

#parrafoFormu{
	text-align: justify;
}


.label-primary{
	background-color:#4ea6ff;
}

.label-primary:hover{
	background-color:#278ce3;
}

.label a{
	color:#FFF;
}

#tablaTodo td{
	vertical-align: top;
}


/** Estilos para ordenación tablas **/

.sorting_asc, .sorting_desc{/* Fondo de celdas por las que se está ordenando */
	background: #EFEFEF !important;
}

.sorting_asc::after {/* Icono orden ascendente */
    font-family: FontAwesome;
    top:0;
    left:-5px;
    color:<?php echo $_CONFIG['colorPrincipal']; ?>;
    content: "\f0de";
    float:right;
}

.sorting_desc::after { /* Icono orden descendente */
    font-family: FontAwesome;
    top:0;
    left:-5px;
    content: "\f0dd";
    float:right;
    color:<?php echo $_CONFIG['colorPrincipal']; ?>;
}

.sorting::after { /* Icono que indica que se puede ordenar  */
    font-family: FontAwesome;
    top:0;
    left:-5px;
    content: "\f0dc";
    float:right;
}


.datatable th{
	cursor: pointer;
}

.table-bordered thead:first-child tr:first-child th:last-child:after{
	content:'' !important;
}

.centro:after{
	content: '' !important;
}


/** Fin estilos para ordenación tablas **/

/* Estilos spinner */
.spinner {
  	width: 90px;
  	text-align: center;
  	margin: 0 auto;
	margin-top:15%;
	margin-bottom:21%;
}

.spinner > div {
  width: 28px;
  height: 28px;
  background-color: <?php echo $_CONFIG['colorPrincipal']; ?>;

  border-radius: 100%;
  display: inline-block;
  -webkit-animation: bouncedelay 1.4s infinite ease-in-out;
  animation: bouncedelay 1.4s infinite ease-in-out;
  /* Prevent first frame from flickering when animation starts */
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

.spinner .bounce1 {
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}

.spinner .bounce2 {
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}

@-webkit-keyframes bouncedelay {
  0%, 80%, 100% { -webkit-transform: scale(0.0) }
  40% { -webkit-transform: scale(1.0) }
}

@keyframes bouncedelay {
  0%, 80%, 100% { 
    transform: scale(0.0);
    -webkit-transform: scale(0.0);
  } 40% { 
    transform: scale(1.0);
    -webkit-transform: scale(1.0);
  }
}

.widget-header .icon-chevron-right{
	font-size:10px;
	font-weight: normal;
}

.modal-header .icon-chevron-right{
	font-size: 10px;
	margin-left:10px;
	margin-right: 10px;
	position: relative;
	bottom: 2px;
}

.cajaUsuario, .cajaUsuario:hover{
	background-color:<?php echo $_CONFIG['colorSecundario']; ?> !important;
}

/******** Estilos personalizados para checkbox y radios **********/

input[type=checkbox]:after{
	content:"";
	display:block;
	width:12px;
	height: 12px;
	background-color:#FFF;
	border:1px solid #CCC;
}

input[type=checkbox]:hover:after{
	display:block;
	width:12px;
	height: 12px;
	background-color:#FFF;
	border:1px solid <?php echo $_CONFIG['colorPrincipal']; ?>;
}

input[type=checkbox]:checked:after{
	display:block;
	width:12px;
	height: 12px;
	border:1px solid <?php echo $_CONFIG['colorPrincipal']; ?>;
	background-color: <?php echo $_CONFIG['colorPrincipal']; ?>;
	font-family: FontAwesome;
	content:"\f00c";
	font-size:10px;
	text-align: center;
	color:#FFF;
}


input[type=radio]{
	visibility: hidden;
}

@-moz-document url-prefix(){
	input[type=radio]{
		visibility: visible !important;
	}
}

input[type=radio]:after{
	visibility: visible;
	content:"";
	display:block;
	width:12px;
	height: 12px;
	background-color:#FFF;
	border:1px solid #CCC;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

input[type=radio]:hover:after{
	visibility: visible;
	display:block;
	width:12px;
	height: 12px;
	background-color:#FFF;
	border:1px solid <?php echo $_CONFIG['colorPrincipal']; ?>;
}

input[type=radio]:checked:after{
	visibility: visible;
	display:block;
	width:12px;
	height: 12px;
	border:1px solid <?php echo $_CONFIG['colorSecundario']; ?>;
	background-color: <?php echo $_CONFIG['colorPrincipal']; ?>;

	color:#FFF;
	font-family: FontAwesome;
	content:"\f00c";
	font-size:10px;
	text-align: center;
}

/******** Fin estilos personalizados para checkbox y radios **********/

/******** Fin estilos personalizados para checkbox y radios **********/

#tablaTrabajos th, #tablaTrabajos td{
	text-align: center;
}

@media(max-width: 1100px) {
	
	.form-horizontal .control-label {
		width:100px;
	}

	.form-horizontal .controls{
		margin-left: 120px;
	}
	
}

/** Estilos select Plus **/

.botonSelectPlus{
	font-size:12px;
	position:relative;
	bottom:13px;
	right:6px;
	height:33px;
	z-index:100;
	-webkit-border-top-left-radius: 0;
	-webkit-border-bottom-left-radius: 0;
	-moz-border-radius-topleft: 0;
	-moz-border-radius-bottomleft: 0;
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
	white-space: nowrap;
}

/** Fin estilos select Plus **/

.observacionesPrueba{
	width:300px;
	height:70px;
}

.selectFiltro{
	width:90%;
}

.iconoFactura{
	font-size:18px;
}

.account-container h1{
	font-size:22px;
}

/********* Estilos para estrellitas de valoración en cuestionario de satisfacción de clientes  ***********/

.controls .rating-input{
	margin-top:5px;
}

.rating-input .icon-star-o, .rating-input .icon-star, #relacion .icon-star, #relacion .icon-star-o{
	color:#C79810;
	font-size:18px;
}

td.relacion .icon-star-o, td.relacion .icon-star{
	color:#C79810;
	white-space: nowrap;
}

/********* Fin estilos para estrellitas de valoración en cuestionario de satisfacción de clientes  ***********/

.negritaCursiva{
	font-weight: bold;
	font-style: italic;
}

.panel input[readonly]{
	background-color: #FFF !important;
	font-size:14px;
	font-weight: bold;
}

#big_stats button i, #big_stats a i {
    font-size: inherit;
    display: inherit;
    line-height: inherit;
    color: inherit;
}

.apartadoTablaFormularioEvaluacion th{
	background-color:<?php echo $_CONFIG['colorSecundario']; ?> !important;
	border-left-color: <?php echo $_CONFIG['colorPrincipal']; ?> !important;
	border-top-color: <?php echo $_CONFIG['colorPrincipal']; ?> !important;
	color:#FFF;
}

.areaTextoTabla{
	min-width: 200px;
}

.table tbody tr:hover td, .table tbody tr:hover th{
	background-color:#eff7dc;
}

.areaTextoTablaMedidas{
	width:98%;
	height:200px;
	min-width: 300px;	
}

button.shortcut{
	border:none;
	line-height: 22px;
}

/*********** Estilos paneles acordeón ***********/

.panel{
	border:1px solid <?php echo $_CONFIG['colorSecundario'];?>;
	margin-bottom:10px;
	border-radius: 2px;
}

.panel .panel-heading{
	background-color:<?php echo $_CONFIG['colorPrincipal'];?>;
	color:#FFF;
	padding:5px;
	padding-right: 19px;
}

.panel-heading a{
	color:#FFF;
}

.panel-default:hover{
	border-color:<?php echo $_CONFIG['colorSecundario'];?>;	
}

.panel-body{
	padding:20px;
}

.panel-default:hover .panel-heading{
	background-color: <?php echo $_CONFIG['colorSecundario'];?>;
}

.colEvaluacionDelito{
	width: 500px;
	display: inline-block;
	vertical-align: top;
	margin-right: 15px;
	margin-bottom:15px;
}

.panel-primary {
  border-color: #428bca;
}

.panel-primary .panel-heading {
  color: #ffffff;
  background-color: #428bca;
  border-color: #428bca;
}

.panel-success {
  border-color: #468847;
}

.panel-success .panel-heading {
  color: #468847;
  background-color: #468847;
  border-color: #468847;
}

.panel-warning {
  border-color: #f89406;
}

.panel-warning .panel-heading {
  color: #c09853;
  background-color: #f89406;
  border-color: #f89406;
}

.panel-danger {
  border-color: #d9534f;
}

.panel-danger .panel-heading {
  color: #b94a48;
  background-color: #d9534f;
  border-color: #d9534f;
}

.panel-info {
  border-color: #bce8f1;
}

.panel-info .panel-heading {
  color: #3a87ad;
  background-color: #d9edf7;
  border-color: #bce8f1;
}

/*********** Fin estilos paneles acordeón ***********/

.cajaGraficoRiesgo{
	margin-bottom:100px;
}

td.centro .bootstrap-filestyle{
	display: inline-block;
	margin:0 auto;
}

.widget-header a{
	color:#FFF;
	font-weight: bolder;
}

.widget-header a.btn-default{
	color:#333;
	font-weight: bolder;
}

.areaTextoEnriquecido{
	width:80%;
	height:300px;	
}

.sinBorde{
	border:none;
}

.sinBorde td{
	border:none;
	background-color:transparent !important;
}

.selectPorcentaje{
	width:100px !important;
}

.listadoChecks .apartadoFormulario{
	margin-bottom:0px;
	border-bottom: none;
	padding-left: 100px;
	font-size: 16px;
}

.listadoChecks label{
	margin-left:100px;
}

fieldset .listadoChecks .apartadoFormulario{
	margin-bottom:0px;
	border-bottom: none;
	padding-left: 30px;
	font-size: 16px;
}

fieldset .listadoChecks label{
	margin-left:40px;
}

.listadoChecks .control-group{
	margin-bottom:0px;
}

.listadoChecks .control-label{
	width:85px !important;
	margin-left: 0px !important;
	margin-top: 10px;
}

.listadoChecks .controls{
	margin-top: 10px;
	margin-left: 105px !important;
}


.listadoChecks .span4 .control-label{
	margin-left:60px !important;
}

.listadoChecks .span4 .controls{
	margin-left:165px !important;
}

.input-super-mini{
	width:40px;
}

textarea.span6{
	height: 80px;
}

.span2 .control-label{
	width:120px;
}

.span2 .controls{
	margin-left:140px;
}

#cke_contenidoProducto{
	width:93%;
}

.tabs-productos{
	margin-bottom:1em;
}

.areaTextoAzafatas{
	width:170px;
	height:100px;
}

.areaTextoMaxi{
	width:95%;
	min-width: 350px;
	height:100px;
}

.marcoCorreo{
	width:100%;
	height: 100%;
	min-height: 800px;
	position: relative;
	bottom:2.8em;
}

.sinMargenes{
	padding: 0 !important;
	margin:0 !important;
	border:0 !important;
}

.marcoCorreoEnviados{
	width:100%;
	height: 400px;
}

.columnaPeque .control-label{
	width:80px !important;
}

.columnaPeque .controls{
	margin-left:100px !important;
}

.areaTextoExpediente{
	width:400px;
	height:120px;
}

.cabeceraTablaProductosPresupuesto{
	background-color: #F5F5F5;
}

.cabeceraTipoProducto{
	background-color: #F5F5F5;
	font-weight: bold;
	font-size:14px;
}

.margenIzqBtn{
	margin-left:10px;
}

.fechaApartado{
	display: inline-block;
	margin-left:20px;
	font-weight: normal;
}

.ancho30{
	width:40%;
	float:left;
	margin-left:40px;
	margin-top:20px;
}

.tabs-produccion .nav-tabs{
	margin-top:20px;
	margin-bottom: 0px;
	border-bottom:none;
}

.tabs-produccion .nav-tabs .active a{
	background-color: #F5F5F5;
}

.tabs-produccion .nav-tabs .pull-right{
	float:right !important;
}

.tabs-produccion .span6{
	width:401px;
}

.dataTables_processing{
	position: absolute;
	left:45%;
}

.tablaTextoPeque{
	font-size:11px !important;
}

.tablaTextoPeque th{
	font-size:7px;
}

.tablaTextoPeque .btn{
	font-size:11px;
	padding:3px 6px;
}

.tablaTextoPeque .btn .caret{
	margin-top: 6px;
}


/* Estilos para el botón "Búsqueda por filtros" de los listados */

.widget-header .btn{
	font-weight: bolder;
}

.widget-header .btn [class^="icon-"]{
	margin-left:0 !important;
	margin-right: 2px;
}

.widget-header .btn-default i{
	color:#333;
}

.widget-header .pull-right{
	margin-right:30px;
}

/* Estilos para los formularios de búsqueda por filtros en los listados */

.widget-table .widget-content{
	background-color: #EFEFEF;
}

.cajaFiltros .apartadoFormulario{
	padding-left:20px;
	padding-top:15px;
	margin-bottom:10px;
	border-bottom-color: #CCC;
	background-color:#f8f8f8;
}

.cajaFiltros{
	border-bottom: 1px solid #CCC;
	padding-bottom:10px;
}

.cajaFiltros:after{
	content: " ";
	display: block; 
	height: 0; 
	clear: both;
}

.cajaFiltros .bootstrap-select{
	margin-bottom: -10px !important;
}

.filaBloqueada td{
	background-color:#ffe6e6 !important;
}

.filaPendiente td{
	background-color:#ffffc8 !important;
}


.tabla-simple{
	margin-bottom: 3px;
	border-collapse: collapse;
}

#tablaDiasImparticion td, #tablaDiasImparticion th{
	text-align: center;
	border:0px;
}

.tabla-simple td, .tabla-simple th{
	padding:4px;
	border:1px solid #DDD;
	border-collapse: collapse;
}

.tabla-simple th{
	background-color:<?php echo $_CONFIG['colorPrincipal']; ?> !important;
	color:#FFF;
	border:1px solid <?php echo $_CONFIG['colorSecundario']; ?>;
	font-size:10px;
	text-transform: uppercase;
}

.tabla-simple th .btn-small{
	line-height: 18px;
	padding: 0 4px;
	font-size:10px;
	float:right;
}

.input-100{
	width:98%;
	min-width: 700px;
}

.tablaVencimientos th:nth-child(4), .tablaVencimientos th:nth-child(5){
	width:25px;
}

.asterisco{
	color:red;
	vertical-align: text-top !important;
}

.asteriscoAmarillo{
	color:#C79810;
	vertical-align: text-top !important;	
}

/** Estilos boton select ajax **/

.botonSelectAjax{
	font-size:12px;
	position:relative;
	bottom:13px;
	right:6px;
	height:31px;
	z-index:100;
	line-height: 31px;
	-webkit-border-top-left-radius: 0;
	-webkit-border-bottom-left-radius: 0;
	-moz-border-radius-topleft: 0;
	-moz-border-radius-bottomleft: 0;
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
}

.botonSelectAjax2{
	font-size:12px;
	position:relative;
	bottom:13px;
	right:15px;
	height:33px;
	z-index:100;
	line-height: 31px;
	-webkit-border-top-left-radius: 0;
	-webkit-border-bottom-left-radius: 0;
	-moz-border-radius-topleft: 0;
	-moz-border-radius-bottomleft: 0;
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
}

.cajaFiltros .botonSelectAjax{
	bottom:3px;
}

/** Fin estilos select ajax **/

.anotacion{
	white-space: nowrap;
	font-size:10px;
	position:relative;
	bottom:25px;
	text-align: center;
}

#tablaAlumnos .areaTexto{
	width:150px !important;
	height:150px;
}

.iconoBorrado{
	margin-left: 5px;
	border:none;
	background: none;
	color:<?php echo $_CONFIG['colorPrincipal']; ?>;
}

.iconoBorrado:hover{
	text-decoration: underline;
}

.datatable .dropdown-menu{
	left:auto;
	right:0 !important;
}

.filaPie td{
	border:none !important;
	background-color:#f8f8f8;
}

.filaPie:hover td{
	background-color:#f8f8f8 !important;
}

.filaPie .celdaSumatorio{
	background-color:<?php echo $_CONFIG['colorPrincipal']; ?> !important;
	color:#FFF !important;
	text-align: right;
	font-weight: bold;
	
	white-space: nowrap;
	border:1px solid <?php echo $_CONFIG['colorSecundario']; ?> !important;

	-webkit-border-bottom-left-radius: 6px;
	-webkit-border-bottom-right-radius: 6px;
	-moz-border-radius-bottomleft: 6px;
	-moz-border-radius-bottomright: 6px;
	border-bottom-left-radius: 6px;
	border-bottom-right-radius: 6px;
}

.filaPie:hover td.celdaSumatorio{
	background-color:<?php echo $_CONFIG['colorPrincipal']; ?> !important;
}

.tablaInternaServicios,
.tablaInternaServicios tr,
.tablaInternaServicios td,
.tablaInternaServicios th{
	background:transparent !important;
}

.apartadoInicioGrupo{
	margin-bottom: 5px;
}

.celdaConTabla{
	padding:0px !important;
	border-bottom:none !important;
	border-left:none !important;
}

.tabla-interna, .centro table.tabla-interna{
	margin-bottom:0px !important;
}

.ancho97{
	width:97% !important;
}


/* Estilos para los checkbox de vencimientos */
#tablaListadoVencimientos input[type=checkbox]:after{
	display:block;
	width:16px;
	height: 16px;

	background-color:#e46969;
	border:1px solid #B02B2C;
	padding-left: 1px;
	padding-top: 1px;

	font-family: FontAwesome;
	content:"\f00d";
	font-size:14px;
	text-align: center;
	color:#fff;
}

#tablaListadoVencimientos input[type=checkbox]:hover:after{
	display:block;
	width:16px;
	height: 16px;

	background-color:#5bc25b;
	border:1px solid #006E2E;
	padding-left: 1px;
	padding-top: 2px;

	font-family: FontAwesome;
	content:"\f095";
	font-size:14px;
	text-align: center;
	color:#fff;
}

#tablaListadoVencimientos input[type=checkbox]:checked:after{
	display:block;
	width:16px;
	height: 16px;
	
	background-color:#5bc25b;
	border:1px solid #006E2E;

	font-family: FontAwesome;
	content:"\f00c";
	font-size:14px;
	text-align: center;
	color:#FFF;
}
/* Fin estilos para los checkbox de vencimientos */

/*** Estilos para el filtro por ejercicios ***/

.filtroEjercicio{
	color:#FFF;
	border:none;
	padding:0px;
	vertical-align: baseline;
	background-color: transparent;
	margin-bottom: 0px;
	width:35px;
	font-size: 11px;
	font-weight: bolder;
	cursor: pointer !important;
}

.tab-content .checkbox.inline{/** Para checkboxs de finalidades **/
    padding-top:10px !important;
    padding-bottom:10px !important;
    padding-right:10px;
    text-align: justify !important;
    width:600px !important;
    white-space:normal !important;
    border:1px solid #FFF;
    background-color: <?php echo $_CONFIG['colorPrincipal']; ?>;
    color: #FFF;
}

.tab-content .checkbox.inline.marcado{
    background-color: #6BBA70;
}

.tab-content .checkbox input[type="checkbox"]{
    margin-right:10px;
    margin-left:-8px;
}

.tabs-small a{
    font-size:10px;
}

/*** Fin estilos para el filtro por ejercicios ***/

.mostrarServicios{
    width:100%;
    min-width: 800px;
}

#tablaConsultorias .tabla-simple{
    margin-bottom: 0 !important;
}

.badge-danger{
    background-color: #b94040;
}

#tablaConsultorias .badge-danger{
    margin-left:10px;
    position: absolute;
}

#tablaVentas th:not(:last-child), #tablaVentas td:not(:last-child){
    white-space: nowrap !important;
    min-width: 110px;
}

.contenedorListadoVentas{
    /*width: auto !important;
    position: absolute;*/
    width:100%;
}

.contenedorListadoVentas .widget-content, .scrollTablaVentas{
	max-width: 100% !important;
	overflow-x: scroll !important;
}

.cajaScroll{
	height: 1px;
}

span.subtitulo{
	background:#AAAAAA;
	color:#FFF;
	padding:5px;
}

.tituloSeguimientoConsultoria{
	background-color: <?php echo $_CONFIG['colorPrincipal']; ?>;
    color: #FFF;
    padding-top:10px !important;
    padding-left:10px !important;
    padding-bottom:10px !important;
    padding-right:10px !important;
    width:96%;
    text-align:left !important;
    margin:20px !important;
    font-size:24px;
}

.tituloSeguimientoConsultoria:hover{
	background-color: <?php echo $_CONFIG['colorPrincipal']; ?>;
    color: #FFF;
}


/** Estilos para campo fecha del calendario **/
#fechaCalendario{
	display: inline-block;
	float:right;
	margin-right:155px;
	margin-top:6px;
	border:1px solid <?php echo $_CONFIG['colorSecundario']; ?>;
	color:#FFF;
	line-height:12px;
}

#fechaCalendario .datepicker{
	margin:0 !important;
	padding:3px !important;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;
	border-right: 0;
	border-bottom: 0;
	border-top:0;
	border-left-color: <?php echo $_CONFIG['colorSecundario']; ?>;
}

#fechaCalendario span{
	padding:0px 5px;
}

.fc-button.fc-corner-left .fc-button-inner{
	margin-left: 0px !important;
}
/** Fin estilos campo fecha calendario **/

.parrafoFormulario{
	padding:10px 30px;
	margin-bottom:20px;
}


/* Estilos para la tabla de participantes en inicios de grupos */

/*

#tablaEmpresasParticipantes .celdaInvisible{
	background-color:#FFF !important;
	border-top:none !important;
	border-right:1px solid #ddd !important;
	border-left:none !important;
	border-bottom:none !important;
}

#tablaEmpresasParticipantes .tabla-interna td{
	border-bottom: 0px !important;
	border-left: 0px !important;
}

#tablaEmpresasParticipantes .tabla-interna .celdaCliente{
	border-bottom:1px solid #ddd !important;
	border-left:1px solid #ddd !important;
}

#tablaEmpresasParticipantes .tabla-interna td:last-child{
	border-right: 0px !important;
}

#tablaEmpresasParticipantes .selectAlumno{
	margin-top:5px;
	width:564px;
}

#tablaEmpresasParticipantes .btn-small{
	font-size:11px;
}


#tablaEmpresasParticipantes .tablaPadre tr:nth-child(1n+2) td:not(.celdaInvisible){
	border-bottom:1px solid #ddd !important;
}*/


#tablaEmpresasParticipantes td{
	background-color: #f9f9f9;
}

#tablaEmpresasParticipantes, #tablaEmpresasParticipantes th, #tablaEmpresasParticipantes td{
	border-top:none !important;
}

#tablaEmpresasParticipantes .celdaInvisible{
	background-color:#FFF !important;
	border-top:none !important;
	border-right:1px solid #ddd !important;
	border-left:none !important;
	border-bottom:none !important;
}

#tablaEmpresasParticipantes .tablaPadre tr:nth-child(1n+2) td:not(.celdaInvisible){
	border-bottom:1px solid #ddd !important;
}


#tablaEmpresasParticipantes .selectAlumno{
	margin-top:5px;
	width:564px;
}

#tablaEmpresasParticipantes .btn-small{
	font-size:11px;
}

#tablaEmpresasParticipantes .tabla-interna .celdaCliente{
	border-top:1px solid #ddd !important;
}

#tablaEmpresasParticipantes .cabecera1{
	width:23%;
}

#tablaEmpresasParticipantes .cabecera2{
	width:20%;
}

#tablaEmpresasParticipantes .cabecera3{
	width:5%;
	font-size:9px;
}

#tablaEmpresasParticipantes .cabecera4{
	width:10%;
}

/* Fin estilos para la tabla de participantes en inicios de grupos */

.observacionesVentaFormacion{
	width:400px;
	height:100px;
}


/** Animación para los botones de importación/exportación **/

.botonImportacion{
	width:30% !important;
	height:30%;
}

.botonImportacion .shortcut-icon:not(.icon-arrow-up){
	font-size:48px!important;
}

.bota {
  -webkit-animation:bounceAlt 2s infinite;
  display: block !important;
}

@keyframes bounceAlt {
  0%, 20%, 53%, 80%, 100% {
    -webkit-transition-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
    transition-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
    -webkit-transform: translate3d(0,0,0);
    -ms-transform: translate3d(0,0,0);
    transform: translate3d(0,0,0);
  }

  40%, 43% {
    -webkit-transition-timing-function: cubic-bezier(0.755, 0.050, 0.855, 0.060);
    transition-timing-function: cubic-bezier(0.755, 0.050, 0.855, 0.060);
    -webkit-transform: translate3d(0, -15px, 0);
    -ms-transform: translate3d(0, -15px, 0);
    transform: translate3d(0, -15px, 0);
  }

  70% {
    -webkit-transition-timing-function: cubic-bezier(0.755, 0.050, 0.855, 0.060);
    transition-timing-function: cubic-bezier(0.755, 0.050, 0.855, 0.060);
    -webkit-transform: translate3d(0, -7.5px, 0);
    -ms-transform: translate3d(0, -7.5px, 0);
    transform: translate3d(0, -7.5px, 0);
  }

  90% {
    -webkit-transform: translate3d(0,-2px,0);
    -ms-transform: translate3d(0,-2px,0);
    transform: translate3d(0,-2px,0);
  }
}

/** Fin animación para los botones de importación/exportación **/

.label-muyS{
	background-color:#356AA0;
	font-size:12px;
}
.label-sa{
	background-color:#6BBA70;
	font-size:12px;
}
.label-poc{
	background-color:#F37D01;
	font-size:12px;
}
.label-muyP{
	background-color:#B02B2C;
	font-size:12px;
}
.label-ns{
	font-size:12px;
}

#tablaHistorico{
	width:50%;
}
