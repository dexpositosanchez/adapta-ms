<?php
  $seccionActiva=15;
  include_once('../cabecera.php');

  operacionesPersonal();
  $estadisticas=creaEstadisticasPersonal();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Personal registrado en el sistema:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-group"></i> <span class="value"><?php echo $estadisticas['personal']?></span> <br>Empleados registrados</div>
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Recursos Humanos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="<?php echo $_CONFIG['raiz']; ?>recursos-humanos/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Alta de Empleado</span> </a>
                <a href="../puestos-trabajos/index.php" class="shortcut"><i class="shortcut-icon icon-sitemap"></i><span class="shortcut-label">Puestos de Trabajo</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>planes-formacion/index.php" class="shortcut"><i class="shortcut-icon icon-pencil"></i><span class="shortcut-label">Planes Formación</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>generaListadoPersonal.php" class="shortcut noAjax"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Listado Personal</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Trabajadores registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Nombre del empleado </th>
					          <th> Función </th>
                    <th> Fecha de entrada en la compañía </th>
                    <th class="td-actions"> </th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimePersonal();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>


<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<?php include_once('../pie.php'); ?>
