function insertaFila(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody tr:last").clone();
    //Obtengo el atributo name para los inputs y selects
   $tr.find("input:not([type=checkbox]),select,textarea").attr("name", function(){
        //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
        var parts = this.id.match(/(\D+)(\d*)$/);
        //Creo un nombre nuevo incrementando el número de fila (++parts[2])
        return parts[1] + ++parts[2];
        
    }).attr("id", function(){//Hago lo mismo con los IDs
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });


    $tr.find("input[type=checkbox]").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox]),textarea").attr("value",""); //MODIFICACION OFICINA 12/06/2015

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
  
  filaAux++;
  var aux=filaAux;
  $('#cantidad'+aux).keyup(function(){
    oyente(aux);
  });
  
  $('#precioUnitario'+aux).keyup(function(){
    oyente(aux);
  });

  $('#descuento'+aux).keyup(function(){
    oyenteDescuento(aux);
  });

  $('#impuesto'+aux).change(function(){
    oyente(aux);
  });
}

function oyente(i){
  var cantidad=$('#cantidad'+i).val();
  var precio=$('#precioUnitario'+i).val();
  var descuento=$('#descuento'+i).val();
  if(cantidad != '' && precio != ''){
    if(descuento!=''){
      var precioInicio=parseFloat(precio)*parseFloat(cantidad);
      var totalDescuento=precioInicio-(precioInicio*(parseFloat(descuento)/100));
    }else{
      var totalDescuento=parseFloat(precio);
      $('#descuento'+i).val('0');
    }
    $('#totalDescuento'+i).val(totalDescuento);
    if($('#impuesto'+i).val()==21){
      iva=totalDescuento*21/100;
      total=totalDescuento+iva;
    } else {
      total=totalDescuento;
    }
    $('#neto'+i).val(Math.round(total*100)/100);
  }
}
  
  function oyenteDescuento(i){
  var cantidad=$('#cantidad'+i).val();
    var precio=$('#precioUnitario'+i).val();
    var descuento=$('#descuento'+i).val();
    if(cantidad != '' && precio != ''){
      if(descuento!=''){
        var precioInicio=parseFloat(precio)*parseFloat(cantidad);
        var totalDescuento=precioInicio-(precioInicio*(parseFloat(descuento)/100));
      }else{
        var totalDescuento=parseFloat(precio);
      }
      $('#totalDescuento'+i).val(totalDescuento);
      if($('#impuesto'+i).val()==21){
        iva=totalDescuento*21/100;
        total=totalDescuento+iva;
      } else {
        total=totalDescuento;
      }
      $('#neto'+i).val(Math.round(total*100)/100);
    }
  }

function eliminaFila(tabla){
  if($('#'+tabla).find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' tr').eq(fila).remove();
    });

    //Renumeración de los índices de la tabla
    var numFilas=$('#'+tabla+' tr:not(:first)').length;
    for(i=0;i<numFilas;i++){
       $('#'+tabla+' tr:not(:first)').eq(i).find("input:not([type=checkbox]),select,textarea").attr("name", function(){
            //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
            var parts = this.id.match(/(\D+)(\d*)$/);
            //Creo un nombre nuevo incrementando el número de fila (++parts[2])
            return parts[1] + i;
            //Hago lo mismo con los IDs
        }).attr("id", function(){
            var parts = this.id.match(/(\D+)(\d*)$/);
            return parts[1] + i;
        });

        $('#'+tabla+' tr:not(:first)').eq(i).find("input[type=checkbox]").attr("value", function(){
            var j=i+1
            return j;
        });
    }
    //Fin renumeración
  }
  filaAux=numFilas;
}