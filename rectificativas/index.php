<?php
  $seccionActiva=2;
  include_once('../cabecera.php');
  operacionesFacturas();

  $estadisticas=creaEstadisticasFacturas('',true, "AND tipoFactura='RECTIFICATIVA'");
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-tasks"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de facturas rectificativas:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat ancho50"> <i class="icon-ticket"></i> <span class="value"><?php echo $estadisticas['facturacion']?></span> <br>Facturas emitidas</div>
                    <!-- .stat -->
                    <div class="stat ancho50"> <i class="icon-credit-card"></i> <span class="value"><?php echo $estadisticas['cobradas']?></span> <br>Facturas rectificadas</div>
                    <!-- .stat --> 
				  </div>
				  <h6 class="bigstats">Totales:</h6>
				  <div id="big_stats" class="cf">
					<div class="stat ancho50"> <i class="icon-euro"></i> <span class="value"><?php echo $estadisticas['cobrado']?></span> <br>Total rectificado</div>
                    <!-- .stat --> 
					<div class="stat ancho50"> <i class="icon-exclamation-circle" id="iconoPlazoSuperado"></i> <span class="value" id='plazoSuperado'><?php echo $estadisticas['pendiente']?></span> <br>Total por rectificar</div>
                    <!-- .stat --> 
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de facturas rectificativas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva Factura</span> </a>
				<a href="#" id='cobradas' class="shortcut noAjax"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label">Marcar pagadas</span> </a>
				<a href="controlRecibos.php" class="shortcut"><i class="shortcut-icon icon-file-text"></i><span class="shortcut-label">Control Recibos</span> </a>
				<a href="#" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Facturas rectificativas emitidas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
					<th> Referencia </th>
					<th> Concepto </th>
                    <th> Alumno </th>
                    <th> Fecha de emisión </th>
					<th> Total </th>
					<th> ¿Enviada? </th>
					<th> ¿Rectificada? </th>
                    <th class="centro"> </th>
					<th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeFacturas('',true,"AND tipoFactura='RECTIFICATIVA'");

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<?php
	abreVentanaModal('Generación masiva facturas','cajaMasiva');
	campoFecha('fechaUno','Fecha inicio');
	campoFecha('fechaDos','Fecha fin');
	campoOculto('SI','generacionMasiva');
	cierraVentanaModal('enviarFacturas');
?>

</div>

<?php include_once('../pie.php'); ?>
<script src="../../api/js/jquery.dataTables.js"></script>
<script src="../../api/js/bootstrap.datatable.js"></script>
<script type="../../api/text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('#fechaUno').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('#fechaDos').datepicker({format:'dd/mm/yyyy',weekStart:1});
	if(parseInt($('#plazoSuperado').text())>0){
        $('#iconoPlazoSuperado').addClass('plazoSuperado');
    }
	$('#generacionMasiva').click(function(){
		$('#cajaMasiva').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#enviarFacturas').click(function(){
		$('#cajaMasiva').modal('hide');
		$('form').submit();		
	});
  });
	
	$('#cobradas').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes una factura.');
      }
      else{
        valoresChecks['cobrar']='SI';
        creaFormulario('index.php',valoresChecks,'post');
      }

    });
</script>