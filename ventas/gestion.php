<?php
  $seccionActiva=11;
  include_once("../cabecera.php");
  $i=gestionVentas();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/editor/ckeditor.js"></script>
<script type="text/javascript">
var editor = CKEDITOR.replace( 'descripcion' );

<?php
	if($i==0){
		$j=$i;
	}else{
		$j=$i-1;
	}
	echo "var filaAux=$j;";
?>
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectpicker').selectpicker();
	
	var i=0;
	while($('#cantidad'+i).val()!=undefined){
		$('#cantidad'+i).keyup(function(){
			var selector=obtieneFilaCampo($(this));
			oyente(selector);
		});
		
		$('#precioUnitario'+i).keyup(function(){
			var selector=obtieneFilaCampo($(this));
			oyente(selector);
		});

		$('#impuesto'+i).change(function(){
			var selector=obtieneFilaCampo($(this));
			oyente(selector);
		});
		
		$('#descuento'+i).keyup(function(){
			var selector=obtieneFilaCampo($(this));
			oyenteDescuento(selector);
		});
		i++;
	}

    $('#calcular').click(function(){
		var i=0;
		var total=0;
		while($('#neto'+i).val() != undefined){
			neto=$('#neto'+i).val() == '' ? 0 : $('#neto'+i).val();
			total+=parseFloat(neto);
			i++;
		}
		$('#subtotal').val(Math.round(total*100)/100);
	});

	$('#total').click(function(){
		var total=parseFloat($('#subtotal').val().replace(',','.'));

		var descuento=parseFloat($('#descuento').val().replace(',','.'));
		$('#descuento').val(descuento);

		var dto=parseFloat(descuento*total/100);

		$('#total').val(Math.round((total-descuento)*100)/100);
	});
  });
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>