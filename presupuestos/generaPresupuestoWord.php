<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común


  	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('presupuestos',$_GET['codigo']);
	$datosCliente=datosRegistro('clientes',$datos['empresa']);
	$datosConceptos=obtieneConceptosPresupuestos($_GET['codigo']);
	$codigo=$_GET['codigo'];
	
	//Carga de la librería PHPWord
	require_once '../../api/phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('../documentos/presupuestos/propuesta_formativa_20170109.docx');

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	

	$document->setValue("empresa",utf8_decode(str_replace('&','&#38;',$datosCliente['nombre'])));
	$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fechaValidez'])));
	$document->setValue("direccion",utf8_decode($datosCliente['direccion1']));
	$document->setValue("contacto",utf8_decode($datosCliente['personaContacto']));
	$document->setValue("email",utf8_decode($datosCliente['email1']));
	$document->setValue("telmov",utf8_decode($datosCliente['fax']));
	$document->setValue("telef",utf8_decode($datosCliente['telefono1']));
	$document->setValue("web",utf8_decode($datosCliente['website1']));
	$document->setValue("subtotal",utf8_decode($datos['subtotal']));

	//Where the magic begins
	$inclusiones='<w:tbl>
					<w:tblPr>
						<w:tblStyle w:val="Tablaconcuadrcula"/>
						<w:tblW w:w="0" w:type="auto"/>
						<w:tblInd w:w="1069" w:type="dxa"/>
						<w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
					</w:tblPr>
					<w:tblGrid>
						<w:gridCol w:w="3894"/>
						<w:gridCol w:w="3893"/>
					</w:tblGrid>';

	$consultaProductos=consultaBD("SELECT * FROM productos_presupuestos WHERE codigoPresupuesto='".$codigo."';");
	while($datosProductos=mysql_fetch_assoc($consultaProductos)){
		//$res.=$datosProductos['codigoProducto'].'....'.number_format((float)$datosProductos['neto'], 2, ',', ''). ' €'.'<w:br />';

		$inclusiones.='<w:tr w:rsidR="00BB507E" w14:paraId="0A07B83F" w14:textId="77777777" w:rsidTr="00BB507E">
						<w:tc>
							<w:tcPr><w:tcW w:w="4390" w:type="dxa"/><w:tcBorders><w:top w:val="nil"/><w:left w:val="nil"/><w:bottom w:val="nil"/><w:right w:val="nil"/></w:tcBorders></w:tcPr><w:p w14:paraId="0526ED8B" w14:textId="77777777" w:rsidR="00BB507E" w:rsidRDefault="00BB507E" w:rsidP="00AC134A"><w:pPr><w:rPr><w:rStyle w:val="EstiloHelveticaNeueLTStd11ptGris40"/><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r><w:t>'.utf8_decode($datosProductos['codigoProducto']).'</w:t></w:r></w:p>
						</w:tc>
						<w:tc>
							<w:tcPr><w:tcW w:w="4390" w:type="dxa"/><w:tcBorders><w:top w:val="nil"/><w:left w:val="nil"/><w:bottom w:val="nil"/><w:right w:val="nil"/></w:tcBorders></w:tcPr><w:p w14:paraId="41739429" w14:textId="77777777" w:rsidR="00BB507E" w:rsidRDefault="00BB507E" w:rsidP="00BB507E"><w:pPr><w:jc w:val="right"/><w:rPr><w:rStyle w:val="EstiloHelveticaNeueLTStd11ptGris40"/><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r><w:t>'.formateaNumeroWeb($datosProductos['neto']).' €</w:t></w:r></w:p>
						</w:tc>
					</w:tr>';

	}
	
					
	$inclusiones.='</w:tbl>';//The magic ends here, shurmano
	
	$res=str_replace('€', '&#8364;', $inclusiones);

	$document->setValue("presupuesto",$res);
	/*$document->setValue("operario",utf8_decode($datos['operario']));
	$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fecha'])));
	$document->setValue("numAviso",utf8_decode($datos['numAviso']));
	$document->setValue("incidencia",utf8_decode($datos['incidencia']));
	$document->setValue("observaciones",utf8_decode($datos['observaciones']));
	$document->setValue("incidencia",utf8_decode($inclusiones));*/


	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$document->save('../documentos/presupuestos/Presupuesto'.$codigo.'.docx');


	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Presupuesto".$codigo.".docx");
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('../documentos/presupuestos/Presupuesto'.$codigo.'.docx');


?>