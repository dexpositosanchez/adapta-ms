<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores

function operacionesGastos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('otros_gastos');
	}
	elseif(isset($_POST['gasto'])){
		$res=insertaDatos('otros_gastos');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('otros_gastos');
	}

	mensajeResultado('gasto',$res,'Gasto');
    mensajeResultado('elimina',$res,'Gasto', true);
}

function listadoGastos(){
	global $_CONFIG;

	$columnas=array('gasto','importe','codigo','codigo');
	$having=obtieneWhereListado("WHERE 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, gasto, importe FROM otros_gastos $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, gasto, importe FROM otros_gastos $having;");
	cierraBD();
	
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['gasto'],
			$datos['importe']." €",
			botonAcciones(array('Detalles'),array('otros_gastos/gestion.php?codigo='.$datos['codigo']),array("icon-search-plus")),
        	"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionGastos(){
	operacionesGastos();

	abreVentanaGestion('Gestión de Gastos','index.php','span3');
	$datos=compruebaDatos('otros_gastos');
	
	abreColumnaCampos('span11');
	campoTexto('gasto','Gasto',$datos);
	campoTextoSimbolo('importe','Importe',"<i class='icon-eur'></i>",$datos);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}



function filtroCursos(){
	abreCajaBusqueda();

	campoTexto(0,'Gasto');
	campoTextoSimbolo(1,'Precio','<i class="icon-eur"></i>');
	cierraCajaBusqueda();
}