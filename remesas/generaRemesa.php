<?php
require_once("../../api/sepassd/SEPASDD.php");
require_once("funciones.php");
				
$codigoRemesa=$_GET['codigo'];

$pagos[]=array();	
$i=0;
$fecha = date('Y-m-d');
$nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'Y-m-d' , $nuevafecha );

conexionBD();
	$datosRemesa=consultaBD("SELECT * FROM remesas WHERE codigo='$codigoRemesa';",false,true);
				
	$consulta=consultaBD("SELECT CONCAT(trabajadores_cliente.nombre,' ',trabajadores_cliente.apellido1,' ',trabajadores_cliente.apellido2) AS alumno, trabajadores_cliente.numCuenta, trabajadores_cliente.bic, vencimientos_facturas.importe, facturacion.referencia
	FROM vencimientos_remesas INNER JOIN vencimientos_facturas ON vencimientos_remesas.codigoVencimiento=vencimientos_facturas.codigo
	INNER JOIN facturacion ON vencimientos_facturas.codigoFactura=facturacion.codigo
	INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo INNER JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo
	WHERE vencimientos_remesas.codigoRemesa='$codigoRemesa' ORDER BY referencia;");
cierraBD();

$config = array("name" => "DAIDA PEREZ HERNANDEZ - AVEFOR",
                "IBAN" => "ES2821005450630200093991",
                "BIC" => "CAIXESBB",
                "batch" => true,
                "creditor_id" => "ES9000178702023W",
                "currency" => "EUR",
				"tipoRemesa" => $datosRemesa['tipoSepa'],
				"id_cabecera" => '78702023W'
				//"version" => "3"
                );

while($datos=mysql_fetch_assoc($consulta)){
	$datos['total']=str_replace(',','.',$datos['importe']);
	$total=$datos['total'];
	$total=number_format($total, 2, '', '');
	$pagos[$i]=array("name" => sanear_string($datos['alumno']),
                 "IBAN" => $datos['numCuenta'],
                 "BIC" => $datos['bic'],
                 "amount" => $total,
                 "type" => "RCUR",
                 "collection_date" => $datosRemesa['fecha'],
                 "mandate_id" => $datos['referencia'],
                 "mandate_date" => $nuevafecha,
                 "description" => "CUOTA"
                );
	$i++;
}
              
try{				
    $SEPASDD = new SEPASDD($config);
	foreach ($pagos as &$valor) {
		$SEPASDD->addPayment($valor);
	}
    $xml=$SEPASDD->save();
	$myfile = fopen("../documentos/remesa".str_replace('-','',$datosRemesa['fecha']).".xml", "w") or die("Imposible abrir el fichero");
	fwrite($myfile, $xml);
	fclose($myfile);
	
	// Definir headers
	header("Content-Type: application/xml");
	header("Content-Disposition: attachment; filename=remesa".str_replace('-','',$datosRemesa['fecha']).".xml");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile("../documentos/remesa".str_replace('-','',$datosRemesa['fecha']).".xml");
}catch(Exception $e){
	header('Location: '.$_CONFIG['raiz'].'remesas/mensajeError.php');
}

?>
