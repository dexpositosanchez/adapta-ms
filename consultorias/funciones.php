<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

function creaEstadisticasSoftware(){
	$datos=array();
	$where=compruebaPerfilIncidenciasSoftware();

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM software $where AND codigoCliente IS NOT NULL;",false,true);
	$datos['total']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM software $where AND realizado='1' AND codigoCliente IS NOT NULL;",false,true);
	$datos['pendientes']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM software $where AND realizado='2' AND codigoCliente IS NOT NULL;",false,true);
	$datos['desarrollo']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM software $where AND realizado='3' AND codigoCliente IS NOT NULL;",false,true);
	$datos['finalizadas']=$consulta['total'];

	//Tareas cuyo plazo de finalización es igual o anterior a la fecha dentro de 2 días.
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM software $where AND (realizado='1' OR realizado='2') AND fechaPrevista<=DATE_ADD(CURDATE(),INTERVAL 2 DAY) AND codigoCliente IS NOT NULL;",false,true);
	$datos['plazoSuperado']=$consulta['total'];

	cierraBD();

	return $datos;
}

function compruebaPerfilIncidenciasSoftware(){
	$where='';
	if($_SESSION['tipoUsuario']=='ADMIN'){
		$where="WHERE 1=1";//Condición irrelevante, pero necesaria para encajar bien la función en las consultas que tengan más de una condición
	}
	else{
		$codigoU=$_SESSION['codigoU'];
		//El código 11 corresponde a Hugo
		$where="WHERE (software.codigo IN (SELECT codigoSoftware FROM tareasSoftware WHERE codigoUsuario='$codigoU' OR codigoUsuario='11') OR software.codigo NOT IN (SELECT codigoSoftware FROM tareasSoftware WHERE codigoSoftware IS NOT NULL))";//La condición de la consulta incluye 2 subconsultas: para comprobar que el proyecto tiene tareas asignadas al usuario actual, o bien el proyecto no tiene asignada ninguna tarea (por tratarse de una incidencia no atendida).
	}
	return $where;
}

function generaEventosCalendarioSoftware(){
	$where=compruebaPerfilParaWhere('tareasSoftware.codigoUsuario');

	$tareas="events: [";

	conexionBD();
	$consulta=consultaBD("SELECT tareasSoftware.codigo, software.codigo AS codigoSoftware, empresa, actividad, tipoTarea, realizado, tareasSoftware.codigoUsuario, fechaInicio, horaInicio, fechaFin, horaFin, tareasSoftware.fechaPrevista, horaPrevista FROM (software INNER JOIN clientes ON software.codigoCliente=clientes.codigo) INNER JOIN tareasSoftware ON software.codigo=tareasSoftware.codigoSoftware $where AND realizado!='3' AND tareasSoftware.codigoUsuario IS NOT NULL;");
	obtieneProgramadoresCalendarioSoftware();
	cierraBD();

	$colores=array(
		'CREACION'=>"backgroundColor: '#7acc76', borderColor: '#00a100'",//Verde
		'DEMO'=>"backgroundColor: '#7acc76', borderColor: '#00a100'",//Verde
		'INCIDENCIA'=>"backgroundColor:'#F22C2C', borderColor:'#610B0B'",//Rojo
		'OTROS'=>"backgroundColor:'#5e96ea', borderColor:'#3f85f5'",//Azul
		'MODIFICACION'=>"backgroundColor:'#f89406', borderColor:'#c67605'"//Amarillo
	);
	$tipos=array('CREACION'=>'Creación','DEMO'=>'Personalización','MODIFICACION'=>'Modificaciones y/o mejoras','INCIDENCIA'=>'Corrección de errores','OTROS'=>'Otros');
	$estado=array('1'=>'<i class="icon-flag"></i>','2'=>'<i class="icon-time"></i>');

	while($datos=mysql_fetch_assoc($consulta)){
		$fecha=obtieneFechasActividad($datos);
		$color=$colores[$datos['tipoTarea']];

		$tareas.="
			{
				id: '".$datos['codigo']."',
				resourceId: ".$datos['codigoUsuario'].",
				codigoSoftware: ".$datos['codigoSoftware'].",
	    		cabecera:'".$estado[$datos['realizado']]." - ".$tipos[$datos['tipoTarea']]."',
	    		title: '".$datos['empresa'].". ".$datos['actividad']."',
			    start: new Date(".$fecha['anioInicio'].", ".$fecha['mesInicio'].", ".$fecha['diaInicio'].", ".$fecha['horaInicio'].", ".$fecha['minutoInicio']."),
		    	end: new Date(".$fecha['anioFin'].", ".$fecha['mesFin'].", ".$fecha['diaFin'].", ".$fecha['horaFin'].", ".$fecha['minutoFin']."),
		    	allDay: false,
		    	".$color."
	    	},";
  	}

  	if($tareas!='events: ['){
  		$tareas=substr_replace($tareas, '', strlen($tareas)-1, strlen($tareas));//Para quitar última coma
  	}

  	$tareas.="]";

  	echo $tareas;
}

function imprimeTareasSoftware($realizadas=false){				//MODIFICADO JOSE LUIS 06/04/15
	$where=compruebaPerfilIncidenciasSoftware();

	if($realizadas){
		$where.=" AND realizado='3'";
	}
	else{
		$where.=" AND (realizado='1' OR realizado='2')";
	}

	$consulta=consultaBD("SELECT software.*, clientes.nombre AS empresa, presupuestos.referenciaPresupuesto FROM software LEFT JOIN clientes ON software.codigoCliente=clientes.codigo
	LEFT JOIN presupuestos ON software.codigoPresupuesto=presupuestos.codigo $where ORDER BY fechaSolicitud DESC;",true);
	$tipos=array('CREACION'=>'Creación','DEMO'=>'Personalización','MODIFICACION'=>'Modificaciones y/o mejoras','INCIDENCIA'=>'Corrección de errores','OTROS'=>'Otros');
	$realizado=array('1'=>'<span class="label label-danger"><i class="icon-remove-sign"></i> Pendiente</span>','2'=>'<span class="label label-warning"><i class="icon-time"></i> En desarrollo</span>','3'=>'<span class="label label-success"><i class="icon-ok-sign"></i> Finalizado</span>');
	
	while($datos=mysql_fetch_assoc($consulta)){
		$celdasExtra='';
		if($realizadas){
			$celdasExtra="<td class='nowrap'> ".formateaFechaWeb($datos['fechaFinalizacion'])." </td>";
			$celdasExtra.="<td class='span3'> ".$datos['comentariosProgramacion']." </td>";
		}
		else{
			$celdasExtra="<td class='centro'> ".$realizado[$datos['realizado']]." </td>";
		}

		echo "
		<tr>
        	<td> ".$datos['empresa']." </td>
			<td> ".$datos['referenciaPresupuesto']." </td>
        	<td> <a href='#' class='enlacePopOver noAjax'>".$tipos[$datos['tipoTarea']]."</a> <div class='hide'>".$datos['observaciones']."<br /><br /> <a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-small btn-primary'><i class='icon-plus-circle'></i> Ver tarea</a> <button tupe='button' class='btn btn-small btn-default cerrar'><i class='icon-remove'></i> Cerrar</button></div> </td>
        	<td class='nowrap'> ".formateaFechaWeb($datos['fechaSolicitud'])." </td>
        	<td class='nowrap'> ".formateaFechaWeb($datos['fechaPrevista'])." </td>
        	".$celdasExtra."
        	<td class='centro'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-plus-circle'></i> Ver Tarea</i></a>
        	</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
	}
}

function obtieneFechasActividad($datos){
	$res=array();

	$fechaInicio=explode('-',$datos['fechaInicio']);
	$horaInicio=explode(':',$datos['horaInicio']);

	if($datos['fechaFin']=='0000-00-00'){
		$fechaFin=explode('-',$datos['fechaPrevista']);
		$horaFin=explode(':',$datos['horaPrevista']);
	}
	else{
		$fechaFin=explode('-',$datos['fechaFin']);	
		$horaFin=explode(':',$datos['horaFin']);
	}

	$fechaInicio[1]--;
	$fechaFin[1]--;
	
	$res['diaInicio']=$fechaInicio[2];
	$res['mesInicio']=$fechaInicio[1];
	$res['anioInicio']=$fechaInicio[0];
	$res['horaInicio']=$horaInicio[0];
	$res['minutoInicio']=$horaInicio[1];

	$res['diaFin']=$fechaFin[2];
	$res['mesFin']=$fechaFin[1];
	$res['anioFin']=$fechaFin[0];
	$res['horaFin']=$horaFin[0];
	$res['minutoFin']=$horaFin[1];

	return $res;
}

function obtieneProgramadoresCalendarioSoftware(){
	$where=compruebaPerfilParaWhere('codigo');

	$programadores='resources:[';
	$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS nombre FROM usuarios $where AND tipo IN('TECNICO') AND codigo!='14' AND codigo!='11';");//14 y 11 son los códigos de Tere y Hugo, respectivamente
	while($datos=mysql_fetch_assoc($consulta)){
		$programadores.="{ name: '".$datos['nombre']."', id: ".$datos['codigo']." },";	
	}

    $programadores=substr_replace($programadores, '', strlen($programadores)-1, strlen($programadores));//Para quitar última coma
    $programadores.='],';

    echo $programadores;
}

function campoSelectProyectoCalendario(){
	$valores=array();
	$nombres=array();
	$where=compruebaPerfilIncidenciasSoftware();
	$consulta=consultaBD("SELECT software.codigo, CONCAT(empresa,' - ',DATE_FORMAT(fechaSolicitud,'%d/%m/%Y')) AS texto, software.observaciones FROM software LEFT JOIN clientes ON software.codigoCliente=clientes.codigo $where AND (realizado='1' OR realizado='2') ORDER BY fechaSolicitud DESC;",true);

	$i=0;
	while($datos=mysql_fetch_assoc($consulta)){
		array_push($valores,$datos['codigo']);
		array_push($nombres,$datos['texto']);
		divOculto($datos['observaciones'],'observaciones'.$i);
		$i++;
	}

	campoSelect('codigoSoftware','Proyecto',$nombres,$valores,false,'selectpicker span4 show-tick');
}

function botonesCalendarioSoftware(){
	echo "<div id='fechaCalendario'>
			<span>Ir a: </span>";
			campoTextoSolo('campoFechaCalendario',fecha(),'input-small datepicker hasDatepicker');
	echo "</div>
		  <div id='botonImprimir'>
			<button type='button' class='btn btn-small btn-info' onclick='window.print();'><i class='icon-print'></i> Imprimir</button>	
		 </div>";
}

function creaSoftware(){
	$res=insertaDatos('software');
	if($res){
		$res=insertaTareasSoftware($res);
	}

	return $res;
}

function actualizaSoftware(){
	$res=actualizaDatos('software');
	if($res){
		$res=insertaTareasSoftware($_POST['codigo'],true);
	}
	return $res;
}

function insertaTareasSoftware($codigoSoftware,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	if($actualizacion){
		$res=consultaBD("DELETE FROM tareasSoftware WHERE codigoSoftware='$codigoSoftware';");
	}

	for($i=0;isset($datos['actividad'.$i]);$i++){
		$res=$res && consultaBD("INSERT INTO tareasSoftware VALUES(NULL,'".$datos['actividad'.$i]."','".$datos['fechaInicio'.$i]."',
			'".$datos['horaInicio'.$i]."','".$datos['fechaPrevista'.$i]."','".$datos['horaPrevista'.$i]."', '".$datos['fechaFin'.$i]."','".$datos['horaFin'.$i]."',
			'".$datos['tiempo'.$i]."','$codigoSoftware','".$datos['codigoUsuario'.$i]."');");
	}
	cierraBD();

	return $res;
}

function operacionesProyectos(){
  $res=false;
  if(isset($_POST['codigo'])){
    $res=actualizaSoftware();
  }
  elseif(isset($_POST['tipoTarea'])){
    $res=creaSoftware();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('software');
  }
  
	mensajeResultado('tipoTarea',$res,'tarea');
    mensajeResultado('elimina',$res,'tarea', true);
}

function gestionProyectos(){
	operacionesProyectos();
	
	abreVentanaGestion('Gestión de Consultoría','index.php');
	$datos=compruebaDatos('software');

	if(!isset($datos['codigo'])){
		campoOculto('NO','creadoCliente');
	}else{	
		campoOculto($datos['codigoPresupuesto'],'codigoPresupuestoSeleccionado');
	}
	campoOculto($_SESSION['codigoU'],'codigoUsuario');
	  $consulta="SELECT codigo, nombre AS texto FROM clientes";
	  campoSelectConsulta('codigoCliente','Cliente',$consulta,$datos,'selectpicker span6 show-tick');
	  echo "<span id='resultadoDos'></span>";
	  campoSelect('tipoTarea','Tipo de tarea',array('Creación','Personalización','Modificaciones y/o mejoras','Corrección de errores','Otros'),array('CREACION','DEMO','MODIFICACION','INCIDENCIA','OTROS'),$datos);
	  
	  echo "<div id='cajaOtros'>";
	  campoTexto('textoOtros','Tarea',$datos);
	  echo "</div>";

	  campoFecha('fechaSolicitud','Fecha de solicitud',$datos);
	  campoFecha('fechaPrevista','Fecha prevista de finalización',$datos);
	  /*$consulta="SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='TECNICO' AND codigo!='14';";
	  campoSelectConsulta('codigoUsuario','Programador asignado',$consulta);
	  campoRadio('realizado','Estado','1',array('Pendiente','En desarrollo','Finalizado'),array('1','2','3'));*/
	  campoSelectEstadoProyecto($datos);
	  creaTablaDesarrollo($datos['codigo']);

	  echo "<div id='cajaRealizado'>";
		campoFecha('fechaFinalizacion','Fecha de finalización',$datos);
		campoTexto('tiempoEmpleado','Tiempo empleado',$datos,'input-small');
	  echo "</div>";

	  areaTexto('observaciones','Observaciones',$datos,'areaInforme');
	  areaTexto('comentariosProgramacion','Comentarios trabajador',$datos,'areaInforme');

	cierraVentanaGestion('index.php',false);
	
	return $datos;
}

function campoSelectEstadoProyecto($valor=false){
	$valor=compruebaValorCampo($valor,'realizado');
	$opciones=array('','<option value="1" data-content="<span class=\'label label-danger\'><i class=\'icon-flag\'></i> Pendiente</span>"','<option value="2" data-content="<span class=\'label label-warning\'><i class=\'icon-clock-o\'></i> En desarrollo</span>"','<option value="3" data-content="<span class=\'label label-success\'><i class=\'icon-check\'></i> Finalizado</span>"');

	echo '
	<div class="control-group">                     
      <label class="control-label" for="realizado">Estado:</label>
      <div class="controls">
        
        <select name="realizado" class="selectpicker">';
    		for($i=1;$i<=count($opciones);$i++){
    			echo $opciones[$i];
	    		if($valor!=false && $valor==$i){
	    			echo "selected='selected'";
	    		}
	    		echo "></span>";
	    	}
    echo '
        </select>
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->';
}

function creaTablaDesarrollo($codigoSoftware=false){
	echo "
	<h3 class='apartadoFormulario'>Tareas vinculadas a la consultoría</h3>

	<table class='table table-striped table-bordered' id='tablaDesarrollo'>
      <thead>
        <tr>
          <th> Actividad </th>
          <th> Trabajador asignado </th>
          <th> Fecha Inicio </th>
          <th> Hora Inicio</th>
          <th> Fecha prev. </th>
          <th> Hora prev. </th>
          <th> Fecha Fin </th>
          <th> Hora Fin </th>
          <th> Tiempo empleado </th>
		  <th> </th>
        </tr>
      </thead>
      <tbody>";
      
      conexionBD();
      if(isset($codigoSoftware)){
      	$i=0;
      	$consulta=consultaBD("SELECT * FROM tareasSoftware WHERE codigoSoftware='$codigoSoftware';");
      	while($datos=mysql_fetch_assoc($consulta)){
			$j=$i+1;
      		echo "<tr>";
	      		campoTextoTabla('actividad'.$i,$datos['actividad']);
	      		campoSelectConsulta('codigoUsuario'.$i,'',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo!='DOCENTE';",$datos['codigoUsuario'],'input-medium','','',1,false);
	      		campoFechaTabla('fechaInicio'.$i,$datos['fechaInicio']);
	      		campoTextoTabla('horaInicio'.$i,formateaHoraWeb($datos['horaInicio']),'input-mini');
	      		campoFechaTabla('fechaPrevista'.$i,$datos['fechaPrevista']);
	      		campoTextoTabla('horaPrevista'.$i,formateaHoraWeb($datos['horaPrevista']),'input-mini');
	      		campoFechaTabla('fechaFin'.$i,$datos['fechaFin']);
	      		campoTextoTabla('horaFin'.$i,formateaHoraWeb($datos['horaFin']),'input-mini');
				campoTextoTabla('tiempo'.$i,$datos['tiempo'],'input-small');
      		echo "
				<td>
					<input type='checkbox' name='filasTabla[]' value='$j'>
	        	</td>
			</tr>";
      		$i++;
      	}
      }
      else{
      	echo "<tr>";
	  		campoTextoTabla('actividad0');
            campoSelectConsulta('codigoUsuario0','',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo!='DOCENTE';",false,'input-medium','','',1,false);
      		campoFechaTabla('fechaInicio0','0000-00-00');
      		campoTextoTabla('horaInicio0','','input-mini');
      		campoFechaTabla('fechaPrevista0','0000-00-00');
      		campoTextoTabla('horaPrevista0','','input-mini');
      		campoFechaTabla('fechaFin0','0000-00-00');
      		campoTextoTabla('horaFin0','','input-mini');
			campoTextoTabla('tiempo0','','input-small');
  		echo "
			<td>
				<input type='checkbox' name='filasTabla[]' value='1'>
			</td>
		</tr>";
      }
      cierraBD();

    echo "</tbody>
   </table>
   <center>
   <button type='button' class='btn btn-success btn-small' onclick='insertaFila(\"tablaDesarrollo\");'><i class='icon-plus'></i> Añadir fila</button> 
   <button type='button' class='btn btn-danger btn-small' onclick='eliminaFila(\"tablaDesarrollo\");'><i class='icon-trash'></i> Eliminar fila</button>
   </center>
   <br /><br /><br />";
}

function imprimeTodo(){						//MODIFICADO 06/04/15 JOSE LUIS
	$where=compruebaPerfilIncidenciasSoftware();
	$tipos=array('CREACION'=>'Creación','DEMO'=>'Personalización','MODIFICACION'=>'Modificaciones y/o mejoras','INCIDENCIA'=>'Incidencias','OTROS'=>'Otros');
	
	conexionBD();
	$consulta=consultaBD("SELECT software.*, clientes.nombre AS empresa FROM software INNER JOIN clientes ON software.codigoCliente=clientes.codigo $where AND realizado='1' ORDER BY fechaSolicitud DESC");

	echo "<td>";
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<span class='label label-danger'><a href='detallesSoftware.php?codigo=".$datos["codigo"]."'>".$datos['empresa']." (".$tipos[$datos['tipoTarea']].")</a></span><br /><br />";
	}
	echo "</td><td>";


	$consulta=consultaBD("SELECT software.*, clientes.nombre AS empresa FROM software INNER JOIN clientes ON software.codigoCliente=clientes.codigo $where AND realizado='2' ORDER BY fechaSolicitud DESC");
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<span class='label label-warning'><a href='detallesSoftware.php?codigo=".$datos["codigo"]."'>".$datos['empresa']." (".$tipos[$datos['tipoTarea']].")</a></span><br /><br />";
	}
	echo "</td><td>";

	$consulta=consultaBD("SELECT software.*, clientes.nombre AS empresa FROM software INNER JOIN clientes ON software.codigoCliente=clientes.codigo $where AND realizado='3' ORDER BY fechaSolicitud DESC");
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<span class='label label-success'><a href='detallesSoftware.php?codigo=".$datos["codigo"]."'>".$datos['empresa']." (".$tipos[$datos['tipoTarea']].")</a></span><br /><br />";
	}
	echo "</td>";

	cierraBD();
}

function estadisticasTiempos(){					//NUEVO JOSE LUIS 06/04/15
	$where=compruebaPerfilParaWhere('tareasSoftware.codigoUsuario');
	return consultaBD("SELECT COUNT(tareasSoftware.codigo) AS total FROM tareasSoftware INNER JOIN software ON tareasSoftware.codigoSoftware=software.codigo $where AND fechaInicio<=CURDATE();",true,true);
}

function imprimeTiempos(){					//MODIFICADO JOSE LUIS 06/04/15
	$where=compruebaPerfilParaWhere('tareasSoftware.codigoUsuario');
	$tipos=array('CREACION'=>'Creación','DEMO'=>'Personalización','MODIFICACION'=>'Modificaciones y/o mejoras','INCIDENCIA'=>'Corrección de errores','OTROS'=>'Otros');
	$consulta=consultaBD("SELECT software.codigo AS codigo, clientes.nombre AS empresa, tipoTarea, usuarios.nombre, actividad, fechaInicio, horaInicio, fechaFin, horaFin, tiempo, presupuestos.referenciaPresupuesto 
	FROM ((software INNER JOIN clientes ON software.codigoCliente=clientes.codigo) 
	INNER JOIN tareasSoftware ON software.codigo=tareasSoftware.codigoSoftware) 
	INNER JOIN usuarios ON tareasSoftware.codigoUsuario=usuarios.codigo
	LEFT JOIN presupuestos ON software.codigoPresupuesto=presupuestos.codigo $where AND fechaInicio<=CURDATE() ORDER BY fechaFin, horaFin DESC",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td> ".$datos['empresa']." </td>
				<td> ".$datos['referenciaPresupuesto']." </td>
				<td> ".$tipos[$datos['tipoTarea']]." </td>
				<td> ".$datos['nombre']."</td>
				<td> ".$datos['actividad']." </td>
				<td> ".formateaFechaWeb($datos['fechaInicio'])." </td>
				<td> ".formateaHoraWeb($datos['horaInicio'])." </td>
				<td> ".formateaFechaWeb($datos['fechaFin'])." </td>
				<td> ".formateaHoraWeb($datos['horaFin'])." </td>
				<td> ".$datos['tiempo']." </td>
				<td class='centro'>
	        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-plus-circle'></i> Detalles</i></a>
	        	</td>
			 </tr>";
	}
}