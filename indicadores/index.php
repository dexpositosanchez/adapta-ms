<?php
	$seccionActiva=15;
	include_once('../cabecera.php');
	operacionesIndicadores();
	$valores=obtieneValoresIndicadores();
?>

<center id="contenido">
	<h3 class="tituloMapaProcesos">MAPA DE PROCESOS</h3><br>
	<table class="tablaMapaProcesos">
		<tr>
			<td rowspan="5" class="celdaVertical"><p>Requisitos del Cliente</p></td>
			<td rowspan="5" class="celdaFlecha"><img src="../img/flechaMapaProceso_d.png" /></td>
			<td colspan="2">
				<table class="tablaInterna">
					<tr>
						<th colspan="11">Procesos Estratégicos</th>
					</tr>
					<tr>
						<td colspan="11">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<?php
							$datos = obtieneIndicadores('ESTRATEGICOS');
							while($indicador=mysql_fetch_assoc($datos)){
						?>
						<td class="cajasContenedor" id="id<?php echo $indicador['codigo']; ?>"><img src="../img/indicador<?php echo $valores[$indicador['codigo']]; ?>.png" /><?php echo $indicador['nombre']; ?></td>
						<td>&nbsp;</td>
						<?php
							}
						?>
					</tr>
					<tr>
						<td colspan="11">&nbsp;</td>
					</tr>
				</table>
			</td>
			<td rowspan="5" class="celdaFlecha"><img src="../img/flechaMapaProceso_d.png" /></td>
			<td rowspan="5" class="celdaVertical"><p>Satisfacción del Cliente</p></td>
		</tr>
		<tr>
			<th><img src="../img/flechaMapaProceso_a.png" /></th>
			<th><img src="../img/flechaMapaProceso_ar.png" /></th>
		</tr>
		<tr>
			<td colspan="2">
				<table class="tablaInterna">
					<tr>
						<th colspan="11">Procesos Misionales</th>
					</tr>
					<tr>
						<td colspan="11">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<?php
							$datos = obtieneIndicadores('MISIONALES');
							while($indicador=mysql_fetch_assoc($datos)){
						?>
						<td class="cajasContenedor" id="id<?php echo $indicador['codigo']; ?>"><img src="../img/indicador<?php echo $valores[$indicador['codigo']]; ?>.png" /><?php echo $indicador['nombre']; ?></td>
						<td>&nbsp;</td>
						<?php
							}
						?>
					</tr>
					<tr>
						<td colspan="11">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th><img src="../img/flechaMapaProceso_a.png" /></th>
			<th><img src="../img/flechaMapaProceso_ar.png" /></th>
		</tr>
		<tr>
			<td colspan="2">
				<table class="tablaInterna">
					<tr>
						<th colspan="11">Procesos Apoyo</th>
					</tr>
					<tr>
						<td colspan="11">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<?php
							$datos = obtieneIndicadores('APOYO');
							while($indicador=mysql_fetch_assoc($datos)){
						?>
						<td class="cajasContenedor" id="id<?php echo $indicador['codigo']; ?>"><img src="../img/indicador<?php echo $valores[$indicador['codigo']]; ?>.png" /><?php echo $indicador['nombre']; ?></td>
						<td>&nbsp;</td>
						<?php
							}
						?>
					</tr>
					<tr>
						<td colspan="11">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<div class="margenMapa"></div>
<div align='center' class='divBoton'>
	<a style='margin-bottom: 40px;' class='btn btn-propio' href='gestion.php?enlace=index.php'>
		<i class='icon-plus-circle'></i>
		Nuevo proceso
	</a>
</div>
</center>
<?php


include_once('../pie.php');
?>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/customBox/jquery.custombox.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.cajasContenedor').click(function(){
			var area=$(this).attr('id');
			area = area.replace('id','');
			window.location='indicadoresArea.php?area='+area;
		});
	});
</script>
