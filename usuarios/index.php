<?php
  $seccionActiva=10;
  include_once('../cabecera.php');
  
  operacionesUsuarios();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de accesos al sistema durante el día de hoy:</h6>

                  <canvas id="area-chart" class="chart-holder" width="538" height="250"></canvas>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Usuarios</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <?php compruebaPerfil('ADMIN','<a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo usuario</span> </a><a href="javascript:void" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>'); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Usuarios registrados</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroUsuarios();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaUsuarios">
                <thead>
                  <tr>
                    <th> Nombre </th>
                    <th> Teléfono </th>
                    <th> eMail </th>
                    <th> Usuario </th>
                    <th> Perfil </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-sign-in"></i>
              <h3>Accesos al sistema</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id="tablaAccesos">
                <thead>
                  <tr>
                    <th> Usuario </th>
                    <th> Fecha </th>
                    <th> Hora </th>
                    <th> Dirección IP </th>
                    <th> Navegador </th>
                    <th> Sistema Operativo </th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeAccesos();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>



      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="../../api/js/jquery.dataTables.js"></script>
<script src="../../api/js/bootstrap.datatable.js"></script>

<script src="../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="../../api/js/chart.js" type="text/javascript"></script>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>

<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaUsuarios','../listadoAjax.php?include=usuarios&funcion=listadoUsuarios();');
    listadoTabla('#tablaAccesos','../listadoAjax.php?include=usuarios&funcion=listadoAccesos();');

    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaUsuarios');
  });

  <?php $datos=generaDatosGraficoAccesos(); ?>
  var datosGrafico = {
            labels: <?php echo $datos['etiquetas']; ?>,
            datasets: [
        {
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            data: <?php echo $datos['accesos']; ?>
        }
      ]

  }

  var opciones={
    scaleOverride : true,
    scaleSteps : <?php echo $datos['max']; ?>,
    scaleStepWidth : 1, //Si el número de incidencias es menor que 10, la escala del gráfico va de 1 en 1, sino de 5 en 5.
    scaleStartValue : 0,
    barValueSpacing:60,
    barDatasetSpacing:20
  }

  var grafico = new Chart(document.getElementById("area-chart").getContext("2d")).Line(datosGrafico,opciones);
</script>
<?php

switch(json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - Sin errores';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Excedido tamaño máximo de la pila';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Desbordamiento de buffer o los modos no coinciden';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Encontrado carácter de control no esperado';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Error de sintaxis, JSON mal formado';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Caracteres UTF-8 malformados, posiblemente están mal codificados';
        break;
        default:
            echo ' - Error desconocido';
        break;
    }
?>

<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>