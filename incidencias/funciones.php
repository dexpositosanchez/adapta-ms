<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias


function creaIncidencia(){
	$res=true;

	$datos=arrayFormulario();

	conexionBD();
	$consulta=consultaBD("INSERT INTO mejoraContinua VALUES(NULL,'".$datos['tipo']."','".$datos['fecha']."','".$datos['area']."','".$datos['responsable']."',
		'".$datos['emailResponsable']."','".$datos['descripcion']."','".$datos['causas']."','".$datos['actuaciones']."','".$datos['correccion']."',
		'".$datos['responsableImplantacion']."','".$datos['emailResponsableImplantacion']."','".$datos['fechaPrevista']."','".$datos['resuelta']."',
		'".$datos['fechaResolucion']."');");
	cierraBD();

	if(!$consulta){
		$res=false;
	}
	else{
		$headers="From: programacion@qmaconsultores.com\r\n";
		$headers.= "MIME-Version: 1.0\r\n";
		$headers.= "Content-Type: text/html; charset=UTF-8";

		$mail_destinatario=$datos['emailResponsable'];

		$tipos=array('nc'=>'No Conformidad','incidencia'=>'Incidencia');
		$tipo=$tipos[$datos['tipo']];

		$mensaje="Se ha creado una nueva $tipo con la siguiente descripción: <br><br><i><b>".$datos['descripcion']."</b></i><br><br>";

		$mensaje.="
			<div style='font:15px Calibri'>
				<a href='http://www.avefor.com' style='font:italic bold 19px Calibri'>www.avefor.com</a>
				<br /><br />
				<img src='http://www.crmparapymes.com.es/avefor/img/logo2.png' />
				<br /><br /><br />
				<b>CLAUSULA DE CONFIDENCIALIDAD</b>: Este mensaje se dirige exclusivamente a su destinatario y puede contener
				información privilegiada o confidencial. Si no es Vd.el destinatario indicado, queda notificado de que la divulgación y/o copia sin autorización está prohibida en virtud de la legislación vigente. Si ha recibido este mensaje por error, le rogamos que nos lo comunique inmediatamente por esta misma via y proceda a su destrucción.Avefor Formación, S.L. garantiza la máxima confidencialidad y seguridad de sus datos de conformidad con lo dispuesto en la Ley Orgánica 15/1999, de Protección de Datos de carácter personal, pudiendo ejercitar los derechos de acceso, rectificación, cancelación y oposición comunicándolo por escrito a esta dirección: Plaza de Extremadura, 8 22004 -  Huesca, o mediante correo electrónico a <a href='mailto:programacion@qmaconsultores.com'>programacion@qmaconsultores.com</a>. En Cumplimiento de la Ley de Servicios de La Sociedad de la Información y Comercio Eléctrónico, le informamos que sus datos han sido obtenidos de fuentes públicas de información. <b>Si no desea recibir mas email´s como este, por favor, responda al presente email indicando en el ASUNTO: BAJA.</b>
				<br /><br />
				<b>LEGAL DISCLAIMER</b>: This electronic message is only for the indicated addresses. Its character is confidential,
				personal and not transferable and it is protected by law. Any revelation, dissemination, disclosure, alteration,
				printing, copying, transmission or use no permitted, total or partial, is prohibited and may be unlawful. If you
				have received this message by mistake, you have to notify it immediately to the person who has sent it and
				delete the original message and also the attached files without read or store.
			</div>
		";

		mail($mail_destinatario, "Avefor - Nueva $tipo",$mensaje,$headers);
	}

	return $res;
}

function actualizaIncidencia(){
	$res=true;

	$datos=arrayFormulario();

	conexionBD();
	$consulta=consultaBD("UPDATE mejoraContinua SET fecha='".$datos['fecha']."', area='".$datos['area']."', responsable='".$datos['responsable']."', 
		emailResponsable='".$datos['emailResponsable']."', descripcion='".$datos['descripcion']."', causas='".$datos['causas']."', resuelta='".$datos['resuelta']."', 
		fechaResolucion='".$datos['fechaResolucion']."' WHERE codigo='".$datos['codigo']."';");
	cierraBD();

	if(!$consulta){
		$res=false;
	}

	return $res;

}

function datosIncidencia($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM mejoraContinua WHERE codigo='$codigo';");
	cierraBD();

	return mysql_fetch_assoc($consulta);
}


function imprimeIncidencias(){
	global $_CONFIG;
	conexionBD();

	$consulta=consultaBD("SELECT codigo, fecha, area, resuelta FROM mejoraContinua WHERE tipo='incidencia' ORDER BY fecha DESC;");
	$datos=mysql_fetch_assoc($consulta);
	$iconoC=array('si'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','no'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fecha']);
		echo "
		<tr>
        	<td> $fecha </td>
        	<td> ".$datos['area']." </td>
        	<td class='centro'> ".$iconoC[$datos['resuelta']]." </td>
        	<td class='td-actions'>
        		<a href='".$_CONFIG['raiz']."incidencias/gestion.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-search-plus'></i> Detalles</i></a>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function generaDatosGraficoIncidencias(){
	$datos=array();

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM mejoraContinua WHERE tipo='incidencia';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];


	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM mejoraContinua WHERE resuelta='si' AND tipo='incidencia';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['resueltas']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function operacionesIncidencias(){	
  if(isset($_POST['codigo'])){
    $res=actualizaIncidencia();
  }
  elseif(isset($_POST['tipo'])){
    $res=creaIncidencia();
  }
  
  if(isset($_POST['codigo'])){
	  if($res){
		mensajeOk("Incidencia actualizada correctamente.");
	  }
	  else{
		mensajeError("no se han podido actualizar la Incidencia. Compruebe los datos introducidos.");
	  }
	}
  elseif(isset($_POST['tipo'])){
	  if($res){
		mensajeOk("Incidencia registrada correctamente.");
	  }
	  else{
		mensajeError("no se han podido registrar la Incidencia. Compruebe los datos introducidos.");
	  }
	}
}	

function gestionIncidencias(){
	operacionesIncidencias();
	
	abreVentanaGestion('Gestión de Incidencias','?');
	$datos=compruebaDatos('mejoraContinua');
	echo '
	<input type="hidden" name="tipo" value="incidencia">
	<div class="control-group">                     
	  <label class="control-label" for="fecha">Fecha:</label>
	  <div class="controls">
		<input type="text" class="input-small datepicker hasDatepicker" id="fecha" name="fecha" value="'.formateaFechaWeb($datos['fecha']).'">
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->



	<div class="control-group">                     
	  <label class="control-label" for="area">Área afectada:</label>
	  <div class="controls">
		<input type="text" class="input-large" id="area" name="area" value="'. $datos['area'].'">
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->

	
	
	<div class="control-group">                     
	  <label class="control-label" for="responsable">Responsable:</label>
	  <div class="controls">
		<input type="text" class="input-large" id="responsable" name="responsable" value="'. $datos['responsable'].'">
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->



	<div class="control-group">                     
	  <label class="control-label" for="emailResponsable">eMail responsable:</label>
	  <div class="controls">
		<input type="text" class="input-large" id="emailResponsable" name="emailResponsable" value="'.$datos['emailResponsable'].'">
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->



	<div class="control-group">                     
	  <label class="control-label" for="descripcion">Descripción:</label>
	  <div class="controls">
		<textarea name="descripcion" class="areaInforme" id="descripcion">'.$datos['descripcion'].'</textarea>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->




	<div class="control-group">                     
	  <label class="control-label" for="causas">Posibles causas:</label>
	  <div class="controls">
		<textarea name="causas" class="areaInforme" id="causas">'.$datos['causas'].'</textarea>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->
	
	<div class="hide">


	  <div class="control-group">                     
		<label class="control-label" for="actuaciones">Actuaciones inmediatas:</label>
		<div class="controls">
		  <textarea name="actuaciones" class="areaInforme" id="actuaciones"></textarea>
		</div> <!-- /controls -->       
	  </div> <!-- /control-group -->




	  <div class="control-group">                     
		<label class="control-label" for="correccion">Acción Correctiva/Preventiva:</label>
		<div class="controls">
		  <textarea name="correccion" class="areaInforme" id="correccion"></textarea>
		</div> <!-- /controls -->       
	  </div> <!-- /control-group -->


	  <div class="control-group">                     
		<label class="control-label" for="responsableImplantacion">Responsable implantación:</label>
		<div class="controls">
		  <input type="text" class="input-medium" id="responsableImplantacion" name="responsableImplantacion">
		</div> <!-- /controls -->       
	  </div> <!-- /control-group -->



	  <div class="control-group">                     
		<label class="control-label" for="emailResponsableImplantacion">eMail responsable:</label>
		<div class="controls">
		  <input type="text" class="input-medium" id="emailResponsableImplantacion" name="emailResponsableImplantacion">
		</div> <!-- /controls -->       
	  </div> <!-- /control-group -->



	  <div class="control-group">                     
		<label class="control-label" for="fechaPrevista">Fecha prevista:</label>
		<div class="controls">
		  <input type="text" class="input-small datepicker hasDatepicker" id="fechaPrevista" name="fechaPrevista" value="<?php imprimeFecha(); ?>">
		</div> <!-- /controls -->       
	  </div> <!-- /control-group -->



	</div><!-- Fin cajaNC -->


	<div class="control-group">                     
	  <label class="control-label" for="resuelta">Resuelta:</label>
	  <div class="controls">
		<label class="radio inline">
		  <input type="radio" name="resuelta" value="si" '; if($datos['resuelta']=='si' || $datos['resuelta']==''){ echo 'checked="checked"'; } echo ' onchange="muestra(\'cajaFechaResolucion\');"> Si
		</label>
		
		<label class="radio inline">
		  <input type="radio" name="resuelta" value="no" '; if($datos['resuelta']=='no'){ echo 'checked="checked"'; } echo ' onchange="muestra(\'cajaFechaResolucion\');"> No
		</label>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->


	  <div class="control-group" id=\'cajaFechaResolucion\'>                     
		<label class="control-label" for="fechaResolucion">Fecha real de resolución:</label>
		<div class="controls">
		  <input type="text" class="input-small datepicker hasDatepicker" id="fechaResolucion" name="fechaResolucion" value="'. formateaFechaWeb($datos['fechaResolucion']).'">
		</div> <!-- /controls -->       
	  </div> <!-- /control-group -->';

	cierraVentanaGestion('index.php');
}

//Fin parte de incidencias