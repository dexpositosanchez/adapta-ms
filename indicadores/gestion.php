<?php
  $seccionActiva=15;
  include_once("../cabecera.php");
  if(isset($_GET['enlace']) || isset($_POST['enlace'])){
  	$enlace =  isset($_GET['enlace']) ? $_GET['enlace'] : $_POST['enlace'];
  	gestionMapaProcesos($enlace);
  } else {
  	gestionIndicadores();
  }
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
    
    crearCampos($('#frecuencia').val(),$('#codigo').val());
    $('#frecuencia').change(function(){
      crearCampos($(this).val(),$('#codigo').val());
    });
	
  });

function crearCampos(frecuencia,codigo){
  $("#inputsFrecuencia").empty();
  var textos= '';

  if(frecuencia == 1){
    var fecha = new Date();
    textos = new Array('Ano de '+fecha.getFullYear());
  } else if (frecuencia == 2){
    textos = new Array('1º Semestre','2º Semestre');
  } else if (frecuencia == 4){
    textos = new Array('1º Trimestre','2º Trimestre','3º Trimestre','4º Trimestre');
  } else if (frecuencia == 6){
    textos = new Array('1º Bimestre','2º Bimestre','3º Bimestre','4º Bimestre','5º Bimestre','6º Bimestre');
  } else if (frecuencia == 12){
    textos = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
  }
   if(codigo != undefined && $('#paraAjax').val() != 0){ 
    $.ajax({
      data: {"codigo":codigo},//Matriz $_POST: se recogera de la forma $_POST['codigo'] (y tendrá el valor 1)
      type: "POST",//Siempre asi
      dataType: "json",//Siempre asi
      url: "recogeValores.php",//URL destino (que solo debe hacer echo del resultado)
    }).done(function(datos,textStatus,jqXHR){
      for(i=0;i<frecuencia;i++){
        if(datos[i] != undefined){
          var checked = datos[i].checkValor == 'SI' ? 'checked="checked"':'';
          if($('#tipoIndicador').val() == 'ESTATICO'){
            $("#inputsFrecuencia").append("<div class='control-group'><label class='control-label' for='valor"+i+"'>"+textos[i]+":</label><div class='controls'><input step='0.01' type='number' name='valor"+i+"' id='valor"+i+"' class='input-mini pagination-right' value='"+datos[i].valor+"' />&nbsp;&nbsp;<label class='checkbox inline'><input type='checkbox' name='checkValor"+i+"' id='checkValor"+i+"' value='SI' "+checked+">Activo</label></div></div>");
          } else {
              $("#inputsFrecuencia").append("<div style='width:100%;clear:both;'><div class='control-group' style='float:left;margin-bottom:9px;'><label class='control-label' for='valorN"+i+"'>"+textos[i]+":</label><div class='controls'><input step='0.01' type='number' name='valorN"+i+"' id='valorN"+i+"' class='input-mini pagination-right' value='"+datos[i].valorN+"' /></div></div><div class='control-group' style='float:left;margin-bottom:9px;'><label class='control-label' style='width:auto;margin-left:5px;margin-right:5px;' for='valorD"+i+"'>/</label><div class='controls' style='margin-left:0px;float:left;'><input step='0.01' type='number' style='float:left;' name='valorD"+i+"' id='valorD"+i+"' class='input-mini pagination-right' value='"+datos[i].valorD+"' /></div></div>&nbsp;&nbsp;<label class='checkbox inline'><input type='checkbox' name='checkValor"+i+"' id='checkValor"+i+"' value='SI' "+checked+">Ativo</label></div>");
          }
        } else {
          creaCamposSinValores(textos,i);
        }
      }
      $("#inputsFrecuencia").append("<br clear='all'>");
    });
  } else {
    for(i=0;i<frecuencia;i++){
      creaCamposSinValores(textos,i);
    }
    $("#inputsFrecuencia").append("<br clear='all'>");
  }
  
  
}

function creaCamposSinValores(textos,i){
  if($('#tipoIndicador').val() == 'ESTATICO'){
    $("#inputsFrecuencia").append("<div class='control-group'><label class='control-label' for='valor"+i+"'>"+textos[i]+":</label><div class='controls'><input step='0.01' type='number' name='valor"+i+"' id='valor"+i+"' class='input-mini pagination-right' value='' />&nbsp;&nbsp;<label class='checkbox inline'><input type='checkbox' name='checkValor"+i+"' id='checkValor"+i+"' value='SI'>Activo</label></div></div>");
  } else {
    $("#inputsFrecuencia").append("<div style='width:100%;clear:both;'><div class='control-group' style='float:left;margin-bottom:9px;'><label class='control-label' for='valorN"+i+"'>"+textos[i]+":</label><div class='controls'><input step='0.01' type='number' name='valorN"+i+"' id='valorN"+i+"' class='input-mini pagination-right' value='' /></div></div><div class='control-group' style='float:left;margin-bottom:9px;'><label class='control-label' style='width:auto;margin-left:5px;margin-right:5px;' for='valorD"+i+"'>/</label><div class='controls' style='margin-left:0px;float:left;'><input step='0.01' type='number' style='float:left;' name='valorD"+i+"' id='valorD"+i+"' class='input-mini pagination-right' value='' /></div></div>&nbsp;&nbsp;<label class='checkbox inline'><input type='checkbox' name='checkValor"+i+"' id='checkValor"+i+"' value='SI'>Ativo</label></div>");
  }
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>