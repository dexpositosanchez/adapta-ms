<?php
  $seccionActiva=11;
  include_once("../cabecera.php");
  gestionCliente();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>

<script src='../../api/js/firma/jquery.signaturepad.js'></script>
<script src='../../api/js/firma/assets/json2.min.js'></script>

<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$("#numCuenta").focusout(function() {
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			updateBic(value);
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	  }).blur(function(){
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			updateBic(value);
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	});
  });
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>