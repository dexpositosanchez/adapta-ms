<?php
  $seccionActiva=4;
  include_once('../cabecera.php');
  
  $res=false;
  if(isset($_POST['firma'])){
    $res=actualizaFirma();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('correos');
  }
  
  $estadisticas=estadisticasGenericasNueva('correos');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre envío de correos electrónicos</h6>

                   <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-mail-forward"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Correos enviados</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de correos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="<?php echo $_CONFIG['raiz']; ?>correos/enviarEmail.php?seccion=13" class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Nuevo eMail</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>correos/firmaCorreo.php" class="shortcut"><i class="shortcut-icon icon-edit"></i><span class="shortcut-label">Editar Firma</span> </a>
                <a href="#" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        
        <?php
          mensajeResultado('destinatarios',$res,'mensaje');
          mensajeResultado('firma',$res,'firma');
          mensajeResultado('elimina',$res,'mensaje', true);
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Mensajes enviados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Usuario </th>
                    <th> Destinatarios </th>
                    <th> Fecha </th>
                    <th> Asunto </th>
                    <th> Mensaje </th>
                    <th class="centro"> </th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeCorreos();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('../pie.php'); ?>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js"></script>
