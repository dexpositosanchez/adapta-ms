<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionAgenda();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    $('input[name=todoDia]').change(function(){
      if($(this).val()=='true'){
        $('#todoDia').css('display','none');
      }
      else{
        $('#todoDia').css('display','block'); 
      }
    })
  });
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>