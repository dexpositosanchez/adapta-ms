<?php

// =========================================================== Parámetros de configuración ====================================================================================

//Inicialización del array de configuración
$_CONFIG=array();

//Parámetros de conexión a la BDD
$_CONFIG['servidorBD']='localhost';

//$_CONFIG['usuarioBD']='adaptams';

//$_CONFIG['claveBD']='Writemaster7';

$_CONFIG['nombreBD']='adaptams';

$_CONFIG['usuarioBD']='root';

$_CONFIG['claveBD']='';

//Nombre de cookie de sesión
$_CONFIG['nombreCookie']='sesionAdaptaMS';

//Perfiles de usuario
$_CONFIG['perfiles']=array(
	'ADMIN'=>'Administrador',
	'ADMINISTRACION1'=>'Administración 1',
	'ADMINISTRACION2'=>'Administración 2',
	'CONTABILIDAD'=>'Contabilidad',
	'TUTOR'=>'Tutor',
	'DIRECTOR'=>'Director de Zona',
	'DELEGADO'=>'Delegado',
	'DOCENTE'=>'Docente'
);

//Parámetros de maquetación
$_CONFIG['textoAcceso']='Software de Gestión';

$_CONFIG['title']='Adapta MS';

$_CONFIG['altLogo']='Adapta MS';

$_CONFIG['tituloSoftware']='- Software de gestión';

$_CONFIG['textoPie']='Software desarrollado por <a href="http://www.qmaconsultores.com" target="_blank">QMA Consultores</a>';

//Parámetros de estilos
$_CONFIG['colorPrincipal']='#0099A1';

$_CONFIG['colorSecundario']='#286568';

$_CONFIG['colorPrincipalRGB']='rgba(0,153,161,0.2)';//126,178,22. Se puede usar la utilidad online http://www.javascripter.net/faq/hextorgb.htm para conversión desde hexadecimal

//Para enlaces/rutas

$_CONFIG['raiz']='/adapta-ms/';

$_CONFIG['enlaceCuestionario']='http://crmparapymes.com.es'.$_CONFIG['raiz'].'cuestionario-de-satisfaccion/';

//Para módulo de incidencias de software

$_CONFIG['codigoIncidenciasSoftware']='271';

$_CONFIG['nombreIncidenciasSoftware']='Adapta Management Services ';
