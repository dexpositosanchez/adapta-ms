function insertaFila(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody tr:last").clone();

    //Si existe un campo de descarga, creo un input file junto a él
    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    //Obtengo el atributo name para los inputs y selects
    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
        var parts = this.id.match(/(\D+)(\d*)$/);
        //Creo un nombre nuevo incrementando el número de fila
        return parts[1] + ++parts[2];
        
    }).attr("id", function(){//Hago lo mismo con los IDs
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
	$tr.find("input[type=checkbox]").removeAttr("checked");//Para quitar el check a la nueva fila clon de la anterior
    $tr.find("select").val("NULL");
    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker
    $tr.find('.bootstrap-filestyle').remove();//Eliminación de los filestyle
    $tr.find('.descargaFichero').remove();//Eliminación del enlace de descarga, si lo hubiere

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){//Misma operación para el input-rating
        var campoRating=$tr.find('.rating');//La librería bootstrap-rating-input mete el input dentro de un div, por lo que primero rescato el input...
        $tr.find('.rating-input').replaceWith(campoRating);//... y sustituyo el div por él (si no crearía 2 filas de estrellas)
        campoRating.rating();//Inicializo la librería sobre el input
    }

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    filaAux++;
    var aux=filaAux;
    $('#gasto'+aux).change(function(){
        oyentePrecio(aux);
    });
}

function eliminaFila(tabla){
  if($('#'+tabla).find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' tr').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' tr:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' tr:not(:first)').eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
                //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
                var parts = this.id.match(/(\D+)(\d*)$/);
                //Creo un nombre nuevo incrementando el número de fila (++parts[2])
                return parts[1] + i;
                //Hago lo mismo con los IDs
            }).attr("id", function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                return parts[1] + i;
            });

            $('#'+tabla+' tr:not(:first)').eq(i).find("input[name='filasTabla[]']").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}

function oyentePrecio(i){
    var cod = '#codProducto'+$('#gasto'+i).val();
    $('#precio'+i).val($(cod).text());
}