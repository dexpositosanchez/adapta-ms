<?php
include_once('config.php');
if(!isset($_GET['ajax'])){
?>
  <script type="text/javascript">
  //El siguiente código se corresponde con el filtro global por ejercicios, que afecta a prácticamente todas las secciones del software
  var enlaceCambioEjercicio='<?php echo $_CONFIG["raiz"];?>inicio.php?ejercicio=';

  $(document).ready(function(){

    $('#cajaFiltroEjercicio').datepicker({format:'dd/mm/yyyy',weekStart:1,viewMode:2,minViewMode:2}).on('changeDate',function(e){
      if(confirm("Esta acción recargará la página, por lo que si ha rellenado algún formulario que aún no ha guardado, pulse en Cancelar, guarde los cambios y a continuación vuelva ha intentarlo.")){
        $(this).datepicker('hide');
        var nuevaFecha=new Date(e.date);
        var anio=nuevaFecha.getFullYear();
        window.location=enlaceCambioEjercicio+anio;
      }
    }).on('show',function(e){
      colocaBotonTodosSelectorEjercicio();
    });
  });

  function colocaBotonTodosSelectorEjercicio(){
    if($('#botonTodosFiltroEjercicios').html()==undefined){
      $('.datepicker-years table').after('<a href="'+enlaceCambioEjercicio+'Todos" class="btn btn-default" id="botonTodosFiltroEjercicios">Todos</a>');
      inicializaOyenteBotonTodos();
    }
  }

  function inicializaOyenteBotonTodos(){
    $('#botonTodosFiltroEjercicios').click(function(){
      if(confirm("Esta acción recargará la página, por lo que si ha rellenado algún formulario que aún no ha guardado, pulse en Cancelar, guarde los cambios y a continuación vuelva ha intentarlo.")){
        window.location=$(this).attr('href');
      }
    });
  }
  </script>


  <div class="footer">
    <div class="footer-inner">
      <div class="container">
        <div class="row">
          <div class="span12"> 
            <a href='http://adaptams.com/' target='_blank'>Adapta management services</a> &copy; <?php echo date('Y').' '.$_CONFIG['textoPie']; ?>                 

          </div>
          <!-- /span12 --> 
        </div>
        <!-- /row --> 
      </div>
      <!-- /container --> 
    </div>
    <!-- /footer-inner --> 
  </div>
  <!-- /footer --> 

  </body>
  </html>
<?php
}