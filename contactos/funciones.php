<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de clientes

function operacionesContactos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaContacto();
		actualizaTodo();
	}
	elseif(isset($_POST['nombre'])){
		$res=creaContacto();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('contactos');
	}
	elseif(isset($_GET['codigoEnviar'])){
		$res=enviaEmailSolicitud($_GET['codigoEnviar']);
  	}
	mensajeResultado('nombre',$res,'Contacto');
    mensajeResultado('elimina',$res,'Contacto', true);
    mensajeResultadoSolicitud('codigoEnviar',$res,'Solicitud');
}

function creaContacto(){
	$res=true;
	$res=insertaDatos('contactos');
	$_POST['codigo']=mysql_insert_id();
	$res=insertaHistorico();
	return $res;
}

function actualizaContacto(){
	$res=true;
	$res=actualizaDatos('contactos');
	$res=insertaHistorico();
	return $res;
}


function insertaHistorico(){
	$res=true;
	$i=1;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM historico_contactos WHERE codigoContacto='".$_POST['codigo']."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['observaciones'.$i]) && $datos['observaciones'.$i]!=""){
		$res = $res && consultaBD("INSERT INTO historico_contactos VALUES (NULL,'".$datos['fecha'.$i]."',
			'".$datos['gestion'.$i]."','".$datos['observaciones'.$i]."','".$_POST['codigo']."');");

		$i++;
	}

	return $res;
}

function eliminaDatosCliente($tabla,$codigoCliente,$actualizacion){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM $tabla WHERE codigoCliente=$codigoCliente");
	}

	return $res;
}

function listadoClientes(){
	global $_CONFIG;

	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');

	//fechaAlta lo utilizo para el filtro por ejercicios
	$columnas=array('nombre','empresa','cargo','telefono1','email1','programa','nombre','nombre');
	$where=obtieneWhereListado("WHERE 1=1",$columnas,true,true);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, apellido1, empresa, cargo, telefono1, email1, programa FROM contactos $where AND activo='NO' $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, nombre, apellido1, empresa, cargo, telefono1, email1, programa FROM contactos $where AND activo='NO';");

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$consultaAux=consultaBD("SELECT nombre FROM programas WHERE codigo='".$datos['programa']."';");
		$datosPrograma=mysql_fetch_assoc($consultaAux);

		$fila=array(
			$datos['nombre']." ".$datos['apellido1'],
			$datos['empresa'],
			$datos['cargo'],
			"<a href='tel:".$datos['telefono1']."' class='nowrap'>".formateaTelefono($datos['telefono1'])."</a>",
			"<a href='mailto:".$datos['email1']."'>".$datos['email1']."</a>",
			$datosPrograma['nombre'],
			botonAcciones(array('Detalles','Pasar a Alumnos','Genera Solicitud','Enviar Email'),array('contactos/gestion.php?codigo='.$datos['codigo'],'alumnos/gestion.php?codigoContacto='.$datos['codigo'],'contactos/generaSolicitudWord.php?codigo='.$datos['codigo'],'contactos/index.php?codigoEnviar='.$datos['codigo']),array("icon-search-plus","icon-users","icon-download","icon-envelope")),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}
	cierraBD();

	echo json_encode($res);
}

function gestionContactos(){
	operacionesContactos();

	abreVentanaGestion('Gestión de contactos','index.php');
	$datos=compruebaDatos('contactos');

	compruebaMensajeConfirmarCliente();
	
	echo "<h4 class='apartadoFormulario sinFlotar'>DATOS DEL CONTACTO</h4>";
	abreColumnaCampos();
	campoOculto($datos,'activo','NO');
		campoTexto('nombre','Nombre',$datos);
		campoFechaNacimiento($datos);
		campoTexto('empresa','Empresa',$datos);
		campoTexto('tipo','Tipo',$datos);
		campoSelectConsulta('programa','Programa',"SELECT codigo, nombre AS texto FROM programas;",$datos);
		//campoTexto('programa','Programa',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('apellido1','Apellido 1',$datos);
		campoTexto('apellido2','Apellido 2',$datos);
		campoSelect('origen','Origen',array('','Mail','Llamada','Referencia','Web'),array('','Mail','Llamada','Referencia','Web'),$datos);
		campoTexto('cargo','Cargo',$datos);
		campoTexto('importancia','Importancia',$datos);
	cierraColumnaCampos();

	echo "<h4 class='apartadoFormulario sinFlotar'>DATOS DE CONTACTO</h4>";
	abreColumnaCampos();
		campoTextoSimbolo('email1','Email 1','<i class="icon-envelope"></i>',$datos,'input-large');
		campoTextoSimbolo('email2','Email 2','<i class="icon-envelope"></i>',$datos,'input-large');
		campoTexto('direccion1','Dirección',$datos);
		campoTexto('website1','Website',$datos);
		campoTextoSimbolo('linkedin','URL LinkedIn','<i class="icon-linkedin"></i>',$datos,'input-large');
		campoTextoSimbolo('twitter','URL Twitter','<i class="icon-twitter"></i>',$datos,'input-large');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTextoSimbolo('telefono1','Teléfono 1','<i class="icon-phone"></i>',$datos,'input-large');
		campoTextoSimbolo('telefono2','Teléfono 2','<i class="icon-phone"></i>',$datos,'input-large');
		campoTexto('provincia','Provincia',$datos);
		campoTextoSimbolo('skype','Usuario Skype','<i class="icon-skype"></i>',$datos,'input-large');
		campoTextoSimbolo('facebook','URL Facebook','<i class="icon-facebook"></i>',$datos,'input-large');
	cierraColumnaCampos();
	
	echo "<h4 class='apartadoFormulario sinFlotar'>OTROS</h4>";
	areaTexto('descripcion','Descripción',$datos,'areaInforme');
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');

	
	echo "<h4 class='apartadoFormulario sinFlotar'>HISTÓRICO</h4>";
	abreColumnaCampos('span11');
		tablaHistorico($datos);
	cierraColumnaCampos();
	
	cierraVentanaGestion('index.php',true);
}

function tablaHistorico($datos){
	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaHistorico'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Fecha </th>
							<th> Gestión </th>
							<th> Observaciones </th>
							<th> </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=1;
	if($datos){
		$consulta=consultaBD("SELECT * FROM historico_contactos WHERE codigoContacto='".$datos['codigo']."';", true);				  
		while($datosHistorico=mysql_fetch_assoc($consulta)){
		 	echo "<tr>";
				campoFechaTabla('fecha'.$i,$datosHistorico['fecha']);
				campoSelect('gestion'.$i, '', array('Email', 'Llamada', 'Visita'), array('email','llamada','visita'), $datosHistorico['gestion'], '', "", 1);
				areaTextoTabla('observaciones'.$i,$datosHistorico['observaciones']);
			echo"
					<td>
						<input type='checkbox' name='filasTabla[]' value='$i'>
		        	</td>
				</tr>";
			$i++;
		}
	}

	if($i==1 || !$datos){
	 	echo "<tr>";
			campoFechaTabla('fecha1');
			campoSelect('gestion1', '', array('Email', 'Llamada', 'Visita'), array('email','llamada','visita'), false, '', "", 1);
			areaTextoTabla('observaciones1','','areaTextoTabla');
		echo"
				<td>
					<input type='checkbox' name='filasTabla[]' value='$i'>
	        	</td>
			</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaHistorico\");'><i class='icon-plus'></i> Añadir</button> 
	              <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaHistorico\");'><i class='icon-minus'></i> Eliminar</button> 
	            </center>
		 	";
}

function compruebaMensajeConfirmarCliente(){
	if(isset($_GET['confirmar'])){
		mensajeAdvertencia("complete los campos obligatorios para confirmar el cliente.");
	}
}


function filtroClientes(){
	abreCajaBusqueda();
	abreColumnaCampos();

	$columnas=array('nombre','empresa','cargo','telefono1','email1','programa');

	campoTexto(0,'Nombre');
	campoTexto(1,'Empresa');
	campoTexto(2,'Cargo');
	
	

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(3,'Teléfono',false,'input-small pagination-right');
	campoTexto(4,'eMail');
	campoTexto(5,'Programa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}



function obtieneNombreCliente($codigoCliente){
	$datos=consultaBD("SELECT razonSocial FROM clientes WHERE codigo=$codigoCliente",true,true);

	return "<a href='gestion.php?codigo=$codigoCliente'>".$datos['razonSocial']."</a>";
}


function creaRegistroExtra($query){
	$res='fallo';

	conexionBD();
	
	$consulta=consultaBD($query);

	if($consulta){
		$res=mysql_insert_id();
	}

	cierraBD();

	echo $res;
}

function campoCuentaBancaria($valor){
	$valor=compruebaValorCampo($valor,'numCuenta');
	echo '<div class="control-group">                     
	  <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
	  <div class="controls numSS">
		<div class="input-prepend input-append">
		  <input type="text" class="input-mini" id="iban" name="iban" maxlength="4" size="4" value="'.substr($valor, 0, 4).'">
		  <span class="add-on">/</span>
		  <input type="text" class="input-medium" id="numCuenta" name="numCuenta" maxlength="20" size="20" value="'.substr($valor, 4, 24).'">
		</div>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->';
}

function campoFechaNacimiento($datos){
	if(!$datos){
		campoFecha('fechaNacimiento','Fecha nacimiento','0000-00-00');
	}
	else{
		campoFecha('fechaNacimiento','Fecha nacimiento',$datos);
	}
}

function botonAccionesModal($opciones,$enlaces,$iconos,$enlacesExternos=false,$celda=false,$textoBoton='Acciones',$id){
	global $_CONFIG;

	$res='';

	if($celda){
		$res="<td>";
	}
	
	$res.="<div class='centro'>
				<div class='btn-group'>
					<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> $textoBoton <span class='caret'></span></button>
					<ul class='dropdown-menu' role='menu'>";
		

	$clase=compruebaEnlaceExternoBotonAcciones($enlacesExternos,0);

	$res.="				<li><a href='".$_CONFIG['raiz'].$enlaces[0]."' ".$clase."><i class='".$iconos[0]."'></i><span>".$opciones[0]."</span> </a></li>";

	for($i=1;$i<count($opciones);$i++){
		$clase=compruebaEnlaceExternoBotonAcciones($enlacesExternos,$i);

		$res.="			<li class='divider'></li>";
		$res.="			<li><a href='".$_CONFIG['raiz'].$enlaces[$i]."' ".$clase."><i class='".$iconos[$i]."'></i><span>".$opciones[$i]."</span> </a></li>";
	}
    
    $res.="			</ul>
    			</div>
    		</div>";

    if($celda){
    	$res.="</td>";
    }

    return $res;
}

function enviaEmailSolicitud($codigo){

	$res=true;

	$datos=datosRegistro('contactos',$codigo);
	//$datosAlumnos=datosRegistro('trabajadores_cliente',$datos['codigoAlumno']);
	$factura='Solicitud'.$codigo.'.docx';
	$temario='PFIBIMManagerEdicion2017.pdf';
	
	/*Parte de envío por correo al cliente de la factura*/
	
	$mensajeEnvio="Buenos Días,
	<br /><br />Soy José María Gutiérrez, responsable del área de formación y gerente del centro  AUTODESK<br />
	ATC/AAP/CACC BIM MÁLAGA – Adapta Management Services.<br /><br />
	Contacto contigo en relación a la solicitud de información sobre formación BIM a través de<br /> 
	nuestra web, y creo que podría interesarte como te he comentado por teléfono la próxima<br /> 
	convocatoria del “Programa PFIBIM Manager&quot;. Esta formación es Oficial de Autodesk y está<br />
	homologada por la ACP Profesional. Si lo deseas, puedes tener más información de nuestra<br />
	formación en www.adaptams.com<br /><br />
	Te acompaño el temario del Programa PFIBIM Manager para la convocatoria vigente. La<br />
	formación se imparte de manera presencial y próximamente online. Nuestra meta como<br />
	ATC/AAP/CACC de Autodesk, es atenderte personalmente, y ofrecerte la mejor propuesta <br />
	formativa.<br /><br />
	En cuanto a fechas, el programa está por concretar. En cuanto a los días, para esta<br />
	convocatoria se puede corresponder con lunes y miércoles o martes y jueves,  por la tarde, en<br />
	horario de 16:00 a 21:00 horas.<br /><br />
	En cuanto al precio del programa asciende a 2150 €. Si quisieras financiarlo, me lo indicas y<br />
	vemos las posibles soluciones para llevarlo a cabo sin ningún problema.<br />
	Para cualquier consulta o duda puedes contactar con nosotros a través de este mail o<br />
	llamándonos al móvil 637 474 372 o al teléfono fijo 952 341 007.<br /><br />
	ADAPTA Management Services:<br />
	<ul>
	<li type='square'>Centro de Formación Oficial de Autodesk ATC/APP. <a href='http://autodesk.force.com/plocator/PLocatorMapViewBeta?id=a5z3A000000CbuuQAC&amp;cnt=Spain&amp;lang=en' target='_blank'>Ver aquí</a></li>
	<li type='square'>Centro de Formación Oficial de BIM Iberica.</li>
	<li type='square'>Instructores ATC Autodesk autorizados.</li>
	<li type='square'>Centro Oficial Certificador de Autodesk ACC.</li>
	<li type='square'>Certificación de Autodesk Revit Profesional NO incluido en el coste del programa.<br />
	Realización del Examen en las instalaciones de ADAPTA MS. Certificación de validez<br />
	internacional, única reconocida.</li>
	<li type='square'>Entrega al alumno de la Licencia Educacional de Medit.</li><br /><br />";
	
	$mensajeEnvio.="<img src='http://crmparapymes.com.es/adapta-ms/img/logo2.png'/><br /><br />";
	
	$mensajeEnvio.="<br /><br /><strong>Adapta Management Services</strong><br />
	<strong><i>Ventas</i></strong><br /><br />
	CP 29002 Málaga<br />
	C/ Cuarteles, 7- Planta 2,Oficina 6. (Edificio Guadalmedina)<br /><br />
	+34 952 34 10 07<br />
	+34 637 474 372<br />
	www.adaptams.com<br /><br />";
	
	

	$mensajeEnvio.="
		Este mensaje se dirige exclusivamente a su destinatario y puede contener información privilegiada o confidencial. Si no es vd. el destinatario indicado, queda notificado de que la utilización, divulgación y/o copia sin autorización está prohibida en virtud de la legislación vigente. Si ha recibido este mensaje por error, le rogamos que nos lo comunique inmediatamente por esta misma vía y proceda a su destrucción.
	";
	
	$aleatorio=md5(time());
	$limiteMime="==TecniBoundary_x{$aleatorio}x";
	$headers="From: info@adaptams.com\r\n";
	$headers.= "MIME-Version: 1.0\r\n";
	$headers.="Content-Type: multipart/mixed;" . "boundary=\"{$limiteMime}\"";

	$mensaje="--{$limiteMime}\r\n"."Content-Type: text/html; charset=\"utf-8\"\r\n"."Content-Transfer-Encoding: 7bit\n\n".$mensajeEnvio."\n\n";

	$fp=fopen('../documentos/solicitudes/'.$factura, "r");
	$tam=filesize('../documentos/solicitudes/'.$factura);
	$fichero=fread($fp,$tam);
	$ficheroAdjunto=chunk_split(base64_encode($fichero));
	fclose($fp);

	$mensaje.="--{$limiteMime}\n";

	$mensaje.="Content-Type: application/octet-stream; name=\"".basename('../documentos/solicitudes/'.$factura)."\"\n"."Content-Description:".basename('../documentos/solicitudes/'.$factura)."\n"."Content-Disposition: attachment;filename=\"".basename('../documentos/solicitudes/'.$factura)."\";size=".$tam."\n"."Content-Transfer-Encoding:base64\n\n".$ficheroAdjunto."\n\n";

	$fp2=fopen('../documentos/solicitudes/'.$temario, "r");
	$tam2=filesize('../documentos/solicitudes/'.$temario);
	$fichero2=fread($fp2,$tam2);
	$ficheroAdjunto2=chunk_split(base64_encode($fichero2));
	fclose($fp2);

	$mensaje.="--{$limiteMime}\n";

	$mensaje.="Content-Type: application/octet-stream; name=\"".basename('../documentos/solicitudes/'.$temario)."\"\n"."Content-Description:".basename('../documentos/solicitudes/'.$temario)."\n"."Content-Disposition: attachment;filename=\"".basename('../documentos/solicitudes/'.$temario)."\";size=".$tam2."\n"."Content-Transfer-Encoding:base64\n\n".$ficheroAdjunto2."\n\n";	

	$mensaje .= "--{$limiteMime}--";

    if (!mail($datos['email1'], 'Envío de Solicitud/Inscripción de Matrícula', $mensaje, $headers)) {
		$res=false;
    } 
	
	/* FIN PARTE ENVIO AL CLIENTE*/
	
	return $res;
}

function actualizaTodo(){

conexionBD();
	$consulta=consultaBD("SELECT * FROM contactos;");

	while($datos=mysql_fetch_assoc($consulta)){
		echo "ENTRA";
		$datos['nombre']=utf8_decode($datos['nombre']);
		$datos['empresa']=utf8_decode($datos['empresa']);		
		consultaBD("UPDATE contactos SET  nombre='".$datos['nombre']."', empresa='".$datos['empresa']."' WHERE codigo='".$datos['codigo']."';");		

	}
cierraBD();

}

//Fin parte de clientes