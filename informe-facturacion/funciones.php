<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales
@include_once('../facturas/funciones.php');//Carga de las funciones de facturación

function cargaVariables(){
  $where="AND fechaEmision>='".formateaFechaBD($_SESSION['fechaUno'])."' AND fechaEmision<='".formateaFechaBD($_SESSION['fechaDos'])."'";
  if(isset($_POST['codigoCurso']) && $_POST['codigoCurso']!='NULL'){
	$_SESSION['curso']=$_POST['codigoCurso'];
	$where.=" AND trabajadores_cliente.curso='".$_SESSION['curso']."'";
  }
  if(isset($_POST['nombreAlumno']) && $_POST['nombreAlumno']!=''){
	$_SESSION['nombreAlumno']=$_POST['nombreAlumno'];
	$where.=" AND trabajadores_cliente.nombre LIKE'%".$_SESSION['nombreAlumno']."%'";
  }
  if(isset($_POST['apellido1']) && $_POST['apellido1']!=''){
	$_SESSION['apellido1']=$_POST['apellido1'];
	$where.=" AND trabajadores_cliente.apellido1 LIKE'%".$_SESSION['apellido1']."%'";
  }
  if(isset($_POST['apellido2']) && $_POST['apellido2']!=''){
	$_SESSION['apellido2']=$_POST['apellido2'];
	$where.=" AND trabajadores_cliente.apellido2 LIKE'%".$_SESSION['apellido2']."%'";
  }
  if(isset($_POST['poblacion']) && $_POST['poblacion']!=''){
	$_SESSION['poblacion']=$_POST['poblacion'];
	$where.=" AND trabajadores_cliente.poblacion LIKE'%".$_SESSION['poblacion']."%'";
  }
  if(isset($_POST['importe']) && $_POST['importe']!=''){
	$_SESSION['importe']=$_POST['importe'];
	$where.=" AND facturacion.coste='".$_SESSION['importe']."'";
  }
  if(isset($_POST['fechaVencimiento']) && $_POST['fechaVencimiento']!=''){
	$_SESSION['fechaVencimiento']=$_POST['fechaVencimiento'];
	$where.=" AND facturacion.fechaVencimiento='".formateaFechaBD($_SESSION['fechaVencimiento'])."'";
  }
  
  return $where;
}