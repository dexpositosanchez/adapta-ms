<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesPresupuestos(){
	$res=true;

	if(isset($_POST['codigoContacto'])){
		$res=insertaClientePresupuesto();
	}
	elseif(isset($_POST['codigo'])){
		$res = actualizaPresupuesto();
	}
	elseif(isset($_POST['referenciaPresupuesto'])){
		$res = insertaPresupuesto();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('presupuestos');
	}

	mensajeResultado('referenciaPresupuesto',$res,'Presupuesto');
    mensajeResultado('elimina',$res,'Presupuesto', true);
}



function insertaProductosPresupuesto($codigo){
	conexionBD();
	$datos=arrayFormulario();
	$consulta=consultaBD("DELETE FROM productos_presupuestos WHERE codigoPresupuesto='$codigo';");
	$i=0;
	while(isset($datos['precioUnitario'.$i])){
		$consulta=consultaBD("INSERT INTO productos_presupuestos VALUES(NULL, '$codigo', '".$datos['producto'.$i]."', '".
		$datos['cantidad'.$i]."', '".$datos['precioUnitario'.$i]."', '".$datos['descuento'.$i]."', '".$datos['totalDescuento'.$i]."', '".$datos['impuesto'.$i]."', '".
		$datos['neto'.$i]."');");
		$i++;
	}
	
	cierraBD();
}



function actualizaPresupuesto(){
	$res = actualizaDatos('presupuestos');
	$res = $res && insertaProductos();

	return $res;
}

function insertaProductos(){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM productos_presupuestos WHERE codigoPresupuesto='".$_POST['codigo']."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['cantidad'.$i]) && $datos['cantidad'.$i]!=""){
		$res = $res && consultaBD("INSERT INTO productos_presupuestos VALUES (NULL,'".$_POST['codigo']."','".$datos['producto'.$i]."',
			'".$datos['cantidad'.$i]."','".$datos['precioUnitario'.$i]."','".$datos['descuento'.$i]."','".$datos['totalDescuento'.$i]."',
			'".$datos['impuesto'.$i]."','".$datos['neto'.$i]."');");

		$i++;
	}

	return $res;
}

function insertaPresupuesto(){
	$res = insertaDatos('presupuestos');
	$_POST['codigo'] = $res;
	$res = $res && insertaProductos();

	return $res;
}

function generaDatosGraficoVentas(){
	$datos=array();

	conexionBD();
	
	$where=compruebaPerfilParaWhere();

	$reg=consultaBD("SELECT COUNT(codigo) AS codigo FROM presupuestos $where;",false,true);
	$datos['totales']=$reg['codigo'];

	$reg=consultaBD("SELECT COUNT(codigo) AS codigo FROM presupuestos $where AND aceptado='SI';",false,true);
	$datos['confirmados']=$reg['codigo'];
	
	if($datos['totales']>=$datos['confirmados']){
		$datos['max']=$datos['totales'];
	}
	elseif($datos['confirmados']>=$datos['totales']){
		$datos['max']=$datos['confirmados'];
	}

	cierraBD();

	return $datos;
}

function escalaGraficoComercial($max){
	$datos=array('escala'=>10,'max'=>$max);//La inicialización a $max del íncide max es irrelevante
	
	//El número máximo de puntos que debe tener el gráfico para que se vea bien es 15. Empiezo con una escala de 10, y si con ella el número de puntos es mayor que 15, multiplico por 10
	while(($max/$datos['escala'])>15){
		$datos['escala']*=10;
	}
	$datos['max']=ceil($max/$datos['escala']);

	return $datos;
}


function gestionPresupuestos(){
	operacionesPresupuestos();
	
	abreVentanaGestion('Gestión de Presupuestos','index.php','','icon-edit','',false,'noAjax');
	$datos=compruebaDatos('presupuestos');

	if($datos){
		$referencia=$datos['referenciaPresupuesto'];
		$fecha=$datos['fechaValidez'];
	} else {
		$referencia=generaNumeroReferencia('presupuestos','referenciaPresupuesto');
		$fecha=fechaBD();
	}
	$fecha=explode('-', $fecha);
	$referenciaMostrar=str_pad($referencia, 3, "0", STR_PAD_LEFT).'/'.$fecha[1].'-'.$fecha[0];
	echo "<fieldset class='sinFlotar'>";
	echo "<fieldset class='span3'>";

	if($datos==false){
		campoOculto('NO','aceptado');
	}

	$consulta="SELECT codigo, nombre AS texto FROM clientes;";
	campoSelectConsulta('empresa','Empresa',$consulta,$datos);
	campoDato('Nº Referencia',$referenciaMostrar);
	campoOculto($referencia,'referenciaPresupuesto');

	echo "</fieldset>";
	echo "<fieldset class='span5'>";

	campoSelect('fase','Fase',array('Primer Contacto','Presentación','Propuesta Económica','Aprobada y en espera de inicio','Ganada','Perdida'),array('contacto','presentacion','propuesta','aprobada','ganada','perdida'),$datos);
	campoFecha('fechaValidez','Fecha emisión',$datos);
	$consulta="SELECT codigo, CONCAT(apellidos, ', ', nombre) AS texto FROM usuarios WHERE usuario!='soporte' AND clave!='soporte15';";
	campoSelectConsulta('comercial','Comercial',$consulta,$datos);		

	echo "</fieldset>";
	echo "</fieldset>";

	echo "<h3 class='apartadoFormulario'> Detalles del Producto</h3>";
	echo "<fieldset class='sinFlotar'>";

	$i=tablaProductos($datos);

	echo "</fieldset>";

	echo "<h3 class='apartadoFormulario'> Totales</h3>";
	echo "<fieldset class='sinFlotar'>";

	campoCalcula('subtotal','Total',$datos);

	echo "</fieldset>";

	echo "<h3 class='apartadoFormulario'> Información de la Descripción</h3>";
	echo "<fieldset class='sinFlotar'>";

	areaTexto('descripcion','Descripción',$datos,'span8');

	echo "</fieldset>";

	cierraVentanaGestion('index.php',false);
	return $i;
}


function imprimePresupuestos($where=' AND aceptado="NO"'){
	$whereFinal=compruebaPerfilParaWhere('presupuestos.codigo');
	$whereFinal.=$where;
	$consulta=consultaBD("SELECT presupuestos.codigo, fase, clientes.nombre AS empresa, subtotal AS total, usuarios.nombre, usuarios.apellidos, aceptado, referenciaPresupuesto, fechaValidez FROM presupuestos 
	INNER JOIN clientes ON clientes.codigo=presupuestos.empresa
	LEFT JOIN usuarios ON usuarios.codigo=presupuestos.comercial $whereFinal ORDER BY empresa;",true);
	
	$fase=array('contacto'=>'Primer contacto','presentacion'=>'Presentación','propuesta'=>'Propuesta económica','aprobada'=>'Aprobada','ganada'=>'Ganada','perdida'=>'Perdida');
	while($datos=mysql_fetch_assoc($consulta)){
		$botones=botonesPresupuestos($datos['codigo'],$datos['aceptado']);
		$referencia=$datos['referenciaPresupuesto'];
		$fecha=$datos['fechaValidez'];
		$fecha=explode('-', $fecha);
		$referencia=str_pad($referencia, 3, "0", STR_PAD_LEFT).'/'.$fecha[1].'-'.$fecha[0];
		echo "<tr>
				<td> ".$referencia."</td>
				<td> ".$datos['empresa']."</td>
				<td> ".$fase[$datos['fase']]."</td>
				<td> ".$datos['nombre']." ".$datos['apellidos']."</td>
				<td> ".formateaNumeroWeb($datos['total'])." €</td>
				<td class='centro'>
					$botones
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

function botonesPresupuestos($codigo,$aceptado){
	$boton="<a href='gestion.php?codigo=".$codigo."' class='btn btn-propio'><i class='icon-search-plus'></i> Ver datos</i></a> ";
	if($aceptado=='NO'){
		$boton="
			<div class='btn-group'>
                <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
                <ul class='dropdown-menu' role='menu'>
					<li><a href='gestion.php?codigo=".$codigo."' class='noAjax'><i class='icon-search-plus'></i> Ver datos</i></a> </li>
					<li class='divider'></li>
					<li><a href='../ventas/index.php?codigo=".$codigo."&activa=1'><i class='icon-check'></i> Aprobar</i></a> </li>
					<li class='divider'></li>
					<li><a href='generaPresupuestoWord.php?codigo=".$codigo."' class='noAjax' target='_blank'><i class='icon-download'></i> Descargar</i></a></li>
				</ul>
		    </div>";
	}

	return $boton;
}


function tablaProductos($datos){
	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCursos'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Servicio </th>
							<th> Cantidad </th>
							<th> Precio Unitario </th>
							<th> Descuento(%) </th>
							<th> Total con Descuento </th>
							<th> Impuesto </th>
							<th> Total Neto </th>
							<th> </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$datosProductosConsulta=datosProductosPresupuestos($datos['codigo']);				  
		while($datosProductosPresupuestos=mysql_fetch_assoc($datosProductosConsulta)){
		 	echo "<tr>";
				$consulta="SELECT codigo, nombre AS texto FROM productos;";
				campoTextoTabla('producto'.$i,$datosProductosPresupuestos['codigoProducto'],'input-large pagination-right');
				campoTextoTabla('cantidad'.$i,$datosProductosPresupuestos['cantidad'],'input-mini pagination-right');
				campoTextoTabla('precioUnitario'.$i,$datosProductosPresupuestos['precioUnitario'],'input-mini pagination-right');
				campoTextoTabla('descuento'.$i,$datosProductosPresupuestos['descuento'],'input-mini pagination-right');
				campoTextoTabla('totalDescuento'.$i,$datosProductosPresupuestos['totalDescuento'],'input-mini pagination-right');
				campoSelect('impuesto'.$i,'',array('21%','0%'),array(21,0),$datosProductosPresupuestos['impuesto'],'span2','data-live-search="true"',1);
				campoTextoTabla('neto'.$i,$datosProductosPresupuestos['neto'],'input-mini pagination-right');
			echo"
					<td>
						<input type='checkbox' name='filasTabla[]' value='$i'>
		        	</td>
				</tr>";
			$i++;
		}
	}

	if($i==0 || !$datos){
	 	echo "<tr>";
			$consulta="SELECT codigo, nombre AS texto FROM productos;";
			campoTextoTabla('producto'.$i,'','input-large pagination-right');
			campoTextoTabla('cantidad'.$i,'','input-mini pagination-right');
			campoTextoTabla('precioUnitario'.$i,'','input-mini pagination-right');
			campoTextoTabla('descuento'.$i,'','input-mini pagination-right');
			campoTextoTabla('totalDescuento'.$i,'','input-mini pagination-right');
			campoSelect('impuesto'.$i,'',array('21%','0%'),array(21,0),21,'span2','data-live-search="true"',1);
			campoTextoTabla('neto'.$i,'','input-mini pagination-right');
		echo"
				<td>
					<input type='checkbox' name='filasTabla[]' value='$i'>
	        	</td>
			</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCursos\");'><i class='icon-plus'></i> Añadir</button> 
	              <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCursos\");'><i class='icon-minus'></i> Eliminar</button> 
	            </center>
		 	";
			
	return $i;
}

function datosProductosPresupuestos($codigo){
	$consulta=consultaBD("SELECT * FROM productos_presupuestos WHERE codigoPresupuesto='$codigo';",true);
	return $consulta;
}


function devuelveMes(){
	$mes=date('m');
	switch($mes){
		case 1:
			$mes= "Enero";
		break;
		case 2:
			$mes= "Febrero";
		break;
		case 3:
			$mes= "Marzo";
		break;
		case 4:
			$mes= "Abril";
		break;
		case 5:
			$mes= "Mayo";
		break;
		case 6:
			$mes= "Junio";
		break;
		case 7:
			$mes= "Julio";
		break;
		case 8:
			$mes= "Agosto";
		break;
		case 9:
			$mes= "Septiembre";
		break;
		case 10:
			$mes= "Octubre";
		break;
		case 11:
			$mes= "Noviembre";
		break;
		case 12:
			$mes= "Diciembre";
		break;
	}
	return $mes;
}

function obtieneConceptosPresupuestos($codigoPresupuesto){
	$res="";

	$consulta=consultaBD("SELECT * FROM productos_presupuestos WHERE codigoPresupuesto='$codigoPresupuesto';");
	while($datos=mysql_fetch_assoc($consulta)){
		//$datosProducto=consultaBD("SELECT * FROM productos WHERE codigo='".$datos['codigoProducto']."';",false,true);
		$res.="<tr>
					<td class='primeraCol'></td>
					<td>".$datos['codigoProducto']."</td>
					<td class='colPrecio'>".number_format((float)$datos['neto'], 2, ',', '')." €</td>
			   </tr>";
	}
	$res.="<tr class='ultimaFila'><td class='primeraCol'></td><td></td><td></td></tr>";

	return $res;
}

//Fin parte de incidencias