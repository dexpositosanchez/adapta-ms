<?php
  $seccionActiva=13;
  include_once("../cabecera.php");
  gestionProgramas();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});

		oyentePlazo($('input[name=tipoPlazo]:checked').val());
		$('input[name=tipoPlazo]').change(function(){
			oyentePlazo($(this).val());
		});
	});

	function oyentePlazo(val){
		if(val=='NO'){
			$('label[for=precio]').html('Precio mensual');
			$('.divTipoPlazo').css('display','none');
		} else {
			$('label[for=precio]').html('Precio total');
			$('.divTipoPlazo').css('display','block');
		}
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>