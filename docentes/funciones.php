<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de usuarios
function operacionesUsuarios(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaUsuario();
	}
	elseif(isset($_POST['nombre'])){
		$res=creaUsuario();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('usuarios');
	}

	mensajeResultado('nombre',$res,'Usuario');
    mensajeResultado('elimina',$res,'Usuario', true);
}

function actualizaUsuario(){
	$res=actualizaDatos('usuarios');


	return $res;
}

function creaUsuario(){
	$res=insertaDatos('usuarios');

	return $res;
}



function compruebaUsuarioAsociado(){
	$tipo=$_POST['tipo'];

	if($tipo=='COMERCIAL'){
		$_POST['codigoUsuario']=$_POST['codigoUsuarioDelegado'];
	}
	else{
		$_POST['codigoUsuario']=$_POST['codigoUsuarioDirector'];
	}
}

function listadoUsuarios(){
	global $_CONFIG;

	$columnas=array('nombre','email','usuario','tipo','activo');
	$having=obtieneWhereListado("HAVING usuario!='soporte' AND tipo='DOCENTE'",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS nombre, telefono, email, usuario, tipo, activo FROM usuarios $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS nombre, telefono, email, usuario, tipo, activo FROM usuarios $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$tipo=$_CONFIG['perfiles'][$datos['tipo']];

		$fila=array(
			$datos['nombre'],
			"<a href='tel:".$datos['telefono']."'>".formateaTelefono($datos['telefono'])."</a>",
			"<a href='mailto:".$datos['email']."'>".$datos['email']."</a>",
			$datos['usuario'],
			$tipo,
        	creaBotonDetalles("usuarios/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);
		$res['aaData'][]=$fila;
	}
	
	echo json_encode($res);
}


function listadoAccesos(){
	global $_CONFIG;

	$columnas=array('usuario','fechaAcceso','hora','ip','navegador','sistema');
	$having=obtieneWhereListado("HAVING usuario!='soporte'",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT accesos.codigo, usuario, DATE(fecha) AS fechaAcceso, TIME(fecha) AS hora, ip, navegador, sistema FROM accesos INNER JOIN usuarios ON accesos.codigoUsuario=usuarios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT accesos.codigo, usuario, DATE(fecha) AS fechaAcceso, TIME(fecha) AS hora, ip, navegador, sistema FROM accesos INNER JOIN usuarios ON accesos.codigoUsuario=usuarios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){

		$fila=array(
			$datos['usuario'],
			formateaFechaWeb($datos['fechaAcceso']),
			formateaHoraWeb($datos['hora']),
			$datos['ip'],
			$datos['navegador'],
			$datos['sistema'],
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function gestionUsuario(){
	operacionesUsuarios();

	abreVentanaGestion('Gestión de Usuarios','index.php');
	$datos=compruebaDatos('usuarios');
	abreColumnaCampos();
	campoTexto('nombre','Nombre',$datos);
	campoTexto('apellidos','Apellidos',$datos);
	campoTextoValidador('dni','DNI',$datos,'input-small validaDNI','usuarios');
	campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right','usuarios');
	campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large','usuarios');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoValidador('usuario','Usuario',$datos,'input-large obligatorio','usuarios','usuario');
	campoClave('clave','Contraseña',$datos,'btn-propio','input-small obligatorio');
	
	campoOculto($datos,'tipo','DOCENTE');
	campoOculto($datos,'colorTareas');
	campoOculto($datos,'activo','SI');
	campoOculto($datos,'codigoUsuario');
	campoOculto($datos,'sesion');
	cierraVentanaGestion('index.php',true);
}

function camposRegistrosAsociados($datos){
	echo "<div id='cajaDirector' class='hide'>";
	campoSelectConsulta('codigoUsuarioDirector','Director asociado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='DIRECTOR';",$datos['codigoUsuario']);
	echo "</div>

	<div id='cajaDelegado' class='hide'>";
	campoSelectConsulta('codigoUsuarioDelegado','Delegado asociado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='DELEGADO';",$datos['codigoUsuario']);
	echo "</div>
	<div id='cajaComercial' class='hide'>";
	campoSelectUsuarioAsociado('codigoComercial','Comercial asociado',"SELECT codigo, codigoUsuarioAsociado, nombre AS texto FROM comerciales ORDER BY nombre",$datos);
	echo "</div>
	<div id='cajaTelemarketing' class='hide'>";
	campoSelectUsuarioAsociado('codigoTelemarketing','Telemarketing asociado',"SELECT codigo, codigoUsuarioAsociado, nombre AS texto FROM telemarketing ORDER BY nombre",$datos);
	echo "</div>";
}

function campoSelectUsuarioAsociado($nombreCampo,$texto,$consulta,$valor){
	$valor=compruebaValorCampo($valor,'codigo');

	echo "
	<div class='control-group'>                     
		<label class='control-label' for='$nombreCampo'>$texto:</label>
		<div class='controls'>

			<select name='$nombreCampo' class='selectpicker span3 show-tick' id='$nombreCampo' data-live-search='true'>
				<option value='NULL'></option>";
		
		$consulta=consultaBD($consulta,true);
		while($datos=mysql_fetch_assoc($consulta)){
			echo "<option value='".$datos['codigo']."'";

			if($valor!=false && $valor==$datos['codigoUsuarioAsociado']){
				echo " selected='selected'";
			}

			echo ">".$datos['texto']."</option>";
		}
		
	echo "</select>
		</div> <!-- /controls -->       
	</div> <!-- /control-group -->";
}


function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectSiNoFiltro(5,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de usuarios