<?php
  $seccionActiva=2;
  include_once('../cabecera.php');
  operacionesRecibos();
  
  if(isset($_GET['codigoFactura'])){
	$_SESSION['codigoFactura']=$_GET['codigoFactura'];
  }

  $estadisticas=creaEstadisticasRecibos("AND codigoFactura='".$_SESSION['codigoFactura']."'",true,'RECTIFICATIVA');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-tasks"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de recibos:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat ancho50"> <i class="icon-ticket"></i> <span class="value"><?php echo $estadisticas['facturacion']?></span> <br>Recibos emitidos</div>
                    <!-- .stat -->
                    <div class="stat ancho50"> <i class="icon-credit-card"></i> <span class="value"><?php echo $estadisticas['cobradas']?></span> <br>Recibos percibidos</div>
                    <!-- .stat --> 
				  </div>
				  <h6 class="bigstats">Totales:</h6>
				  <div id="big_stats" class="cf">
					<div class="stat ancho50"> <i class="icon-euro"></i> <span class="value"><?php echo $estadisticas['cobrado']?></span> <br>Total cobrado</div>
                    <!-- .stat --> 
					<div class="stat ancho50"> <i class="icon-exclamation-circle" id="iconoPlazoSuperado"></i> <span class="value" id='plazoSuperado'><?php echo $estadisticas['pendiente']?></span> <br>Total por cobrar</div>
                    <!-- .stat --> 
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de recibos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="index.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
				<a href="#" id='cobradas' class="shortcut noAjax"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label">Marcar cobrados</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Recibos emitidos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
					<th> Referencia de Factura </th>
					<th> Alumno </th>
                    <th> Fecha de vencimiento </th>
					<th> Total </th>
					<th> ¿Cobrado? </th>
					<th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeRecibos("AND codigoFactura='".$_SESSION['codigoFactura']."'",true,'RECTIFICATIVA');

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>
<script src="../../api/js/jquery.dataTables.js"></script>
<script src="../../api/js/bootstrap.datatable.js"></script>
<script type="../../api/text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('#fechaEmision').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('#fechaVencimiento').datepicker({format:'dd/mm/yyyy',weekStart:1});
	if(parseInt($('#plazoSuperado').text())>0){
        $('#iconoPlazoSuperado').addClass('plazoSuperado');
    }
  });
	
	$('#cobradas').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes un vencimiento.');
      }
      else{
        valoresChecks['cobrar']='SI';
        creaFormulario('controlRecibosFactura.php',valoresChecks,'post');
      }

    });
</script>