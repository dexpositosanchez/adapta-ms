ALTER TABLE `contactos` CHANGE `apellidos` `apellido1` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `contactos` ADD `apellido2` VARCHAR(255) NOT NULL AFTER `apellido1`;

ALTER TABLE `clientes` ADD `personaContacto` VARCHAR(200) NOT NULL AFTER `programa`;