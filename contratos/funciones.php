<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores

function operacionesContratos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaContrato();
	}
	elseif(isset($_POST['fecha'])){
		$res=insertaContrato();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('contratos');
	}
	elseif(isset($_GET['codigoEnviar'])){
		$res=enviaEmailMatricula($_GET['codigoEnviar']);
  	}
	mensajeResultado('fecha',$res,'Contrato');
    mensajeResultado('elimina',$res,'Contrato', true);
    mensajeResultadoSolicitud('codigoEnviar',$res,'Matrícula');    
}

function insertaContrato(){
	$res=true;
	$_POST['importeCurso']=str_replace(',', '.', $_POST['importeCurso']);
	$res=insertaDatos('contratos');
	if($res){
		$codigo=mysql_insert_id();
		$res=insertaProductos($codigo);
	}
	return $res;
}

function actualizaContrato(){
	$res=true;
	$_POST['importeCurso']=str_replace(',', '.', $_POST['importeCurso']);
	$res=actualizaDatos('contratos');
	if($res){
		$res=insertaProductos($_POST['codigo']);
	}
	return $res;
}

function insertaProductos($contrato){
	$res=true;
	$i=0;
	$datos=arrayFormulario();
	$res = $res && consultaBD("DELETE FROM contratos_facturas WHERE codigoContrato='".$contrato."' AND facturado IS NULL");
	// Sino se insertan los nuevos registros
	while(isset($datos['importe'.$i])){
		if($datos['importe'.$i] != ''){
			$importe=str_replace(',', '.', $datos['importe'.$i]);
			$consulta = "INSERT INTO contratos_facturas VALUES (NULL,'".$contrato."','".$datos['fecha'.$i]."',
			'".$importe."',NULL);";
		
			$res = $res && consultaBD($consulta);
		}

		$i++;
	}

	return $res;
}


function creaEstadisticasContratos(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    $anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos'){
		$anio=$_SESSION['ejercicio'];
		$anio="AND (contratos.fecha LIKE '".$anio."-%' OR facturacion.fechaEmision LIKE '".$anio."-%' OR facturacion.codigo IN (SELECT codigoFactura FROM vencimientos_facturas WHERE fecha LIKE '".$anio."-%'))";
	}
    $res=consultaBD("SELECT COUNT(contratos.codigo) AS total FROM contratos LEFT JOIN facturacion ON contratos.codigo=facturacion.codigoContrato WHERE contratos.baja='NO' ".$anio,true,true);

    return $res;
}



function formateaPreciosTrabajadores(){
	formateaPrecioBDD('salarioBruto');
	formateaPrecioBDD('costeHora');
}

function listadoAlumnos(){
	global $_CONFIG;

	$columnas=array('apellido1','apellido2','nombre','curso','contratos.fecha','contratos.importeCurso','nombre','nombre');
	$having=obtieneWhereListado("HAVING 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos'){
		$anio=$_SESSION['ejercicio'];
		$anio="AND (contratos.fecha LIKE '".$anio."-%' OR facturacion.fechaEmision LIKE '".$anio."-%' OR facturacion.codigo IN (SELECT codigoFactura FROM vencimientos_facturas WHERE fecha LIKE '".$anio."-%'))";
	}

	$having .= ' AND contratos.baja = "NO"';
	conexionBD();
	$consulta=consultaBD("SELECT contratos.codigo AS codigoContrato, trabajadores_cliente.nombre, apellido1, apellido2, contratos.importeCurso, contratos.fecha, programas.nombre AS nombrePrograma, contratos.baja, facturacion.fechaEmision, facturacion.codigo FROM contratos LEFT JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo
	LEFT JOIN programas ON contratos.codigoPrograma=programas.codigo LEFT JOIN facturacion ON contratos.codigo=facturacion.codigoContrato $having $anio $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT contratos.codigo AS codigoContrato, trabajadores_cliente.nombre, apellido1, apellido2, contratos.importeCurso, contratos.fecha, programas.nombre AS nombrePrograma, contratos.baja, facturacion.fechaEmision, facturacion.codigo FROM contratos LEFT JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo
	LEFT JOIN programas ON contratos.codigoPrograma=programas.codigo LEFT JOIN facturacion ON contratos.codigo=facturacion.codigoContrato $having $anio;");
	cierraBD();
	
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$gastos=consultaBD('SELECT SUM(precio) AS suma FROM contratos_gastos WHERE codigoContrato='.$datos['codigoContrato'],true,true);
		$importe = number_format((float)$datos['importeCurso'], 2, ',', '');
		$totalPendiente=consultaBD("SELECT SUM(contratos_facturas.importe) AS importe FROM contratos_facturas WHERE codigoContrato=".$datos['codigoContrato']." AND facturado IS NOT NULL;",true,true);
		if(!isset($totalPendiente['importe'])){
			$totalPendiente['importe']=$datos['importeCurso'];
		} else {
			$totalPendiente['importe']=$datos['importeCurso']-$totalPendiente['importe'];
		}
		$fila=array(
			$datos['apellido1'],
			$datos['apellido2'],
			$datos['nombre'],
			$datos['nombrePrograma'],
			utf8_encode(formateaFechaWeb($datos['fecha'])),
			$importe,
			number_format((float)$totalPendiente['importe'], 2, ',', '').' €',
			botonAcciones(array('Detalles','Genera Matrícula','Enviar Email'),array('contratos/gestion.php?codigo='.$datos['codigoContrato'],'contratos/generaMatriculaWord.php?codigo='.$datos['codigoContrato'],'contratos/index.php?codigoEnviar='.$datos['codigoContrato']),array("icon-search-plus","icon-download","icon-envelope")),
        	obtieneCheckTabla($datos,'codigoContrato'),
        	"DT_RowId"=>$datos['codigoContrato']
		);
		$res['aaData'][]=$fila;
	}
	echo json_encode($res);
	//echo json_last_error_msg();
}


function gestionContratos(){
	operacionesContratos();

	abreVentanaGestion('Gestión de Matrículas','gestion.php','span3');
	$datos=compruebaDatos('contratos');
	$factura=datosRegistro('facturacion',$datos['codigo'],'codigoContrato');

	abreColumnaCampos('span9');
	campoOculto($datos,'pagoInicial');
	if($datos && $factura){
		campoOculto($factura['codigo'],'factura');
		campoDato('Fecha',formateaFechaWeb($datos['fecha']),'fecha');
		$alumno=datosRegistro('trabajadores_cliente',$datos['codigoAlumno']);
		campoDato('Alumno',$alumno['apellido1'].' '.$alumno['apellido2'].', '.$alumno['nombre'].' - '.$alumno['nif'],'codigoAlumno');
		$programa=datosRegistro('programas',$datos['codigoPrograma']);
		campoDato('Programa',$programa['nombre'],'codigoPrograma');
		campoDato('Coste',$datos['importeCurso'],'importeCurso');
		campoDato('Forma de pago',$datos['formaPago'],'formaPago');
	} else {
		campoFecha('fecha','Fecha',$datos);
		campoSelectConsultaAjax('codigoAlumno','Alumno',"SELECT codigo, CONCAT(apellido1,' ',apellido2,', ',nombre,' - ',nif) AS texto FROM trabajadores_cliente ORDER BY nombre, apellido1, apellido2, nif;",$datos,"alumnos/gestion.php?codigo=");
		campoSelectConsultaAjax('codigoPrograma','Programa',"SELECT codigo, CONCAT(codigoInterno,' - ',nombre) AS texto FROM programas ORDER BY nombre;",$datos,"programas/gestion.php?codigo=",'selectpicker selectAjax span8 show-tick');
	
		campoTextoSimbolo('importeCurso','Importe',"<i class='icon-euro'></i>",number_format((float)$datos['importeCurso'], 2, ',', ''));
	
		campoSelect('formaPago','Forma pago',array('TPV','Ingreso en cuenta','Transferencia','Recibo domiciliado'),array('Cuenta corriente','Paypal','Transferencia','Recibo domiciliado'),$datos);
	}
	cierraColumnaCampos();

	echo "<h4 class='apartadoFormulario sinFlotar'>Facturas</h4>";
	abreColumnaCampos('span11');
		$i=tablaFacturas($datos);
	cierraColumnaCampos();

	echo '<br clear="all">';
	campoRadio('facturaEmpresa','¿Facturar a empresa?',$datos);
	echo '<div id="datosEmpresa" class="hide">';
		campoTexto('denominacionSocial','Denominación Social',$datos);
		campoTexto('cif','CIF',$datos,'input-small');
		campoTexto('direccion','Dirección',$datos);
		campoTexto('cp','Código postal',$datos,'input-mini');
		campoTexto('provincia','Provincia',$datos);
	echo '</div>';

	abreColumnaCampos('span11');
	if($datos && $factura){
		cierraColumnaCampos();
		echo "<h4 class='apartadoFormulario sinFlotar'>Baja</h4>";
		$fechaBaja='';
		if($datos){
			$fechaBaja=$datos['fechaBaja'];
		}
		abreColumnaCampos('span11');
		campoRadio('baja','Baja',$datos);
		campoFecha('fechaBaja','Fecha de baja',$datos);
		campoTexto('motivoBaja','Motivo de baja',$datos);
		$totalPendiente=consultaBD("SELECT SUM(contratos_facturas.importe) AS importe FROM contratos_facturas WHERE facturado IS NOT NULL;",true,true);
		if(!isset($totalPendiente['importe'])){
			$totalPendiente['importe']=$datos['importeCurso'];
		} else {
			$totalPendiente['importe']=$datos['importeCurso']-$totalPendiente['importe'];
		}
		campoDato('Saldo pendiente por facturar',number_format((float)$totalPendiente['importe'], 2, ',', '').' €','importePendienteAlumno');

		$totalPendiente=consultaBD("SELECT SUM(contratos_facturas.importe) AS importe FROM contratos_facturas INNER JOIN facturacion ON contratos_facturas.facturado=facturacion.codigo WHERE facturado IS NOT NULL AND facturacion.cobrada='NO';",true,true);
		if(!isset($totalPendiente['importe'])){
			$totalPendiente['importe']=0;
		} 
		campoDato('Saldo pendiente por cobrar',number_format((float)$totalPendiente['importe'], 2, ',', '').' €','importePendienteAlumno');

	} else {
		cierraColumnaCampos();
		echo "<h4 class='apartadoFormulario sinFlotar'>Baja</h4>";
		abreColumnaCampos('span11');
		campoRadio('baja','Baja',$datos);
		campoFecha('fechaBaja','Fecha de baja',$datos);
		campoTexto('motivoBaja','Motivo de baja',$datos);
	}
	cierraColumnaCampos();
	

	cierraVentanaGestionContratos('index.php',true);
	
	return $i;
}

function tablaFacturas($datos){
	echo"	
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaGastos'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Fecha </th>
							<th> Importe € </th>
							<th> Facturada </th>
							<th> Cobrada </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$iconoC=array('SI'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
		$consulta=consultaBD("SELECT * FROM contratos_facturas WHERE codigoContrato=".$datos['codigo'].' ORDER BY fecha',true);				  
		while($factura=mysql_fetch_assoc($consulta)){
			$facturado='NO';
			$cobrado='NO';
			if($factura['facturado']!=NULL){
				$facturado='SI';
				$factura2=datosRegistro('facturacion',$factura['facturado']);
				if($factura2['cobrada']=='SI'){
					$cobrado='SI';
				}
			}
		 	echo "<tr>";
		 	if($facturado=='SI'){
		 		echo "<td>".formateaFechaWeb($factura['fecha'])."</td>";
		 		echo "<td>".$factura['importe']." €</td>";
			} else {
				campoFechaTabla('fecha'.$i,$factura['fecha']);
				campoTextoSimbolo('importe'.$i,'','€',number_format((float)$factura['importe'], 2, ',', ''),'input-mini pagination-right',1);
				$i++;
			}

			echo"<td>".$facturado."</td>
				<td>".$cobrado."</td>
				</tr>";
		}
	}
	if($i==0){
	echo "<tr>";
		echo "<tr>";
		 	campoFechaTabla('fecha'.$i);
			campoTextoSimbolo('importe'.$i,'','€','','input-mini pagination-right',1);

			echo"<td></td><td></td>
				</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaGastos\");'><i class='icon-plus'></i> Añadir recibo</button> 
				</center>
		 	";
	return $i;
}

function camposPreciosProductos(){
	$consulta=consultaBD("SELECT codigo, importe FROM otros_gastos", true);
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto($datos['importe'],'codProducto'.$datos['codigo']);
	}
}

//Hago una función personalizada de comprobación de datos porque existe la opción de "duplicar", en cuyo caso el campoOculto no se crea (para hacer una inserción)
function compruebaDatosTrabajador(){
	if(isset($_REQUEST['codigo']) && !isset($_GET['duplicar'])){
		$datos=datosRegistro('trabajadores_cliente',$_REQUEST['codigo']);
		campoOculto($datos);
	}
	elseif(isset($_REQUEST['codigo']) && isset($_GET['duplicar'])){
		$datos=datosRegistro('trabajadores_cliente',$_REQUEST['codigo']);
	}
	else{
		$datos=false;
	}
	return $datos;
}

function filtroAlumnos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nombre');
	campoTexto(1,'Apellido 1');
	campoTexto(2,'Apellido 2');
	campoDNI(3,'DNI');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(5,'NISS');
	campoSelectClienteFiltradoPorUsuario(false,'Empresa',6);
	campoSelectSiNoFiltro(7,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function consultaDuplicidadTrabajador(){
	$res='OK';
	$datos=arrayFormulario();

	$nombre=limpiaCadena($datos['nombre']);
	$apellido1=limpiaCadena($datos['apellido1']);
	$apellido2=limpiaCadena($datos['apellido2']);
	$whereNombre='';
	if($nombre!='' || $apellido1!='' || $apellido2!=''){
		$whereNombre="(nombre='$nombre' AND apellido1='$apellido1' AND apellido2='$apellido2')";
	}

	$nif=$datos['nif'];
	$whereNif='';
	if($nif!=''){
		$whereNif="OR nif='$nif'";
	}

	$whereCodigo='';
	if($datos['codigo']!='NO'){
		$whereCodigo='AND codigo!='.$datos['codigo'];
	}


	if($whereNombre!='' || $whereNif!=''){
		$consulta=consultaBD("SELECT nombre, apellido1, apellido2 FROM trabajadores_cliente WHERE ($whereNombre $whereNif) $whereCodigo",true,true);
		if($consulta['nombre'] != ''){
			$res=$consulta['nombre'].' '.$consulta['apellido1'].' '.$consulta['apellido2'].' de la empresa '.$consulta['razonSocial'];
		} 
	}
	
	echo $res;
}

function campoCuentaBancaria($valor){
	$valor=compruebaValorCampo($valor,'numCuenta');
	echo '<div class="control-group">                     
	  <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
	  <div class="controls numSS">
		<div class="input-prepend input-append">
		  <input type="text" tabla="trabajadores_cliente" class="input-mini" id="iban" name="iban" maxlength="4" size="4" value="'.substr($valor, 0, 4).'">
		  <span class="add-on">/</span>
		  <input type="text" class="input-medium" tabla="trabajadores_cliente" id="numCuenta" name="numCuenta" maxlength="20" size="20" value="'.substr($valor, 4, 24).'">
		</div>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->';
}

function cargarDatosCurso(){
	$datos=arrayFormulario();
	$curso=datosRegistro('programas',$datos['programa']);
	echo $curso['precio'];
}

function nuevaMetodologia(){
	$alumnos=consultaBD('SELECT * FROM trabajadores_cliente',true);
	while($alumno=mysql_fetch_assoc($alumnos)){
		$curso=datosRegistro('cursos',$alumno['curso']);
		echo "INSERT INTO contratos VALUES(NULL,".$alumno['codigo'].",".$alumno['curso'].",'".$alumno['fechaAlta']."','".$alumno['importeCurso']."','".$alumno['pagoInicial']."','".$curso['tipoPlazo']."','".$alumno['plazosCurso']."','".$alumno['formaPago']."','".$alumno['formaPagoInicial']."','".$alumno['fechaBaja']."','".$alumno['motivoBaja']."');<br/>";
	}
}

function generaSolicitud($codigo){
	$datos=datosRegistro('contratos',$codigo);
	$programa=datosRegistro('programas',$datos['codigoPrograma']);
	$curso=datosRegistro('cursos',$programa['codigoCurso']);
    $contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:12px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }
	        table td, .tablaConceptos td{
	            font-size: 12px;
	        }

	        .logo{
	            float:left;
	            vertical-align: bottom;
	            margin-right: 15px;
	        }
			#cajaLogo{
				width:100%;
			}
		}
	-->
	</style>
	<page footer='page'>
		<div id='cajaLogo'>
	    <img src='../img/logo3.png' class='logo' />
	    </div><br/><br/>
		DOCUMENTO DE SOLICITUD DE MATRÍCULA O CONTRATO DE INSCRIPCIÓN<br/><br/>		
		<u>DATOS DEL PROGRAMA FORMATIVO:</u>
		<table>
			<tr>
				<td>Módulo</td>
				<td>".$curso['curso']."</td>
			</tr>
			<tr>
				<td>Código identificativo</td>
				<td>".$programa['codigoInterno']."</td>
			</tr>
			<tr>
				<td>Nombre del programa</td>
				<td>".$programa['nombre']."</td>
			</tr>
			<tr>
				<td>Precio (PVP)</td>
				<td>".$programa['precio']."</td>
			</tr>
			<tr>
				<td>Importe matrícula</td>
				<td>".$programa['importeMatricula']."</td>
			</tr>
			<tr>
				<td>Lugar de celebración</td>
				<td>".$programa['lugar']."</td>
			</tr>
			<tr>
				<td>Horas lectivas</td>
				<td>".$programa['horasLectivas']."</td>
			</tr>
			<tr>
				<td>Días previstos</td>
				<td>".$programa['diasPrevistos']."</td>
			</tr>
			<tr>
				<td>Horario</td>
				<td>".$programa['horario']."</td>
			</tr>
			<tr>
				<td>Modalidad</td>
				<td>".$programa['modalidad']."</td>
			</tr>
			<tr>
				<td>Fecha/s prevista/s</td>
				<td>".$programa['previstaFecha']."</td>
			</tr>
		</table>
		<page_footer>

		</page_footer>
		
	</page>";

	return $contenido;
}

function enviaEmailMatricula($codigoContrato){

	$res=true;

	$datos=datosRegistro('contratos',$codigoContrato);
	$datosAlumnos=datosRegistro('trabajadores_cliente',$datos['codigoAlumno']);
	$factura='Matricula'.$codigoContrato.'.docx';
	
	/*Parte de envío por correo al cliente de la factura*/
	
	$mensajeEnvio="Buenos Días,
	<br /><br />Adjunto remito su Matrícula.<br /><br />";
	
	$mensajeEnvio.="<img src='http://crmparapymes.com.es/adapta-ms/img/logo2.png'/><br /><br />";
	
	$mensajeEnvio.="<br /><br /><strong>Adapta Management Services</strong><br />
	<strong><i>Ventas</i></strong><br /><br />
	CP 29002 Málaga<br />
	C/ Cuarteles, 7- Planta 2,Oficina 6. (Edificio Guadalmedina)<br /><br />
	+34 952 34 10 07<br />
	+34 637 474 372<br />
	www.adaptams.com<br /><br />";
	
	

	$mensajeEnvio.="
		Este mensaje se dirige exclusivamente a su destinatario y puede contener información privilegiada o confidencial. Si no es vd. el destinatario indicado, queda notificado de que la utilización, divulgación y/o copia sin autorización está prohibida en virtud de la legislación vigente. Si ha recibido este mensaje por error, le rogamos que nos lo comunique inmediatamente por esta misma vía y proceda a su destrucción.
	";
	
	$aleatorio=md5(time());
	$limiteMime="==TecniBoundary_x{$aleatorio}x";
	$headers="From: info@adaptams.com\r\n";
	$headers.= "MIME-Version: 1.0\r\n";
	$headers.="Content-Type: multipart/mixed;" . "boundary=\"{$limiteMime}\"";

	$mensaje="--{$limiteMime}\r\n"."Content-Type: text/html; charset=\"utf-8\"\r\n"."Content-Transfer-Encoding: 7bit\n\n".$mensajeEnvio."\n\n";

	$fp=fopen('../documentos/matriculas/'.$factura, "r");
	$tam=filesize('../documentos/matriculas/'.$factura);
	$fichero=fread($fp,$tam);
	$ficheroAdjunto=chunk_split(base64_encode($fichero));
	fclose($fp);
	
	$mensaje.="--{$limiteMime}\r\n";
	$mensaje.="Content-Type: application/octet-stream; name=\"".basename('../documentos/matriculas/'.$factura)."\"\r\n"."Content-Description:".basename('../documentos/matriculas/'.$factura)."\r\n"."Content-Disposition: attachment;filename=\"".basename('../documentos/matriculas/'.$factura)."\";size=".$tam."\r\n"."Content-Transfer-Encoding:base64\r\n\r\n".$ficheroAdjunto."\r\n\r\n";

	$mensaje .= "--{$limiteMime}--";
	
    
    if (!mail($datosAlumnos['email'], 'Envío de Matricula', $mensaje, $headers)) {
		$res=false;
    } 
	
	/* FIN PARTE ENVIO AL CLIENTE*/
	
	return $res;
}

function cierraVentanaGestionContratos($destino,$columnas=false,$botonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left',$salto=true){
	if($columnas){
		echo "		  </fieldset>
			  		  <fieldset class='sinFlotar'>";
	}
	if($salto){
		echo "			<br />";
	}


	echo "
						
	                    <div class='form-actions'>";
	if($botonVolver){
	    echo "            <a href='$destino' class='btn btn-default'><i class='$iconoVolver'></i> $textoVolver</a>";
	}

	if($botonGuardar){                      
	    echo "            <button type='submit' class='btn contratosBt btn-propio'><i class='$icono'></i> $texto</button>";
	}

	echo "
	                    </div> <!-- /form-actions -->
	                  </fieldset>
	                </form>
	                </div>


	            </div>
	            <!-- /widget-content --> 
	          </div>

	      </div>
	    </div>
	    <!-- /container --> 
	  </div>
	  <!-- /main-inner --> 
	</div>
	<!-- /main -->";
}

//Fin parte de trabajadores