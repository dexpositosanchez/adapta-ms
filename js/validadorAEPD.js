var mensajeMostrado = 0;

//Datos declarante
$('#razon_s').attr('maxlength',140);
$('#cif_nif').attr('maxlength',9);
$('#nombre').attr('maxlength',35);
$('#apellido1').attr('maxlength',35);
$('#apellido2').attr('maxlength',35);
$('#nif').attr('maxlength',9);
$('#dir_postal').attr('maxlength',100);
$('#localidad').attr('maxlength',50);
$('#postal').attr('maxlength',5);
$('#telefono').attr('maxlength',9);
$('#fax').attr('maxlength',9);
$('#email').attr('maxlength',70);
$('#cif_responsableFichero').attr('maxlength',9);
$('#dir_postal_responsableFichero').attr('maxlength',100);
$('#localidad_responsableFichero').attr('maxlength',50);
$('#postal_responsableFichero').attr('maxlength',5);
$('#telefono_responsableFichero').attr('maxlength',9);
$('#fax_responsableFichero').attr('maxlength',9);
$('#email_responsableFichero').attr('maxlength',70);
$('#n_razon').attr('maxlength',140);
$('#oficina').attr('maxlength',70);
$('#nif_cif').attr('maxlength',9);
$('#dir_postal_derecho').attr('maxlength',100);
$('#localidad_derecho').attr('maxlength',50);
$('#postal_derecho').attr('maxlength',5);
$('#telefono_derecho').attr('maxlength',9);
$('#fax_derecho').attr('maxlength',9);
$('#email_derecho').attr('maxlength',70);
$('#n_razon_encargado').attr('maxlength',140);
$('#cif_nif_encargado').attr('maxlength',9);
$('#dir_postal_encargado').attr('maxlength',100);
$('#localidad_encargado').attr('maxlength',50);
$('#postal_encargado').attr('maxlength',5);
$('#telefono_encargado').attr('maxlength',9);
$('#fax_encargado').attr('maxlength',9);
$('#email_encargado').attr('maxlength',70);
$('#fichero').attr('maxlength',70);
$('#desc_fin_usos').attr('maxlength',350);
$('#ODCI').attr('maxlength',100);

$('.maximoCaracteres').keypress(function(event){
	oyenteMensajeMaximo($(this),$(this).attr('maxlength'),event.which);
})

function oyenteMensajeMaximo(campo,maximo,tecla){
	if(tecla != 13 && tecla != 0 && tecla != 8){
		if(campo.val().length == maximo){
			alert('La AEPD no permite más de '+maximo+' carácteres en este campo');
		}
	}
}

$('.submit').click(function(e){
	e.preventDefault();
	$('#accion').attr('value',$(this).attr('value'));
	validaCamposObligatorios();
});

//El parámetro selectorFormulario se utiliza para los casos en que se use el script en páginas con ventanas modales, que tienen también #formcontrols
function validaCamposObligatorios(selectorFormulario){
	var res=0;
	
	if(selectorFormulario==undefined){
		selectorFormulario='#formcontrols form';
	}

	//Le digo que no seleccione los .bootstrap-select porque corresponden al selectpicker en sí (que hereda la clase del select original), y hay que trabajar con los select directamente.
	$(selectorFormulario).find('.obligatorio:not(.bootstrap-select)').each(function(){
		res+=compruebaValor($(this));
	});
	
	$('[name*=telefono],[name*=fax],[name*=movil]').each(function(){//Validador para teléfono/fax y móvil
		res+=asignaValidacion($(this),/^[679]\d{8}$/,'Número de teléfono no válido');
	});
	mensajeMostrado = 0;

	$('[name*=email]').each(function(){//Validador para email
		res+=asignaValidacion($(this),/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,'Dirección de email no válida');
	});
	mensajeMostrado = 0;

	$('[name*=cp],[name|=postal]').each(function(){//Validador para código postal
		res+=asignaValidacion($(this),/^(01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|AD)\d{3}$/,'Código Postal no válido');
	});
	mensajeMostrado = 0;

	$('[name*=cif]').each(function(){
		if($(this).attr('name').indexOf('nif')!=-1){
			res+=validarNifoCif($(this));
		} else {
			res+=validaCif($(this));
		}
	});
	mensajeMostrado=0;

	$('[name*=nif]').each(function(){
		if($(this).attr('name').indexOf('cif')!=-1){
			//res+=validarNifoCif($(this));
		} else {
			res+=validaNif($(this));
		}
	});

	if(res==0){
		$(selectorFormulario).submit();
	}
	else{
		alert('Por favor, rellene todos los campos marcados en rojo correctamente.');
	}

	return res;//Se devuelve res para usarlo en otras funciones personalizadas de validación (como en acciones-formativas/gestion.php)
}

function compruebaValor(campo){
	var res=0;
	
	if(campo.val()=='' || campo.val()=='NULL'){
		res=1;
	}

	formateaCampoObligatorio(campo,res);

	return res;
}

function formateaCampoObligatorio(campo,res){
	if(campo.hasClass('selectpicker') && res==0){
		campo.selectpicker('setStyle', 'btn-danger','remove');
	}
	else if(campo.hasClass('selectpicker') && res==1){
		campo.selectpicker('setStyle', 'btn-danger');
	}
	else if(res==0){
		campo.css('border','1px solid #cccccc');
	}
	else{
		campo.css('border','1px solid red');
	}
}


function asignaValidacion(campo,expresion,mensaje){//Función general de validación. Recibe el campo, la expresión regular a utilizar y el mensaje en caso de que no se cumpla.
	res = 0;
	if(campo.val().trim()!=''){
		var validador=new RegExp(expresion);
					
		if(!validador.test(campo.val())){
			if(mensajeMostrado == 0){
				alert(mensaje);
				mensajeMostrado = 1;
			}
			res = 1;

		}
		formateaCampoObligatorio(campo,res);
	}
	return res;
}

function validaCif(campo,doble=false){
	res = 0;
	abc = campo.val();
	par = 0;
	non = 0;
	letras = "ABCDEFGHJKLMNPQRSUVW";
	let = abc.charAt(0);
 	mensaje = '';
	if (abc.length!=9) {
		mensaje='El CIF debe tener 9 dígitos';
		res=1;
	}
 	
	if (letras.indexOf(let.toUpperCase())==-1) {
		mensaje="El comienzo del CIF no es válido";
		res=1;
	}

 	for (zz=2;zz<8;zz+=2) {
 		par = par+parseInt(abc.charAt(zz));
 	}
 	
 	for (zz=1;zz<9;zz+=2) {
 		nn = 2*parseInt(abc.charAt(zz));
 		if (nn > 9) nn = 1+(nn-10);
 		non = non+nn;
 	}
 	
 	parcial = par + non;
 	control = (10 - ( parcial % 10));
 	if (control==10) control=0;
 	
 	if (control!=abc.charAt(8)) {
 		mensaje="El CIF no es válido";
 		res=1;
 	}

 	
 	formateaCampoObligatorio(campo,res);
 	if(!doble){
 		if(mensaje!='' && mensajeMostrado == 0){
 			alert(mensaje);
 			mensajeMostrado = 1;
 		}
 	}
 	return res;
}

function validaNif(campo,doble=false) {
  var res = 0;
  var dni = campo.val();
  var mensaje = '';	
  var numero
  var let
  var letra
  var expresion_regular_dni
 
  expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
 
  if(expresion_regular_dni.test (dni) == true){
     numero = dni.substr(0,dni.length-1);
     let = dni.substr(dni.length-1,1);
     numero = numero % 23;
     letra='TRWAGMYFPDXBNJZSQVHLCKET';
     letra=letra.substring(numero,numero+1);
     if (letra!=let.toUpperCase()) {
       	mensaje='Dni erroneo, la letra del NIF no se corresponde';
     	res = 1;
     }
  }else{
     	mensaje='Dni erroneo, formato no válido';
     	res=1;
   }

   
   	formateaCampoObligatorio(campo,res);
   	if(!doble){
 		if(mensaje!='' && mensajeMostrado == 0){
 			alert(mensaje);
 			mensajeMostrado = 1;
 		}
 	}
 	return res;
}

function validarNifoCif(campo){
	var res = 0;
	res=validaNif(campo,true);
	if(res > 0){
		res=validaCif(campo,true);
	}
	if(res > 0){
		if(mensajeMostrado == 0){
 			alert('CIF/NIF Erroneo, compruebe letras y formatos');
 			mensajeMostrado = 1;
 		}
 	}
 	return res;

}