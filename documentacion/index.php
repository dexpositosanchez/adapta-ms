<?php
  $seccionActiva=1;
  include_once('../cabecera.php');
  $_POST['codigoUsuario'] = $_SESSION['codigoU'];
  operacionesDocumentacion();
?> 
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">


        <!-- /span6 -->
        <div class="span12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Documentos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">

              <h6>A continuación se muestra la documentación subida al sistema:</h6><br>

            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tabbable">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#pagina1" data-toggle="tab">Internos</a></li>
                  <li><a href="#pagina2" data-toggle="tab">Externos</a></li>
                  <li><a href="#pagina3" data-toggle="tab">Registros</a></li>
                  <li><a href="#pagina4" data-toggle="tab">Distribución</a></li>
                </ul>

                <br>

                <div class="tab-content">
                  <div class="tab-pane active" id="pagina1">
                    <fieldset>  
                      <form id="formSubidaInterno" action="?" method="post" enctype="multipart/form-data">
                        <table class="table table-striped table-bordered datatable" id="internos">
                          <thead>
                            <tr>
                                <th> Código </th>
                                <th> Documento </th>
                                <th> Revisión </th>
                                <th> Fecha </th>
                                <th class="centro"> </th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                              imprimeDocumentacion('internos');
                          ?>
                          </tbody>
                        </table>
                        <br/>
                        <a href="javascript:void" class="btn btn-success" onclick="$('#ficheroInterno').click();"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
                        <input type="file" class="hide" name="ficheroInterno" id="ficheroInterno" onchange="nuevaFilaInternos();"/>
                      </form>
                    </fieldset>
                  </div>

                  <div class="tab-pane" id="pagina2">
                    <fieldset>  
                      <form id="formSubidaExterno" action="?" method="post" enctype="multipart/form-data">
                        <table class="table table-striped table-bordered datatable" id="externos">
                          <thead>
                            <tr>
                                <th> Código </th>
                                <th> Documento </th>
                                <th> Ubicación </th>
                                <th class="centro"> </th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                              imprimeDocumentacion('externos');
                          ?>
                          </tbody>
                        </table>
                        <br/>
                        <a href="javascript:void" class="btn btn-success" onclick="$('#ficheroExterno').click();"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
                        <input type="file" class="hide" name="ficheroExterno" id="ficheroExterno" onchange="nuevaFilaExternos();"/>
                      </form>
                    </fieldset>
                  </div>

                  <div class="tab-pane" id="pagina3">
                    <fieldset>  
                      <form id="formSubidaRegistro" action="?" method="post" enctype="multipart/form-data">
                        <table class="table table-striped table-bordered datatable" id="registros">
                          <thead>
                            <tr>
                                <th> Código </th>
                                <th> Nombre </th>
                                <th> Responsable </th>
                                <th> Ubicación </th>
                                <th> Tiempo Conservación </th>
                                <th class="centro"> </th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                              imprimeDocumentacion('registros');
                          ?>
                          </tbody>
                        </table>
                        <br/>
                        <a href="javascript:void" class="btn btn-success" onclick="$('#ficheroRegistro').click();"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
                        <input type="file" class="hide" name="ficheroRegistro" id="ficheroRegistro" onchange="nuevaFilaRegistros();"/>
                      </form>
                    </fieldset>
                  </div>

                  <div class="tab-pane" id="pagina4">
                    <fieldset>  
                      <form id="formSubidaDistribucion" action="?" method="post" enctype="multipart/form-data">
                        <table class="table table-striped table-bordered datatable" id="distribucion">
                          <thead>
                            <tr>
                                <th> Fecha </th>
                                <th> Documento </th>
                                <th> Ubicación </th>
                                <th> Copia </th>
                                <th class="centro"> </th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                              imprimeDocumentacion('distribucion');
                          ?>
                          </tbody>
                        </table>
                        <br/>
                        <a href="javascript:void" class="btn btn-success" onclick="$('#ficheroDistribucion').click();"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
                        <input type="file" class="hide" name="ficheroDistribucion" id="ficheroDistribucion" onchange="nuevaFilaDistribucion();"/>
                      </form>
                    </fieldset>
                  </div>
            
            </div>
                </div>
              </div>

          </div>  

          <br/>
          <!--<a href="generaExcelDocumentos.php" class="btn btn-inverse pull-right"><i class="icon-download"></i> Descargar Excel</a>-->            
            
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>


<div class="margenAb"></div>


<!-- /main -->
</div>

<script type="text/javascript" src="../js/jquery.form.min.js"></script>
<script type="text/javascript"> 
var fecha='<?php echo date('d/m/Y'); ?>';
  function nuevaFilaInternos(){
    $('#internos').find("tr:last").after("<tr><td><input type='text' name='codigoDocumento' class='input-mini' /></td><td><input type='text' name='nombreDocumento' class='input-large' /></td><td><input type='text' name='fechaRevision' class='input-small datepicker hasDatepicker' value='"+fecha+"' /></td><td><input type='text' name='fecha' class='input-small datepicker hasDatepicker' value='"+fecha+"' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('#formSubidaInterno').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  function nuevaFilaExternos(){
    $('#externos').find("tr:last").after("<tr><td><input type='text' name='codigoDocumento' class='input-mini' /></td><td><input type='text' name='nombreDocumento' class='input-large' /></td><td><input type='text' name='ubicacion' class='input-large' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('#formSubidaExterno').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  function nuevaFilaRegistros(){
    $('#registros').find("tr:last").after("<tr><td><input type='text' name='codigoDocumento' class='input-mini' /></td><td><input type='text' name='nombreDocumento' class='input-small' /></td><td><input type='text' name='responsable' class='input-small' /></td><td><input type='text' name='ubicacion' class='input-small' /></td><td><input type='text' name='tiempoConservacion' class='input-large' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('#formSubidaRegistro').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  function nuevaFilaDistribucion(){
    $('#distribucion').find("tr:last").after("<tr><td><input type='text' name='fecha' class='input-small datepicker hasDatepicker' value='"+fecha+"' /></td><td><input type='text' name='nombreDocumento' class='input-large' /></td><td><input type='text' name='ubicacion' class='input-large' /></td><td><select name='copia' class='selectpicker span3 show-tick'><option value='ORIGINAL'>Original</option><option value='CONTROLADO'>Controlado</option><option value='NOCONTROLADO'>No Controlado</option></select></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('#formSubidaDistribucion').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
</script>
<?php include_once('../pie.php'); ?>