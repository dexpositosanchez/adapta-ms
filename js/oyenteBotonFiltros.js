$('.hasDatepicker').val('');//Le quito el valor a los campos fecha, que por defecto es la fecha actual

//La siguiente función sirve como oyente del botón "Busqueda por filtros". Inicializa los oyentes de los filtros si se pulsa y los quita si se vuelve a pulsar
function oyenteBotonFiltros(selectorBoton,selectorCaja,selectorTabla){
  $(selectorBoton).click(function(){
    var estado=$(this).attr('estado');//Atributo personalizado del botón
    
    if(estado=='visible'){//Filtros activos -> se procede a eliminarlos
      $(selectorCaja).slideUp();
      $(this).attr('estado','oculto');
      $(this).html("<i class='icon-filter'></i> Búsqueda por filtros");

      $(selectorTabla+'_wrapper').find('input[type=search]').attr('disabled',false);//Reactivación del campo de búsqueda global

      eliminaFiltros(selectorCaja, selectorTabla);
    }
    else{//Filtros inactivos -> se procede a mostrarlos y cargar los oyentes
      $(selectorCaja).slideDown();
      $(this).attr('estado','visible');
      $(this).html("<i class='icon-remove'></i> Quitar filtros");

      $(selectorTabla+'_wrapper').find('input[type=search]').attr('disabled',true);//Bloqueo del campo de búsqueda global
      
      oyentesCamposFiltro(selectorCaja,selectorTabla);
    }

    $(this).toggleClass("btn-primary");
    $(this).toggleClass("btn-danger");
  });
}

//Detecta las teclas en los input y los cambios en los select, para llamar a la función de filtrado
function oyentesCamposFiltro(selectorCaja,selectorTabla){
  $(selectorCaja).find('input:not(.input-block-level)').each(function(){
    $(this).keyup(function(){
    
      realizaBusquedaFiltrada(selectorCaja,selectorTabla);
    });

    $(this).on('changeDate',function(){
      realizaBusquedaFiltrada(selectorCaja,selectorTabla);
    });
  });

  $(selectorCaja).find('select').each(function(){
    $(this).change(function(){
      
      realizaBusquedaFiltrada(selectorCaja,selectorTabla);
    });
  });
}

//La siguiente función recorre todos los campos de filtro, obteniendo de sus name las columnas asociadas por las que hay que filtrar
function realizaBusquedaFiltrada(selectorCaja,selectorTabla){
  var tabla=$(selectorTabla).dataTable();//Llamada a la API de datatable

  var busquedas=new Array();
  var columnas=new Array();
  
  var max=0;
  var i=0;
  $(selectorCaja).find('input:not(.input-block-level),select').each(function(){
    var valor=$(this).val();//Término de búsqueda
    var columna=$(this).attr('name');//Columna por la que filtrar

    valor=valor.replace('NULL','');
    busquedas[i]=valor;
    columnas[i]=columna;
  
    if(max<parseInt(columna)){
      max=parseInt(columna);
    }

    i++;
  });

  max++;//Esta variable sirve para los casos en los que el número de campos del filtro sea mayor al de las columnas a filtrar. Lo incremento a 1 porque si el último campo es 22, crearía sSearch de 0 a 21

  tabla.filtroMultiple(busquedas,columnas,max);//Llamada a función personalizada de datatables
}

function eliminaFiltros(selectorCaja, selectorTabla){
  $(selectorCaja).find('input:not(.input-block-level),select').each(function(){
    $(this).val('');
  });

  realizaBusquedaFiltrada(selectorCaja, selectorTabla);//Para dejar el listado sin filtrar
  $(selectorCaja).find('.selectpicker').selectpicker('refresh');
}