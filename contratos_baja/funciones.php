<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores

function operacionesContratos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaContrato();
	}
	elseif(isset($_POST['fecha'])){
		$res=insertaContrato();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('contratos');
	}

	mensajeResultado('fecha',$res,'Contrato');
    mensajeResultado('elimina',$res,'Contrato', true);
}

function insertaContrato(){
	$res=true;
	$res=insertaDatos('contratos');
	if($res){
		$codigo=mysql_insert_id();
		$res=insertaProductos($codigo);
	}
	return $res;
}

function actualizaContrato(){
	$res=true;
	$res=actualizaDatos('contratos');
	if($res){
		$res=insertaProductos($_POST['codigo']);
	}
	return $res;
}

function insertaProductos($contrato){
	$res=true;
	$i=0;
	$datos=arrayFormulario();
	$res = $res && consultaBD("DELETE FROM contratos_gastos WHERE codigoContrato='".$contrato."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['gasto'.$i])){
		if($datos['gasto'.$i] != 'NULL'){
			$consulta = "INSERT INTO contratos_gastos VALUES (NULL,'".$contrato."','".$datos['gasto'.$i]."',
			'".$datos['precio'.$i]."');";
		
			$res = $res && consultaBD($consulta);
		}

		$i++;
	}

	return $res;
}


function creaEstadisticasContratos(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    $anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos'){
		$anio=$_SESSION['ejercicio'];
		$anio="AND (contratos.fecha LIKE '".$anio."-%' OR facturacion.fechaEmision LIKE '".$anio."-%' OR facturacion.codigo IN (SELECT codigoFactura FROM vencimientos_facturas WHERE fecha LIKE '".$anio."-%'))";
	}
    $res=consultaBD("SELECT COUNT(contratos.codigo) AS total FROM contratos LEFT JOIN facturacion ON contratos.codigo=facturacion.codigoContrato WHERE contratos.baja='SI' ".$anio,true,true);

    return $res;
}



function formateaPreciosTrabajadores(){
	formateaPrecioBDD('salarioBruto');
	formateaPrecioBDD('costeHora');
}

function listadoAlumnos(){
	global $_CONFIG;

	$columnas=array('apellido1','apellido2','nombre','curso','contratos.fecha','contratos.importeCurso','nombre','nombre');
	$having=obtieneWhereListado("HAVING 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos'){
		$anio=$_SESSION['ejercicio'];
		$anio="AND (contratos.fecha LIKE '".$anio."-%' OR facturacion.fechaEmision LIKE '".$anio."-%' OR facturacion.codigo IN (SELECT codigoFactura FROM vencimientos_facturas WHERE fecha LIKE '".$anio."-%'))";
	}

	$having .= ' AND contratos.baja = "SI"';
	conexionBD();
	$consulta=consultaBD("SELECT contratos.codigo AS codigoContrato, nombre, apellido1, apellido2, contratos.importeCurso, contratos.fecha, cursos.curso, contratos.tipoPlazo, contratos.baja, contratos.pagoInicial, facturacion.fechaEmision, facturacion.codigo FROM contratos LEFT JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo
	LEFT JOIN cursos ON contratos.codigoCurso=cursos.codigo LEFT JOIN facturacion ON contratos.codigo=facturacion.codigoContrato $having $anio $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT contratos.codigo AS codigoContrato, nombre, apellido1, apellido2, contratos.importeCurso, contratos.fecha, cursos.curso, contratos.tipoPlazo, contratos.baja, contratos.pagoInicial, facturacion.fechaEmision, facturacion.codigo FROM contratos LEFT JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo
	LEFT JOIN cursos ON contratos.codigoCurso=cursos.codigo LEFT JOIN facturacion ON contratos.codigo=facturacion.codigoContrato $having $anio;");
	cierraBD();
	
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$gastos=consultaBD('SELECT SUM(precio) AS suma FROM contratos_gastos WHERE codigoContrato='.$datos['codigoContrato'],true,true);
		$importe = $datos['tipoPlazo'] == 'SI' ? 'Total: '.number_format((float)$datos['importeCurso']+$gastos['suma'], 2, ',', '').' €' : 'Mensual: '.number_format((float)$datos['importeCurso'], 2, ',', '').' €';
		$pagoInicial = $datos['pagoInicial']=='' ? '0 €':$datos['pagoInicial'].' €';
		$totalPendiente=consultaBD("SELECT SUM(vencimientos_facturas.importe) AS importe FROM vencimientos_facturas WHERE codigoFactura IN(SELECT codigo FROM facturacion WHERE codigoContrato='".$datos['codigo']."') AND vencimientos_facturas.cobrado='NO';",true,true);
		if(!isset($totalPendiente['importe'])){
			$totalPendiente['importe']=$datos['importeCurso']+$gastos['suma']-$datos['pagoInicial'];
		}
		$fila=array(
			$datos['apellido1'],
			$datos['apellido2'],
			$datos['nombre'],
			$datos['curso'],
			utf8_encode(formateaFechaWeb($datos['fecha'])),
			$importe,
			$pagoInicial,
			number_format((float)$totalPendiente['importe'], 2, ',', '').' €',
			botonAcciones(array('Detalles'),array('contratos/gestion.php?codigo='.$datos['codigoContrato']),array("icon-search-plus")),
        	obtieneCheckTabla($datos,'codigoContrato'),
        	"DT_RowId"=>$datos['codigoContrato']
		);
		$res['aaData'][]=$fila;
	}
	echo json_encode($res);
	//echo json_last_error_msg();
}


function gestionContratos(){
	operacionesContratos();

	abreVentanaGestion('Gestión de Contratos','index.php','span3');
	$datos=compruebaDatos('contratos');
	$factura=datosRegistro('facturacion',$datos['codigo'],'codigoContrato');

	abreColumnaCampos('span9');
	if($datos && $factura){
		campoOculto($factura['codigo'],'factura');
		campoDato('Fecha',formateaFechaWeb($datos['fecha']),'fecha');
		$alumno=datosRegistro('trabajadores_cliente',$datos['codigoAlumno']);
		campoDato('Alumno',$alumno['apellido1'].' '.$alumno['apellido2'].', '.$alumno['nombre'].' - '.$alumno['nif'],'codigoAlumno');
		$curso=datosRegistro('cursos',$datos['codigoCurso']);
		campoDato('Curso',$curso['curso'],'codigoCurso');
		echo "<span id='datosCursoOculto'></span>";
		campoDato('Forma de pago',$datos['formaPago'],'formaPago');
	} else {
		campoFecha('fecha','Fecha',$datos);
		campoSelectConsultaAjax('codigoAlumno','Alumno',"SELECT codigo, CONCAT(apellido1,' ',apellido2,', ',nombre,' - ',nif) AS texto FROM trabajadores_cliente ORDER BY nombre, apellido1, apellido2, nif;",$datos,"alumnos/gestion.php?codigo=");
		campoSelectConsultaAjax('codigoCurso','Curso',"SELECT codigo, curso AS texto FROM cursos ORDER BY curso;",$datos,"cursos/gestion.php?codigo=");
	
		echo "<span id='datosCursoOculto'></span>";
	
		campoSelect('formaPago','Forma pago',array('TPV','Ingreso en cuenta','Transferencia','Recibo domiciliado'),array('Cuenta corriente','Paypal','Transferencia','Recibo domiciliado'),$datos);
	}
	cierraColumnaCampos();

	echo "<h4 class='apartadoFormulario sinFlotar'>Otros gastos</h4>";
	abreColumnaCampos('span11');
		$i=tablaGastos($datos);
	cierraColumnaCampos();

	echo "<h4 class='apartadoFormulario sinFlotar'>Pago inicial</h4>";
	abreColumnaCampos('span11');
	if($datos && $factura){
		campoDato('Pago inicial',$datos['pagoInicial'] == ''?'0 €':$datos['pagoInicial'].' €','pagoInicial');
		campoDato('Forma de pago inicial',$datos['formaPagoInicial'],'formaPagoInicial');
		cierraColumnaCampos();
		echo "<h4 class='apartadoFormulario sinFlotar'>Baja</h4>";
		$fechaBaja='';
		if($datos){
			$fechaBaja=$datos['fechaBaja'];
		}
		abreColumnaCampos('span11');
		campoRadio('baja','Baja',$datos);
		campoFecha('fechaBaja','Fecha de baja',$datos);
		campoTexto('motivoBaja','Motivo de baja',$datos);
		$totalPendiente=consultaBD("SELECT SUM(vencimientos_facturas.importe) AS importe FROM vencimientos_facturas WHERE codigoFactura IN(SELECT codigo FROM facturacion WHERE codigoContrato='".$datos['codigo']."') AND vencimientos_facturas.cobrado='NO';",true,true);
		if(!isset($totalPendiente['importe'])){
			$totalPendiente['importe']=$datos['importeCurso']-$datos['pagoInicial'];
		}
		campoDato('Saldo pendiente',number_format((float)$totalPendiente['importe'], 2, ',', '').' €','importePendienteAlumno');

	} else {
		campoTextoSimbolo('pagoInicial','Pago inicial',"<i class='icon-euro'></i>",$datos);
		campoSelect('formaPagoInicial','Forma pago inicial',array('TPV','Ingreso en cuenta','Transferencia','Recibo domiciliado'),array('Cuenta corriente','Paypal','Transferencia','Recibo domiciliado'),$datos);
		cierraColumnaCampos();
		echo "<h4 class='apartadoFormulario sinFlotar'>Baja</h4>";
		abreColumnaCampos('span11');
		campoRadio('baja','Baja',$datos);
		campoFecha('fechaBaja','Fecha de baja',$datos);
		campoTexto('motivoBaja','Motivo de baja',$datos);
	}
	cierraColumnaCampos();
	

	cierraVentanaGestion('index.php',true);
	
	return $i;
}

function tablaGastos($datos){
	camposPreciosProductos();
	echo"	
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaGastos'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Gasto </th>
							<th> Importe € </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$consulta=consultaBD("SELECT * FROM contratos_gastos WHERE codigoContrato=".$datos['codigo'],true);				  
		while($gasto=mysql_fetch_assoc($consulta)){
		 	echo "<tr>";
		campoSelectConsulta('gasto'.$i,'','SELECT codigo,gasto AS texto FROM otros_gastos ORDER BY gasto',$gasto['codigoGasto'],'selectpicker span3 show-tick',"data-live-search='true'",'',1);
		campoTextoSimbolo('precio'.$i,'','€',$gasto['precio'],'input-mini pagination-right',1);
			echo"
				</tr>";
			$i++;
		}
	}
	echo "<tr>";
		campoSelectConsulta('gasto'.$i,'','SELECT codigo,gasto AS texto FROM otros_gastos ORDER BY gasto','','selectpicker span3 show-tick',"data-live-search='true'",'',1);
		campoTextoSimbolo('precio'.$i,'','€','','input-mini pagination-right',1);
			echo"
				</tr>";

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaGastos\");'><i class='icon-plus'></i> Añadir gasto</button> 
				</center>
		 	";
	return $i;
}

function camposPreciosProductos(){
	$consulta=consultaBD("SELECT codigo, importe FROM otros_gastos", true);
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto($datos['importe'],'codProducto'.$datos['codigo']);
	}
}

//Hago una función personalizada de comprobación de datos porque existe la opción de "duplicar", en cuyo caso el campoOculto no se crea (para hacer una inserción)
function compruebaDatosTrabajador(){
	if(isset($_REQUEST['codigo']) && !isset($_GET['duplicar'])){
		$datos=datosRegistro('trabajadores_cliente',$_REQUEST['codigo']);
		campoOculto($datos);
	}
	elseif(isset($_REQUEST['codigo']) && isset($_GET['duplicar'])){
		$datos=datosRegistro('trabajadores_cliente',$_REQUEST['codigo']);
	}
	else{
		$datos=false;
	}
	return $datos;
}

function filtroAlumnos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nombre');
	campoTexto(1,'Apellido 1');
	campoTexto(2,'Apellido 2');
	campoDNI(3,'DNI');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(5,'NISS');
	campoSelectClienteFiltradoPorUsuario(false,'Empresa',6);
	campoSelectSiNoFiltro(7,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function consultaDuplicidadTrabajador(){
	$res='OK';
	$datos=arrayFormulario();

	$nombre=limpiaCadena($datos['nombre']);
	$apellido1=limpiaCadena($datos['apellido1']);
	$apellido2=limpiaCadena($datos['apellido2']);
	$whereNombre='';
	if($nombre!='' || $apellido1!='' || $apellido2!=''){
		$whereNombre="(nombre='$nombre' AND apellido1='$apellido1' AND apellido2='$apellido2')";
	}

	$nif=$datos['nif'];
	$whereNif='';
	if($nif!=''){
		$whereNif="OR nif='$nif'";
	}

	$whereCodigo='';
	if($datos['codigo']!='NO'){
		$whereCodigo='AND codigo!='.$datos['codigo'];
	}


	if($whereNombre!='' || $whereNif!=''){
		$consulta=consultaBD("SELECT nombre, apellido1, apellido2 FROM trabajadores_cliente WHERE ($whereNombre $whereNif) $whereCodigo",true,true);
		if($consulta['nombre'] != ''){
			$res=$consulta['nombre'].' '.$consulta['apellido1'].' '.$consulta['apellido2'].' de la empresa '.$consulta['razonSocial'];
		} 
	}
	
	echo $res;
}

function campoCuentaBancaria($valor){
	$valor=compruebaValorCampo($valor,'numCuenta');
	echo '<div class="control-group">                     
	  <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
	  <div class="controls numSS">
		<div class="input-prepend input-append">
		  <input type="text" tabla="trabajadores_cliente" class="input-mini" id="iban" name="iban" maxlength="4" size="4" value="'.substr($valor, 0, 4).'">
		  <span class="add-on">/</span>
		  <input type="text" class="input-medium" tabla="trabajadores_cliente" id="numCuenta" name="numCuenta" maxlength="20" size="20" value="'.substr($valor, 4, 24).'">
		</div>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->';
}

function cargarDatosCurso(){
	$datos=arrayFormulario();
	if(isset($datos['codigo']) && isset($datos['factura'])){
		$datosAlumno=datosRegistro('contratos',$datos['codigo']);
		$importe='Importe total';
		if($datosAlumno['tipoPlazo'] == 'NO'){
			$importe='Importe mensual';
		} 
		campoDato($importe,$datosAlumno['importeCurso'].' €','importeCurso');
		campoDato('Precio definido',$datosAlumno['tipoPlazo'],'tipoPlazo');
		if($datosAlumno['tipoPlazo'] == 'SI'){
			campoDato('Plazos',$datosAlumno['plazosCurso'],'plazosCurso');
		}
	}elseif(isset($datos['curso']) && $datos['curso']!='NULL'){
		if(!isset($datos['codigo']) || $datos['codigo'] == 'false'){
			$datosCurso=datosRegistro('cursos',$datos['curso']);
			$campoPrecio='precio';
			$campoPlazos='plazos';
		} else {
			$datosCurso=datosRegistro('contratos',$datos['codigo']);
			$campoPrecio='importeCurso';
			$campoPlazos='plazosCurso';
		}
		$importe='Importe total';
		if($datosCurso['tipoPlazo'] == 'NO'){
			$importe='Importe mensual';
		} 
		campoTextoSimbolo('importeCurso',$importe,"<i class='icon-euro'></i>",$datosCurso[$campoPrecio]);
		campoRadio('tipoPlazo','Precio definido',$datosCurso);
		if($datosCurso['tipoPlazo'] == 'SI'){
			campoTexto('plazosCurso','Plazos',$datosCurso[$campoPlazos],'input-small');
		} else {
			campoOculto($datosCurso[$campoPlazos],'plazosCurso');
		} 
	}else{
		campoTextoSimbolo('importeCurso','Importe mensual',"<i class='icon-euro'></i>");
		campoRadio('tipoPlazo','Precio definido');
		echo '<div class="divTipoPlazo">';
			campoTexto('plazosCurso','Plazos','','input-small');
		echo '</div>';
	}
}

function nuevaMetodologia(){
	$alumnos=consultaBD('SELECT * FROM trabajadores_cliente',true);
	while($alumno=mysql_fetch_assoc($alumnos)){
		$curso=datosRegistro('cursos',$alumno['curso']);
		echo "INSERT INTO contratos VALUES(NULL,".$alumno['codigo'].",".$alumno['curso'].",'".$alumno['fechaAlta']."','".$alumno['importeCurso']."','".$alumno['pagoInicial']."','".$curso['tipoPlazo']."','".$alumno['plazosCurso']."','".$alumno['formaPago']."','".$alumno['formaPagoInicial']."','".$alumno['fechaBaja']."','".$alumno['motivoBaja']."');<br/>";
	}
}

//Fin parte de trabajadores