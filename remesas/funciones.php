<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de tareas de remesas

function operacionesRemesas(){
	$res=true;
	
	if(isset($_POST['codigo'])){
		$res=actualizaDatos('remesas');
		$res=$res && insertaFacturasRemesas($_POST['codigo']);
	}
	elseif(isset($_POST['fecha'])){
		$res=insertaDatos('remesas');
		$codigoRemesa=$res;
		$res=$res && insertaFacturasRemesas($codigoRemesa);
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('remesas');
	}

	mensajeResultado('fecha',$res,'remesa');
	mensajeResultado('elimina',$res,'remesa', true);
}

function gestionRemesa(){
	operacionesRemesas();

	abreVentanaGestion('Gestión de Remesas','index.php','span5','icon-edit');
	$datos=compruebaDatos('remesas');
	
	if(!$datos){
		$fechaUno=formateaFechaBD($_POST['fechaUno']);
		$fechaDos=formateaFechaBD($_POST['fechaDos']);
		campoFecha('fecha','Fecha pasar Remesa');
		campoOculto($_POST['tipoSepa'],'tipoSepa');
		campoOculto('NO','enviadaBanco');
		
		$consulta=consultaBD("SELECT DISTINCT codigoFactura, importe FROM vencimientos_facturas
		 INNER JOIN facturacion ON facturacion.codigo=vencimientos_facturas.codigoFactura WHERE fecha>='$fechaUno' AND fecha<='$fechaDos' AND cobrado='NO' AND formaPago='RECIBO DOMICILIADO';",true);

		$estadisticas = 0;
		while($recibo=mysql_fetch_assoc($consulta)){
			$estadisticas = $estadisticas + $recibo['importe'];
		}
		abreColumnaCampos('span11');
		echo '<center>
			<h1 class="titulosFacturacionAux">Total facturado</h1>
			<div class="stat"> <h1 class="titulosFacturacion"> <span class="value">'.number_format((float)$estadisticas, 2, ',', '').'</span> €</h1></div>
					  
		</center>';
		
		abreTablaRemesas();
		imprimeFacturasDescarga($fechaUno,$fechaDos,$_POST['tipoSepa']);
		cierraTablaRemesas();
		
		cierraColumnaCampos();
			
	}else{
		abreColumnaCampos('span11');
		if($datos['enviadaBanco'] == 'SI'){
			campoDato('Fecha pasara Remesa',formateaFechaWeb($datos['fecha']));
		} else {
			campoFecha('fecha','Fecha pasar Remesa',$datos);
		}

		campoDato('Tipo de remesa',$datos['tipoSepa']);
		abreTablaRemesas($datos['enviadaBanco']);
		imprimeFacturasDetalle($datos['codigo']);
		cierraTablaRemesas();
		if($datos['enviadaBanco'] == 'NO'){
			campoRadio('enviadaBanco','Enviada al banco',$datos);
		}
		cierraColumnaCampos();
	}
	
	if(isset($datos) && $datos['enviadaBanco'] == 'SI'){
		cierraVentanaGestion('index.php',true,false);
	} else {
		cierraVentanaGestion('index.php',true);
	}
}


//Parte de Remesas
function imprimeRemesas(){
	conexionBD();

	$anio='';
	if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos'){
		$anio=$_SESSION['ejercicio'];
		$anio="WHERE fecha LIKE '".$anio."-%'";
	}

	$consulta=consultaBD("SELECT codigo, fecha, tipoSepa, enviadaBanco FROM remesas ".$anio." ORDER BY fecha DESC;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		$iconoC=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<a href="?codigoEnviada='.$datos['codigo'].'"><i class="icon-remove iconoFactura icon-danger"></i></a>');
	
		$num=consultaBD("SELECT COUNT(vencimientos_facturas.codigo) AS total, SUM(vencimientos_facturas.importe) AS cantidad FROM vencimientos_facturas WHERE codigo IN(
		SELECT codigoVencimiento FROM vencimientos_remesas WHERE codigoRemesa='".$datos['codigo']."');",false,true);
		$total=$num['cantidad'];

		echo "
		<tr>
        	<td> ".$datos['codigo']." </td>
        	<td> ".formateaFechaWeb($datos['fecha'])." </td>
			<td> ".$datos['tipoSepa']." </td>
			<td> ".$num['total']." </td>
			<td> ".number_format((float) $total, 2, ',', '')." € </td>
			<td class='centro'> ".$iconoC[$datos['enviadaBanco']]." </td>
			<td class='centro'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				<a href='generaRemesa.php?codigo=".$datos['codigo']."' class='btn btn-success noAjax'><i class='icon-download'></i> Generar XML</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function formateaNumero($dato,$bd=false){
	if($bd){
		$parts=explode(',', $dato);

		if(isset($parts[1])){
			$dato = $parts[0].".".$parts[1];
		}

		$res = round($dato,2);
	}
	else{
		$parts=explode('.', $dato);
		if(isset($parts[1])){
			$res = $parts[0].",".$parts[1];
		}else{
			$res = $parts[0];
		}
	}
	return $res;
}

function insertaFacturasRemesas($codigoRemesa){
	$res=true;
	$i=0;
	conexionBD();
	$consulta=consultaBD("DELETE FROM vencimientos_remesas WHERE codigoRemesa='$codigoRemesa';");
    while(isset($_POST['codigoLista'][$i])){
		$consulta=consultaBD("INSERT INTO vencimientos_remesas VALUES('".$_POST['codigoLista'][$i]."','$codigoRemesa');",true);
		if(!$consulta){
			$res=false;
		} else {
			$consulta=consultaBD('UPDATE vencimientos_facturas SET cobrado="SI" WHERE codigo='.$_POST['codigoLista'][$i]);
		}
		$i++;
	}
	return $res;
}

function imprimeFacturasDetalle($codigoRemesa,$enviada='NO'){
	//$codigoS=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	conexionBD();
	
	$consulta=consultaBD("SELECT vencimientos_facturas.codigo, vencimientos_facturas.importe, vencimientos_facturas.fecha, facturacion.referencia, CONCAT(nombre,' ',apellido1,' ',apellido2) AS alumno, contratos.fecha AS fechaEmision, trabajadores_cliente.numCuenta, trabajadores_cliente.cp, contratos.importeCurso, contratos.pagoInicial
	FROM vencimientos_facturas INNER JOIN facturacion ON vencimientos_facturas.codigoFactura=facturacion.codigo
	INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo LEFT JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo LEFT JOIN vencimientos_remesas ON vencimientos_facturas.codigo=vencimientos_remesas.codigoVencimiento
	WHERE vencimientos_remesas.codigoRemesa='$codigoRemesa' ORDER BY alumno;");

	/*$consulta=consultaBD("SELECT facturacion.codigo, facturacion.referencia, coste, clientes.razonSocial, fechaEmision, fechaVencimiento, clientes.cp, clientes.numCuenta 
	FROM facturacion LEFT JOIN trabajadores_cliente ON facturacion.codigoAlumno=trabajadores_cliente.codigo
	LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo 
	INNER JOIN vencimientos_remesas ON vencimientos_remesas.codigoFactura=facturacion.codigo
	WHERE vencimientos_remesas.codigoRemesa='$codigoRemesa' ORDER BY fechaEmision DESC;");*/
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		$check="<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' checked='checked'>
        	</td>";
		if($enviada == 'SI'){
			$check='';
		}
		$clase='';
		$mensaje='';
		$iconoAviso='';
		$titulo=$datos['alumno'];
		if(strlen($datos['numCuenta'])<24){
			$clase='mensajeAviso';
			$iconoAviso=' <span class="label label-danger"><i class="icon-exclamation-circle"></i></span>';
			$mensaje='Número de cuenta erróneo.';
		}
		$datos['importe']=str_replace(',','.',$datos['importe']);
		$total=$datos['importe'];
		$total=number_format($total, 2, ',', '');
		echo "
		<tr>
        	<td> ".$datos['referencia']." </td>
        	<td class='$clase' mensaje='$mensaje' titulo='$titulo'> ".$datos['alumno']." $iconoAviso</td>
			<td> ".$datos['numCuenta']." </td>
			<td> ".formateaFechaWeb($datos['fechaEmision'])." </td>
			<td> ".formateaFechaWeb($datos['fecha'])." </td>
        	<td> ".$total." </td>
			".$check."
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function sanear_string($string){

    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("\\", "¨", "º", "-", "~",
             "#", "@", "|", "!", "\"",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "`", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             "."),
        '',
        $string
    );
	 return $string;
}

function enviaRemesa($codigo){
	$res=true;
	$consulta=consultaBD("UPDATE remesas SET enviadaBanco='SI' WHERE codigo='$codigo';",true);
	if(!$consulta){
		$res=false;
	}
	return $res;
}

function mensajeResultadoGet($indice,$res){
	if(isset($_GET[$indice])){
		if($res){
		  mensajeOk("Datos guardados correctamente."); 
		}
		else{
		  mensajeError("se ha producido un error al procesar datos. Pruebe de nuevo más tarde."); 
		}
	}
}

function abreTablaRemesas($enviada='NO'){
	$check='<th><input type="checkbox" id="todo" checked="checked"></th>';
	if($enviada == 'SI'){
		$check='';
	}
	echo '<div class="widget widget-table action-table">
		<div class="widget-header"> <i class="icon-th-list"></i>
		  <h3>Factura/s</h3>
		</div>
		<!-- /widget-header -->
		<div class="widget-content">
		  <table class="table table-striped table-bordered datatable">
			<thead>
			  <tr>
				<th> Referencia </th>
				<th> Alumno </th>
				<th> Nº Cuenta </th>
				<th> Fecha de emisión </th>
				<th> Fecha de vencimiento </th>
				<th> Importe </th>
				'.$check.'
			  </tr>
			</thead>
			<tbody>';
}

function cierraTablaRemesas(){
	echo "</tbody>
		  </table>
		</div>
		<!-- /widget-content --> 
	  </div>";
}

function imprimeFacturasDescarga($fechaUno, $fechaDos, $tipoRemesa){
	//$codigoS=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	conexionBD();
	
	$consulta=consultaBD("SELECT vencimientos_facturas.codigo, vencimientos_facturas.importe, vencimientos_facturas.fecha, facturacion.referencia, CONCAT(nombre,' ',apellido1,' ',apellido2) AS alumno, contratos.fecha AS fechaEmision, trabajadores_cliente.numCuenta, trabajadores_cliente.cp, contratos.importeCurso, contratos.pagoInicial, contratos.baja AS baja, contratos.fechaBaja
	FROM vencimientos_facturas INNER JOIN facturacion ON vencimientos_facturas.codigoFactura=facturacion.codigo
	INNER JOIN contratos ON facturacion.codigoContrato=contratos.codigo LEFT JOIN trabajadores_cliente ON contratos.codigoAlumno=trabajadores_cliente.codigo LEFT JOIN vencimientos_remesas ON vencimientos_facturas.codigo=vencimientos_remesas.codigoVencimiento
	$where AND vencimientos_facturas.fecha>='$fechaUno' AND vencimientos_facturas.fecha<='$fechaDos' AND facturacion.formaPago='RECIBO DOMICILIADO' AND vencimientos_facturas.cobrado='NO' ORDER BY alumno;");

	
	while($datos=mysql_fetch_assoc($consulta)){
		$introducir = true;
		/*if($datos['baja'] == 'SI'){
			$introducir=false;
			$fechaRecibo=explode('-', $datos['fecha']);
			$fechaBaja=explode('-', $datos['fechaBaja']);
			if($fechaBaja[1]>=$fechaRecibo[1]){
				if($fechaBaja[2] > 15){
					$introducir=true;
				}
			} 
		}*/
		if($introducir){
			$clase='';
			$mensaje='';
			$iconoAviso='';
			$titulo=$datos['alumno'];
			if(strlen($datos['numCuenta'])<24){
				$clase='mensajeAviso';
				$iconoAviso=' <span class="label label-danger"><i class="icon-exclamation-circle"></i></span>';
				$mensaje='Número de cuenta erróneo.';
			}
			$datos['importe']=str_replace(',','.',$datos['importe']);
			$total=$datos['importe'];
			$total=number_format($total, 2, ',', '');
			echo "
			<tr>
        		<td> ".$datos['referencia']." </td>
        		<td class='$clase' mensaje='$mensaje' titulo='$titulo'> ".$datos['alumno']." $iconoAviso</td>
				<td> ".$datos['numCuenta']." </td>
				<td> ".formateaFechaWeb($datos['fechaEmision'])." </td>
				<td> ".formateaFechaWeb($datos['fecha'])." </td>
        		<td> ".$total." </td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' checked='checked'>
        		</td>
    		</tr>";
    	}
	}
	cierraBD();
}
//Fin parte Remesas
