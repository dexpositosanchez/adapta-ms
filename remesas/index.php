<?php
  $seccionActiva=2;
  include_once('../cabecera.php');
  
  
  operacionesRemesas(); 
  if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos'){
    $anio=$_SESSION['ejercicio'];
    $anio="fecha LIKE '".$anio."-%'";
    $estadisticas=estadisticasGenericas('remesas',false,$anio);
  } else {
    $estadisticas=estadisticasGenericas('remesas');
  }
  
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de remesas</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-paste"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Remesas registradas</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Remesas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="seleccionaFechas.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Remesar</span> </a>
                <a href="#" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Remesas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Código </th>
                    <th> Fecha </th>
					<th> Tipo Remesa </th>
					<th> Nº Facturas </th>
					<th> Total </th>
					<th> Enviada </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeRemesas();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('../pie.php'); ?>

<script src="../../api/js/jquery.dataTables.js"></script>
<script src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
