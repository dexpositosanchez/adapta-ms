<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de infraestructuras

function operacionesInfraestructuras(){
	$res=true;

	if(isset($_POST['codigo'])){
    	$res=actualizaInfraestructura();
  	}
  	elseif(isset($_POST['fechaCompra'])){
    	$res=insertaInfraestructura();
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('infraestructuras');
	}

	mensajeResultado('fechaCompra',$res,'Infraestructura');
    mensajeResultado('elimina',$res,'Infraestructura', true);
}

function insertaInfraestructura(){
	$res = false;
	$res=insertaDatos('infraestructuras');
	return $res;
}

function actualizaInfraestructura(){
	$res = false;
	$res=actualizaDatos('infraestructuras');
	return $res;
}

function imprimeInfraestructuras(){
	global $_CONFIG;

	$consulta=consultaBD("SELECT codigo, identificacion, tipo, fechaProximaRevision FROM infraestructuras ORDER BY fechaCompra DESC;",true);
	$tipos=array('VEHICULO'=>'Vehículo','MAQUINARIA'=>'Maquinaria','ORDENADOR'=>'Ordenador', 'OTROS'=>'Otros (ver detalles)');

	while($datos=mysql_fetch_assoc($consulta)){
		$fecha=formateaFechaWeb($datos['fechaProximaRevision']);
		echo "
		<tr>
			<td> ".$datos['identificacion']."</td>
        	<td> ".$tipos[$datos['tipo']]."</td>
        	<td> $fecha";
		if(compruebaFechaRevision($fecha)){
			echo ' <span class="label label-danger"><i class="icon-exclamation"></i></span>';
		}
		echo "
			</td>
        	<td class='td-actions'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-zoom-in'></i> Ver datos</i></a>
        	</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function gestionInfraestructuras(){
	operacionesInfraestructuras();

	abreVentanaGestion('Gestión de Infraestructura','index.php');
	$datos=compruebaDatos('infraestructuras');

	$displayTipo = "style='display:none;'";
	$displayRevision = "style='display:none;'";
	$displayProveedor = "style='display:none;'";
	if($datos){
		if($datos['tipo'] == 'OTROS'){
			$displayTipo = "style='display:block;'";
		}
		if($datos['tipoRevision'] == 'OTRO'){
			$displayRevision = "style='display:block;'";
		}
		if($datos['subcontratada'] == 'SI'){
			$displayProveedor = "style='display:block;'";
		}
	}

	campoFecha('fechaCompra','Fecha de compra',$datos);
	campoTexto('identificacion','Identificación',$datos);
	
	campoSelect('tipo','Tipo',array('Vehículo','Maquinaria','Ordenador','Otros'),array('VEHICULO','MAQUINARIA','ORDENADOR','OTROS'),$datos);
	echo "<div class='definicionTipo' ".$displayTipo.">";
		campoTexto('definicionTipo','Defina el tipo',$datos ? $datos: '');
	echo "</div>";

	campoSelect('tipoRevision','Tipo de revisión',array('Visual','Prueba','Otro'),array('VISUAL','PRUEBA','OTRO'),$datos);
	echo "<div class='definicionRevision' ".$displayRevision.">";
		campoTexto('definicionRevision','Defina la revision',$datos ? $datos: '');
	echo "</div>";

	echo "<div class='subcontratada'>";
		campoRadio('subcontratada','¿Subcontratada?',$datos);
	echo "</div>";
	echo "<div class='proveedor' ".$displayProveedor.">";
		campoTexto('proveedor','Proveedor',$datos);
	echo "</div>";

	campoFecha('fechaUltimaRevision','Fecha última revisión',$datos);
	campoFecha('fechaProximaRevision','Fecha próxima revisión',$datos);
	campoRadio('seguro','¿Seguro?',$datos);
	campoFecha('fechaVencimiento','Fecha de vencimiento',$datos ? $datos: '');
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');

	cierraVentanaGestion('index.php');
}

function generaDatosGraficoInfraestructuras(){
	$datos=array();

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM infraestructuras WHERE fechaProximaRevision >= CURDATE();");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['enPlazo']=$consulta['codigo'];


	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM infraestructuras WHERE fechaProximaRevision<CURDATE();");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['fueraPlazo']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function compruebaFechaRevision($fechaRevision){
	$res=false;

	$fecha=explode('/',$fechaRevision);

	if($fecha[2]<date('Y')){
		$res=true;
	}
	elseif($fecha[2]==date('Y') && $fecha[1]<date('m')){
		$res=true;
	}
	elseif($fecha[2]==date('Y') && $fecha[1]==date('m') && $fecha[0]<date('d')){
		$res=true;
	}

	return $res;
}

//Fin parte de infraestructuras

