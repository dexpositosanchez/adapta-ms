<?php
  $seccionActiva=15;
  include_once("../cabecera.php");
  $datos=datosPorcentajes();
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Cuestionario sobre satisfacción de los clientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" name="formulario">
                  <fieldset>
                    <br>
                
                    <center>    
                      <div class="widget widget-nopad mitadAncho">
                        <!-- /widget-header -->
                        <div class="widget-content">
                          <div class="widget big-stats-container sinMargenAb">
                            <div class="widget-content">
                              <h6 class="bigstats">Grado de satisfacción global:</h6>
                              <div id="big_stats" class="cf">
                                <div class="stat"> <i class="icon-check"></i> <span class="value"></span> <br><span class="valoracion"></span></div>
                                <!-- .stat -->
                              </div>
                            </div>
                            <!-- /widget-content --> 
                            
                          </div>
                        </div>
                      </div>
                    </center>
                    <br><br>



                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th> Pregunta </th>
                          <th> Valoración </th>
						              <th> Porcentaje </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          creaPreguntaPorcentajesClientes(1,"Asistencia Previa a la contratación de nuestra empresa (Atención, solicitud de información inicial, facilidad de contacto):",$datos);
                          creaPreguntaPorcentajesClientes(2,"Documentación Recibida ( Propuestas, Ofertas, Facturas Proformas) y explicaciones /atenciones recibidas sobre la misma:",$datos);
                          creaPreguntaPorcentajesClientes(3,"Atención Telefónica / Administrativa:",$datos);
                          creaPreguntaPorcentajesClientes(4,"Cumplimientos de plazos:",$datos);
                          creaPreguntaPorcentajesClientes(5,"Asistencia ante incidencias:",$datos);
                          creaPreguntaPorcentajesClientes(6,"Valoración de los Servicios Prestados / Productos Adquiridos:",$datos);
						  creaPreguntaPorcentajesClientes(7,"En general, su satisfacción con los nuestra organización es:",$datos);
                        ?>
                      </tbody>
                    </table>
                    

                      
                     <br />
                    
                      
                    <div class="form-actions">
                      <a href="<?php echo $_CONFIG['raiz']; ?>satisfaccion/" class="btn btn-default"><i class="icon-arrow-left"></i> Volver</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('../pie.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
  
  var total=0, sum=0;
  $('.valorGlobal').each(function(){
    sum+=parseFloat($(this).text());
    total++
  });
  sum=(sum)/total;
  $('.value').text(sum.toFixed(1)+'%');

  if(sum<60){
   $('.valoracion').text('Valoración: Muy Baja');
  }
  else if(sum<80){
    $('.valoracion').text('Valoración: Baja');
  }
  else{
    $('.valoracion').text('Valoración: Alta');
  }

})
</script>

