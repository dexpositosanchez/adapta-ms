<?php
  $seccionActiva=12;
  include_once('../cabecera.php');
  
  $res=false;
  if(isset($_POST['codigo'])){
    //$res=actualizaDatos('software');
    $res=actualizaSoftware();
  }
  elseif(isset($_POST['tipoTarea'])){
    $res=insertaDatos('software');
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('software');
  }

  $estadisticas=creaEstadisticasSoftware();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Proyectos:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-laptop"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Total registradas</div>
                     <div class="stat"> <i class="icon-flag"></i> <span class="value"><?php echo $estadisticas['pendientes']?></span> <br>Pendientes</div>
                     <div class="stat"> <i class="icon-clock-o"></i> <span class="value"><?php echo $estadisticas['desarrollo']?></span> <br>En desarrollo</div>
                     <div class="stat"> <i class="icon-check"></i> <span class="value"><?php echo $estadisticas['finalizadas']?></span> <br>Finalizadas</div>                      
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Tareas de Proyectos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva tarea</span> </a>
                <a href="#" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
                <br />
                <a href="index.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
                <a href="tododoindone.php" class="shortcut"><i class="shortcut-icon icon-columns"></i><span class="shortcut-label">ToDo-Doing-Done</span> </a>
                <a href="vistaTiempos.php" class="shortcut"><i class="shortcut-icon icon-clock-o"></i><span class="shortcut-label">Vista de tiempos</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        
        <?php
          mensajeResultado('tipoTarea',$res,'tarea');
          mensajeResultado('elimina',$res,'tarea', true);
        ?>
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Tareas de Proyectos finalizadas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Cliente </th>
					<th> Presupuesto </th>
                    <th> Tipo de Tarea </th>
                    <th> Fecha de Solicitud </th>
                    <th> Fecha de Prevista de resolución </th>
                    <th> Fecha de Finalización </th>
                    <th> Comentarios técnico </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeTareasSoftware(true);
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('../pie.php'); ?>

<script src="../../api/js/jquery.dataTables.js"></script>
<script src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>