function insertaFilaParticipante(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla+" > tbody > tr:last").clone();

    $tr.find('.tablaPadre > tbody > tr:not(:first,:eq(1))').remove();

    //Obtengo el atributo name para los inputs y selects
    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        return obtieneIDInsercion(this.id);
    }).attr("id", function(){//Hago lo mismo con los IDs
        return obtieneIDInsercion(this.id);
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find(".btn-success").attr("onclick", function(){//Para los botones de añadir alumno
        var evento=$(this).attr('onclick');
        var indice=evento.replace(/\D/g,'');
        
        evento=evento.slice(0,27);
        indice++;

        return evento+indice+'")';
    });

    $tr.find(".tabla-interna:not(.subtabla)").attr("id", function(){//Para el identificador de la subtabla padre
        var id=this.id;
        var indice=id.replace(/\D/g,'');
        
        id=id.slice(0,12);
        indice++;

        return id+indice;
    });


    $tr.find(".btn-danger").attr("onclick", function(){//Para los botones de eliminar alumno
        var evento=$(this).attr('onclick');
        var indice=evento.replace(/\D*\d\-/g,'');
        indice=indice.replace('")','');
        
        evento=evento.slice(0,15);
        indice++;

        return evento+indice+'-0")';
    });


    $tr.find('.filaSubtabla').attr('id',function(){
        var id=this.id;
        id=id.split('-');

        return ++id[0] + '-' + id[1];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker

    //Añado la nueva fila a la tabla
    $('#'+tabla+" > tbody > tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
}

function obtieneIDInsercion(id){
    var res='';

    if(id.indexOf('-')>0){
        var parts=id.match(/(\D+)(\d*)-(\d*)$/);
        res=parts[1] + ++parts[2]+'-0';
    }
    else{
        var parts=id.match(/(\D+)(\d*)$/);
        res=parts[1] + ++parts[2];
    }

    return res;
}


function eliminaFilaParticipante(tabla){
  if($('#'+tabla+" > tbody > tr").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' > tbody > tr').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' > tbody > tr').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' > tbody > tr').eq(i).find("input:not([type=checkbox],.input-block-level),select,textarea").attr("name", function(){
                return obtieneIDEliminacion(this.id);
            }).attr("id", function(){
                return obtieneIDEliminacion(this.id);
            });

            
            $('#'+tabla+' > tbody > tr').eq(i).find("input[type=checkbox]").attr("value", function(){
                return i;
            });

            $('#'+tabla+' > tbody > tr').eq(i).find('.filaSubtabla').attr('id',function(){
                var id=this.id;
                id=id.split('-');

                return i + '-' + id[1];
            });
        }
        //Fin renumeración

        if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la refresco para que los selectpicker coincidan con los campos
            $('#'+tabla).find(".selectpicker").selectpicker('refresh');
        }
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}

function obtieneIDEliminacion(id){
    var res='';

    if(id.indexOf('-')>0){
        var parts=id.match(/(\D+)(\d*)-(\d*)$/);
        res=parts[1] + i + '-' + parts[3];
    }
    else{
        var parts=id.match(/(\D+)(\d*)$/);
        res=parts[1] + i;
    }

    return res;
}


//Parte de alumnos en sub-tabla
function insertaFilaAlumno(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla+" > tbody > tr:last").clone();

    //Obtengo el atributo name para los inputs y selects
    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        var parts=this.id.match(/(\D+)(\d*)-(\d*)$/);
        return parts[1] + parts[2]+'-'+ ++parts[3];
    }).attr("id", function(){//Hago lo mismo con los IDs
        var parts=this.id.match(/(\D+)(\d*)-(\d*)$/);
        return parts[1] + parts[2]+'-'+ ++parts[3];
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("onclick", function(){//Para los botones de eliminar alumno
        var evento=$(this).attr('onclick');
        var indice=evento.replace(/\D*\d\-/g,'');
        indice=indice.replace('")','');
        
        evento=evento.slice(0,17);
        indice++;

        return evento+indice+'")';
    });

    $tr.attr('id',function(){
        var id=this.id;
        id=id.split('-');

        return id[0] + '-' + ++id[1];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker

    //Añado la nueva fila a la tabla
    $('#'+tabla+" > tbody > tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
}


function eliminaAlumno(id){
    var contenedor=$('#'+id).parent();
    var numSubTablas=contenedor.find('.subtabla').length;

    if(numSubTablas>1){//Si la tabla tiene más de 1 subtabla...
        $('#'+id).remove();

        //Renumeración de los índices de la tabla
        for(i=1,j=0;i<numSubTablas;i++,j++){//i empieza en 1 porque la 0 es la de codigoCliente
           contenedor.find('tr:eq('+i+')').find("input:not(.input-block-level),select").attr("name", function(){
                var parts=this.id.match(/(\D+)(\d*)-(\d*)$/);
                return parts[1] + parts[2]+'-'+ j;
            }).attr("id", function(){
                var parts=this.id.match(/(\D+)(\d*)-(\d*)$/);
                return parts[1] + parts[2]+'-'+ j;
            });

            
            contenedor.find('tr:eq('+i+')').find("button:not(.selectpicker)").attr("onclick", function(){//Para los botones de eliminar alumno
                var evento=$(this).attr('onclick');
                evento=evento.slice(0,17);

                return evento+j+'")';
            });

            contenedor.find('tr:eq('+i+')').attr('id',function(){
                var id=this.id;
                id=id.split('-');

                return id[0] + '-' + j;
            });
            
        }
        //Fin renumeración

        if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la refresco para que los selectpicker coincidan con los campos
            contenedor.find(".selectpicker").selectpicker('refresh');
        }
    }
    else{
        alert('La tabla debe de contar con al menos 1 alumno.');
    }
}