<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común


  	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('presupuestos',$_GET['codigo']);
	$datosCliente=datosRegistro('clientes',$datos['empresa']);
	$datosContacto=datosRegistro('contactos',$datosCliente['codigoContacto']);
	$datosProductos=consultaBD("SELECT productos.* FROM productos_presupuestos INNER JOIN productos ON productos.codigo=productos_presupuestos.codigoProducto WHERE codigoPresupuesto='".$_GET['codigo']."';",true);

	//Carga de la librería PHPWord
	require_once '../phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('../ficheros/plantillaPresupuesto.docx');

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	
	$contacto=$datosContacto['nombre']." ".$datosContacto['apellidos'];
	$document->setValue("contacto",utf8_decode($contacto));
	$document->setValue("nif",utf8_decode($datosContacto['nif']));
	$document->setValue("cliente",utf8_decode($datosCliente['empresa']));
	$document->setValue("direccion",utf8_decode($datosCliente['domicilio']));
	$document->setValue("cif",utf8_decode($datosCliente['cif']));
	$document->setValue("total",utf8_decode($datos['total']));

	$datosProductosPresupuesto=mysql_fetch_assoc($datosProductos);
	$productos='';
	while(isset($datosProductosPresupuesto['codigo'])){
		$productos.=$datosProductosPresupuesto['nombre'].", ";
		$datosProductosPresupuesto=mysql_fetch_assoc($datosProductos);
	}
	$productos= substr_replace($productos, '', strlen($productos)-2, strlen($productos));//Para quitar última coma
	$document->setValue("producto",utf8_decode($productos));
	$document->setValue("dia",utf8_decode(date('d')));
	$document->setValue("mes",utf8_decode(devuelveMes(date('m'))));
	$document->setValue("anio",utf8_decode(date('Y')));


	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$document->save('../ficheros/Presupuesto-'.$datosCliente['empresa'].'.docx');


	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Presupuesto-".$datosCliente['empresa'].".docx");
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('../ficheros/Presupuesto-'.$datosCliente['empresa'].'.docx');


?>