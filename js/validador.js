//Clientes
$('.cuentaSS').attr('maxlength',12);
$('#cif').attr('maxlength',9);

//Series de facturas
$('#serie').attr('maxlength',2);

//Trabajadores
$('#niss').attr('maxlength',12);

//Funciones generales de validación

$('input.obligatorio,select.obligatorio').each(function(){//Para poner el asterisco a los campos obligatorios
	if($(this).parent().hasClass('input-prepend')){
		var label=$(this).parent().parent().prev();//Doble salto para los campoTextoSimbolo
	}
	else{
		var label=$(this).parent().prev();	
	}
	
	var texto=label.text();
	label.html('<span class="asterisco">*</span> '+texto);
});

$(':submit').click(function(e){
	e.preventDefault();
	
	validaCamposObligatorios();
});

//El parámetro selectorFormulario se utiliza para los casos en que se use el script en páginas con ventanas modales, que tienen también #formcontrols
function validaCamposObligatorios(selectorFormulario){
	var res=0;
	
	if(selectorFormulario==undefined){
		selectorFormulario='#formcontrols form';
	}

	//Le digo que no seleccione los .bootstrap-select porque corresponden al selectpicker en sí (que hereda la clase del select original), y hay que trabajar con los select directamente.
	$(selectorFormulario).find('.obligatorio:not(.bootstrap-select)').each(function(){
		res+=compruebaValor($(this));
	});

	if(res==0){
		$(selectorFormulario).submit();
	}
	else{
		alert('Por favor, rellene todos los campos marcados en rojo.');
	}

	return res;//Se devuelve res para usarlo en otras funciones personalizadas de validación (como en acciones-formativas/gestion.php)
}

function compruebaValor(campo){
	var res=0;
	
	if(campo.val()=='' || campo.val()=='NULL'){
		res=1;
	}

	formateaCampoObligatorio(campo,res);

	return res;
}

function formateaCampoObligatorio(campo,res){
	if(campo.hasClass('selectpicker') && res==0){
		campo.selectpicker('setStyle', 'btn-danger','remove');
	}
	else if(campo.hasClass('selectpicker') && res==1){
		campo.selectpicker('setStyle', 'btn-danger');
	}
	else if(res==0){
		campo.css('border','1px solid #cccccc');
	}
	else{
		campo.css('border','1px solid red');
	}
}


$('[name*=telefono],[name*=fax],[name*=movil]').each(function(){//Validador para teléfono/fax y móvil
	asignaValidacion($(this),/^[69]\d{8}$/,'Número de teléfono no válido');
});

$('[name*=email]').each(function(){//Validador para email
	asignaValidacion($(this),/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,'Dirección de email no válida');
});

$('[class*=cuentaSS]').each(function(){//Validador para cuenta de seguridad social
	asignaValidacion($(this),/^\d{9,11}$/,'Cuenta de Seguridad Social no válida');
});

$('[name*=cp]').each(function(){//Validador para código postal
	asignaValidacion($(this),/^(01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|AD)\d{3}$/,'Código Postal no válido');
});

if($('#codigo').val()==undefined){
	//$('.hasDatepicker').val('');//Le quito el valor a los campos fecha, que por defecto es la fecha actual
	$('input[name*=hora]').val('');//Lo mismo para las horas
}


function asignaValidacion(campo,expresion,mensaje){//Función general de validación. Recibe el campo, la expresión regular a utilizar y el mensaje en caso de que no se cumpla.
	campo.blur(function(){
		if(campo.val().trim()!=''){
			var validador=new RegExp(expresion);
					
			if(!validador.test(campo.val())){
				 alert(mensaje);
				 campo.val('');
			}
		}
	});
}



//Funciones para detección de duplicidades
$('[name*=email],[name*=telefono],[name*=email],[name*=nif],[name*=movil],[name=usuario]').each(function(){
	$(this).blur(function(){
		compruebaDuplicidadCampo($(this).attr('name'),$(this).val(),$(this).attr('tabla'),$(this).attr('campoCodigo'));
	});
});

function compruebaDuplicidadCampo(nombreCampo,valor,tabla,campoCodigo){
	if(valor.trim()!=''){
		nombreCampo=nombreCampo.replace(/\d/g,'');//Para quitar el índice concatenado al nombre, en caso de que el campo esté en una tabla dinámica

		var valorCodigo='NO';
		if($('#codigo').val()!=undefined){//Si se está en detalles...
			valorCodigo=$('#codigo').val();
		}

		var consulta=$.post('../listadoAjax.php?funcion=compruebaDuplicidadCampo();',{
			'tabla':tabla,
			'campo':nombreCampo,
			'valor':valor,
			'campoCodigo':campoCodigo,
			'valorCodigo':valorCodigo
		});

		consulta.done(function(respuesta){
			if(respuesta=='error'){
				alert('Atención: el dato introducido ya existe en otro registro');
			}
		});
	}
}

//La siguiente función coloca un asterisco amarillo a los campos que el sistema a determinado como modificados por un comercial
/*function oyenteCambios(){
	var camposModificados=$('#camposModificados').val();
	if(camposModificados!=''){
		camposModificados=camposModificados.split(';');

		for(i=0;i<camposModificados.length;i++){
			if(camposModificados[i]!='firma'){//El campo firma no es compatible con esta función
				var id='#'+camposModificados[i];

				if($(id).parent().prop('tagName')=='TD' || $(id).parent().parent().prop('tagName')=='TD'){//Los campos en una tabla son un caso especial
					if($(id).parent().hasClass('input-prepend')){
						var label=$(id).parent();
						label.before('<span class="asteriscoAmarillo">*</span> ');
					}
					else{
						$(id).before('<span class="asteriscoAmarillo">*</span> ');
					}
				}
				else{
					if($(id).parent().hasClass('input-prepend')){
						var label=$(id).parent().parent().prev();//Doble salto para los campoTextoSimbolo
					}
					else if($(id).parent().hasClass('checkbox')){
						var label=$(id).parent().parent().parent().parent().parent().parent().parent().prev();//7 saltos para los checks en tablas
					}
					else{
						var label=$(id).parent().prev();	
					}
					
					var texto=label.html();
					label.html('<span class="asteriscoAmarillo">*</span> '+texto);
				}
			}
		}
	}
}*/