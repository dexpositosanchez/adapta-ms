<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesVentas(){
	$res=true;

	if(isset($_GET['codigo'])&&isset($_GET['activa'])){
	    $res=activaPresupuesto($_GET['codigo']);
	}
	elseif(isset($_POST['codigo'])){
		$res = actualizaPresupuesto();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('presupuestos');
	}

	if(isset($_GET['activa'])){
		if($res){
	  		mensajeOk("Presupuesto aceptado."); 
		}
		else{
	  		mensajeError("se ha producido un error al gestionar los datos. Compruebe la información introducida."); 
		}
  	}
	mensajeResultado('referenciaPresupuesto',$res,'Presupuesto');
    mensajeResultado('elimina',$res,'Presupuesto', true);
}

function actualizaPresupuesto(){
	$res = actualizaDatos('presupuestos');
	$res = $res && insertaProductos();
	$res = $res && insertaPlazos($_POST['codigo']);

	return $res;
}

function activaPresupuesto($codigoPresupuesto){
	$res=true;
	conexionBD();
	$consulta=consultaBD("UPDATE presupuestos SET aceptado='SI' WHERE codigo='$codigoPresupuesto';");
	if(!$consulta){
		$res=false;
	}
	$consulta=consultaBD("SELECT clientes.codigo FROM clientes INNER JOIN presupuestos ON presupuestos.empresa=clientes.codigo WHERE presupuestos.codigo='$codigoPresupuesto';");
	$cliente=mysql_fetch_assoc($consulta);
	$consulta=consultaBD("UPDATE clientes SET activo='SI' WHERE codigo='".$cliente['codigo']."';");
	if(!$consulta){
		$res=false;
	}
	cierraBD();
	return $res;
}

function insertaProductos(){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM productos_presupuestos WHERE codigoPresupuesto='".$_POST['codigo']."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['cantidad'.$i]) && $datos['cantidad'.$i]!=""){
		$res = $res && consultaBD("INSERT INTO productos_presupuestos VALUES (NULL,'".$_POST['codigo']."','".$datos['producto'.$i]."',
			'".$datos['cantidad'.$i]."','".$datos['precioUnitario'.$i]."','".$datos['descuento'.$i]."','".$datos['totalDescuento'.$i]."',
			'".$datos['impuesto'.$i]."','".$datos['neto'.$i]."');");

		$i++;
	}

	return $res;
}

function insertaPlazos($contrato){
	$res=true;
	$i=0;
	$datos=arrayFormulario();
	$res = $res && consultaBD("DELETE FROM presupuestos_facturas WHERE codigoPresupuesto='".$contrato."' AND facturado IS NULL");
	// Sino se insertan los nuevos registros
	while(isset($datos['importe'.$i])){
		if($datos['importe'.$i] != ''){
			$consulta = "INSERT INTO presupuestos_facturas VALUES (NULL,'".$contrato."','".$datos['fecha'.$i]."',
			'".$datos['importe'.$i]."',NULL);";
		
			$res = $res && consultaBD($consulta);
		}

		$i++;
	}

	return $res;
}

function generaDatosGraficoVentas(){
	$datos=array();

	conexionBD();
	
	$where=compruebaPerfilParaWhere();

	$reg=consultaBD("SELECT COUNT(codigo) AS codigo FROM presupuestos $where;",false,true);
	$datos['totales']=$reg['codigo'];

	$reg=consultaBD("SELECT COUNT(codigo) AS codigo FROM presupuestos $where AND aceptado='SI';",false,true);
	$datos['confirmados']=$reg['codigo'];
	
	if($datos['totales']>=$datos['confirmados']){
		$datos['max']=$datos['totales'];
	}
	elseif($datos['confirmados']>=$datos['totales']){
		$datos['max']=$datos['confirmados'];
	}

	cierraBD();

	return $datos;
}

function escalaGraficoComercial($max){
	$datos=array('escala'=>10,'max'=>$max);//La inicialización a $max del íncide max es irrelevante
	
	//El número máximo de puntos que debe tener el gráfico para que se vea bien es 15. Empiezo con una escala de 10, y si con ella el número de puntos es mayor que 15, multiplico por 10
	while(($max/$datos['escala'])>15){
		$datos['escala']*=10;
	}
	$datos['max']=ceil($max/$datos['escala']);

	return $datos;
}


function gestionVentas(){
	operacionesVentas();
	
	abreVentanaGestion('Gestión de Venta','?','','icon-edit','',false,'noAjax');
	$datos=compruebaDatos('presupuestos');

	echo "<fieldset class='sinFlotar'>";
	echo "<fieldset class='span3'>";

	$consulta="SELECT codigo, nombre AS texto FROM clientes;";
	campoSelectConsulta('empresa','Empresa',$consulta,$datos);

	if($datos){
		$referencia=$datos['referenciaPresupuesto'];
		$fecha=$datos['fechaValidez'];
	}
	$fecha=explode('-', $fecha);
	$referenciaMostrar=str_pad($referencia, 3, "0", STR_PAD_LEFT).'/'.$fecha[1].'-'.$fecha[0];
	campoDato('Nº Referencia',$referenciaMostrar);

	echo "</fieldset>";
	echo "<fieldset class='span5'>";

	campoSelect('fase','Fase',array('Primer Contacto','Presentación','Propuesta Económica','Aprobada y en espera de inicio','Ganada','Perdida'),array('contacto','presentacion','propuesta','aprobada','ganada','perdida'),$datos);
	campoFecha('fechaValidez','Fecha emisión',$datos);
	$consulta="SELECT codigo, CONCAT(apellidos, ', ', nombre) AS texto FROM usuarios WHERE usuario!='soporte' AND clave!='soporte15';";
	campoSelectConsulta('comercial','Comercial',$consulta,$datos);		

	echo "</fieldset>";
	echo "</fieldset>";

	echo "<h3 class='apartadoFormulario'> Detalles del Producto</h3>";
	echo "<fieldset class='sinFlotar'>";

	$i=tablaProductos($datos);

	echo "</fieldset>";

	echo "<h3 class='apartadoFormulario'> Totales</h3>";
	echo "<fieldset class='sinFlotar'>";

	campoCalcula('subtotal','Total',$datos);

	echo "</fieldset>";

	echo "<h3 class='apartadoFormulario sinFlotar'>Facturas</h4>";
	abreColumnaCampos('span11');
		$i=tablaFacturas($datos);
	cierraColumnaCampos();

	echo "<h3 class='apartadoFormulario'> Información de la Descripción</h3>";
	echo "<fieldset class='sinFlotar'>";

	areaTexto('descripcion','Descripción',$datos,'span8');

	echo "</fieldset>";

	cierraVentanaGestion('index.php');
	return $i;
}

function tablaFacturas($datos){
	echo"	
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaGastos'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Fecha </th>
							<th> Importe € </th>
							<th> Facturada </th>
							<th> Cobrada </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$iconoC=array('SI'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
		$consulta=consultaBD("SELECT * FROM presupuestos_facturas WHERE codigoPresupuesto=".$datos['codigo'].' ORDER BY fecha',true);				  
		while($factura=mysql_fetch_assoc($consulta)){
			$facturado='NO';
			$cobrado='NO';
			if($factura['facturado']!=NULL){
				$facturado='SI';
				$factura2=datosRegistro('facturacion',$factura['facturado']);
				if($factura2['cobrada']=='SI'){
					$cobrado='SI';
				}
			}
		 	echo "<tr>";
		 	if($facturado=='SI'){
		 		echo "<td>".formateaFechaWeb($factura['fecha'])."</td>";
		 		echo "<td>".$factura['importe']." €</td>";
			} else {
				campoFechaTabla('fecha'.$i,$factura['fecha']);
				campoTextoSimbolo('importe'.$i,'','€',$factura['importe'],'input-mini pagination-right',1);
				$i++;
			}

			echo"<td>".$facturado."</td>
				<td>".$cobrado."</td>
				</tr>";
		}
	}
	if($i==0){
	echo "<tr>";
		echo "<tr>";
		 	campoFechaTabla('fecha'.$i);
			campoTextoSimbolo('importe'.$i,'','€','','input-mini pagination-right',1);

			echo"<td></td><td></td>
				</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaGastos\");'><i class='icon-plus'></i> Añadir</button> 
				</center>
		 	";
	return $i;
}


function imprimeVentas($where=' AND aceptado="SI"'){
	$whereFinal=compruebaPerfilParaWhere('presupuestos.codigo');
	$whereFinal.=$where;
	$consulta=consultaBD("SELECT presupuestos.codigo, fase, clientes.nombre AS empresa, subtotal AS total, usuarios.nombre, usuarios.apellidos, aceptado, clientes.codigo AS codigoCliente, referenciaPresupuesto, fechaValidez FROM presupuestos 
	INNER JOIN clientes ON clientes.codigo=presupuestos.empresa
	LEFT JOIN usuarios ON usuarios.codigo=presupuestos.comercial ".$whereFinal,true);
	
	$fase=array('contacto'=>'Primer contacto','presentacion'=>'Presentación','propuesta'=>'Propuesta económica','aprobada'=>'Aprobada','ganada'=>'Ganada','perdida'=>'Perdida');
	while($datos=mysql_fetch_assoc($consulta)){
		$botones=botonesPresupuestos($datos['codigo'],$datos['aceptado'],$datos['codigoCliente']);
		$referencia=$datos['referenciaPresupuesto'];
		$fecha=$datos['fechaValidez'];
		$fecha=explode('-', $fecha);
		$referencia=str_pad($referencia, 3, "0", STR_PAD_LEFT).'/'.$fecha[1].'-'.$fecha[0];
		echo "<tr>
				<td> ".$referencia."</td>
				<td> ".$datos['empresa']."</td>
				<td> ".$fase[$datos['fase']]."</td>
				<td> ".$datos['nombre']." ".$datos['apellidos']."</td>
				<td> ".formateaNumeroWeb($datos['total'])." €</td>";
				if(isset($datos['facturada'])){
					echo '<td><label class="label label-success">Facturado</label></td>';
				}else{	
					echo '<td><label class="label label-danger">No facturado</label></td>';
				}
				echo "
				<td class='centro'>
					$botones
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

function botonesPresupuestos($codigo,$aceptado,$codigoCliente){
	global $_CONFIG;
	$boton="<a href='".$_CONFIG['raiz']."ventas/gestion.php?codigo=".$codigo."' class='btn btn-propio noAjax'><i class='icon-search-plus'></i> Ver datos</i></a> 
	<a href='' class='btn facturarBt btn-success' codigoPresupuesto=".$codigo." codigoCliente=".$codigoCliente."><i class='icon-euro'></i> Facturar</i></a> ";
	return $boton;
}


function tablaProductos($datos){
	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCursos'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Producto </th>
							<th> Cantidad </th>
							<th> Precio Unitario </th>
							<th> Descuento </th>
							<th> Total con Descuento </th>
							<th> Impuesto </th>
							<th> Total Neto </th>
							<th> </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$datosProductosConsulta=datosProductosPresupuestos($datos['codigo']);				  
		while($datosProductosPresupuestos=mysql_fetch_assoc($datosProductosConsulta)){
		 	echo "<tr>";
				$consulta="SELECT codigo, nombre AS texto FROM productos;";
				campoTextoTabla('producto'.$i,$datosProductosPresupuestos['codigoProducto'],'input-large pagination-right');
				campoTextoTabla('cantidad'.$i,$datosProductosPresupuestos['cantidad'],'input-mini pagination-right');
				campoTextoTabla('precioUnitario'.$i,$datosProductosPresupuestos['precioUnitario'],'input-mini pagination-right');
				campoTextoTabla('descuento'.$i,$datosProductosPresupuestos['descuento'],'input-mini pagination-right');
				campoTextoTabla('totalDescuento'.$i,$datosProductosPresupuestos['totalDescuento'],'input-mini pagination-right');
				campoSelect('impuesto'.$i,'',array('21%','0%'),array(21,0),$datosProductosPresupuestos['impuesto'],'span2','data-live-search="true"',1);
				campoTextoTabla('neto'.$i,$datosProductosPresupuestos['neto'],'input-mini pagination-right');
			echo"
					<td>
						<input type='checkbox' name='filasTabla[]' value='$i'>
		        	</td>
				</tr>";
			$i++;
		}
	}

	if($i==0 || !$datos){
	 	echo "<tr>";
			$consulta="SELECT codigo, nombre AS texto FROM productos;";
			campoTextoTabla('producto'.$i,'','input-large pagination-right');
			campoTextoTabla('cantidad'.$i,'','input-mini pagination-right');
			campoTextoTabla('precioUnitario'.$i,'','input-mini pagination-right');
			campoTextoTabla('descuento'.$i,'','input-mini pagination-right');
			campoTextoTabla('totalDescuento'.$i,'','input-mini pagination-right');
			campoSelect('impuesto'.$i,'',array('21%','0%'),array(21,0),21,'span2','data-live-search="true"',1);
			campoTextoTabla('neto'.$i,'','input-mini pagination-right');
		echo"
				<td>
					<input type='checkbox' name='filasTabla[]' value='$i'>
	        	</td>
			</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCursos\");'><i class='icon-plus'></i> Añadir</button> 
	              <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCursos\");'><i class='icon-minus'></i> Eliminar</button> 
	            </center>
		 	";
			return $i;
}

function datosProductosPresupuestos($codigo){
	$consulta=consultaBD("SELECT * FROM productos_presupuestos WHERE codigoPresupuesto='$codigo';",true);
	return $consulta;
}

function plazosVentas(){
	$res='';
	$datos=arrayFormulario();

    conexionBD();
    
    $consulta=consultaBD("SELECT * FROM presupuestos_facturas WHERE codigoPresupuesto='".$datos['codigoPresupuesto']."';");
    $datosDocu=mysql_num_rows($consulta);

    if($datosDocu!=false){
    	$res=$datosDocu;
    }else{
        $res='fallo';
    }

    cierraBD();

    echo $res;
}

//Fin parte de incidencias