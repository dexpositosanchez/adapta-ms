<?php
  $seccionActiva=0;
  include_once("cabecera.php");
  $res=false;

  if(isset($_POST['pregunta1'])){
    $res=insertaDatos('satisfaccion');
  }
?>
<br><br>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">

		<?php
          mensajeResultado('pregunta1',$res,'encuesta');
        ?>

        <div class="widget">
            
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="?" method="post">
                  <fieldset>
				  
					<?php
						campoTexto('cliente','Cliente');
						campoFecha('fecha','Fecha');
					?>
					 <br />
					
					<p class="justificado parrafo-margenes" > <strong>Estimado Cliente.</strong> En Adapta MS nos preocupa mucho su satisfacción con los servicios

						contratados con nuestra entidad y la calidad final de los proyectos ejecutados. Por ello, le agradecemos que pueda 

						hacernos llegar la presente encuesta de satisfacción, indicando además en el apartado observaciones cualquier 

						comentario que crea que pueda ayudarnos a mejorar la calidad de nuestros servicios o a ajustar los mismos a sus 

						necesidades. Sin duda, creemos firmemente que en sus opiniones y sugerencias se encuentran nuestras mayores 

						oportunidades de mejorar. Gracias por anticipado.<br><br>
						
						<strong>Puntúe de 1 a 5 estrellas (1 como Muy Mal y 5 como Excelente).</strong>
						</p><br />				
					<?php 
						creaTablaSatisfaccion(); 
						creaPreguntaTablaEncuesta("Asistencia Previa a la contratación de nuestra empresa (Atención, solicitud de información inicial, facilidad de contacto):",1);
						creaPreguntaTablaEncuesta("Documentación Recibida ( Propuestas, Ofertas, Facturas Proformas) y explicaciones /atenciones recibidas sobre la misma:",2);
						creaPreguntaTablaEncuesta("Atención Telefónica / Administrativa:",3);
						creaPreguntaTablaEncuesta("Cumplimientos de plazos:",4);
						creaPreguntaTablaEncuesta("Asistencia ante incidencias:",5);
						creaPreguntaTablaEncuesta("Valoración de los Servicios Prestados / Productos Adquiridos:",6);
						creaPreguntaTablaEncuesta("En general, su satisfacción con los nuestra organización es:",7);
					?>
						<td colspan="2">A continuación, incluya cuantos comentarios o sugerencias estime oportuno para aclarar o ampliar las cuestiones anteriores:</td>	
						<tr>
							<td colspan="2"><textarea rows="10" class="textarea-amplia" name="comentarios" id="comentarios"></textarea></td>	
						</tr>
					<?php 
						cierraTablaSatisfaccion();
					?>
					

                     <br />
                    
                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-share"></i> Enviar cuestionario</button> 
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script src="../../api/js/bootstrap-rating-input.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#fecha').datepicker({format:'dd/mm/yyyy',weekStart:1});
  });

</script>


