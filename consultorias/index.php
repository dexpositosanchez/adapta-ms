<?php
  $seccionActiva=12;
  include_once('../cabecera.php');
  
  $res=false;
  if(isset($_POST['codigo'])){
    $res=actualizaSoftware();
  }
  elseif(isset($_POST['tipoTarea'])){
    $res=creaSoftware();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('software');
  }

  $estadisticas=creaEstadisticasSoftware();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Conusltorías:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-laptop"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Consultorías</div>
                     <div class="stat"> <i class="icon-flag"></i> <span class="value"><?php echo $estadisticas['pendientes']?></span> <br>Pendientes</div>
                     <div class="stat"> <i class="icon-clock-o"></i> <span class="value"><?php echo $estadisticas['desarrollo']?></span> <br>En desarrollo</div>
                     <div class="stat"> <i class="icon-exclamation-circle" id="iconoPlazoSuperado"></i> <span class="value" id="plazoSuperado"><?php echo $estadisticas['plazoSuperado']?></span> <br>Con plazo superado</div>                      
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Consultorías</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva consultoría</span> </a>
                <a href="#" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
                <br />
                <a href="calendarioTareasSoftware.php" class="shortcut"><i class="shortcut-icon icon-calendar"></i><span class="shortcut-label">Vista de calendario</span> </a>
                <a href="tododoindone.php" class="shortcut"><i class="shortcut-icon icon-columns"></i><span class="shortcut-label">ToDo-Doing-Done</span> </a>
                <a href="vistaTiempos.php" class="shortcut"><i class="shortcut-icon icon-clock-o"></i><span class="shortcut-label">Vista de tiempos</span> </a>
                <a href="softwareIncidenciasFinalizadas.php" class="shortcut"><i class="shortcut-icon icon-check"></i><span class="shortcut-label">Finalizadas</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        
        <?php
          mensajeResultado('tipoTarea',$res,'tarea');
          mensajeResultado('elimina',$res,'tarea', true);
        ?>
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Consultorías registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Cliente </th>
					<th> Presupuesto </th>
                    <th> Tipo de Tarea </th>
                    <th> Solicitud </th>
                    <th> R. Prevista </th>
                    <th> Estado </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeTareasSoftware();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('../pie.php'); ?>

<script src="../../api/js/jquery.dataTables.js"></script>
<script src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
      inicializaPopOvers();

      $('th, .pagination a').click(function(){
        inicializaPopOvers();        
      });

      $('input[type=search]').change(function(){
        inicializaPopOvers();
      });

      if(parseInt($('#plazoSuperado').text())>0){
        $('#iconoPlazoSuperado').addClass('plazoSuperado');
      }

    });

    function inicializaPopOvers(){
      $('.enlacePopOver').each(function(){
        $(this).popover({//Crea el popover
          title: 'Descripción <div class="pull-right"><button type="button" class="btn btn-small btn-default cerrar"><i class="icon-remove"></i></button>',
          content: $(this).next().html().replace(/[\r\n|\n]/g,'<br />'),
          placement:'right',
          trigger:'manual'
        });

        $(this).click(function(e){
          e.preventDefault();
          $('.enlacePopOver').not($(this)).popover('hide');
          $(this).popover('show');
          oyenteCerrar();
        });
      });
    }


    function oyenteCerrar(){
      $('.cerrar').click(function(){
        $('.enlacePopOver').popover('hide');
      });
    }
</script>