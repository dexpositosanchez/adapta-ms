<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de empleados

function operacionesPuestos(){
	$res=true;

	if(isset($_POST['codigo'])){
    	$res=actualizaDatos('puestosTrabajo');
  	}
  	elseif(isset($_POST['funcion'])){
    	$res=insertaDatos('puestosTrabajo');
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('puestosTrabajo');
	}

	mensajeResultado('funcion',$res,'Puesto');
    mensajeResultado('elimina',$res,'Puesto', true);
}

function imprimePuestos(){
	global $_CONFIG;

	$codigoS=$_SESSION['codigoU'];
	$consulta=consultaBD("SELECT codigo, funcion, fechaActualizacion FROM puestosTrabajo ORDER BY fechaActualizacion DESC;", true);

	while($datos=mysql_fetch_assoc($consulta)){
		$fecha=formateaFechaWeb($datos['fechaActualizacion']);
		echo "
		<tr>
			<td> ".$datos['funcion']." </td>
        	<td> $fecha </td>
        	<td class='td-actions'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-zoom-in'></i> Detalles</i></a>
        	</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function gestionPuestos(){
	operacionesPuestos();

	abreVentanaGestion('Gestión de puestos de trabajo','index.php');
	$datos=compruebaDatos('puestosTrabajo');

	campoTexto('funcion','Función',$datos,'span4');
	areaTexto('actividades','Descripción de actividades',$datos,'areaInforme');
	campoTexto('cualificacion','Requisitos de cualificación',$datos,'span4');
	campoTexto('experiencia','Requisitos de experiencia',$datos,'span4');
	campoFecha('fechaActualizacion','Fecha de actualizacion',$datos);
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');
	campoOculto($_SESSION['codigoU'],'codigoUsuario');

	cierraVentanaGestion('index.php');
}

//Fin parte de empleados


