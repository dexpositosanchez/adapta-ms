<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

function operacionesIndicadores(){
	$res=true;

  	if(isset($_POST['codigo'])){
  		if(isset($_POST['valorFinal'])){
    		$res=actualizaMapa();
  		} else {
  			$res=actualizaDatos('indicadores');
  		}
  	}
  	elseif(isset($_POST['valorFinal'])){
    	$res=insertaMapa();
  	}
  	elseif(isset($_POST['nombre'])){
    	$res=insertaDatos('indicadores');
  	}
  	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    	$res=eliminaDatos('mapaProcesos');
  	}
  	elseif(isset($_GET['eliminaProceso'])){
    	$_POST['codigo0'] = $_GET['codigo'];
    	$res=eliminaDatos("indicadores");
  	}

	mensajeResultado('descripcion',$res,'Indicador');
    mensajeResultado('elimina',$res,'Indicador', true);
}

function insertaMapa(){
	$res=insertaDatos('mapaProcesos');
	$_POST['codigo'] =mysql_insert_id();
	$res=insertaValores($_POST['codigo']);
	return $res;
}

function actualizaMapa(){
	$res=actualizaDatos('mapaProcesos');
	$res=insertaValores($_POST['codigo']);
	return $res;
}

function insertaValores($mapa){
	$res = true;
	conexionBD();

	$datos=arrayFormulario();
	$consulta=consultaBD("DELETE FROM valores_mapa WHERE codigoMapa='$mapa';");
	$i=0;
	$indicador = datosRegistro('mapaProcesos',$mapa);
	$tipo = datosRegistro('indicadores',$indicador['indicador']);
	if($tipo['valor_real'] == 'ESTATICO'){
		while(isset($datos['valor'.$i])){
			$valor = $datos['valor'.$i] == '' ? 0 : $datos['valor'.$i];
			$check = isset($datos['checkValor'.$i]) ? 'SI':'NO';
			$res=$res && consultaBD("INSERT INTO valores_mapa VALUES(NULL, '$mapa', '$valor','','','$check');");
			$i++;
		}
	} else {
		while(isset($datos['valorN'.$i])){
			$valorN = $datos['valorN'.$i] == '' ? 0 : $datos['valorN'.$i];
			$valorD = $datos['valorD'.$i] == '' ? 0 : $datos['valorD'.$i];
			$check = isset($datos['checkValor'.$i]) ? 'SI':'NO';
			$res=$res && consultaBD("INSERT INTO valores_mapa VALUES(NULL, '$mapa', '', '$valorN', '$valorD', '$check');");
			$i++;
		}
	}

	cierraBD();
	return $res;
}

function imprimeIndicadores($area){
	$consulta=consultaBD("SELECT codigo, indicador, descripcion, tolerancia, condicion, valorFinal, numerador, denominador, frecuencia FROM mapaProcesos WHERE indicador='$area' ORDER BY descripcion",true);
	$frecuencias=array('' => '',1 =>'Anual',2 =>'Semestral',4 =>'Trimestral',6 => 'Bimestral',12 => 'Mensual');
	while($datos=mysql_fetch_assoc($consulta)){
		$tipo = consultaBD("SELECT valor_real FROM indicadores WHERE codigo=".$datos['indicador'],true,true);

		if ($tipo['valor_real'] == 'ESTATICO'){
			$valorReal=consultaBD("select count(codigo) as numero, sum(valor) as total FROM valores_mapa WHERE codigoMapa=".$datos['codigo']." AND checkValor='SI'",true,true);
			$valorReal = round($valorReal['total'] / $valorReal['numero']);
			
			$tdValor = "<td> ".$valorReal." </td>";
			$indicador=calculaIndicador($datos['condicion'],$datos['tolerancia'],$datos['valorFinal'],$valorReal);
		} else {
			$valorReal=consultaBD("select count(codigo) as numero, sum(valorN) as numerador, sum(valorD) denominador FROM valores_mapa WHERE codigoMapa=".$datos['codigo']." AND checkValor='SI'",true,true);
			$tdValor = "<td> ".$valorReal['numerador']." / ".$valorReal['denominador']."</td>";
			$valorReal = (100 * $valorReal['numerador']) / $valorReal['denominador'];
			$valorFinal = (100 * $datos['numerador']) / $datos['denominador'];
 			$datos['valorFinal'] = $datos['numerador']." / ".$datos['denominador'];
			$indicador=calculaIndicador($datos['condicion'],$datos['tolerancia'],$valorFinal,$valorReal);
		}

		echo "
		<tr>
			<td> ".$datos['descripcion']."</td>"
			.$tdValor."
			<td class='centro'> ".$datos['condicion']."</td>
			<td> ".$datos['valorFinal']." </td>
			<td> ".$frecuencias[$datos['frecuencia']]." </td>
        	<td class='centro'> <img src='../img/indicador".$indicador.".png' /></td>
        	<td class='centro'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
	}
}

function gestionIndicadores(){
	operacionesIndicadores();

	$area=obtieneAreaIndicador();
  	$nombreArea=obtieneNombreAreaIndicador($area);
	
	abreVentanaGestion('Nuevo indicador para el área ' .$nombreArea,'index.php');
	$datos=compruebaDatos('mapaProcesos');

	if($datos){
		$indicador = $datos['indicador'];
	} else{
		$indicador = $area;
	}

	campoSelectConsulta('indicador','Área',"SELECT codigo, nombre AS texto FROM indicadores",$indicador);
    campoTexto('descripcion','Descripción del indicador',$datos,'span5');
	campoSelect('condicion','Condición',array('IGUAL','MAYOR O IGUAL','MAYOR','MENOR O IGUAL','MENOR'),array('==','>=','>','<=','<'),$datos,'selectpicker span2 show-tick');
	$tipo = consultaBD("SELECT valor_real FROM indicadores WHERE codigo=".$indicador,true,true);
	campoOculto($tipo['valor_real'],'tipoIndicador');
    if ($tipo['valor_real'] == 'ESTATICO'){
    	campoNumero('valorFinal','Valor final esperado',$datos);
    	campoOculto('0','numerador');
    	campoOculto('0','denominador');
	} else {
		campoNumeroIndicador('numerador','Valor final esperado','left',$datos);
		campoNumeroIndicador('denominador','','none',$datos);
		campoOculto('0','valorFinal');
	}
	campoNumero('tolerancia','Tolerancia',$datos);
    campoSelect('frecuencia','Frecuencia',array('','Mensual','Bimestral','Trimestral','Semestral','Anual'),array('0','12','6','4','2','1'),$datos);
    if($datos){
    	$paraAjax =consultaBD("SELECT COUNT(codigo) AS codigo FROM valores_mapa WHERE codigoMapa=".$datos['codigo'],true,true);
    	campoOculto($paraAjax['codigo'],'paraAjax','0');
	}
    echo "<div id='inputsFrecuencia'></div>";
    areaTexto('observaciones','Observaciones',$datos);

	cierraVentanaGestion('indicadoresArea.php?area='.$indicador);
}

function gestionMapaProcesos($enlace){
	operacionesIndicadores();

	abreVentanaGestion('Gestión de areas para el Mapa de procesos','?');
	$datos=compruebaDatos('indicadores');

	campoTexto('nombre','Nombre',$datos);
	campoRadio('tipo','Tipo de proceso',$datos,'ESTRATEGICOS',array('Estratégicos','Misionales','Apoyo'),array('ESTRATEGICOS','MISIONALES','APOYO'));
	campoRadio('valor_real','Valor real',$datos,'ESTATICO',array('Estático','Fórmula'),array('ESTATICO','FORMULA'));
	

	 abreColumnaCampos();
	 	areaTexto('descripcion','Descripción',$datos);
    	areaTexto('referencias_documentales','Referencias documentales',$datos);
    	areaTexto('proveedores','Proveedores',$datos);
    	areaTexto('entradas','Entradas',$datos);
    	areaTexto('responsable','Responsable',$datos);
    cierraColumnaCampos();

    abreColumnaCampos();
    	areaTexto('recursos','Recursos',$datos);
    	areaTexto('clientes','Clientes',$datos);
    	areaTexto('salidas','Salidas',$datos);
    	areaTexto('registros','Registros',$datos);
    	areaTexto('inspecciones','Inspecciones',$datos);
    cierraColumnaCampos();
   
	campoOculto($enlace,'enlace');
	
	cierraVentanaGestion($enlace, true);
}

function obtieneValoresIndicadores(){
	$datos=array();
	conexionBD();
	$consulta=consultaBD("SELECT * FROM indicadores ORDER BY codigo");
	while($indicador=mysql_fetch_assoc($consulta)){
		$colores = array('Verde'=>0,'Amarillo'=>0,'Rojo'=>0,'Gris'=>0);
		$marcadores = consultaBD("SELECT * FROM mapaProcesos WHERE indicador=".$indicador['codigo']);
		while($marcador=mysql_fetch_assoc($marcadores)){
			if ($indicador['valor_real'] == 'ESTATICO'){
				$valorReal=consultaBD("select count(codigo) as numero, sum(valor) as total FROM valores_mapa WHERE codigoMapa=".$marcador['codigo']." AND checkValor='SI'",true,true);
				$valorReal = round($valorReal['total'] / $valorReal['numero']);

				$color=calculaIndicador($marcador['condicion'],$marcador['tolerancia'],$marcador['valorFinal'],$valorReal);
			} else {
				$valorReal=consultaBD("select count(codigo) as numero, sum(valorN) as numerador, sum(valorD) denominador FROM valores_mapa WHERE codigoMapa=".$marcador['codigo']." AND checkValor='SI'",true,true);
				$valorReal = (100 * $valorReal['numerador']) / $valorReal['denominador'];
				$valorFinal = (100 * $marcador['numerador']) / $marcador['denominador'];

				$color=calculaIndicador($marcador['condicion'],$marcador['tolerancia'],$valorFinal,$valorReal);
			}
			$colores[$color]++;
		}
		unset($colores['Gris']);
		if($colores['Verde'] == 0 && $colores['Amarillo'] == 0 && $colores['Rojo']== 0){
			$datos[$indicador['codigo']] = "Gris";
		} 
		elseif (($colores['Verde'] == $colores['Rojo'] && $colores['Rojo'] == $colores['Amarillo']) || ($colores['Rojo'] > $colores['Amarillo'] && $colores['Rojo'] > $colores['Verde'])){
			$datos[$indicador['codigo']] = "Rojo";
		} 
		elseif ($colores['Rojo'] == $colores['Amarillo'] && $colores['Rojo'] > $colores['Verde']){
			$datos[$indicador['codigo']] = "Rojo";
		} 
		elseif ($colores['Verde'] == $colores['Amarillo'] || $colores['Amarillo'] > $colores['Verde']){
			$datos[$indicador['codigo']] = "Amarillo";
		} 
		else {
			$datos[$indicador['codigo']] = "Verde";
		}
	}
	
	cierraBD();

	return $datos;
}

function calculaIndicador($condicion,$tolerancia,$valorFinal,$valorReal){
	$res="Gris";

	/*$cinco=($valorFinal*5)/100;//El indicador será amarillo cuando se encuentre entre el +/- 5% del valor final
	$tres=($valorFinal*3)/100;
	$mediaAbajoCinco=$valorFinal-$cinco;
	$mediaArribaCinco=$valorFinal+$cinco;
	$mediaAbajoTres=$valorFinal-$tres;
	$mediaArribaTres=$valorFinal+$tres;*/

	switch ($condicion) {
		case '==':
			$marcado = false;
			if($valorReal == $valorFinal){
				$res="Verde";
				$marcado = true;
			} 
			if(!$marcado){
				for($i=1;$i<=$tolerancia;$i++){
					if(($valorReal == ($valorFinal+$i)) || ($valorReal == ($valorFinal-$i))){
						$res="Amarillo";
						$marcado = true;
					}
				}
			}
			if(!$marcado){
				$res="Rojo";
			}
			break;
		case '>=':
			if($valorReal >= $valorFinal){
				$res="Verde";
			} elseif($valorReal >= ($valorFinal-$tolerancia)){
				$res="Amarillo";
			} else {
				$res="Rojo";
			}
			break;
		case '>':
			if($valorReal > $valorFinal){
				$res="Verde";
			} elseif($valorReal > ($valorFinal-$tolerancia)){
				$res="Amarillo";
			} else {
				$res="Rojo";
			}
			break;
		case '<=':
			if($valorReal <= $valorFinal){
				$res="Verde";
			} elseif($valorReal <= ($valorFinal+$tolerancia)){
				$res="Amarillo";
			} else {
				$res="Rojo";
			}
			break;
		case '<':
			if($valorReal < $valorFinal){
				$res="Verde";
			} elseif($valorReal < ($valorFinal+$tolerancia)){
				$res="Amarillo";
			} else {
				$res="Rojo";
			}
			break;
	}

	return $res;
}

function generaGraficoIndicadores($area){
	conexionBD();
		$indicador=datosRegistro('indicadores',$area);
		$colores = array('Verde'=>0,'Amarillo'=>0,'Rojo'=>0,'Gris'=>0);
		$marcadores = consultaBD("SELECT * FROM mapaProcesos WHERE indicador=".$area);
		while($marcador=mysql_fetch_assoc($marcadores)){
			if ($indicador['valor_real'] == 'ESTATICO'){
				$valorReal=consultaBD("select count(codigo) as numero, sum(valor) as total FROM valores_mapa WHERE codigoMapa=".$marcador['codigo']." AND checkValor='SI'",true,true);
				$valorReal = round($valorReal['total'] / $valorReal['numero']);

				$color=calculaIndicador($marcador['condicion'],$marcador['tolerancia'],$marcador['valorFinal'],$valorReal);
			} else {
				$valorReal=consultaBD("select count(codigo) as numero, sum(valorN) as numerador, sum(valorD) denominador FROM valores_mapa WHERE codigoMapa=".$marcador['codigo']." AND checkValor='SI'",true,true);
				$valorReal = (100 * $valorReal['numerador']) / $valorReal['denominador'];
				$valorFinal = (100 * $marcador['numerador']) / $marcador['denominador'];

				$color=calculaIndicador($marcador['condicion'],$marcador['tolerancia'],$valorFinal,$valorReal);
			}
			$colores[$color]++;
		}
	
	cierraBD();

	return $colores;
}

function obtieneAreaIndicador(){
	if(isset($_GET['area'])){
		$_SESSION['areaIndicador']=$_GET['area'];
	}

	return $_SESSION['areaIndicador'];
}

function obtieneNombreAreaIndicador($area){
	$consulta = consultaBD("SELECT * FROM indicadores WHERE codigo = ".$area,true,true);

	return $consulta['nombre'];
}

function creaEstadisticasIndicadores($area){
	$tipo = consultaBD("SELECT valor_real FROM indicadores WHERE codigo=".$area,true,true);
    if ($tipo['valor_real'] == 'ESTATICO'){
		$datos=consultaBD("SELECT SUM(valorFinal) AS valorFinal, SUM(valorReal) AS valorReal FROM mapaProcesos WHERE indicador='$area';",true,true);
	} else {
		$datos=consultaBD("SELECT SUM(valorFinal) AS valorFinal, SUM(numerador/denominador) AS valorReal FROM mapaProcesos WHERE indicador='$area';",true,true);
	}
	$cinco=($datos['valorFinal']*5)/100;//El indicador será amarillo cuando se encuentre entre el +/- 5% del valor final
	$tres=($datos['valorFinal']*3)/100;
	$mediaAbajoCinco=$datos['valorFinal']-$cinco;
	$mediaArribaCinco=$datos['valorFinal']+$cinco;
	$mediaAbajoTres=$datos['valorFinal']-$tres;
	$mediaArribaTres=$datos['valorFinal']+$tres;

	if($datos['valorFinal']=='' || $datos['valorReal']==''){
		$res="<div class='stat'> <i class='icon-circle-o valorIndicador indicadorGris'></i> <br>No se ha introducido valores</div>";
	}
	elseif($datos['valorReal']>=$mediaArribaCinco || $datos['valorReal']<=$mediaAbajoCinco){
		$res="<div class='stat'> <i class='icon-exclamation-triangle valorIndicador indicadorRojo'></i> <br>Desviación superior o igual al 5%</div>";
	}
	elseif($datos['valorReal']>=$mediaArribaTres || $datos['valorReal']<=$mediaAbajoTres){
		$res="<div class='stat'> <i class='icon-exclamation-triangle valorIndicador indicadorRojo'></i> <br>Desviación superior al 3%</div>";
	}
	else{
		$res="<div class='stat'> <i class='icon-check-circle valorIndicador indicadorVerde'></i> <br>Sin desviación</div>";
	}

	return $res;
}

function obtieneIndicadores($tipo){
	return consultaBD("SELECT * FROM indicadores WHERE tipo ='".$tipo."' ORDER BY codigo;",true);
}

function obtieneValores($codigo){
	$valores='';
	$consulta=consultaBD("SELECT * FROM valores_mapa WHERE codigoMapa=".$codigo,true);// 9 PARA PROBAR
	$i=0;
	while($datos=mysql_fetch_assoc($consulta)){
		$valores[$i] = $datos;
		$i++;
	}
	echo json_encode($valores);
}

?>