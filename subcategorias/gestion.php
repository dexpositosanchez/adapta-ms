<?php
  $seccionActiva=13;
  include_once("../cabecera.php");
  gestionSubcategorias();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();

	});

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>