<?php
  $seccionActiva=13;
  include_once('../cabecera.php');

  $firma=firmaUsuario();
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Editar firma de correo electrónico</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="<?php echo $_CONFIG['raiz']; ?>correos/index.php" method="post">
                  <fieldset>
                    

                    <div class="control-group">                     
                      <label class="control-label" for="firma"></label>
                      <div class="controls">
                        <textarea name="firma" id="mensajeCorreo"><?php echo $firma; ?></textarea>
                      </div>
                    </div>

                  
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar firma</button> 
                      <a href="<?php echo $_CONFIG['raiz']; ?>correos/index.php" class="btn btn-default"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                  
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#mensajeCorreo').wysihtml5({locale: "es-ES"});
  });
</script>