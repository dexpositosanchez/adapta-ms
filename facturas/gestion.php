<?php
  $seccionActiva=2;
  include_once("../cabecera.php");
  $datos=gestionFactura();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/filasTablaGastos.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/filasVentas.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
		$('.selectpicker').selectpicker();
		if($("#codigo").length) {
			var alumno=$('#codigoAlumno').val();
			var codigo=$('#codigo').val();
			//listadoVencimientos(alumno,codigo);
			otrosGastos('false');
		}
		$('#codigoContrato').change(function(){
			var contrato=$('#codigoContrato').val();
			recogePlazos(contrato,'contratos_facturas');
		});

		$('#codigoVenta').change(function(){
			var venta=$('#codigoVenta').val();
			recogePlazos(venta,'presupuestos_facturas');
		});
		mostrarSelect($('input[name=origen]:checked').val());
		$('input[name=origen]').change(function(){
			mostrarSelect($(this).val());
			$('#codigoContrato').val('');
			$('#codigoContrato').selectpicker('refresh');
			$('#codigoVenta').val('');
			$('#codigoVenta').selectpicker('refresh');
		});

		$('input[name=bonificada]').change(function(){
			var bonificada=$('input:radio[name=bonificada]:checked').val();
			if(bonificada=='SI'){
				$('#participantes').css('display','block');
			}else{
				$('#participantes').css('display','none');
			}
		});		
	});

	function mostrarSelect(val){
		$('.origen').addClass('hide');
		$('#div'+val).removeClass('hide');
		if(val=='CONTRATO'){
			var contrato=$('#codigoContrato').val();
			recogePlazos(contrato,'contratos_facturas');
		} else {
			var venta=$('#codigoVenta').val();
			recogePlazos(venta,'presupuestos_facturas');
		}
	}

	function recogePlazos(codigo,tabla){
		var parametros = {
				"codigo" : codigo,
				"tabla"  : tabla
        };
		$.ajax({
			 type: "POST",
			 url: "../listadoAjax.php?include=facturas&funcion=recogePlazos();",
			 data: parametros,
			  beforeSend: function () {
				  
			  },
			 success: function(response){
				   $("#plazo").html(response);
				   $('#plazo').selectpicker('refresh');
			 }
		});
}
	
	function listadoVencimientos(codigoAlumno,codigo){
		var parametros = {
                "alumno" : codigoAlumno,
				"codigo" : codigo
        };
		$.ajax({
			 type: "POST",
			 url: "../listadoAjax.php?include=facturas&funcion=listadoVencimientos();",
			 data: parametros,
			  beforeSend: function () {
				   $("#vencimientosOcultos").html('<center><i class="icon-refresh icon-spin"></i></center>');
			  },
			 success: function(response){
				   $("#vencimientosOcultos").html(response);
			 }
		});
	}

	function otrosGastos(codigo){
		var parametros = {
				"codigo" : codigo
        };
		$.ajax({
			 type: "POST",
			 url: "../listadoAjax.php?include=facturas&funcion=otrosGastos();",
			 data: parametros,
			  beforeSend: function () {
				   $("#otrosGastos").html('<center><i class="icon-refresh icon-spin"></i></center>');
			  },
			 success: function(response){
				   $("#otrosGastos").html(response);
				   var i=0;
					while($('#gasto'+i).val()!=undefined){
						$('#gasto'+i).selectpicker();
						$('#fecha'+i).datepicker({format:'dd/mm/yyyy',weekStart:1});
						$('#gasto'+i).change(function(){
							var selector=$(this).attr('id').charAt($(this).attr('id').length-1);
							oyentePrecio(selector);
						});
						i++;
					}
			 }
		});
}

	function datosContrato(codigo){
		var parametros = {
				"codigo" : codigo
        };
		$.ajax({
			 type: "POST",
			 url: "../listadoAjax.php?include=facturas&funcion=datosContrato();",
			 dataType: "json",
			 data: parametros
			}).
			 done( function(response){
			 		var fecha = response.fecha.split('-');
				   //$('#fechaEmision').datepicker('setValue', fecha[2]+'/'+fecha[1]+'/'+fecha[0]);
				   $('#formaPago').val(response.formaPago.toUpperCase());
				   $('#formaPago').selectpicker('refresh');
			 });
	}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>