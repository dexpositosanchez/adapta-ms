<?php
	session_start();
	include_once('funciones.php');
	compruebaSesion();
	require_once '../../api/phpword/PHPWord.php';


	$nombreFichero=generaInforme(new PHPWord(),$_GET['codigo']);

	// Definir headers
	header("Content-Type: application/msword");
	header("Content-Disposition: attachment; filename=".$nombreFichero);
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentos/personal/'.$nombreFichero);

	?>