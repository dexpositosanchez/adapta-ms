<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de empleados

function operacionesPlanes(){
	$res=true;

	if(isset($_POST['codigo'])){
    	$res=actualizaDatos('planesFormacion');
  	}
  	elseif(isset($_POST['contenido'])){
    	$res=insertaDatos('planesFormacion');
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('planesFormacion');
	}

	mensajeResultado('contenido',$res,'Plan');
    mensajeResultado('elimina',$res,'Plan', true);
}

function imprimePlanes(){
	global $_CONFIG;

	$codigoS=$_SESSION['codigoU'];
	$consulta=consultaBD("SELECT codigo, SUBSTRING(contenido,1,20) AS contenido, docentes, fechaReal, evaluacion FROM planesFormacion ORDER BY fechaReal DESC;", true);
	$docentes=array('INTERNOS'=>'Internos','EXTERNOS'=>'EXTERNOS');

	while($datos=mysql_fetch_assoc($consulta)){
		$fecha=formateaFechaWeb($datos['fechaReal']);

		echo "
		<tr>
			<td> ".$datos['contenido']."[...] </td>
			<td> ".$docentes[$datos['docentes']]." </td>
        	<td> $fecha </td>
        	<td> ".$datos['evaluacion']." </td>
        	<td class='td-actions'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-zoom-in'></i> Detalles</i></a>
        	</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function gestionPlanes(){
	operacionesPlanes();

	abreVentanaGestion('Gestión de planes de formación','index.php');
	$datos=compruebaDatos('planesFormacion');
	
	$displayDocentes = "style='display:none;'";
	if($datos){
		if($datos['docentes'] == 'EXTERNOS'){
			$displayDocentes = "style='display:block;'";
		}
	}

	areaTexto('contenido','Contenido',$datos,'areaInforme');
	campoFecha('fechaPrevista','Fecha prevista',$datos);
	echo "<div class='docentes'>";
		campoRadio('docentes','Docentes',$datos,'INTERNOS',array('Internos','Externos'),array('INTERNOS','EXTERNOS'));
	echo "</div>";
	echo "<div class='indicar' ".$displayDocentes.">";
		campoTexto('indicar','Indicar docentes',$datos,'span4');
	echo "</div>";
	campoFecha('fechaReal','Fecha real',$datos);
	campoTexto('evaluacion','Evaluación Eficacia de la Acción',$datos,'input-small');
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');
	campoOculto($_SESSION['codigoU'],'codigoUsuario');

	cierraVentanaGestion('index.php');
}

//Fin parte de empleados


