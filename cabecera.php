<?php
  session_start();
  include_once("funciones.php");
  compruebaSesion();
  if(!isset($_GET['ajax'])){
?>
  <!DOCTYPE html>
  <html lang="es">
  <head>
  <meta charset="utf-8">
  <title><?php echo $_CONFIG['title']; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=0.65, maximum-scale=1.0, user-scalable=1">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <link href="<?php echo $_CONFIG['raiz']; ?>css/bootstrap.min.php" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>css/style.php" rel="stylesheet /">
  <link href="<?php echo $_CONFIG['raiz']; ?>css/paneles.php" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/datepicker.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-select.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-wysihtml5.css" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/animate.css" rel="stylesheet" type="text/css" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/js/guidely/guidely.css" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-colorpicker.css" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>css/jquery.orgchart.php" rel="stylesheet" />

  <!-- Script HTML5, para el soporte del mismo en IE6-8 -->
  <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->


  <!-- Común -->
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery-1.7.2.min.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/controladorAJAX.js" type="text/javascript"></script>
  <!-- Fin común -->


  </head>
  <body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">

        <span class="logo2">
          <img src="<?php echo $_CONFIG['raiz']; ?>img/logo2.png" alt="<?php echo $_CONFIG['altLogo']; ?>"> &nbsp; <span><?php echo $_CONFIG['tituloSoftware']; ?></span>
        </span>

        <div class="nav-collapse">
          <ul class="nav pull-right cajaUsuario">
            <li class="dropdown"  id="target-5"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo ucfirst($_SESSION['usuario']).perfilUsuario(); ?> <b class="caret"></b></a>
              <ul class="dropdown-menu menu">
                <li><a href="<?php echo $_CONFIG['raiz']; ?>inicio.php"><i class="icon-home"></i> Inicio</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo $_CONFIG['raiz']; ?>cerrarSesion.php" class='noAjax'><i class="icon-power-off"></i> Cerrar sesión</a></li>
              </ul>
            </li>
          </ul>

        </div>

        <div class="nav-collapse">
          <?php cajaFiltroEjercicio(); ?>
        </div>
        <!--/.nav-collapse -->
      </div>
      <!-- /container -->
    </div>
    <!-- /navbar-inner -->
  </div>
  <!-- /navbar -->
  <div class="subnavbar">
    <div class="subnavbar-inner">
      <div class="container">
        <ul class="mainnav menu" id="target-4">
          <?php
      creaOpcionDesplegableMenu(11,'Empresas','icon-briefcase',array('Posibles clientes','Clientes','Presupuestos','Ventas'),array('posibles_clientes/','clientes/','presupuestos/','ventas/'),array('icon-user-plus','icon-users','icon-tags','icon-check'),$seccionActiva);
      creaOpcionMenu(12,'Consultorías','consultorias/','icon-commenting',$seccionActiva);
      creaOpcionDesplegableMenu(8,'Formación','icon-book',array('Módulos','Otros gastos','Contactos','Alumnos','Matrículas','Docentes'),array('cursos/','otros_gastos/','contactos/','alumnos/','contratos/','docentes/'),array('icon-list','icon-asterisk','icon-address-book','icon-group','icon-file-text','icon-graduation-cap'),$seccionActiva);
      creaOpcionDesplegableMenu(13,'Programas','icon-map-o',array('Programas','Categorías','Subcategorias'),array('programas/','categorias/','subcategorias/'),array('icon-map-o','icon-sitemap','icon-code-fork'),$seccionActiva);
      //creaOpcionMenu(8,'Cursos','cursos/','icon-book',$seccionActiva);
			creaOpcionDesplegableMenu(2,'Facturación','icon-eur',array('Facturas','Rectificativas','Remesas'),array('facturas/','rectificativas/','remesas/'),array('icon-file-text','icon-registered','icon-file-code-o'),$seccionActiva,array('ADMIN','CONTABILIDAD'));
			creaOpcionDesplegableMenu(9,'Informes','icon-paste',array('Informe facturación'),array('informe-facturacion/'),array('icon-credit-card'),$seccionActiva,array('ADMIN','CONTABILIDAD'));
			creaOpcionMenu(3,'Tareas','tareas/','icon-calendar',$seccionActiva);
			creaOpcionMenu(4,'Correos','correos/','icon-envelope',$seccionActiva);
      creaOpcionDesplegableMenu(5,'Incidencias','icon-exclamation-circle',array('Incidencias','Incidencias de Software'),array('incidencias/','incidencias-software/'),array('icon-exclamation-circle','icon-laptop'),$seccionActiva);
			creaOpcionMenu(7,'Com. Interna','comunicacion-interna/','icon-comments-o',$seccionActiva);
      creaOpcionDesplegableMenu(15,'Gestión de la Calidad','icon-user-md',array('Documentación','Objetivos','Infraestructuras','Recursos Humanos','Auditoría Interna','Satisfacción'),array('documentacion/','objetivos/','infraestructuras/','recursos-humanos/','auditorias_internas/','satisfaccion/'),array('icon-file-text','icon-flag','icon-laptop','icon-briefcase','icon-edit','icon-star'),$seccionActiva);
			creaOpcionMenu(10,'Usuarios','usuarios/','icon-user',$seccionActiva);
			/*creaOpcionMenu(0,'Ventas de cursos','inicios-grupos/','icon-pencil',$seccionActiva,array('COMERCIAL','TELEMARKETING','DELEGADO','DIRECTOR'));
            creaOpcionDesplegableMenu(0,'Administración','icon-file-text-o',array('Asesorias','Agrupaciones','Trabajadores','Inicios de grupos','Participantes','Complementos'),array('asesorias/','agrupaciones/','trabajadores/','inicios-grupos/','participantes/','complementos/'),array('icon-life-ring','icon-university','icon-industry','icon-calendar','icon-list','icon-tablet'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','CONTABILIDAD'));
            creaOpcionDesplegableMenu(1,'Configuración','icon-cogs',array('Servicios','Acciones Formativas','Centros de Formación','Tutores'),array('servicios/','acciones-formativas/','centros-formacion/','tutores/'),array('icon-cogs','icon-graduation-cap','icon-building-o','icon-briefcase'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));
            creaOpcionDesplegableMenu(12,'Consultorías','icon-commenting',array('Gestión','Seguimiento'),array('consultoria/','seguimiento-consultoria/'),array('icon-list','icon-eye'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));
            creaOpcionDesplegableMenu(9,'Tutorías','icon-pencil',array('Tareas Tutorías','Tutorías pendientes','Tutorías terminadas'),array('tareas-tutorias/','tutorias/index.php?terminadas=NO','tutorias/index.php?terminadas=SI'),array('icon-list-ol','icon-flag','icon-check-square-o'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));
            creaOpcionDesplegableMenu(2,'Comercial','icon-shopping-cart',array('Clientes potenciales','Clientes','Categorías','Agentes','Telemarketing','Colaboradores','Ventas de servicios','Listado de ventas','Pago de comisiones'),array('posibles-clientes/','clientes/','categorias/','comerciales/','telemarketing/','colaboradores/','ventas-servicios/','listado-ventas/','pagos-comisiones/'),array('icon-exclamation-circle','icon-check-circle','icon-sitemap','icon-group','icon-headphones','icon-share-alt','icon-tags','icon-list-ol','icon-money'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','CONTABILIDAD'));
            creaOpcionDesplegableMenu(2,'Comercial','icon-shopping-cart',array('Clientes potenciales','Clientes','Ventas de servicios'),array('posibles-clientes/','clientes/','ventas-servicios/'),array('icon-exclamation-circle','icon-check-circle','icon-tags'),$seccionActiva,array('COMERCIAL','DELEGADO','DIRECTOR'));
            creaOpcionMenu(11,'Agenda','agenda/','icon-calendar',$seccionActiva);
			      creaOpcionMenu(2,'Ventas de servicios','ventas-servicios/','icon-tags',$seccionActiva,array('TELEMARKETING'));
            creaOpcionMenu(10,'Incidencias','incidencias/','icon-exclamation-triangle',$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','COMERCIAL'));
            creaOpcionDesplegableMenu(3,'XML','icon-code',array('Exportar XML de Acciones','Exportar XML inicio','Exportar XML fin'),array('exportar-xml-acciones/','exportar-xml-inicio/','exportar-xml-fin'),array('icon-cloud-download','icon-cloud-download','icon-cloud-download'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));
            creaOpcionMenu(4,'Almacén','almacen/','icon-archive',$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));
            creaOpcionDesplegableMenu(5,'Facturación','icon-eur',array('Emisores','Cuentas bancarias propias','Facturas','Vencimientos','Recibos','Remesas'),array('emisores/','cuentas-propias/','facturas/','vencimientos/','recibos/','remesas/'),array('icon-list-ul','icon-cc','icon-file-text','icon-calendar-o','icon-ticket','icon-file-code-o'),$seccionActiva,array('ADMIN','CONTABILIDAD'));
            creaOpcionDesplegableMenu(5,'Facturación','icon-eur',array('Emisores','Cuentas bancarias propias','Facturas','Vencimientos','Recibos'),array('emisores/','cuentas-propias/','facturas/','vencimientos/','recibos/'),array('icon-list-ul','icon-cc','icon-file-text','icon-calendar-o','icon-ticket'),$seccionActiva,array('ADMINISTRACION1','ADMINISTRACION2','TUTOR'));
            creaOpcionDesplegableMenu(6,'Importar','icon-cloud-upload',array('Clientes potenciales','Clientes','Trabajadores','Acciones formativas','Inicio Grupo'),array('importar-clientes-potenciales/','importar-clientes/','importar-trabajadores/','importar-acciones-formativas/','importar-inicios-grupos/'),array('icon-exclamation-circle','icon-check-circle','icon-industry','icon-graduation-cap','icon-calendar'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));
            creaOpcionDesplegableMenu(7,'Exportar','icon-cloud-download',array('Clientes','Trabajadores','Acciones formativas','Inicio Grupo'),array('exportar-clientes/','exportar-trabajadores/','exportar-acciones-formativas/','exportar-inicios-grupos/'),array('icon-check-circle','icon-industry','icon-graduation-cap','icon-calendar'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));
            creaOpcionMenu(8,'Usuarios','usuarios/','icon-lock',$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));*/
          ?>
        </ul>
      </div>
      <!-- /container -->
    </div>
    <!-- /subnavbar-inner -->
  </div>
  <!-- /subnavbar -->
<?php
}
