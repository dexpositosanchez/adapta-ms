//Oyentes y funciones compartidas entre Clientes y Posibles Clientes

$(document).ready(function(){
	//inicializaDatepicker();
	$('select').selectpicker();

	//oyenteCambios();

	oyenteMedioPago();
	$('#medioPago').change(function(){
		oyenteMedioPago();
	});

	oyenteNumeroCuenta();
	oyenteNuevaCreacion();
	$('input[name=nuevaCreacion]').change(function(){
		oyenteNuevaCreacion();
	});

	inicializaFirma();

	//Oyentes de ventanas de creación
	$('#boton-codigoAsesoria').click(function(){
		$('#cajaAsesoria').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraAsesoria').click(function(){
		creaAsesoria();
	});

	$('#boton-codigoComercial').click(function(){
		$('#cajaComercial').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraComercial').click(function(){
		creaComercial();
	});

	$('#boton-codigoColaborador').click(function(){
		$('#cajaColaborador').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraColaborador').click(function(){
		creaColaborador();
	});

	$('#boton-codigoTelemarketing').click(function(){
		$('#cajaTelemarketing').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraTelemarketing').click(function(){
		creaTelemarketing();
	});

	//Parte de procedencia de listado
	oyenteProcedencia();
	$('input[name=procedeDeListado]').change(function(){
		oyenteProcedencia();
	});

	oyenteTextoProcedencia();
	$('#procedenciaListado').change(function(){
		oyenteTextoProcedencia();
	})
	//Fin parte de procedencia de listado

	//Oyente para actualizar los contactos de la asesoría
	$('#codigoAsesoria').change(function(){
		oyenteAsesoria($(this).val());
	});
	//Fin oyente para actualizar los contactos de la asesoría

	//Oyente cálculo créditos
	oyenteCalculoPrecio('#tablaCreditos');
	//Fin oyente cálculo créditos
});

function oyenteMedioPago(){
	if($('#medioPago').val()=='RECIBO SEPA'){
		$('#cajaMedioPago').removeClass('hide');
	}
	else{
		$('#cajaMedioPago').addClass('hide');	
	}
}

function oyenteNumeroCuenta(){
	$('#tablaCuentas').find('.numeroCuenta').each(function(){
		calculaIbanCuenta($(this));
	});
}

function calculaIbanCuenta(numeroCuenta){
	numeroCuenta.blur(function(){
		var fila=obtieneFilaCampo(numeroCuenta);

		$("#iban"+fila).val(CalcularIBAN(numeroCuenta.val(),'ES'));
	
		if(isAccountComplete(numeroCuenta.val())){
			var bic=obtieneBic(numeroCuenta.val());
			$("#bic"+fila).val(bic);
		}
		else{
			alert('Por favor, introduzca un númere de cuenta correcto.');
		}
	});
}

function oyenteNuevaCreacion(){
	if($('input[name=nuevaCreacion]:checked').val()=='NO'){
		$('#cajaNueva').addClass('hide');
	}
	else{
		$('#cajaNueva').removeClass('hide');
	}
}

function inicializaFirma(){
	var firma=$('#firma').val().trim();

	if(firma==''){
		$('#edit-profile').signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false});
	}
	else{
		$('#edit-profile').signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false}).regenerate(firma);
	}
}



function creaRegistro(idCampo,idBoton,idVentana,campos,campoNombre,funcion){
	$(idBoton).html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var consulta=$.post('../listadoAjax.php?include=clientes&funcion='+funcion,campos);

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar el colaborador.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			actualizaSelectsTrasCreacion(idCampo,respuesta,campoNombre);
		}

		$(idVentana).modal('hide');
		$(idBoton).html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original
	});
}

function actualizaSelectsTrasCreacion(idCampo,codigo,nombre){
	$(idCampo).find('option[value=NULL]').after('<option value="'+codigo+'">'+nombre+'</option>');
	$(idCampo).selectpicker('refresh');
}

function creaBotonesTrabajadores(){
	if($('#codigo').val()!=undefined && $('#posibleCliente').val()=='NO'){
		var codigo=$('#codigo').val();
		var botones="<div class='pull-right'>";
		botones+="<a href='../trabajadores/index.php?codigoEmpresa="+codigo+"' class='btn btn-small btn-primary'><i class='icon-industry'></i> Ver trabajadores</a>";
		botones+=" <a href='../trabajadores/gestion.php?codigoEmpresa="+codigo+"' class='btn btn-small btn-primary'><i class='icon-user-plus'></i> Crear trabajador</a>";
		botones+="</div>";

		$('.widget-header h3').after(botones);
	}
}

function oyenteProcedencia(){
	if($('input[name=procedeDeListado]:checked').val()=='SI'){
		$('#cajaProcedencia').removeClass('hide');
	}
	else{
		$('#cajaProcedencia').addClass('hide');
	}
}

function oyenteTextoProcedencia(){
	if($('#procedenciaListado').val()=='CARTERA'){
		$('#cajaTextoProcedencia').removeClass('hide');
		$('#textoProcedencia').parent().prev().text('Nombre de comercial:');
	}
	else if($('#procedenciaListado').val()=='OTROS'){
		$('#cajaTextoProcedencia').removeClass('hide');
		$('#textoProcedencia').parent().prev().text('Otros:');
	}
	else{
		$('#cajaTextoProcedencia').addClass('hide');
	}
}


function oyenteCalculoPrecio(selector){
	$(selector).find('.calculo').change(function(){
		var fila=obtieneFilaCampo($(this));
		var credito=formateaNumeroCalculo($('#credito'+fila).val());
		var creditoOtras=formateaNumeroCalculo($('#creditoOtras'+fila).val());
		var formacionTramitada=formateaNumeroCalculo($('#formacionTramitada'+fila).val());
		var formacionBonificada=formateaNumeroCalculo($('#formacionBonificada'+fila).val());
		
		
		credito=credito-creditoOtras-formacionTramitada-formacionBonificada;

		$('#creditoDisponible'+fila).val(formateaNumeroWeb(credito));
	});
}

function inicializaDatepicker(){
	var fechaActual=new Date();
 
	$('.campoFecha').datepicker({
  		onRender: function(date) {
    		return date.valueOf()>fechaActual.valueOf() ? 'disabled' : '';
  		}
	}).on('changeDate',function(e){$(this).datepicker('hide');});
}