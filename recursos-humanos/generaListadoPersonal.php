<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/personal/plantilla.xlsx");
	

	listadoPlanificacion($objPHPExcel);//Llamada a la función que rellena el Excel

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/personal/Listado_Personal.xlsx');


	// Definir headers
	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Listado_Personal.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentos/personal/Listado_Personal.xlsx');
?>