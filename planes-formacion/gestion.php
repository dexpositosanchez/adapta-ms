<?php
  $seccionActiva=15;
  include_once("../cabecera.php");
  gestionPlanes();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-colorpicker.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.selectpicker').selectpicker();

		$(".docentes input[type=radio]").change(function(){
        	if($(this).val() == 'EXTERNOS'){
        		$('.indicar').css('display','block');
        	} else {
        		$('.indicar').css('display','none');
        	}
     	});
	});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>