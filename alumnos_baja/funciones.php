<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores
function operacionesAlumnos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
		$res=actualizaDatos('trabajadores_cliente');
	}
	elseif(isset($_POST['nombre'])){
		$res=creaTrabajador();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('trabajadores_cliente');
	}

	mensajeResultado('nombre',$res,'Alumno');
    mensajeResultado('elimina',$res,'Alumno', true);
}

function creaTrabajador(){
	$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	$res=insertaDatos('trabajadores_cliente');

	return $res;
}

function creaEstadisticasAlumnos(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='ADMINISTRACION2'){
    	$res=consultaBD("SELECT COUNT(trabajadores_cliente.codigo) AS total FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo WHERE codigoUsuario=$codigoUsuario",true,true);
    }
    else{
    	$res=estadisticasGenericas('trabajadores_cliente',false,'activo="NO"');
    }

    return $res;
}



function formateaPreciosTrabajadores(){
	formateaPrecioBDD('salarioBruto');
	formateaPrecioBDD('costeHora');
}

function listadoAlumnos(){
	global $_CONFIG;

	$columnas=array('apellido1','apellido2','nombre','nif','fechaNacimiento','apellido1','apellido2');
	$having=obtieneWhereListado("HAVING 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$having .= ' AND activo = "NO"';
	conexionBD();
	$consulta=consultaBD("SELECT trabajadores_cliente.codigo, nombre, apellido1, apellido2, nif, fechaNacimiento, email, activo, numCuenta FROM trabajadores_cliente $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT trabajadores_cliente.codigo, nombre, apellido1, apellido2, nif, fechaNacimiento, email, activo, numCuenta FROM trabajadores_cliente $having;");
	cierraBD();
	
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['apellido1'],
			$datos['apellido2'],
			$datos['nombre'],
			$datos['nif'],
			formateaFechaWeb($datos['fechaNacimiento']),
			botonAcciones(array('Detalles'),array('alumnos_baja/gestion.php?codigo='.$datos['codigo']),array("icon-search-plus")),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionAlumno(){
	operacionesAlumnos();

	abreVentanaGestion('Gestión de Alumnos','index.php','span3');
	$datos=compruebaDatosTrabajador();

	abreColumnaCampos();
	campoTextoValidador('nombre','Nombre',$datos,'input-large obligatorio','trabajadores_cliente');
	campoTextoValidador('apellido1','Apellido 1',$datos,'input-large obligatorio','trabajadores_cliente');
	campoTextoValidador('apellido2','Apellido 2',$datos,'input-large','trabajadores_cliente');
	campoTextoValidador('nif','DNI',$datos,'input-small validaDNI obligatorio','trabajadores_cliente');
	campoFechaNacimiento($datos);
	campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small','trabajadores_cliente');
	campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',$datos,'input-small','trabajadores_cliente');
	campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large','trabajadores_cliente');
	
	
	cierraColumnaCampos();

	abreColumnaCampos();
	
	campoTexto('direccion','Dirección',$datos);
	campoTexto('cp','CP',$datos,'input-small obligatorio');
	campoTexto('poblacion','Población',$datos);
	campoTexto('provincia','Provincia',$datos);
	campoCuentaBancaria($datos);
	campoSelect('bic','Entidad bancaria',array('BANESTO','THE ROYAL BANK OF SCOTLAND PLC, SUCURSAL EN ESPAÑA','AHORRO CORPORACION FINANCIERA, S.A., SOCIEDAD DE VALORES','BANCO ALCALA, S.A.','ARESBANK, S.A.','BANCA PUEYO, S.A.','KUTXABANK, S.A.','BANCO BPI, S.A., SUCURSAL EN ESPAÑA','ING BELGIUM, S.A., SUCURSAL EN ESPAÑA','BANCO BILBAO VIZCAYA ARGENTARIA, S.A.','BANCO DE CRÉDITO SOCIAL COOPERATIVO S.A.','CAJA RURAL DE CASTILLA-LA MANCHA, S.C.C.','BANCO COOPERATIVO ESPAÑOL, S.A.','NOVO BANCO, S.A., SUCURSAL EN ESPAÑA','BANCO MEDIOLANUM, S.A.','BANKINTER, S.A.','BANKOA, S.A.','BANCA MARCH, S.A.','BANQUE MAROCAINE COMMERCE EXTERIEUR INTERNATIONAL, S.A.','THE BANK OF TOKYO-MITSUBISHI UFJ, LTD, SUCURSAL EN ESPAÑA','BANCO DO BRASIL AG, SUCURSAL EN ESPAÑA','BANCO DE SABADELL, S.A.','BANCO SANTANDER, S.A.','CREDIT AGRICOLE CORPORATE AND INVESTMENT BANK, SUCURSAL EN ESPAÑA','RBC INVESTOR SERVICES ESPAÑA, S.A.','ABANCA CORPORACIÓN BANCARIA, S.A.','BANKIA, S.A.','CAIXABANK, S.A.','CM CAPITAL MARKETS BOLSA, SOCIEDAD DE VALORES, S.A.','CAJA DE ARQUITECTOS, S.C.C.','IBERCAJA BANCO, S.A.','BANCO CAMINOS, S.A.','CAJAS RURALES UNIDAS, S.C.C.','CAIXA DE CREDIT DELS ENGINYERS - CAJA DE CREDITO DE LOS INGENIEROS, S.C.C.','CAJA DE AHORROS Y M.P. DE ONTINYENT','LIBERBANK, S.A.','COLONYA - CAIXA DESTALVIS DE POLLENSA','BANCO DE CASTILLA-LA MANCHA, S.A. /CAJA DE AHORROS DE CASTILLA-LA MANCHA','CECABANK, S.A.','CATALUNYA BANC, S.A.','BANCO CAIXA GERAL, S.A.','CITIBANK ESPAÑA, S.A.','CAJA LABORAL POPULAR, C.C.','CREDIT SUISSE AG, SUCURSAL EN ESPAÑA','BANCO DE CAJA ESPAÑA DE INVERSIONES, SALAMANCA Y SORIA, S.A.','CAJASUR BANCO, S.A.','DEXIA SABADELL, S.A.','BANCO DE ESPAÑA','BANCO ESPIRITO SANTO DE INVESTIMENTO, S.A., SUCURSAL EN ESPAÑA','EVO BANCO S.A.U.','BANCO FINANTIA SOFINLOC, S.A.','BANCO MARE NOSTRUM, S.A.','GVC GAESCO VALORES, SOCIEDAD DE VALORES, S.A.','SOCIEDAD DE GESTION DE LOS SISTEMAS DE REGISTRO, COMPENSACION Y LIQUIDACION DE VALORES, S.A.U.','INSTITUTO DE CREDITO OFICIAL','ING BANK, N.V., SUCURSAL EN ESPAÑA','INVERSEGUROS, SOCIEDAD DE VALORES, S.A.','BANCO INVERSIS, S.A.','SOCIEDAD ESPAÑOLA DE SISTEMAS DE PAGO, S.A.','INTERMONEY VALORES, SOCIEDAD DE VALORES, S.A.','LINK SECURITIES, SOCIEDAD DE VALORES, S.A.','BANCO DE MADRID, S.A.','BME CLEARING, S.A.','MAPFRE INVERSION, SOCIEDAD DE VALORES, S.A.','MERRILL LYNCH CAPITAL MARKETS ESPAÑA, S.A., SOCIEDAD DE VALORES','BANCO DE LA NACION ARGENTINA, SUCURSAL EN ESPAÑA','NATIXIS, S.A., SUCURSAL EN ESPAÑA','TARGOBANK, S.A.','POPULAR BANCA PRIVADA, S.A.','BANCOPOPULAR-E, S.A.','BANCO POPULAR ESPAÑOL, S.A.','COOPERATIEVE CENTRALE RAIFFEISEN- BOERENLEENBANK B.A. (RABOBANK NEDERLAND), SUCURSAL EN ESPAÑA','EBN BANCO DE NEGOCIOS, S.A.','BANCO PASTOR, S.A.','RENTA 4 BANCO, S.A.','RENTA 4 SOCIEDAD DE VALORES, S.A.','UBI BANCA INTERNATIONAL, S.A., SUCURSAL EN ESPAÑA','UNICAJA BANCO, S.A.','SOCIEDAD RECTORA BOLSA VALORES DE BARCELONA, S.A., S.R.B.V.','SOCIEDAD RECTORA BOLSA DE VALORES DE BILBAO, S.A., S.R.B.V.','SOCIEDAD RECTORA BOLSA VALORES DE VALENCIA, S.A., S.R.B.V.','BARCLAYS BANK PLC, SUCURSAL EN ESPAÑA','BARCLAYS BANK, S.A.','BNP PARIBAS ESPAÑA, S.A.','BNP PARIBAS FORTIS, S.A., N.V., SUCURSAL EN ESPAÑA','BNP PARIBAS SECURITIES SERVICES, SUCURSAL EN ESPAÑA','BNP PARIBAS, SUCURSAL EN ESPAÑA','CITIBANK INTERNATIONAL LTD, SUCURSAL EN ESPAÑA','COMMERZBANK AKTIENGESELLSCHAFT, SUCURSAL EN ESPAÑA','DEUTSCHE BANK, S.A.E.','FINANDUERO, SOCIEDAD DE VALORES, S.A.','HSBC BANK PLC, SUCURSAL EN ESPAÑA','HYPOTHEKENBANK FRANKFURT AG., SUCURSAL EN ESPAÑA','PORTIGON AG, SUCURSAL EN ESPAÑA','SOCIETE GENERALE, SUCURSAL EN ESPAÑA','UBS BANK, S.A.'),
						array('BAEMESM1XXX','ABNAESMMXXX','AHCFESMMXXX','ALCLESMMXXX','AREBESMMXXX','BAPUES22XXX','BASKES2BXXX','BBPIESMMXXX','BBRUESMXXXX','BBVAESMMXXX','BCCAESMMXXX','BCOEESMM081','BCOEESMMXXX','BESMESMMXXX','BFIVESBBXXX','BKBKESMMXXX','BKOAES22XXX','BMARES2MXXX','BMCEESMMXXX','BOTKESMXXXX','BRASESMMXXX','BSABESBBXXX','BSCHESMMXXX','BSUIESMMXXX','BVALESMMXXX','CAGLESMMVIG','CAHMESMMXXX','CAIXESBBXXX','CAPIESMMXXX','CASDESBBXXX','CAZRES2ZXXX','CCOCESMMXXX','CCRIES2AXXX','CDENESBBXXX','CECAESMM045','CECAESMM048','CECAESMM056','CECAESMM105','CECAESMMXXX','CESCESBBXXX','CGDIESMMXXX','CITIES2XXXX','CLPEES2MXXX','CRESESMMXXX','CSPAES2L108','CSURES2CXXX','DSBLESMMXXX','ESPBESMMXXX','ESSIESMMXXX','EVOBESMMXXX','FIOFESM1XXX','GBMNESMMXXX','GVCBESBBETB','IBRCESMMXXX','ICROESMMXXX','INGDESMMXXX','INSGESMMXXX','INVLESMMXXX','IPAYESMMXXX','IVALESMMXXX','LISEESMMXXX','MADRESMMXXX','MEFFESBBXXX','MISVESMMXXX','MLCEESMMXXX','NACNESMMXXX','NATXESMMXXX','POHIESMMXXX','POPIESMMXXX','POPLESMMXXX','POPUESMMXXX','PRABESMMXXX','PROAESMMXXX','PSTRESMMXXX','RENBESMMXXX','RENTESMMXXX','UBIBESMMXXX','UCJAES2MXXX','XBCNESBBXXX','XRBVES2BXXX','XRVVESVVXXX','BPLCESMMXXX','BARCESMMXXX','BNPAESMZXXX','GEBAESMMBIL','PARBESMHXXX','BNPAESMHXXX','CITIESMXSEC','COBAESMXTMA','DEUTESBBASS','CSSOES2SFIN','MIDLESMXXXX','EHYPESMXXXX','WELAESMMFUN','SOGEESMMAGM','UBSWESMMNPB'),$datos);
	
	
	campoSelect('medioConocimiento','Medio conocimiento',array('Google', 'Facebook', 'Twitter', 'Foro del Guardia Civil', 'Amigo', 'Antiguo alumno', 'Otro'),array('Google', 'Facebook', 'Twitter', 'Foro del Guardia Civil', 'Amigo', 'Antiguo alumno', 'Otro'),$datos);	
	campoRadio('activo','Baja',$datos,'SI',array('Si','No'),array('NO','SI'));

	cierraColumnaCampos();
	
	echo "<h4 class='apartadoFormulario sinFlotar'>Observaciones</h4>";
	abreColumnaCampos('span11');
	areaTexto('observacionesTrabajador','Observaciones',$datos);
	cierraColumnaCampos();

	if($datos){
	echo "<h4 class='apartadoFormulario sinFlotar'>Contratos</h4>";
	abreColumnaCampos('span11');
	tablaContratos($datos);
	cierraColumnaCampos();
	}

	cierraVentanaGestion('index.php',true);
	
	return $datos;
}

function tablaContratos($datos){
	echo"	
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCentros'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Curso </th>
							<th> Fecha alta </th>
							<th> Importe </th>
							<th> Pago inicial </th>
							<th> Pendiente </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$consulta=consultaBD("SELECT * FROM contratos WHERE codigoAlumno=".$datos['codigo'],true);				  
		while($contrato=mysql_fetch_assoc($consulta)){
			$curso=datosRegistro('cursos',$contrato['codigoCurso']);
			$importe = $contrato['tipoPlazo'] == 'SI' ? 'Total: '.$contrato['importeCurso'].' €' : 'Mensual: '.$contrato['importeCurso'].' €';
			$pagoInicial = $contrato['pagoInicial']=='' ? '0 €':$contrato['pagoInicial'].' €';
			$totalPendiente=consultaBD("SELECT SUM(vencimientos_facturas.importe) AS importe FROM vencimientos_facturas WHERE codigoFactura IN(SELECT codigo FROM facturacion WHERE codigoContrato='".$contrato['codigo']."') AND vencimientos_facturas.cobrado='NO';",true,true);
			if(!isset($totalPendiente['importe'])){
				$totalPendiente['importe']=$contrato['importeCurso']-$contrato['pagoInicial'];
			}
		 	$style="style='color:black'";
			if($contrato['baja'] == 'SI'){
				$style="style='color:red'";
			}
		 	echo "<tr ".$style.">
		 			<td>".$curso."</td>
		 			<td>".formateaFechaWeb($contrato['fecha'])."</td>
		 			<td>".$importe."</td>
		 			<td>".$pagoInicial."</td>
		 			<td>".number_format((float)$totalPendiente['importe'], 2, ',', '')." €</td>
				</tr>";
		}
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
		 	";
}

function campoFechaNacimiento($datos){
	if(!$datos){
		campoFecha('fechaNacimiento','Fecha nacimiento','0000-00-00');
	}
	else{
		campoFecha('fechaNacimiento','Fecha nacimiento',$datos);
	}
}

//Hago una función personalizada de comprobación de datos porque existe la opción de "duplicar", en cuyo caso el campoOculto no se crea (para hacer una inserción)
function compruebaDatosTrabajador(){
	if(isset($_REQUEST['codigo']) && !isset($_GET['duplicar'])){
		$datos=datosRegistro('trabajadores_cliente',$_REQUEST['codigo']);
		campoOculto($datos);
	}
	elseif(isset($_REQUEST['codigo']) && isset($_GET['duplicar'])){
		$datos=datosRegistro('trabajadores_cliente',$_REQUEST['codigo']);
	}
	else{
		$datos=false;
	}
	return $datos;
}

function filtroAlumnos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(2,'Nombre');
	campoTexto(0,'Apellido 1');
	campoTexto(1,'Apellido 2');
	

	cierraColumnaCampos();
	abreColumnaCampos();
	campoDNI(3,'DNI');
	campoFecha(4,'Fecha de nacimiento');

	cierraColumnaCampos();
	cierraCajaBusqueda();

}


function consultaDuplicidadTrabajador(){
	$res='OK';
	$datos=arrayFormulario();

	$nombre=limpiaCadena($datos['nombre']);
	$apellido1=limpiaCadena($datos['apellido1']);
	$apellido2=limpiaCadena($datos['apellido2']);
	$whereNombre='';
	if($nombre!='' || $apellido1!='' || $apellido2!=''){
		$whereNombre="(nombre='$nombre' AND apellido1='$apellido1' AND apellido2='$apellido2')";
	}

	$nif=$datos['nif'];
	$whereNif='';
	if($nif!=''){
		$whereNif="OR nif='$nif'";
	}

	$whereCodigo='';
	if($datos['codigo']!='NO'){
		$whereCodigo='AND codigo!='.$datos['codigo'];
	}


	if($whereNombre!='' || $whereNif!=''){
		$consulta=consultaBD("SELECT nombre, apellido1, apellido2 FROM trabajadores_cliente WHERE ($whereNombre $whereNif) $whereCodigo",true,true);
		if($consulta['nombre'] != ''){
			$res=$consulta['nombre'].' '.$consulta['apellido1'].' '.$consulta['apellido2'].' de la empresa '.$consulta['razonSocial'];
		} 
	}
	
	echo $res;
}

function campoCuentaBancaria($valor){
	$valor=compruebaValorCampo($valor,'numCuenta');
	echo '<div class="control-group">                     
	  <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
	  <div class="controls numSS">
		<div class="input-prepend input-append">
		  <input type="text" tabla="trabajadores_cliente" class="input-mini" id="iban" name="iban" maxlength="4" size="4" value="'.substr($valor, 0, 4).'">
		  <span class="add-on">/</span>
		  <input type="text" class="input-medium" tabla="trabajadores_cliente" id="numCuenta" name="numCuenta" maxlength="20" size="20" value="'.substr($valor, 4, 24).'">
		</div>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->';
}

function cargarDatosCurso(){
	$datos=arrayFormulario();
	if(isset($datos['codigo'])){
		$datosAlumno=datosRegistro('trabajadores_cliente',$datos['codigo']);
		campoTextoSimbolo('importeCurso','Importe',"<i class='icon-euro'></i>",$datosAlumno);
		campoTexto('plazosCurso','Plazos',$datosAlumno,'input-small');
	}elseif(isset($datos['curso']) && $datos['curso']!='NULL'){
		$datosCurso=datosRegistro('cursos',$datos['curso']);
		campoTextoSimbolo('importeCurso','Importe',"<i class='icon-euro'></i>",$datosCurso['precio']);
		campoTexto('plazosCurso','Plazos',$datosCurso['plazos'],'input-small');
	}else{
		campoTextoSimbolo('importeCurso','Importe',"<i class='icon-euro'></i>");
		campoTexto('plazosCurso','Plazos','','input-small');
	}
}

function borrarDuplicados(){
	$alumnos=consultaBD('SELECT * FROM trabajadores_cliente ORDER BY nombre, apellido1, apellido2',true);
	$nombre='';
	$nombreAux='';
	$codigo='';
	while($alumno=mysql_fetch_assoc($alumnos)){
		$contrato=datosRegistro('contratos',$alumno['codigo'],'codigoAlumno');
		$nombreAux=$alumno['nombre'].' '.$alumno['apellido1'].' '.$alumno['apellido2'];
		if($nombre == $nombreAux){
			//echo $nombre.'- Codigo a Borrar: '.$alumno['codigo'].' - Contrato a modificar: '.$contrato['codigo'].'<br/>';
			echo 'UPDATE contratos SET codigoAlumno='.$codigo.' WHERE codigo='.$contrato['codigo'].';<br/>';
			echo 'DELETE FROM trabajadores_cliente WHERE codigo='.$alumno['codigo'].';<br/>';
		}
		$nombre=$alumno['nombre'].' '.$alumno['apellido1'].' '.$alumno['apellido2'];
		$codigo=$alumno['codigo'];
	}
}

function listadoAlumnosRevisar(){
	$alumnos=consultaBD('SELECT * FROM trabajadores_cliente ORDER BY nombre, apellido1, apellido2',true);
	while($alumno=mysql_fetch_assoc($alumnos)){
		$nombre=$alumno['nombre'].' '.$alumno['apellido1'].' '.$alumno['apellido2'].'<br/>';
		echo $nombre;
	}
}

//Fin parte de trabajadores