<?php
  $seccionActiva=11;
  include_once('../cabecera.php');
    
  operacionesPresupuestos();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Presupuestos Emitidos/Aceptados</h6>
                  <canvas id="graficoBarras" class="chart-holder" width="500" height="250"></canvas>
                  <span class="grafico-2 grafico4"></span>Emitidos: <span id="valor1"></span>
                  <span class="grafico-2 grafico5"></span>Aceptados: <span id="valor2"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="gestion.php" class="shortcut noAjax"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo Presupuesto</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


      <div class="span12">        
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Presupuestos registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Referencia </th>
                  <th> Empresa </th>
                  <th> Fase de Presupuesto </th>
                  <th> Comercial </th>
                  <th> Total </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

                <?php
                  imprimePresupuestos();
                ?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content --> 
        </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>

<script type="text/javascript">
  <?php
    $datosGrafico=generaDatosGraficoVentas();
    $datosEscala=escalaGraficoComercial($datosGrafico['max']);
  ?>

  var barChartData = {
    labels: [""],
    datasets: [
      {
          fillColor: "rgba(220,220,220,0.5)",
          strokeColor: "rgba(220,220,220,1)",
          data: [<?php echo $datosGrafico['totales']; ?>]
      },
      {
          fillColor: "rgba(151,187,205,0.5)",
          strokeColor: "rgba(151,187,205,1)",
          data: [<?php echo $datosGrafico['confirmados']; ?>]
      }
    ]
  }

  var opciones={
    scaleOverride : true,
    scaleSteps :  <?php echo $datosEscala['max']; ?>,
    scaleStepWidth : <?php echo $datosEscala['escala']; ?>, //Si el número de incidencias es menor que 10, la escala del gráfico va de 1 en 1, sino de 5 en 5.
    scaleStartValue : 0,
    barValueSpacing:60,
    barDatasetSpacing:20
  }

  var myLine = new Chart(document.getElementById("graficoBarras").getContext("2d")).Bar(barChartData,opciones);

  $("#valor1").text("<?php echo $datosGrafico['totales']; ?>");
  $("#valor2").text("<?php echo $datosGrafico['confirmados']; ?>");
</script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/eventoGrafico.js"></script>


<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>