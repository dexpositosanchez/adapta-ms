var fila=0;
function insertaFila(){//Función para la tabla de "Metas"
    //Clono la última fila de la tabla
    var $tr = $('#tablaAlumnos').find("tbody tr:last").clone();
    //Obtengo el atributo name para los inputs y selects
    $tr.find("input,select").attr("name", function()
    {
     //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
     var parts = this.id.match(/(\D+)(\d*)$/);
     //Creo un nombre nuevo incrementando el número de fila (++parts[2])
     return parts[1] + ++parts[2];
    //Hago lo mismo con los IDs
    }).attr("id", function(){
     var parts = this.id.match(/(\D+)(\d*)$/);
     return parts[1] + ++parts[2];
    });

    $tr.find("input[type=text]").attr("value","");//Quito los valores que tuvieran las casillas de esa fila

    //Añado la nueva fila a la tabla
    $('#tablaAlumnos').find("tbody tr:last").after($tr);

    fila++;
}

function eliminaFila(){
  if($('#tablaAlumnos').find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    fila--;
    $('#tablaAlumnos').find("tbody tr:last").remove();
  }
}