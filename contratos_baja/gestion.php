<?php
  $seccionActiva=8;
  include_once("../cabecera.php");
  $i=gestionContratos();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/filasTablaGastos.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/iban.js"></script>
<script type="text/javascript" src="../js/calculaBic.js"></script>

<script type="text/javascript">
<?php
	if($i==0){
		$j=$i;
	}else{
		$j=$i-1;
	}
	echo "var filaAux=$j;";
?>
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});


	//Parte de validación y comprobación de duplicado
	$(':submit').attr('id','enviar').prop('type','button');//Le pongo un ID al submit y le cambio el tipo, para que no salte primero por validador.js
	//Fin parte de validación y comprobación de duplicado
	var i=0;
	while($('#gasto'+i).val()!=undefined){
		$('#gasto'+i).change(function(){
			var selector=$(this).attr('id').charAt($(this).attr('id').length-1);
			oyentePrecio(selector);
		});
		i++;
	}
	$("#numCuenta").focusout(function() {
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			updateBic(value);
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	  }).blur(function(){
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			updateBic(value);
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	});
	if($("#codigo").length) {
		var codigoCurso=$('#codigoCurso').val();
		var codigo=$('#codigo').val();
		var factura=$('#factura').val();				
		listadoDatos(codigoCurso,codigo,factura);
	}else{
		var codigoCurso=$('#codigoCurso').val();
		listadoDatos(codigoCurso);
	}

	$('#codigoCurso').change(function(){
		var codigoCurso=$('#codigoCurso').val();
		listadoDatos(codigoCurso,'false');
		comprobarObligatorio(codigoCurso);
	});

	oyentePlazo($('input[name=tipoPlazo]:checked').val());
		$('input[name=tipoPlazo]').change(function(){
			oyentePlazo($(this).val());
		});
	
	ordenarSelect('bic');
});

function comprobarObligatorio(curso){
	if(curso == 12 || curso == 13){
		$('#pagoInicial').addClass('obligatorio');
		var label=$('#pagoInicial').parent().parent().prev();
		var texto=label.text();
		label.html('<span class="asterisco">*</span> '+texto);
	} else {
		$('#pagoInicial').removeClass('obligatorio');
		var label=$('#pagoInicial').parent().parent().prev();
		label.html('Pago inicial');
	}
}

function oyentePlazo(val){
		if(val=='NO'){
			$('label[for=importeCurso]').html('Importe mensual');
			$('.divTipoPlazo').css('display','none');
		} else {
			$('label[for=importeCurso]').html('Importe total');
			$('.divTipoPlazo').css('display','block');
		}
	}

function compruebaTrabajadorDuplicado(){
	var nombre=$('#nombre').val();
	var apellido1=$('#apellido1').val();
	var apellido2=$('#apellido2').val();
	var nif=$('#nif').val();
	
	var codigo=$('#codigo').val();//En la creación será undefined. Sirve para que no dé un falso positivo con el propio registro
	if(codigo==undefined){
		codigo='NO';
	}

	var consulta=$.post('../listadoAjax.php?include=alumnos&funcion=consultaDuplicidadTrabajador();',{'nombre':nombre,'apellido1':apellido1,'apellido2':apellido2,'nif':nif,'codigo':codigo});
	consulta.done(function(respuesta){
		if(respuesta=='OK' || (respuesta!='OK' && confirm('Los datos introducidos ya existen en el trabajador '+respuesta+'. ¿Desea continuar?'))){
			validaCamposObligatorios();
		}
	});
}

function listadoDatos(codigoCurso,codigo,factura){
	var parametros = {
			"curso" : codigoCurso,
			"codigo" : codigo,
			"factura" : factura
	};
	$.ajax({
		 type: "POST",
		 url: "../listadoAjax.php?include=contratos&funcion=cargarDatosCurso();",
		 data: parametros,
		  beforeSend: function () {
			   $("#datosCursoOculto").html('<center><i class="icon-refresh icon-spin"></i></center>');
		  },
		 success: function(response){
			   $("#datosCursoOculto").html(response);
			   oyentePlazo($('input[name=tipoPlazo]:checked').val());
			   $('input[name=tipoPlazo]').change(function(){
					oyentePlazo($(this).val());
				});
			   var plazos=$('#plazosCurso').val();
			   if(plazos > 0){
			   	$('#formaPago').val('Recibo domiciliado');
			   	$('#formaPago').selectpicker('refresh');
			   }
		 }
	});
}

function ordenarSelect(id_componente){
	var selectToSort = jQuery('#' + id_componente);
	var optionActual = selectToSort.val();
	selectToSort.html(selectToSort.children('option').sort(function (a, b) {
		return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
	})).val(optionActual);
	$('#'+id_componente).selectpicker('refresh');
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>