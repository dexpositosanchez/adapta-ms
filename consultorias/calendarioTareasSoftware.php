<?php
  $seccionActiva=12;
  include_once('../cabecera.php');

  if(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('tareasSoftware');
  }
?>

<!-- /subnavbar -->
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      
      <div class="row">
        <div class="span12">
          <center><a href='index.php' class='enlaceVolver'>Volver</a></center>
          <br />

          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-laptop"></i>
               <h3>Agenda de tareas de proyectos</h3>
               <?php botonesCalendarioSoftware(); ?>
            </div>
            <div class="widget-content">
              <div id='calendario'></div>
            </div>
          </div>
        </div>

        <br />
        <center><a href='index.php' class='enlaceVolver'>Volver</a></center>
        
      </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<!-- Caja de reserva -->

<div id='cajaTarea' class='modal hide fade cajaSelect'> 
  <div class='modal-header'> 
    <a class='close' id='cierra'>&times;</a>
    <h3> <i class="icon-calendar"></i> &nbsp; Nueva tarea </h3> 
  </div> 
  <div class='modal-body cajaSelect'>
    
    <form id="edit-profile" class="form-horizontal" method="post">
      <fieldset>
        <?php
          campoSelectProyectoCalendario();
          campoTexto('tarea','Tarea','','span4');
          campoSelectConsulta('codigoUsuario','Técnico',"SELECT codigo, CONCAT(apellidos,', ',nombre) AS texto FROM usuarios WHERE tipo='TECNICO' AND codigo!='11' AND codigo!='14' ORDER BY apellidos;",false,'selectpicker span3 show-tick','');
        ?>
      </fieldset>
    </form>

  </div> 
  <div class='modal-footer'> 
    <button type="button" class="btn btn-primary" id="registrar"><i class="icon-ok"></i> Registrar tarea</button> 
    <button type="button" class="btn" id='cancelar'><i class="icon-remove"></i> Cancelar</button> 
  </div> 
</div>

<!-- Fin caja de reserva -->

<!-- Caja de opciones -->

<div id='cajaOpciones' class='modal hide fade'> 
  <div class='modal-header'> 
    <a class='close' data-dismiss='modal'>&times;</a>
    <h3> <i class="icon-calendar"></i> &nbsp; Opciones de tarea </h3> 
  </div> 
  <div class='modal-body centro'>
    <a href='#' id='enlace' class="btn btn-primary noAjax" target="_blank"><i class="icon-search-plus"></i> Detalles de tarea</a> 
    <button type="button" class="btn btn-danger" id='eliminarTarea'><i class="icon-trash"></i> Eliminar tarea</button>
  </div> 
</div>

<!-- Fin caja de opciones -->


</div>

<?php include_once('../pie.php'); ?>

<script type="text/javascript" src="../../api/js/full-calendar/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="../../api/js/full-calendar/fullcalendar.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript">

$(document).ready(function() {
  $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');
      var d=new Date(e.date.valueOf());
      $('#calendario').fullCalendar('gotoDate', d);
  });

  var colorFondo={'CREACION':"#7acc76",'DEMO':"#7acc76",'INCIDENCIA':"#F22C2C",'OTROS':"#5e96ea",'MODIFICACION':"#f89406"};
  var colorBorde={'CREACION':"#00a100",'DEMO':"#00a100",'INCIDENCIA':"#610B0B",'OTROS':"#3f85f5",'MODIFICACION':"#c67605"};
  var tipos={'CREACION':'Creación','DEMO':'Personalización','MODIFICACION':'Modificaciones y/o mejoras','INCIDENCIA':'Corrección de errores','OTROS':'Otros'};
  var estado={'1':'<i class="icon-flag"></i>','2':'<i class="icon-time"></i>'};

  if($.cookie('diaElegidoCalendarioCRMQMA')!=undefined){
    var ultimaFecha=$.cookie('diaElegidoCalendarioCRMQMA');
    ultimaFecha=ultimaFecha.split('/');
    ultimaFecha[1]--;//El formato de mes que acepta fullcalendar empieza en 0
  }
  else{
    var fecha=new Date();
    var ultimaFecha=[fecha.getDate(),fecha.getMonth(),fecha.getFullYear()];
  }

  //Inicialización del calendario
  var calendario = $('#calendario').fullCalendar({
    //Formateo de calendario y eventos
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,resourceDay'
    },
    defaultView: 'agendaWeek',
    axisFormat: 'H:mm',
    minTime:8,
    maxTime:22,
    firstHour:7,
    weekends:false,
    allDaySlot:false,
    slotMinutes:30,
    height: 1600,
    firstDay:1,
    timeFormat: '-',
    //Funciones de renderizado
    year:ultimaFecha[2],
    month:ultimaFecha[1],
    date:ultimaFecha[0],
    viewRender: function(view, element){
      fechaActual=$.fullCalendar.formatDate(view.start, 'dd/MM/yyyy');
      $.cookie('diaElegidoCalendarioCRMQMA', fechaActual);
    },
    eventRender: function (event, element) {
      element.find('.fc-event-head').html(event.cabecera);
    },
    //Definición de acciones
    selectable:true,
    selectHelper: true,
    editable:true,
    select: function(start, end, allDay, event, resourceId) {
      abreVentana(resourceId);
      $('#registrar').click(function(){          
         
          var tarea=$('#tarea').val();
          var codigoSoftware=$('select[name=codigoSoftware]').val();
          var nombreProyecto=$('select[name=codigoSoftware] option[value='+codigoSoftware+']').text()
          var codigoUsuario=$('select[name=codigoUsuario]').val();

          cierraVentana();

          var fecha = $.fullCalendar.formatDate(start, 'dd/MM/yyyy');
          var horaI = $.fullCalendar.formatDate(start, 'HH:mm');
          var horaF = $.fullCalendar.formatDate(end, 'HH:mm');
          
          

          //Creación-renderización del evento
          var creacion=$.post("creaTareaCalendarioSoftware.php", {tarea: tarea, codigoSoftware: codigoSoftware, fecha:fecha, horaI: horaI, horaF: horaF,codigoUsuario: codigoUsuario});
          creacion.done(function(datos){
            datos=datos.split('-');//Formato: codigoTarea - estado - tipoTarea

            calendario.fullCalendar('renderEvent',{
                id: datos[0],
                resourceId: parseInt(codigoUsuario),
                codigoSoftware:codigoSoftware,
                cabecera: estado[datos[1]]+" - "+tipos[datos[2]],
                title: nombreProyecto+'. '+tarea,
                start: start,
                end: end,
                allDay: false,
                backgroundColor: colorFondo[datos[2]],
                borderColor: colorBorde[datos[2]]
              },
              true
            );

            calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
          });
          //Fin creación-renderización
          

        });
      calendario.fullCalendar('unselect');
    },

    eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {//Actualización del evento cuando se modifica tu tamaño (duración)
      var fecha = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');

      var creacion=$.post("actualizaTareaCalendarioSoftware.php", {codigo: event._id, codigoUsuario:event.resourceId, fecha:fecha, horaI: horaI, horaF: horaF});
      creacion.done(function(datos){
        calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
      });
    },
    eventDrop: function (event, dayDelta, minuteDelta) {//Actualización del evento cuando se mueve de hora
      var fecha = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');

      var creacion=$.post("actualizaTareaCalendarioSoftware.php", {codigo: event._id, fecha:fecha, codigoUsuario:event.resourceId, horaI: horaI, horaF: horaF});
      creacion.done(function(datos){
        calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
      });
    },
    eventClick:function(evento){
      abreVentanaOpciones(evento);
    },
    titleFormat: {
      month: 'MMMM yyyy',
      week: "d MMM [ yyyy]{ '&#8212;' d [ MMM] yyyy}",
      day: 'dddd, d MMM, yyyy'
    },
    columnFormat: {
      month: 'ddd',
      week: 'ddd d/M',
      day: 'dddd d/M'
    },
    //Volcado de eventos
    <?php
      generaEventosCalendarioSoftware();
    ?>
  });

});

  //Inicializaciones y oyentes
  $('#cajaTarea').find('select').selectpicker();

  $('#cancelar, #cierra').click(function(){
    cierraVentana();
  });


  //Funciones para el manejo de eventos
  function abreVentana(usuario){
    $('select[name=codigoUsuario] option[value='+usuario+']').attr('selected',true);

    $('#cajaTarea').find('select[name=codigoSoftware]').selectpicker('refresh');
    $('#cajaTarea').find('select[name=codigoUsuario]').selectpicker('refresh');
    $('#tarea').val('');
    $('#cajaTarea').modal({'show':true,'backdrop':'static','keyboard':false});
    creaPopOvers();
  }

  function cierraVentana(){
    $('#registrar').unbind();//Necesario para que no se acumulen clicks en el botón de envío al AJAX.
    $('#cajaTarea').modal('hide');
  }


  function abreVentanaOpciones(evento){
    $('#enlace').attr('href','gestion.php?codigo='+evento.codigoSoftware);
    $('#eliminarTarea').attr('onclick','eliminarTarea('+evento.id+')');
    $('#cajaOpciones').modal({'show':true,'backdrop':'static','keyboard':false});
  }


  function creaPopOvers(){
    var i=0;
    $('#codigoSoftware').next().find('li').each(function(){
      $(this).popover({//Crea el popover
        title: 'Descripción',
        content: $('#observaciones'+i).text(),
        placement:'bottom'
      });
      i++;
    })
  }

  function eliminarTarea(codigoEvento){
    var valoresChecks=[];

    valoresChecks['codigo0']=codigoEvento;
    valoresChecks['elimina']='SI';
    creaFormulario('calendarioTareasSoftware.php',valoresChecks,'post');
  }

</script><!-- /Calendar -->