<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de objetivos

function operacionesObjetivos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaObjetivos();
	}
	elseif(isset($_POST['area'])){
		$res=insertaObjetivos('objetivos');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('objetivos');
	}

	mensajeResultado('area',$res,'Objetivos');
    mensajeResultado('elimina',$res,'Objetivos', true);
}

function insertaObjetivos(){
	$res=insertaDatos('objetivos');
	$_POST['codigo'] = $res;
	$res = $res && insertaMetas();
	return $res;
}

function actualizaObjetivos(){
	$res=actualizaDatos('objetivos');
	$res = $res && insertaMetas();
	return $res;
}

function insertaMetas(){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM metas WHERE codigoObjetivo='".$_POST['codigo']."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['definicionMeta'.$i]) && $datos['definicionMeta'.$i]!=""){
		$consulta = "INSERT INTO metas VALUES (NULL,'".$datos['definicionMeta'.$i]."','".$datos['fechaLimiteMeta'.$i]."','".$datos['responsableMeta'.$i]."','".$datos['recursosMeta'.$i]."','".$datos['conseguidaMeta'.$i]."','".$_POST['codigo']."');";
		$res = $res && consultaBD($consulta);

		$i++;
	}

	return $res;
}

function imprimeObjetivos(){
	global $_CONFIG;

	$consulta=consultaBD("SELECT codigo, definicion, conseguido FROM objetivos ORDER BY fechaLimite DESC;",true);
	$iconoC=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
		<tr>
        	<td> ".$datos['definicion']." </td>
        	<td class='centro'> ".$iconoC[$datos['conseguido']]." </td>
        	<td class='td-actions'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-zoom-in'></i> Detalles</i></a>
        	</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function gestionObjetivos(){
	operacionesObjetivos();

	abreVentanaGestion('Gestión de Objetivos','index.php');
	$datos=compruebaDatos('objetivos');

	campoSelect('area','Area',array('Calidad','Medioambiente','Prevención','Comercial'),array('CALIDAD','MEDIO','PREVENCIÓN','COMERCIAL'),$datos);
	campoTexto('definicion','Definición del objetivo',$datos,'span6');
	campoTexto('valorPartida','Valor de partida',$datos,'input-small');
	campoTexto('valorEsperado','Valor esperado',$datos,'input-small');
	campoTexto('formula','Fórmula de cálculo',$datos);
	campoFecha('fechaLimite','Fecha límite',$datos);
	campoRadio('conseguido','¿Conseguido?',$datos);

	echo "<br/><h3 class='apartadoFormulario'> Metas/Hitos a llevar a cabo para conseguir el objetivo</h3>";
	echo "<fieldset class='sinFlotar'>";

	$i=tablaMetas($datos);

	echo "</fieldset>";

	cierraVentanaGestion('index.php');
}

//Fin parte de objetivos

function generaDatosGraficoObjetivos(){
    $datos=array();

    conexionBD();

    $consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM objetivos WHERE conseguido='SI';");
    $consulta=mysql_fetch_assoc($consulta);

    $datos['conseguidos']=$consulta['codigo'];


    $consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM objetivos WHERE conseguido='NO';");
    $consulta=mysql_fetch_assoc($consulta);

    $datos['noConseguidos']=$consulta['codigo'];

    cierraBD();

    return $datos;
}

function tablaMetas($datos){
	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaMetas'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Definición </th>
							<th> Fecha límite </th>
							<th> Responsable </th>
							<th> Recursos </th>
							<th> ¿Conseguida? </th>
							<th> Eliminar</th>
                    	</tr>
                  	</thead>
                  	<tbody>";

    $i=0;
    $j=1;
	if($datos){
		$datosMetas=obtenerMetas($datos['codigo']);				  
		while($meta=mysql_fetch_assoc($datosMetas)){
		 	echo "<tr>";
				campoTextoTabla('definicionMeta'.$i,$meta['definicionMeta']);
				campoFechaTabla('fechaLimiteMeta'.$i,$meta['fechaLimiteMeta']);
				campoTextoTabla('responsableMeta'.$i,$meta['responsableMeta']);
				campoTextoTabla('recursosMeta'.$i,$meta['recursosMeta']);
				campoRadioTabla('conseguidaMeta'.$i,$meta['conseguidaMeta']);
			echo"
					<td style='text-align:center;'>
						<input class='prod' type='checkbox' name='filasTabla[]' value=$j>
		        	</td>
				</tr>";
			$i++;
			$j++;
		}
	}

	if($i==0 || !$datos){
	 	echo "<tr>";
			campoTextoTabla('definicionMeta'.$i,'');
			campoFechaTabla('fechaLimiteMeta'.$i);
			campoTextoTabla('responsableMeta'.$i,'');
			campoTextoTabla('recursosMeta'.$i,'');
			campoRadioTabla('conseguidaMeta'.$i);
		echo"
				<td style='text-align:center;'>
					<input class='prod' type='checkbox' name='filasTabla[]' value=$j>
		        </td>
			</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaMetas\");'><i class='icon-plus'></i> Añadir</button> 
	              <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaMetas\");'><i class='icon-minus'></i> Eliminar</button> 
	            </center>
		 	";
			
	return $i;
}

function obtenerMetas($codigo){
	$consulta=consultaBD("SELECT * FROM metas WHERE codigoObjetivo='$codigo';",true);
	return $consulta;
}

function campoRadioSinPuntos($nombreCampo,$valor='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){//MODIFICACIÓN 09/04/2015 NO RETROCOMPATIBLE: añadido el parámetro $valorPorDefecto | MODIFICACIÓN 05/01/2015: añadido el parámetro $salto | MODIFICACIÓN 21/07/2014 DE OFICINA: $valor antes que textos. $campos, $textosCampos y $valoresCampos arrays con misma longitud
    if($valor!=''){
        $valor=compruebaValorCampo($valor,$nombreCampo);
    }
    else{
        $valor=$valorPorDefecto;
    }

    echo "
    <div class='control-group'>                     
      <div class='nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='$nombreCampo' id='$nombreCampo' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function campoRadioTabla($nombreCampo,$valor=false){
    echo "<td>";
        campoRadioSinPuntos($nombreCampo,$valor,'NO',array('Si','No'),array('SI','NO'),false);
    echo "</td>";
}