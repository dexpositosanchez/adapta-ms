<?php
  $seccionActiva=15;
  include_once("../cabecera.php");
  gestionEncuesta();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-rating-input.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
  });

  function muestra(id){
    if($('#'+id).css("display")=="none"){
      $('#'+id).css("display","block");
    }
    else{
      $('#'+id).css("display","none");
    }
  }
</script>
<?php include_once('../pie.php'); ?>