<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de documentación

function operacionesDocumentacion(){
	$res=true;

	if(isset($_FILES['ficheroInterno'])){
    	$res=insertaDatos('documentosInternos',time(),'../documentos/documentacion/internos');
  	}
    if(isset($_FILES['ficheroExterno'])){
        $res=insertaDatos('documentosExternos',time(),'../documentos/documentacion/externos');
    }
    if(isset($_FILES['ficheroRegistro'])){
        $res=insertaDatos('documentosRegistros',time(),'../documentos/documentacion/registros');
    }
    if(isset($_FILES['ficheroDistribucion'])){
        $res=insertaDatos('documentosDistribucion',time(),'../documentos/documentacion/distribucion');
    }
  	elseif(isset($_GET['codigo']) && isset($_GET['eliminarInterno'])){
    	$res=eliminaDocumentacion($_GET['codigo'],'documentosInternos','ficheroInterno','../documentos/documentacion/internos');
  	}
    elseif(isset($_GET['codigo']) && isset($_GET['eliminarExterno'])){
        $res=eliminaDocumentacion($_GET['codigo'],'documentosExternos','ficheroExterno','../documentos/documentacion/externos');
    }
    elseif(isset($_GET['codigo']) && isset($_GET['eliminarRegistro'])){
        $res=eliminaDocumentacion($_GET['codigo'],'documentosRegistros','ficheroRegistro','../documentos/documentacion/registros');
    }
    elseif(isset($_GET['codigo']) && isset($_GET['eliminarDistribucion'])){
        $res=eliminaDocumentacion($_GET['codigo'],'documentosDistribucion','ficheroDistribucion','../documentos/documentacion/distribucion');
    }

	if(isset($_FILES['ficheroInterno'])||isset($_FILES['ficheroExterno'])||isset($_FILES['ficheroRegistro'])|| isset($_FILES['ficheroDistribucion'])){
        if($res){
            mensajeOk("Documento registrado correctamente.");
        }
        else{
            mensajeError("no se han podido registrar el Documento. Compruebe su tamaño.");
        }
    }
    elseif(isset($_GET['codigo']) && (isset($_GET['eliminarInterno']) || isset($_GET['eliminarExterno']) || isset($_GET['eliminarRegistro']) || isset($_GET['eliminarDistribucion']))){
        if($res){
            mensajeOk("Documento eliminado correctamente.");
        }
        else{
            mensajeError("no se han podido eliminar el Documento. Contacte con el webmaster.");
        }
    }
}

function imprimeDocumentacion($tipo){
	global $_CONFIG;
	$codigoUsuario=$_SESSION['codigoU'];

    if($tipo == 'internos'){
        imprimeDocumentosInternos();
    }
    elseif($tipo == 'externos'){
        imprimeDocumentosExternos();
    }
    elseif($tipo == 'registros'){
        imprimeDocumentosRegistros();
    }
    elseif($tipo == 'distribucion'){
        imprimeDocumentosDistribucion();
    }
}

function imprimeDocumentosInternos(){
    conexionBD();
    $consulta=consultaBD("SELECT * FROM documentosInternos ORDER BY codigo DESC;");
    cierraBD();

    $datos=mysql_fetch_assoc($consulta);
    while($datos!=0){
        $fecha=formateaFechaWeb($datos['fecha']);
        $revision=formateaFechaWeb($datos['fechaRevision']);
        echo "
        <tr>
            <td> ".$datos['codigoDocumento']." </td>
            <td> ".$datos['nombreDocumento']." </td>
            <td> $revision </td>
            <td> $fecha </td>
            <td class='centro'>
                <a href='../documentos/documentacion/internos/".$datos['ficheroInterno']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>
                <a href='index.php?codigo=".$datos['codigo']."&eliminarInterno' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>
            </td>
        </tr>";
        $datos=mysql_fetch_assoc($consulta);
    }
}

function imprimeDocumentosExternos(){
    conexionBD();
    $consulta=consultaBD("SELECT * FROM documentosExternos ORDER BY codigo DESC;");
    cierraBD();

    $datos=mysql_fetch_assoc($consulta);
    while($datos!=0){
        echo "
        <tr>
            <td> ".$datos['codigoDocumento']." </td>
            <td> ".$datos['nombreDocumento']." </td>
            <td> ".$datos['ubicacion']." </td>
            <td class='centro'>
                <a href='../documentos/documentacion/externos/".$datos['ficheroExterno']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>
                <a href='index.php?codigo=".$datos['codigo']."&eliminarExterno' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>
            </td>
        </tr>";
        $datos=mysql_fetch_assoc($consulta);
    }
}

function imprimeDocumentosRegistros(){
    conexionBD();
    $consulta=consultaBD("SELECT * FROM documentosRegistros ORDER BY codigo DESC;");
    cierraBD();

    $datos=mysql_fetch_assoc($consulta);
    while($datos!=0){
        echo "
        <tr>
            <td> ".$datos['codigoDocumento']." </td>
            <td> ".$datos['nombreDocumento']." </td>
            <td> ".$datos['responsable']." </td>
            <td> ".$datos['ubicacion']." </td>
            <td> ".$datos['tiempoConservacion']." </td>
            <td class='centro'>
                <a href='../documentos/documentacion/registros/".$datos['ficheroRegistro']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>
                <a href='index.php?codigo=".$datos['codigo']."&eliminarRegistro' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>
            </td>
        </tr>";
        $datos=mysql_fetch_assoc($consulta);
    }
}

function imprimeDocumentosDistribucion(){
    conexionBD();
    $consulta=consultaBD("SELECT * FROM documentosDistribucion ORDER BY codigo DESC;");
    cierraBD();
    $copia=array('ORIGINAL'=>'Original','CONTROLADO'=>'Controlado','NOCONTROLADO'=>'No Controlado');
    $datos=mysql_fetch_assoc($consulta);
    while($datos!=0){
        $fecha=formateaFechaWeb($datos['fecha']);
        echo "
        <tr>
            <td> $fecha </td>
            <td> ".$datos['nombreDocumento']." </td>
            <td> ".$datos['ubicacion']." </td>
            <td> ".$copia[$datos['copia']]." </td>
            
            <td class='centro'>
                <a href='../documentos/documentacion/distribucion/".$datos['ficheroDistribucion']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>
                <a href='index.php?codigo=".$datos['codigo']."&eliminarDistribucion' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>
            </td>
        </tr>";
        $datos=mysql_fetch_assoc($consulta);
    }
}
//Fin parte de documentación

function creaNombreFichero($nombreFichero){
	$extension = end(explode(".", $nombreFichero));
	return str_replace(".".$extension, "", $nombreFichero);
}

function eliminaDocumentacion($codigo,$tabla,$campo='fichero',$ruta=false){
    $res=true;

    conexionBD();

    if(!!$ruta){
        $imagen=consultaBD("SELECT * FROM $tabla WHERE codigo='".$codigo."';",false, true);
        if($imagen[$campo] != '' && $imagen[$campo] != 'NO'){
            unlink($ruta."/".$imagen[$campo]);
        }
    }
    $consulta=consultaBD("DELETE FROM $tabla WHERE codigo=".$codigo.";");
    if(!$consulta){
        $res=false;
        echo mysql_error();
    }
    cierraBD();

    return $res;
}