<?php
  $seccionActiva=4;
  include_once('../cabecera.php');

  $datos=datosCorreo($_GET['codigo']);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Detalles de correo electrónico</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal">
                  <fieldset>
                    
                    <div class="control-group">                     
                      <label class="control-label" for="email">Remitente:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="email" name="email" value="<?php echo $datos['remitente']; ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="usuario">Usuario:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="usuario" name="usuario" value="<?php echo $datos['usuario']; ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fecha">Fecha:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="fecha" name="fecha" value="<?php echo formateaFechaWeb($datos['fecha']); ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="hora">Hora:</label>
                      <div class="controls">
                        <input type="text" class="input-mini" id="hora" name="hora" value="<?php echo formateaHoraWeb($datos['hora']); ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="detinatarios">Destinatario/s:</label>
                      <div class="controls">
                        <input type="text" class="span5" id="destinatarios" name="destinatarios" value="<?php echo $datos['destinatarios']; ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="asunto">Asunto:</label>
                      <div class="controls">
                        <input type="text" class="span5" id="asunto" name="asunto" value="<?php echo $datos['asunto']; ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                     

                    <div class="control-group">                     
                      <label class="control-label" for="mensajeCorreo"></label>
                      <div class="controls">
                        <div id="mensajeCorreo" class="conBorde"><?php echo $datos['mensaje']; ?></div>
                      </div>
                    </div>

                    <?php compruebaAdjuntos($datos['adjunto']); ?>
                  
                    <div class="form-actions">
                      <a href="<?php echo $_CONFIG['raiz']; ?>correos/index.php" class="btn btn-default"><i class="icon-arrow-left"></i> Volver</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                  
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>
