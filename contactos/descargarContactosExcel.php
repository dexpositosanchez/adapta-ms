<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/plantillaContactos.xlsx");
	
	//COMIENZO DE TODOS LAS FACTURAS COMPRENDIDAS
	//conexionBD();
	$tiempo=time();
	$j=2;
	$consultaAux=consultaBD("SELECT * FROM contactos",true);
	while($datosLinea=mysql_fetch_assoc($consultaAux)){
		$objPHPExcel->getActiveSheet()->getCell('A'.$j)->setValue($datosLinea['nombre'].$datosLinea['apellido1'].$datosLinea['apellido2']);
		$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue($datosLinea['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue($datosLinea['cargo']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$j)->setValue($datosLinea['telefono1']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$j)->setValue($datosLinea['email1']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$j)->setValue($datosLinea['programa']);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':F'.$j)->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'FFEB9C')
				)
			)
		);
		 $objPHPExcel->getActiveSheet()->getStyle('A'.$j.':F'.$j)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$j++;
	}
		
	//cierraBD();

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	
	$objWriter->save('../documentos/ListadoContactos'.$tiempo.'.xlsx');

	/*
	// Definir headers
	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=Facturacion-".$tiempo.".zip");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Facturacion-'.$tiempo.'.zip');*/

	
	
	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=ListadoContactos".$tiempo.".xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentos/ListadoContactos'.$tiempo.'.xlsx');
?>