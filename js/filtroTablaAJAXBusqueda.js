function listadoTabla(tabla,rutaListado){
  $(tabla).dataTable({
    'bProcessing': true, 
    'bServerSide': true,
    "stateSave":false,
    "aLengthMenu":[ 25, 50, 100, -1 ],
    'sAjaxSource': rutaListado,
    "iDisplayLength":25,
    "aoColumnDefs":[{'bSortable':false, 'aTargets':[-1]}],
    "oLanguage": {
      "sLengthMenu": "_MENU_ registros por página",
      "sSearch":"Búsqueda global:",
      "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
      "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
      "sEmptyTable":"Aún no hay datos que mostrar",
      "sInfoEmpty":"",
      'sInfoFiltered':"",
      'sZeroRecords':'No se han encontrado coincidencias',
      'sProcessing':'<i class="icon-spinner icon-spin"></i> Procesando...'
    },
    "fnDrawCallback": function (oSettings) {
      $(tabla).find('.bloqueado').parent().parent().parent().addClass('filaBloqueada');
      $(tabla).find('.pendiente').parent().parent().parent().addClass('filaPendiente');
      
        $('.enlacePopOver').each(function(){
          $(this).popover({//Crea el popover
            title: 'Descripción de incidencia',
            content: $(this).next().html().replace(/[\r\n|\n]/g,'<br />'),
            placement:'right',
            thisrigger:'manual'
          });

          $('.enlacePopOver').click(function(e){
            e.preventDefault();
            $('.enlacePopOver').not($(this)).popover('hide');
            $(this).popover('show');
           
          });
        });
      
      creaSumatoriosPie(tabla);
      compruebaOyentesVencimientos(tabla);
      compruebaOyentesConsultorias(tabla);
    }
  });
}

$('.dataTables_wrapper').find('.dataTables_paginate a').each(function(){
  $(this).addClass('noAjax');
});


//Función que detecta las columnas con la clase sumatorio, y les crea abajo una celda con el importe total de los valores en esa columna
function creaSumatoriosPie(tabla){
  //Obtención de sumatorios de columnas
  if($(tabla).find('.sumatorio').length>0){//Si hay columnas de sumatorio...
    var celdasSumatorio=new Array();//Para almacenar los índices de las columnas con sumatorio
    var totalColumna=new Array();//Para almacenar los totales de las columnas con sumatorio

    $(tabla).find('.sumatorio').each(function(){
      var indice=$(this).index();
      celdasSumatorio.push(indice);//Guardo el número de columna (empezando en 0) de cada celda con la clase sumatorio

      $(tabla).find('td:nth-child('+(indice+1)+')').each(function(){//Recorro todas las celdas en la columna especificada y almaceno sus importes. Le sumo 1 porque nth-child empieza en 1
          if($(this).parent().find('.bloqueado').html()==undefined){//Compruebo que la fila actual no tenga la clase bloqueado, en cuyo caso sus importes no deben entrar en el sumatorio
            var importe=$(this).text().replace('.','');
            importe=importe.replace(',','.');
            importe=importe.replace(' €','');
            if(importe==''){
               importe=0;
            }

            if(totalColumna[indice]==undefined){
              totalColumna[indice]=parseFloat(importe);
            }
            else{
              totalColumna[indice]+=parseFloat(importe); 
            }
          }
      });
    });



    //Creción de la nueva fila
    var nuevaFila='<tr class="filaPie">';
    
    for(i=0;i<$(tabla).find('tr:last td').length;i++){
        nuevaFila+='<td';

        if($.inArray(i,celdasSumatorio)>-1){//Si la celda actual pertenece a la columna de la clase sumatorio...
          nuevaFila+=' class="celdaSumatorio">';//..le añado la clase y el valor correspondiente
          
          var importe=totalColumna[i].toFixed(2).toString();
          importe=importe.replace('.',',');
          
          nuevaFila+=importe;
          nuevaFila+=' €';
        }
        else{
          nuevaFila+='>';
        }

        nuevaFila+='</td>';
    }

    nuevaFila+='</tr>';
    
    $(tabla+' tr:last').after(nuevaFila);
    //Fin creación nueva fila

  }
  //Fin obtención de sumatorios de columnas
}

function compruebaOyentesVencimientos(tabla){
  if(tabla=='#tablaListadoVencimientos'){
    $(tabla).find(':checkbox').click(function(){
      oyenteChecksVencimientos($(this));//En /vencimientos/index.php
    });
  }
}

//Parte de consultoría
function compruebaOyentesConsultorias(tabla){
  if(tabla=='#tablaConsultorias'){
    
   colocaIndicadores();

    $('.mostrarServicios').click(function(){
        var id=$(this).attr('tabla');//Atributo personalizado
        var estado=$(this).attr('estado');//Atributo personalizado
        var textoBoton=$(this).html();

        if(estado=='oculto'){
          $(id).slideDown();
          textoBoton=textoBoton.replace('down','up');
          textoBoton=textoBoton.replace('Mostrar','Ocultar');
          $(this).html(textoBoton);
          $(this).attr('estado','mostrado');

          //Estilos
          $(this).css('-webkit-border-radius','0');
          $(this).css('-moz-border-radius','0');
          $(this).css('border-radius','0');

          $(this).parent().css('padding','0');//Celda contenedora
        }
        else{
          $(id).slideUp();
          textoBoton=textoBoton.replace('up','down');
          textoBoton=textoBoton.replace('Ocultar','Mostrar');
          $(this).html(textoBoton);
          $(this).attr('estado','oculto');

          //Estilos
          $(this).css('-webkit-border-radius','4px');
          $(this).css('-moz-border-radius','4px');
          $(this).css('border-radius','4px');

          $(this).parent().css('padding','8px');//Celda contenedora
        }
    });
  }
}

function colocaIndicadores(){
   $('.badge-danger').each(function(){
      var alertas=$(this).parent().parent().find('.label-danger').length;//Cuento el número de fechas pasadas
      if(alertas==0){//Si son 0, oculto el indicador
        $(this).addClass('hide');
      }
      else{
        $(this).text(alertas);//Si hay al menos 1 fecha pasada, muestro el indicador
      }
    });
}

//Fin parte de consultoría