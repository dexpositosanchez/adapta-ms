<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común


  	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('facturacion',$_GET['codigo']);
	$datosContrato=datosRegistro('contratos',$datos['codigoContrato']);
	$datosVenta=datosRegistro('presupuestos',$datos['codigoVenta']);
	$datosPrograma=datosRegistro('programas',$datosContrato['codigoPrograma']);
	$cliente=datosRegistro('clientes',$datosVenta['empresa']);
	$datosAlumno=datosRegistro('trabajadores_cliente',$datosContrato['codigoAlumno']);	
	
	//Carga de la librería PHPWord
	require_once '../../api/phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	if($datos['bonificada']=='SI'){
		$document = $PHPWord->loadTemplate('../documentos/facturas/plantillaFacturaBonificada.docx');
	}else{
		$document = $PHPWord->loadTemplate('../documentos/facturas/plantillaFacturaSinBonificar.docx');
	}

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	if($datos['origen'] == 'CONTRATO' && $datos['bonificada']=='NO'){
		$document->setValue("num",utf8_decode($datos['referencia']));
		$document->setValue("empresa",utf8_decode($datosAlumno['nombre'].' '.$datosAlumno['apellido1'].' '.$datosAlumno['apellido2']));
		$document->setValue("cif",utf8_decode($datosAlumno['nif']));
		$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fechaEmision'])));
		$document->setValue("mov",utf8_decode($datosAlumno['movil']));
		$document->setValue("fijo",utf8_decode($datosAlumno['telefono']));
		$document->setValue("email",utf8_decode($datosAlumno['email']));
		$document->setValue("web",utf8_decode($datosAlumno['website']));
		$document->setValue("direccion",utf8_decode($datosAlumno['direccion']));
		$document->setValue("programa",utf8_decode($datosPrograma['nombre']));
		$document->setValue("modalidad",utf8_decode($datosPrograma['modalidad']));
		$document->setValue("horas",utf8_decode($datosPrograma['horasLectivas']));
		$document->setValue("horario",utf8_decode($datosPrograma['horario']));
		$document->setValue("dias",utf8_decode($datosPrograma['diasPrevistos']));
		$document->setValue("subtotal",utf8_decode($datosContrato['importeCurso']));
		$cantidadIva=$datosContrato['importeCurso']*0.21;
		$document->setValue("iva",utf8_decode($cantidadIva));
		$totalFact=$datosContrato['importeCurso']+$cantidadIva;
		$document->setValue("total",utf8_decode($totalFact));		
	}else if($datos['origen'] == 'VENTA' && $datos['bonificada']=='NO'){
		$document->setValue("num",utf8_decode($datos['referencia']));
		$document->setValue("empresa",utf8_decode($cliente['denominacionSocial']));
		$document->setValue("cif",utf8_decode($cliente['nif']));
		$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fechaEmision'])));
		$document->setValue("mov",utf8_decode($cliente['telefono1']));
		$document->setValue("fijo",utf8_decode($cliente['telefono2']));
		$document->setValue("email",utf8_decode($cliente['email1']));
		$document->setValue("web",utf8_decode($cliente['website1']));
		$document->setValue("direccion",utf8_decode($cliente['direccion1']));
		$document->setValue("subtotal",utf8_decode($datosVenta['subtotal']));
		$cantidadIva=$datosVenta['subtotal']*0.21;
		$document->setValue("iva",utf8_decode($cantidadIva));
		$totalFact=$datosVenta['subtotal']+$cantidadIva;
		$document->setValue("total",utf8_decode($totalFact));				
	}else if($datos['origen'] == 'CONTRATO' && $datos['bonificada']=='SI'){
		$document->setValue("num",utf8_decode($datos['referencia']));
		$document->setValue("empresa",utf8_decode($datosAlumno['nombre'].' '.$datosAlumno['apellido1'].' '.$datosAlumno['apellido2']));
		$document->setValue("cif",utf8_decode($datosAlumno['nif']));
		$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fechaEmision'])));
		$document->setValue("direccion",utf8_decode($datosAlumno['direccion']));
		$document->setValue("subtotal",utf8_decode($datosContrato['importeCurso']));
		$cantidadIva=$datosContrato['importeCurso']*0.21;
		$document->setValue("iva",utf8_decode($cantidadIva));
		$totalFact=$datosContrato['importeCurso']+$cantidadIva;
		$document->setValue("total",utf8_decode($totalFact));		
	}else if($datos['origen'] == 'VENTA' && $datos['bonificada']=='SI'){
		$participa='';
		$i=0;
		$consultaPart=consultaBD("SELECT nombre, apellidos FROM participantes_factura WHERE codigoFactura='".$datos['codigo']."';",true);
		while($datosPart=mysql_fetch_assoc($consultaPart)){
			$participa.=$datosPart['nombre'].' '.$datosPart['apellidos'].'-';
			$i++;
		}

		$document->setValue("num",utf8_decode($datos['referencia']));
		$document->setValue("empresa",utf8_decode($cliente['denominacionSocial']));
		$document->setValue("cif",utf8_decode($cliente['nif']));
		$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fechaEmision'])));
		$document->setValue("direccion",utf8_decode($cliente['direccion1']));		
		$document->setValue("numParti",utf8_decode($i));
		$document->setValue("participantes",utf8_decode($participa));
		$document->setValue("subtotal",utf8_decode($datosVenta['subtotal']));
		$cantidadIva=$datosVenta['subtotal']*0.21;
		$document->setValue("iva",utf8_decode($cantidadIva));
		$totalFact=$datosVenta['subtotal']+$cantidadIva;
		$document->setValue("total",utf8_decode($totalFact));
	}

	
	/*if($datos['codigoVenta'] != NULL){
		$document->setValue("num",utf8_decode($datos['referencia']));
		$document->setValue("empresa",utf8_decode($cliente['denominacionSocial']));
		$document->setValue("cif",utf8_decode($cliente['nif']));
		$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fechaEmision'])));
		$document->setValue("telfMovil",utf8_decode($cliente['telefono1']));
		$document->setValue("telf",utf8_decode($cliente['telefono2']));
		$document->setValue("email",utf8_decode($cliente['email1']));
		$document->setValue("web",utf8_decode($cliente['website1']));
		$document->setValue("direccion",utf8_decode($cliente['direccion1']));
	}else{
		$document->setValue("num",utf8_decode($datos['referencia']));
		$document->setValue("empresa",utf8_decode($datosAlumno['nombre'].' '.$datosAlumno['apellido1'].' '.$datosAlumno['apellido2']));
		$document->setValue("cif",utf8_decode($datosAlumno['nif']));
		$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fechaEmision'])));
		$document->setValue("telfMovil",utf8_decode($datosAlumno['movil']));
		$document->setValue("telf",utf8_decode($datosAlumno['telefono']));
		$document->setValue("email",utf8_decode($datosAlumno['email']));
		$document->setValue("web",utf8_decode($datosAlumno['website1']));
		$document->setValue("direccion",utf8_decode($datosAlumno['direccion']));		
	}*/


	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$document->save('../documentos/facturas/Factura'.$datos['codigo'].'.docx');


	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Factura".$datos['codigo'].".docx");
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('../documentos/facturas/Factura'.$datos['codigo'].'.docx');


?>