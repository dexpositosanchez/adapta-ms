<?php
  session_start();// Es necesario ponerlo ANTES DE IMPRIMIR CONTENIDO EN EL NAVEGADOR.
  include_once("funciones.php");


  $titulos=array('Cuestionario de Satisfacción de Clientes');
?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Adapta MS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

 <link href="<?php echo $_CONFIG['raiz']; ?>css/bootstrap.min.php" rel="stylesheet" />
 <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/bootstrap-responsive.css" rel="stylesheet" />
 <link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
 <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/font-awesome.css" rel="stylesheet" />
 <link href="<?php echo $_CONFIG['raiz']; ?>css/style.php" rel="stylesheet /">

 <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/datepicker.css" rel="stylesheet" />
 <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/bootstrap-select.css" rel="stylesheet" />
 <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/bootstrap-wysihtml5.css" rel="stylesheet" />
 
 <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/animate.css" rel="stylesheet" type="text/css" />

 <link href="<?php echo $_CONFIG['raiz']; ?>../../api/js/guidely/guidely.css" rel="stylesheet" />

 <!-- Script HTML5, para el soporte del mismo en IE6-8 -->
 <!--[if lt IE 9]>
       <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
     <![endif]-->


 <!-- Común -->
 <script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery-1.7.2.min.js" type="text/javascript"></script>
 <script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.js" type="text/javascript"></script>
 <script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-datepicker.js" type="text/javascript"></script>
 <script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/controladorAJAX.js" type="text/javascript"></script>
 <!-- Fin común -->

<!-- Script HTML5, para el soporte del mismo en IE6-8 -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<meta name="google-translate-customization" content="e9f98e7be4eb5188-5eafebbb97469dfa-g7344405143381f20-29"></meta>
</head>
<body>
<div class="navbar navbar-fixed-top cabeceraCuestionario">
  <div class="navbar-inner">
    <div class="container"> 
      
      <span class="logo2">
        <img src="../img/logo2.png" alt="Adatpa MS"> - Cuestionario de Satisfacción de Clientes
      </span>

    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  
</div>
<!-- /subnavbar -->