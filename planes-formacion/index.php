<?php
  $seccionActiva=15;
  include_once('../cabecera.php');

  operacionesPlanes();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Planes de Formación</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="<?php echo $_CONFIG['raiz']; ?>recursos-humanos" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
                <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo plan</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


      <div class="span12">
        
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Planes de Formación registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Contenido </th>
					          <th> Docentes </th>
                    <th> Fecha Real </th>
                    <th> Evaluación Eficacia de la Acción </th>
                    <th class="td-actions"> </th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimePlanes();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<?php include_once('../pie.php'); ?>
