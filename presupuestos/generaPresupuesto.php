<?php
	session_start();
	include_once('funciones.php');
	compruebaSesion();

	//global $_CONFIG;

	conexionBD();
	$datos=datosRegistro('presupuestos',$_GET['codigo']);
	$datosCliente=datosRegistro('clientes',$datos['empresa']);
	$datosConceptos=obtieneConceptosPresupuestos($_GET['codigo']);
	cierraBD();

	$contenido = "
	<style type='text/css'>
	<!--
		*{
			color:#333;
			font-family: Arial;
		}

		.datosCliente{
			border:2px solid #428bca;
			position:relative;
			padding:10px;
			line-height:14px;
			font-size:10px;
			font-weight: lighter;
			left:15%;
			width:45%;
			border-radius:6px;
		}
		.cajaDatosPresupuesto{
			margin-top:20px;
			line-height:18px;
		}
		.tablaConceptos{
			margin-top:20px;
			width:100%;
			border:1px solid #428bca;
			border-spacing: 0;
			border-collapse: collapse;
		}
		.tablaConceptos th{
			border:1px solid #428bca;
			padding:10px;
			font-weight:lighter;
		}
		.colExtremo{
			width:25%;
		}
		.colCentral{
			width:50%;
		}
		thead th{
			color: #357ebd;
			background-color: #D4DFEE;
			text-align:center;
			text-transform:uppercase;
		}
		.tablaConceptos td{
			border-right:1px solid #428bca;
			padding:10px;
		}
		.tablaConceptos .primeraCol{
			border-left:1px solid #428bca;
		}
		.colPrecio{
			text-align:right
		}
		.total td{
			text-align:right;
			border:1px solid #428bca;
			color: #357ebd;
			background-color: #D4DFEE;
			font-weight:bold;
		}
		.ultimoConcepto td{
			border-bottom:1px solid #428bca;
		}

		.cajaPie{
			width:100%;
			border:1px solid #428bca;
			text-align:center;
			font-size:10px;
			color:#357ebd;
			padding:10px;
		}

		.cajaCondiciones{
			margin-top:20px;
		}

		.tablaAcepto{
			margin-top:60px;
			width:100%;
		}
		.tablaAcepto .primeraCol{
			width:20%;
		}
		.tablaAcepto .segundaCol{
			width:50%;
			border-bottom:1px dotted #428bca;
		}
		.notaFirma{
			color:#DDD;
			font-weight: lighter;
			font-size:10px;
		}
		.ultimaFila td{
			border-bottom:1px solid #428bca;
		}
	-->
	</style>
	<page>
		<img src='../img/logo3.png' />
		<div class='datosCliente'>
			Nombre: <strong>".$datosCliente['nombre']."</strong><br />
			NIF/CIF: ".$datosCliente['nif']."<br />
			Dirección: ".$datosCliente['direccion1']."<br />
			Teléfono: ".formateaTelefono($datosCliente['telefono1'])."<br />
			eMail: 
		</div>
		<div class='cajaDatosPresupuesto'>
			Presupuesto nº: ".$datos['referenciaPresupuesto']."<br />
			Fecha: ".formateaFechaWeb($datos['fechaValidez'])."
		</div>
		<table class='tablaConceptos'>
			<thead>
				<tr>
					<th colspan='3'>PRODUCTOS</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th class='colExtremo'>Código producto</th>
					<th class='colCentral'>Descripción</th>
					<th class='colExtremo'>Precio</th>
				</tr>

				".$datosConceptos."
			</tbody>
		</table>

		<br /><br /><br />
		<div class='cajaCondiciones'>
			Descripción: <br />
			".$datos['descripcion']."
		</div>
		<br />
		<table class='tablaAcepto'>
			<tr>
				<td class='primeraCol'>Acepto presupuesto:</td>
				<td class='segundaCol'></td>
			</tr>
			<tr>
				<td></td>
				<td class='notaFirma'>(Firma y sello)</td>
			</tr>
		</table>

		<page_footer>
	        <div class='cajaPie'>
	           CISOL, SL &#8226; Av. San Fco. Javier, Ed. Sevilla 2, planta 4 Mod 14. &#8226; 41018 Sevilla &#8226; 955 09 74 53 &#8226; direccion@qmaconsultores.com
	        </div>
	    </page_footer>
	</page>";

	require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Presupuesto-'.$datos['referenciaPresupuesto'].'.pdf');
?>