<?php
  $seccionActiva=8;
  include_once("../cabecera.php");
  gestionUsuario();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('#colorTareas').colorpicker();

		oyentePerfil();
		$('#tipo').change(function(){
			oyentePerfil();
		});
	});

	function oyentePerfil(){
		var tipo=$('#tipo').val();
		if(tipo=='COMERCIAL'){
			$('#cajaDirector').addClass('hide');
			$('#cajaDelegado').removeClass('hide');
			$('#cajaComercial').removeClass('hide');
			$('#cajaTelemarketing').addClass('hide');
		}
		else if(tipo=='DELEGADO'){
			$('#cajaDirector').removeClass('hide');
			$('#cajaDelegado').addClass('hide');	
			$('#cajaComercial').addClass('hide');
			$('#cajaTelemarketing').addClass('hide');
		}
		else if(tipo=='TELEMARKETING'){
			$('#cajaDirector').addClass('hide');
			$('#cajaDelegado').addClass('hide');
			$('#cajaComercial').addClass('hide');
			$('#cajaTelemarketing').removeClass('hide');
		}
		else{
			$('#cajaDirector').addClass('hide');
			$('#cajaDelegado').addClass('hide');
			$('#cajaComercial').addClass('hide');
			$('#cajaTelemarketing').addClass('hide');
		}
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>