<?php
  $seccionActiva=8;
  include_once("../cabecera.php");
  $datos=gestionAlumno();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/iban.js"></script>
<script type="text/javascript" src="../js/calculaBic.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});


	//Parte de validación y comprobación de duplicado
	$(':submit').attr('id','enviar').prop('type','button');//Le pongo un ID al submit y le cambio el tipo, para que no salte primero por validador.js
	$('#enviar').unbind();//Elimino los oyentes asignados anteriormente en validador.js
	$('#enviar').click(function(e){
		e.preventDefault();
		compruebaTrabajadorDuplicado();
	});
	//Fin parte de validación y comprobación de duplicado
	
	$("#numCuenta").focusout(function() {
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			updateBic(value);
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	  }).blur(function(){
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			updateBic(value);
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	});
	
	<?php if(isset($datos['codigo'])){ 
			echo "var codigoCurso=$('#curso').val();
			var codigo=$('#codigo').val();
			listadoDatos(codigoCurso,codigo);";
		}else{
			echo "var codigoCurso=$('#curso').val();
			listadoDatos(codigoCurso);";
		}
	?>
	$('#curso').change(function(){
		var codigoCurso=$('#curso').val();
		listadoDatos(codigoCurso);
	});
	
	ordenarSelect('bic');
});

function compruebaTrabajadorDuplicado(){
	var nombre=$('#nombre').val();
	var apellido1=$('#apellido1').val();
	var apellido2=$('#apellido2').val();
	var nif=$('#nif').val();
	
	var codigo=$('#codigo').val();//En la creación será undefined. Sirve para que no dé un falso positivo con el propio registro
	if(codigo==undefined){
		codigo='NO';
	}

	var consulta=$.post('../listadoAjax.php?include=alumnos&funcion=consultaDuplicidadTrabajador();',{'nombre':nombre,'apellido1':apellido1,'apellido2':apellido2,'nif':nif,'codigo':codigo});
	consulta.done(function(respuesta){
		if(respuesta=='OK' || (respuesta!='OK' && confirm('Los datos introducidos ya existen en el trabajador '+respuesta+'. ¿Desea continuar?'))){
			validaCamposObligatorios();
		}
	});
}

function listadoDatos(codigoCurso,codigo){
	var parametros = {
			"curso" : codigoCurso,
			"codigo" : codigo
	};
	$.ajax({
		 type: "POST",
		 url: "../listadoAjax.php?include=alumnos&funcion=cargarDatosCurso();",
		 data: parametros,
		  beforeSend: function () {
			   $("#datosCursoOculto").html('<center><i class="icon-refresh icon-spin"></i></center>');
		  },
		 success: function(response){
			   $("#datosCursoOculto").html(response);
		 }
	});
}

function ordenarSelect(id_componente){
	var selectToSort = jQuery('#' + id_componente);
	var optionActual = selectToSort.val();
	selectToSort.html(selectToSort.children('option').sort(function (a, b) {
		return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
	})).val(optionActual);
	$('#'+id_componente).selectpicker('refresh');
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>