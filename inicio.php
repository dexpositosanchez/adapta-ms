<?php
  $seccionActiva=-1;
  include_once('cabecera.php');
  defineFiltroEjercicio();//Para filtro por años
  $estadisticas=creaEstadisticasInicio();
  /*$num=crearVencimientos();
  if($num > 0){
    $mensaje = $num.' recibos nuevos generados';
    if($num == 1){
      $mensaje = $num.' recibo nuevo generado';
    }
    mensajeOk($mensaje);
  }*/
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-home"></i>
              <h3>Inicio</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Bienvenido/a <?php echo $_SESSION['usuario']; ?>. A continuación se muestra el estado del sistema:</h6>

                   <div id="big_stats" class="cf">
                      <div class="stat"> <i class="icon-group"></i> <span class="value"><?php echo $estadisticas['alumnos']; ?></span> <br />Alumno/s</div>
					  <div class="stat"> <i class="icon-book"></i> <span class="value"><?php echo $estadisticas['cursos']; ?></span> <br />Curso/s</div>
					  <div class="stat"> <i class="icon-eur"></i> <span class="value"><?php echo $estadisticas['facturas']; ?></span> <br />Facturas</div>
                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content -->

              </div>
            </div>
          </div>

        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-th"></i>
              <h3>Opciones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="javascript:void;" id='guia' class="shortcut"><i class="shortcut-icon icon-book"></i><span class="shortcut-label">Guía rápida</span> </a>
                <a href="cerrarSesion.php" class="shortcut"><i class="shortcut-icon icon-power-off"></i><span class="shortcut-label">Cerrar sesión</span> </a>
              </div>
              <!-- /shortcuts -->
            </div>
            <!-- /widget-content -->
          </div>

        </div>
      </div><!-- /row -->



      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-list"></i>
          <h3>Listado de alumnos</h3>
        </div>
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id="tablaTrabajadores">
                <thead>
                  <tr>
                    <th> Nombre </th>
                    <th> Apellido 1 </th>
                    <th> Apellido 2 </th>
                    <th> DNI </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
        </div>
      </div>


    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/guidely/guidely.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="js/oyenteBotonFiltros.js"></script>

<script type="text/javascript">

$(document).ready(function(){

  <?php
    //animacionInicio();
  ?>

  guidely.add ({
    attachTo: '#target-1'
    , anchor: 'top-left'
    , title: 'Panel de Estadísticas'
    , text: 'En el lado izquierdo de su pantalla encontrará el Panel de Estadísticas, que le servirá para ver de forma rápida y visual el estado del sistema.'
  });

  guidely.add ({
    attachTo: '#target-2'
    , anchor: 'top-right'
    , title: 'Panel de Acciones'
    , text: 'En este panel se agrupan las distintas opciones disponibles en cada sección (crear un nuevo elemento, acceder a una subsección, generar un fichero, ...).<br>Para acceder a una opción solo tiene que pulsar el botón correspondiente.'
  });

  guidely.add ({
    attachTo: '#target-3'
    , anchor: 'middle-middle'
    , title: 'Listado de Elementos'
    , text: 'Esta tabla muestra un listado en el que podrá consultar los elementos creados en cada sección. Utilice la caja de <b>Búsqueda</b> para filtrar los registros a visualizar.'
  });

  guidely.add ({
    attachTo: '#target-4'
    , anchor: 'top-right'
    , title: 'Menú principal'
    , text: 'Una vez que accede al sistema, el Menú Principal está siempre disponible para permitirle navegar entre los distintos módulos de la herramienta. Solo tiene que pulsar en la sección correspondiente para acceder a ella.'
  });

    guidely.add ({
    attachTo: '#target-5'
    , anchor: 'bottom-right'
    , title: 'Menú de Usuario'
    , text: 'Por último, el Menú de Usuario le recuerda en cada momento con que cuenta está trabajando. Además, haciendo click en él se le proporciona un acceso directo para cerrar su sesión.'
  });

  $('#guia').click(function(){
    guidely.init({welcome: true, startTrigger: false, welcomeTitle: 'Guía de uso básico', welcomeText: 'Bienvenido a la Guía de uso del sistema. Pulsando en el botón Iniciar, la plataforma le mostrará unas sencillas explicaciones sobre los distintos elementos que encontrará en la mayoría de las secciones.' });
  });
	listadoTabla('#tablaTrabajadores','listadoAjax.php?include=alumnos&funcion=listadoAlumnos();');
});

</script>

<!-- /contenido -->
</div>

<?php 
include_once('pie.php');

?>