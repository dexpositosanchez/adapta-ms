<?php
  $seccionActiva=5;
  include_once('../cabecera.php');
  
  if(isset($_POST['tipo'])){
    $res=creaIncidencia();
  }
  elseif(isset($_POST['codigo'])){
    $res=actualizaIncidencia();
  }

?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
        if(isset($_POST['tipo'])){
          if($res){
            mensajeOk("Incidencia registrada correctamente.");
          }
          else{
            mensajeError("no se han podido registrar la Incidencia. Compruebe los datos introducidos.");
          }
        }
        elseif(isset($_POST['codigo'])){
          if($res){
            mensajeOk("Incidencia actualizada correctamente.");
          }
          else{
            mensajeError("no se han podido actualizar la Incidencia. Compruebe los datos introducidos.");
          }
        }
    
        ?>


        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para las incidencias:</h6>

                  <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
                  
                  <div class="leyenda" id="leyenda"></div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Incidencias</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
                <a href="<?php echo $_CONFIG['raiz']; ?>incidencias/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva Incidencia</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


        </div><!-- /row -->

        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Incidencias registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Fecha </th>
                    <th> Área afectada </th>
                    <th> ¿Resuelta? </th>
                    <th class="td-actions"> </th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeIncidencias();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>



    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">
    <?php
      $datos=generaDatosGraficoIncidencias();
    ?>
    var pieData = [
        {
            value: <?php echo $datos['total']; ?>,
            color: "#428bca",
			label: "Incidencias"
        },
        {
            value: <?php echo $datos['resueltas']; ?>,
            color: "#6BBA70",
			label: "Resueltas"
        }
      ];

    var grafico = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
    
</script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/eventoGrafico.js"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js"></script>
