<?php
@include_once('config.php');//Carga del archivo de configuración
@include_once($_SERVER['DOCUMENT_ROOT'].'/api/nucleo.php');//Carga del núcleo de funciones comunes

//Inicio de funciones específicas

//transformaValoresPOST();//Llamada a función que debe de ejecutarse siempre que se envien parámetros por $_POST
//La siguiente función transforma todos los datos pasados por $_POST a mayúsculas, a excepción del email (que va en minúsculas) y las observaciones
function transformaValoresPOST(){
    foreach($_POST as $nombre=>$valor){
        if(substr_count($nombre,'email')==1){//Para pasar el email a minúsculas
            $_POST[$nombre]=strtolower($_POST[$nombre]);
        }
        elseif(substr_count($nombre,'observaciones')==0 && substr_count($nombre,'firma')==0
            && substr_count($nombre,'consulta')==0 && substr_count($nombre,'busqueda')==0
            && substr_count($nombre,'usuario')==0  && substr_count($nombre,'clave')==0 
            && substr_count($nombre,'camposModificados')==0 && !is_array($_POST[$nombre])){//Para cambiar a mayúsculas todo lo demás, excepto las observaciones, las consultas, las credenciales de usuario y los array

            $mayusculas=strtoupper($_POST[$nombre]);

            $mayusculas=str_replace('á','Á',$mayusculas);
            $mayusculas=str_replace('é','É',$mayusculas);
            $mayusculas=str_replace('í','Í',$mayusculas);
            $mayusculas=str_replace('ó','Ó',$mayusculas);
            $mayusculas=str_replace('ú','Ú',$mayusculas);
            $mayusculas=str_replace('Ê','Ú',$mayusculas);//Por algún motivo, la Ú mayúscula se convierte en Ê cuando ya está escrita así en el campo
            $mayusculas=str_replace('ñ','Ñ',$mayusculas);
            
            $_POST[$nombre]=$mayusculas;
        }
        
    }
}

//Función específica de login para el software de "La Academia Empresas" (comprueba que el usuario esté activo)

function loginAcademia($usuario,$clave){
    global $_CONFIG;

    conexionBD();
    $consulta=consultaBD("SELECT codigo, usuario, tipo FROM usuarios WHERE usuario='$usuario' AND clave='$clave' AND activo='SI';");
    cierraBD();
    if(mysql_num_rows($consulta)==1){
        $datos=mysql_fetch_assoc($consulta);
        $_SESSION['codigoU']=$datos['codigo'];
        $_SESSION['usuario']=$datos['usuario'];
        $_SESSION['tipoUsuario']=$datos['tipo'];
        $_SESSION['ejercicio']=date('Y');//Define por defecto el filtro de ejercicios al año actual

        if(isset($_POST['sesion']) && $_POST['sesion']=="si"){//Crea cookie de Sesión
            $valor=md5(rand());
            conexionBD();
            $consulta=consultaBD("UPDATE usuarios SET sesion='$valor' WHERE usuario='$usuario' AND clave='$clave';");
            cierraBD();
            // Caduca en un año
            setcookie($_CONFIG['nombreCookie'],$valor, time() + 365 * 24 * 60 * 60);
        }
        
        if($usuario!='soporte'){//MODIFICACIÓN 04/09/2015: para incluir el usuario "fantasma" de soporte
            registraAcceso();
        }

        header('Location: '.$_CONFIG['raiz'].'inicio.php');
    }
    return 1;
}

//Fin función específica de login


//Parte de inicio

function creaEstadisticasInicio(){
	$res=array();

	conexionBD();

	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM cursos;",false,true);
	$res['cursos']=$datos['total'];

    $anio='';
    if(isset($_SESSION['ejercicio']) && $_SESSION['ejercicio'] != 'Todos'){
        $anio=$_SESSION['ejercicio'];
        $anio="AND (facturacion.fechaEmision LIKE '".$anio."-%' OR facturacion.codigo IN (SELECT codigoFactura FROM vencimientos_facturas WHERE fecha LIKE '".$anio."-%'))";
    }
    $datos=consultaBD("SELECT COUNT(codigo) AS total FROM facturacion WHERE tipoFactura='GENERAL' $anio;",false,true);
    $res['facturas']=$datos['total'];

    $datos=consultaBD("SELECT COUNT(codigo) AS total FROM trabajadores_cliente;",false,true);
    $res['alumnos']=$datos['total'];

	cierraBD();

	return $res;
}

//Fin parte de inicio


//Parte de manejo de listados

function obtieneLimitesListado(){
	$limite='';
	if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength']!='-1') {
        $limite='LIMIT '.(int)$_GET['iDisplayStart'].', '.(int)$_GET['iDisplayLength'];
    }
    return $limite;
}

function obtieneOrdenListado($columnas){
	$orden = '';
    if (isset($_GET['iSortCol_0'])) {
        $orden = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $orden .= $columnas[ (int)$_GET['iSortCol_'.$i] ].' '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $orden = substr_replace($orden, '', -2);
        if ($orden == 'ORDER BY') {
            $orden = '';
        }
    }
    return $orden;
}

/*
    El algoritmo de creación del WHERE/HAVING de esta función es específico de "La Academia Empresas", ya que tiene búsquedas por columnas.
    El filtrado de usuario, determinado por el parámetro $compruebaPerfil, se realiza siguiendo los siguientes criterios:
        Perfil COMERCIAL, ADMINISTRACION2 y TELEMARKETING: se filtran los registros por el código del usuario actual.
        Perfil DIRECTOR y DELEGADO: se filtran los registros cuyos código de usuario dependan del que tiene el usuario actual
        Resto de perfiles: no se comprueba el usuario.
*/
function obtieneWhereListado($where,$columnas,$compruebaPerfil=false,$listadoClientes=false){
	$camposFecha=array();//Este array sirve para ir almacenando las columnas de tipo fecha, e identificar así si la condición debe ser >= ó <=

    $res=obtieneWhereEjercicioListado($columnas,$listadoClientes);

    for($i=0; $i<count($columnas) ; $i++){
        if(isset($_GET['sSearch']) && $_GET['sSearch']!='') {//Búsqueda normal (global)
            $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch']."%' USING latin1) AS binary) USING utf8)  OR  ";
        }

        if(isset($_GET['sSearch_'.$i]) && $_GET['sSearch_'.$i]!=''){//Búsqueda por columnas (nótese el índice _$i)
            if(substr_count($columnas[$i],'fecha')>0){
                $_GET['sSearch_'.$i]=formateaFechaBD($_GET['sSearch_'.$i]);
            }

            if($_GET['sSearch_'.$i]=='ISNUL'){//Para los casos de NULL
                $res.=$columnas[$i]." IS NULL AND ";
            }
            elseif($_GET['sSearch_'.$i]=='NOTNUL'){//Para los casos de NOT NULL
                $res.=$columnas[$i]." IS NOT NULL AND ";
            }
            elseif(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$camposFecha)){
                $res.=$columnas[$i].">='".$_GET['sSearch_'.$i]."' AND ";
                
                array_push($camposFecha,$columnas[$i]);
            }
            elseif(substr_count($columnas[$i],'fecha')>0){
                $res.=$columnas[$i]."<='".$_GET['sSearch_'.$i]."' AND ";
            }
            elseif(substr_count($columnas[$i],'precio')>0 || substr_count($columnas[$i],'importe')>0){//Para que la búsqueda de 9,99 coincida con el valor 9.99
                $_GET['sSearch_'.$i]=formateaNumeroWeb($_GET['sSearch_'.$i],true);
                $res.=$columnas[$i]."='".$_GET['sSearch_'.$i]."' AND ";
            }
            else{//Casos normales de cadenas (hay que hacerle conversiones de codificación de caracteres para que no haga cosas raras con tildes y ñ)
                $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch_'.$i]."%' USING latin1) AS binary) USING utf8) AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
            }
        }
    }

    $res=substr_replace($res,'',-4);
    $res.=')';

    if($res==' )'){//No se ha realizado búsquedas...
        $res='';
    }

    if($compruebaPerfil){
        $res.=filtraListadoPorUsuario();
    }

    return $where.$res;
}

function filtraListadoPorUsuario(){
    $res='';
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='COMERCIAL'){
        $res=" AND comerciales.codigoUsuarioAsociado=$codigoUsuario";
    }
    elseif($perfil=='ADMINISTRACION2'){
        $res=" AND codigoUsuario=$codigoUsuario";
    }
    elseif($perfil=='TELEMARKETING'){
        $res=" AND telemarketing.codigoUsuarioAsociado=$codigoUsuario";
    }
    elseif($perfil=='DELEGADO'){
        $res=" AND comerciales.codigo IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario)";
    }
    elseif($perfil=='DIRECTOR'){
        $res=" AND comerciales.codigo IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario IN (SELECT usuarios.codigo FROM usuarios WHERE usuarios.codigoUsuario=$codigoUsuario))";
    }


    return $res;
}

function inicializaArrayListado($consulta,$consultaPaginacion){
	$intervalo=25;
	if(isset($_GET['sEcho'])){
		$intervalo=intval($_GET['sEcho']);
	}

	$seleccionado=mysql_num_rows($consulta);
	$total=mysql_num_rows($consultaPaginacion);

	return array(
        'sEcho' => $intervalo,
        'iTotalRecords' => $seleccionado,
        'iTotalDisplayRecords' => $total,
        'aaData' => array()
    );
}

//Fin parte manejo listados


//Parte de satisfacción de clientes


function creaTablaSatisfaccion(){
    echo '
    <center>
        <table class="table table-striped table-bordered mitadAncho">
          <tbody>
            <tr>
              <th> Apartado Analizado </th>
              <th> Valoración </th>
            </tr>';
}

function cierraTablaSatisfaccion(){
    echo '</tbody>
        </table>
    </center>';
}

function creaPreguntaTablaEncuesta($texto,$numero,$datos=false){    
    echo "
    <tr>
        <td class='justificado'>$texto</td>
        <td class='centro'>
            <input type='number' name='pregunta".$numero."' id='pregunta".$numero."' class='rating' data-min='1' data-max='5'";
    
    if($datos!=false){      
        echo "value=".$datos['pregunta'.$numero];       
    }

    echo" />
        </td>
    </tr>";
}

function creaAreaTextoTablaEncuesta($datos=false){
    $valor=compruebaValorCampo($datos,'comentarios');
    echo "
    <tr>
        <td colspan='2'>Si desea realizar algún comentario adicional, por favor, hágalo a continuación:</td>
    </tr>
    <tr>
        <td colspan='2'>
            <textarea name='comentarios' class='textarea-amplia' id='comentarios'>$valor</textarea>
        </td>
    </tr>";
}

function textoCuestionario(){
    global $_CONFIG;

    echo "
    <p class='justificado parrafo-margenes'>
      <span class='negritaCursiva'>Estimado cliente:</span><br />en ".$_CONFIG['tituloGeneral']." nos preocupa mucho su satisfacción con los servicios
      contratados con nuestra entidad y la calidad final de los proyectos ejecutados. Por ello, le agradecemos que pueda 
      hacernos llegar la presente encuesta de satisfacción, indicando además en el apartado observaciones cualquier 
      comentario que crea que pueda ayudarnos a mejorar la calidad de nuestros servicios o a ajustar los mismos a sus 
      necesidades. Sin duda, creemos firmemente que en sus opiniones y sugerencias se encuentran nuestras mayores 
      oportunidades de mejorar.<br />
      Gracias por anticipado.<br /><br />
      <strong>Puntúe de en una escala del 1 al 5 cada uno de los siguientes aspectos:</strong>
    </p>";
}

//Fin parte de satisfacción de clientes

//Funciones comunes en varias secciones

function campoSelectProvincia($valor=false,$nombreCampo='provincia',$texto='Provincia',$tipo=0,$obligatorio='obligatorio'){
    $provincias=array('','ÁLAVA','ALBACETE','ALICANTE','ALMERÍA','ÁVILA','BADAJOZ','ILLES BALEARS','BARCELONA','BURGOS','CÁCERES','CÁDIZ','CASTELLÓN DE LA PLANA','CIUDAD REAL','CÓRDOBA','A CORUÑA','CUENCA','GIRONA','GRANADA','GUADALAJARA','GUIPÚZCOA','HUELVA','HUESCA','JAÉN','LEÓN','LLEIDA','LA RIOJA','LUGO','MADRID','MÁLAGA','MURCIA','NAVARRA','OURENSE','ASTURIAS','PALENCIA','LAS PALMAS','PONTEVEDRA','SALAMANCA','SANTA CRUZ DE TENERIFE','CANTABRIA','SEGOVIA','SEVILLA','SORIA','TARRAGONA','TERUEL','TOLEDO','VALENCIA','VALLADOLID','VIZCAYA','ZAMORA','ZARAGOZA','CEUTA','MELILLA');
    campoSelect($nombreCampo,$texto,$provincias,$provincias,$valor,"selectpicker $obligatorio span3 show-tick",'data-live-search="true"',$tipo);
}

function formateaPrecioBDD($indice){
    $_POST[$indice]=formateaNumeroWeb($_POST[$indice],true);
}

function creaBotonDetalles($url,$texto='Detalles',$icono='icon-search-plus'){
    global $_CONFIG;
    return "<div class='centro'><a class='btn btn-propio' href='".$_CONFIG['raiz'].$url."'><i class='$icono'></i> $texto</a></div>";
}

/* 
La siguiente función comprueba si el registro actual tiene el campo "activo" a No, en cuyo caso le añade al checkbox la clase bloqueado para usarlo
en el callback fnDrawCallback de js/filtroTablaAJAXBusqueda.js
*/
function obtieneCheckTabla($datos,$campo='codigo'){
    $res="<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos[$campo]."'></div>";
    
    return $res;
}

//Añadida la forma de pago DESCONTADO Jose Luis
function campoFormaPago($datos,$nombreCampo='medioPago',$tipo=0){
    /*$mediosPago=array('','Efectivo','Pagaré','Transferencia','Recibo SEPA','Descontado');
    $valores=array('','EFECTIVO','PAGARÉ','TRANSFERENCIA','RECIBO SEPA','DESCONTADO');
    campoSelect($nombreCampo,'Medio de pago',$mediosPago,$valores,$datos,'selectpicker span3 show-tick','',$tipo);*/

    //Porque contemplas 2 tipos de Recibos: domiciliados y SEPA. Si son lo mismo, cambiarlo de nuevo, pero la cadena valor debe ser la misma para que funcione todo!
    campoSelect('formaPago','Forma pago',array('Cuenta corriente','Ingreso en cuenta','Transferencia','Recibo domiciliado'),array('CUENTA CORRIENTE','PAYPAL','TRANSFERENCIA','RECIBO DOMICILIADO'),$datos);
}


function insertaImagenFirmaWord($firma,$documento,$nombreImagen='image2.png'){
    if(trim($firma)!=''){
        $imagenFirma=sigJsonToImage($firma, array('imageSize'=>array(310, 100)));
        imagepng($imagenFirma,'../documentos/firmas/'.$nombreImagen);

        $documento->replaceImage('../documentos/firmas/',$nombreImagen);
    }
}

function compruebaDuplicidadCampo(){
    $res='ok';

    $tabla=$_POST['tabla'];
    $campo=$_POST['campo'];
    $valor=$_POST['valor'];
    $campoCodigo=$_POST['campoCodigo'];
    $valorCodigo=$_POST['valorCodigo'];

    $whereDetalles='';
    if($valorCodigo!='NO'){
        $whereDetalles="AND $campoCodigo!=$valorCodigo";
    }

    /*La siguiente consulta extrae los registros que tengan en el campo indicado el valor indicado
      Si además se está en los detalles de una ficha, se pasa otra condición al where para que no dé coincidencia con el registro que se está viendo. */
    $consulta=consultaBD("SELECT codigo FROM $tabla WHERE $campo='$valor' $whereDetalles",true);
    if(mysql_num_rows($consulta)>0){
        $res='error';
    }

    echo $res;
}

//Duplicación de la función campoTextoValidador, para usar solo en La Academia Empresas. Contiene el atributo $tabla
function campoTextoValidador($nombreCampo,$texto,$valor='',$clase='input-large',$tabla='',$campoCodigo='codigo',$readonly=false,$tipo=0){
    $des='';
    if($readonly){
        $des='readonly';
    }
    $valor=compruebaValorCampo($valor,$nombreCampo);

    if($tipo==0){
        echo "
        <div class='control-group'>                     
          <label class='control-label' for='$nombreCampo'>$texto:</label>
          <div class='controls'>";
    }
    elseif($tipo==1){
        echo "$texto: ";
    }
    elseif($tipo==2){
        echo "<td>";
    }
        
    
    echo "<input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' tabla='$tabla' campoCodigo='$campoCodigo' value='$valor' $des>";
    
    if($tipo==0){
        echo "
          </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==2){
        echo "</td>";
    }
}

function campoTextoSimboloValidador($nombreCampo,$texto,$simbolo,$valor='',$clase='input-mini pagination-right',$tabla='',$campoCodigo='codigo',$tipo=0,$disabled=false){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    $des='';
    if($disabled){
        $des="readonly='readonly'";
    }

    if($tipo==0){
        echo "
        <div class='control-group'>                     
          <label class='control-label' for='$nombreCampo'>$texto:</label>
          <div class='controls'>";
    }
    elseif($tipo==1){
        echo "
        <td>";
    }

    echo "
        <div class='input-prepend input-append nowrap'>
          <input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' tabla='$tabla' campoCodigo='$campoCodigo' value='$valor' $des>
          <span class='add-on'> $simbolo</span>
        </div>";

    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "
        </td>";
    }

}


function campoTextoTablaValidador($nombreCampo,$valor='',$clase='input-large',$tabla='',$campoCodigo='codigo',$disabled=false){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    $des='';
    if($disabled){
        $des="readonly='readonly'";
    }

    echo "<td>
            <input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' tabla='$tabla' campoCodigo='$campoCodigo' value='$valor' $des>
          </td>";
}

function compruebaPerfil($perfil,$html){
    $res='<div class="alert alert-info"><i class="icon-info-circle"></i> <strong> Sin acciones disponibles para el usuario actual </strong></div>';

    if($_SESSION['tipoUsuario']==$perfil){
        $res=$html;
    }

    echo $res;
}

function cierraVentanaGestionPerfil($perfil,$destino,$columnas=false){
    if($_SESSION['tipoUsuario']==$perfil){
        cierraVentanaGestion($destino,$columnas);
    }
    else{
        cierraVentanaGestion($destino,$columnas,false,'','');
    }
}

function creaEstadisticasClientes($condicion){
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];
    $condicion.=obtieneWhereEjercicioEstadisticas('fechaAlta',true);

    if($perfil=='COMERCIAL'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo WHERE codigoUsuarioAsociado=$codigoUsuario AND ".$condicion,true,true);
    }
    elseif($perfil=='ADMINISTRACION2'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo WHERE codigoUsuario=$codigoUsuario AND ".$condicion,true,true);
    }
    elseif($perfil=='TELEMARKETING'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes LEFT JOIN telemarketing ON clientes.codigoTelemarketing=telemarketing.codigo WHERE codigoUsuarioAsociado=$codigoUsuario AND ".$condicion,true,true);
    }
    elseif($perfil=='DELEGADO'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes WHERE codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario) AND ".$condicion,true,true);
    }
    elseif($perfil=='DIRECTOR'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes WHERE codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario IN (SELECT usuarios.codigo FROM usuarios WHERE usuarios.codigoUsuario=$codigoUsuario)) AND ".$condicion,true,true);
    }
    else{
        $res=estadisticasGenericas('clientes',false,$condicion);
    }

    return $res;
}

function campoSelectClienteFiltradoPorUsuario($datos,$texto='Cliente',$nombreCampo='codigoCliente',$obligatorio='',$posibleCliente=false){
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    $wherePosibleCliente="AND posibleCliente='NO'";
    if($posibleCliente){
        $wherePosibleCliente="";
    }

    if($perfil=='ADMINISTRACION2'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT codigo, razonSocial AS texto FROM clientes WHERE activo='SI' $wherePosibleCliente AND codigoComercial IN(SELECT codigo FROM comerciales WHERE codigoUsuario=$codigoUsuario) ORDER BY razonSocial;",$datos,'clientes/gestion.php?codigo=',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    elseif($perfil=='COMERCIAL'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT codigo, razonSocial AS texto FROM clientes WHERE activo='SI' $wherePosibleCliente AND codigoComercial IN(SELECT codigo FROM comerciales WHERE codigoUsuarioAsociado=$codigoUsuario) ORDER BY razonSocial;",$datos,'clientes/gestion.php?codigo=',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    elseif($perfil=='TELEMARKETING'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT codigo, razonSocial AS texto FROM clientes WHERE activo='SI' $wherePosibleCliente AND codigoTelemarketing IN(SELECT codigo FROM telemarketing WHERE codigoUsuarioAsociado=$codigoUsuario) ORDER BY razonSocial;",$datos,'clientes/gestion.php?codigo=',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    elseif($perfil=='DELEGADO'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT codigo, razonSocial AS texto FROM clientes WHERE activo='SI' $wherePosibleCliente AND codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario) ORDER BY razonSocial;",$datos,'clientes/gestion.php?codigo=',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    elseif($perfil=='DIRECTOR'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT codigo, razonSocial AS texto FROM clientes WHERE activo='SI' $wherePosibleCliente AND codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario IN (SELECT usuarios.codigo FROM usuarios WHERE usuarios.codigoUsuario=$codigoUsuario)) ORDER BY razonSocial;",$datos,'clientes/gestion.php?codigo=',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    else{
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT codigo, CONCAT(nombre,' ',apellido1,' ',apellido2) AS texto FROM trabajadores_cliente WHERE activo='SI'ORDER BY nombre;",$datos,'alumnos/gestion.php?codigo=',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
}

function campoActivoVenta($datos){
    $perfil=$_SESSION['tipoUsuario'];

    if($perfil=='COMERCIAL' || $perfil=='TELEMARKETING' || $perfil=='DELEGADO' || $perfil=='DIRECTOR'){
        campoOculto($datos,'activo','PENDIENTE');
    }
    else{
        campoRadio('activo','Activo',$datos,'SI',array('Si','Pendiente de validar','No'),array('SI','PENDIENTE','NO'),true);
    }
}


//Función para detectar los cambios realizados en el formulario y guardar los campos modificados en "camposModificados"
function detectaCambios($tabla,$datos){
    $perfil=$_SESSION['tipoUsuario'];

    if($perfil=='COMERCIAL' || $perfil=='TELEMARKETING' || $perfil=='DELEGADO' || $perfil=='DIRECTOR'){//Solo para los perfiles COMERCIAL y derivados...
        $camposModificados='';

        $consulta=consultaBD("SELECT * FROM $tabla WHERE codigo=".$datos['codigo'],true,true);

        foreach($consulta as $indice => $valor){
            $datos=formateaDatoComprobacionCambios($datos,$indice);

            /* Utilizo isset en combinación con $valor==SI para detectar los cambios en los checks. 
               Además, el substr_count hace que si el comercial entra una 2ª vez en el formulario, no se marquen los campos como no modificados */
            if(substr_count($indice,'camposModificados')==0 && ((isset($datos[$indice]) && $datos[$indice]!=$valor) || (!isset($datos[$indice]) && $valor=='SI') || substr_count($datos['camposModificados'],$indice)==1)){
                $camposModificados.=$indice.';';
            }
        }

        if($camposModificados!=''){
            $camposModificados=quitaUltimaComa($camposModificados,1);
            $_POST['camposModificados']=$camposModificados;//Lo añado a $_POST para que actualizaDatos lo obtenga
            $_POST['activo']='PENDIENTE';
        }
    }
    elseif($datos['activo']=='SI'){
        $_POST['camposModificados']='';//Para que si el perfil de administración ha activado el registro, se limpie el campo de cambios
    }
}

function detectaCambiosSubTabla($indiceReferencia,$campoCodigo,$tabla,$datos){
    $perfil=$_SESSION['tipoUsuario'];

    if($perfil=='COMERCIAL' || $perfil=='TELEMARKETING' || $perfil=='DELEGADO' || $perfil=='DIRECTOR'){//Solo para los perfiles COMERCIAL y derivados...
        $camposModificados='';

        conexionBD();

        for($i=0;isset($datos[$indiceReferencia.$i]);$i++){
            $consulta=consultaBD("SELECT * FROM $tabla WHERE $campoCodigo=".$datos['codigo']." LIMIT $i,1",false,true);

            if(!$consulta){
                $camposModificados.=$indiceReferencia.$i.';';
            }
            else{
                foreach($consulta as $indice => $valor){
                    $datos=formateaDatoComprobacionCambios($datos,$indice,true,$i);

                    //Condición similar a la utilizada en detectaCambios(), solo que se añade una comprobación de que el campo actual no se haya añadido ya a $_POST['camposModificados']
                    if((isset($datos[$indice.$i]) && $datos[$indice.$i]!=$valor) || (!isset($datos[$indice.$i]) && $valor=='SI') || (substr_count($datos['camposModificados'],$indice.$i)==1 && substr_count($_POST['camposModificados'],$indice.$i)==0)){
                        $camposModificados.=$indice.$i.';';
                    }
                }
            }
        }

        cierraBD();

        if($camposModificados!=''){
            $camposModificados=quitaUltimaComa($camposModificados,1);
            if($_POST['camposModificados']!=''){
                $_POST['camposModificados'].=';'.$camposModificados;//Lo añado a $_POST para que actualizaDatos lo obtenga
            }
            else{
                $_POST['camposModificados']=$camposModificados;//Lo añado a $_POST para que actualizaDatos lo obtenga
            }

            $_POST['activo']='PENDIENTE';
        }
    }
}

function formateaDatoComprobacionCambios($datos,$indice,$subtabla=false,$i=false){
    $indiceForm=$indice;
    if($subtabla){
        $indiceForm=$indice.$i;
    }

    if(substr_count($indice,'hora')==1){
        $datos[$indiceForm].=':00';//Para que no identifique la misma hora como diferente porque del formulario no viene con los segundos
    }
    elseif(substr_count($indice,'coste')==1 || substr_count($indice,'precio')==1 || substr_count($indice,'importe')==1){
        $datos[$indiceForm]=formateaNumeroWeb($datos[$indiceForm],true);//Lo mismo para los importes
    }
    elseif(isset($datos[$indiceForm]) && substr_count($indice,'codigo')==1 && $datos[$indiceForm]=='NULL'){
        $datos[$indiceForm]=NULL;//Similar para los códigos, para que 'NULL' coincida con NULL
    }

    return $datos;
}


function campoID($datos){
    if($datos){
        campoDato('ID',$datos['codigo']);
    }
}

//Fin funciones comunes en varias secciones

//Parte común para los listados con filtros personalizados

function abreCajaBusqueda($id="cajaFiltros"){
    echo '<div class="cajaFiltros form-horizontal hide" id="'.$id.'">
            <h3 class="apartadoFormulario">Filtrar por:</h3>';
}

function cierraCajaBusqueda(){
    echo '</div>';
}

function campoSelectSiNoFiltro($nombreCampo,$texto){
    campoSelect($nombreCampo,$texto,array('','Si','No'),array('','SI','NO'),false,'selectpicker span1 show-tick');
}

function campoSelectTieneFiltro($nombreCampo,$texto){
    campoSelect($nombreCampo,$texto,array('','Si','No'),array('','NOTNUL','ISNUL'),false,'selectpicker span1 show-tick');//Uso "NUL" porque datatables quita la cadena "NULL" de los valores
}

//Fin parte común para los listados con filtros personalizados

//Parte común de los selectpicker con AJAX

function campoSelectConsultaAjax($nombreCampo,$texto,$consulta,$valor=false,$enlace='',$clase='selectpicker selectAjax span2 show-tick',$disabled='',$tipo=0,$conexion=true){
    global $_CONFIG;

    $valor=compruebaValorCampo($valor,$nombreCampo);
    if($disabled){
        $disabled="disabled='disabled'";
    }

    if($tipo==0){
        echo "
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls nowrap'>";
    }
    elseif($tipo==1){
        echo "<td>";
    }

    echo "<select name='$nombreCampo' consulta=\"$consulta\" class='$clase' id='$nombreCampo' $disabled data-live-search='true'>
            <option value='NULL'></option>";
        
    $consulta=consultaBD($consulta,$conexion);
    while($datos=mysql_fetch_assoc($consulta)){

        if($valor!=false && $valor==$datos['codigo']){//Por defecto solo se muestra el valor seleccionado, si existiese
            echo "<option value='".$datos['codigo']."' selected='selected' >".$datos['texto']."</option>";
        }
    }
        
    echo "</select> <a enlace='".$_CONFIG['raiz']."$enlace' href='#' class='btn btn-primary btn-small botonSelectAjax noAjax' target='_blank' id='boton-$nombreCampo'><i class='icon-external-link'></i></a>";

    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "</td>";
    }
}

function cargaBusquedaSelectAjax(){
    $consulta=$_POST['consulta'];
    $busqueda=$_POST['busqueda'];

    $partesConsulta=explode('ORDER BY',$consulta);
    if(isset($partesConsulta[1])){//La consulta está ordenada...
        $consulta=$partesConsulta[0]." HAVING texto LIKE '%$busqueda%' ORDER BY ".$partesConsulta[1];
    }
    else{
        $consulta=$partesConsulta[0]." HAVING texto LIKE '%$busqueda%'";//Uso HAVING para utilizar el alias del campo a filtrar
    }
    
    $res="<option value='NULL'></option>";
    $consulta=consultaBD($consulta,true);
    while($datos=mysql_fetch_assoc($consulta)){
        $res.="<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
    }

    echo $res;
}

//Fin parte común de los selectpicker con AJAX


function campoTextoFormulario($numero,$indice,$texto,$valor='',$clase='input-large',$readonly=false,$tipo=0){
    campoTexto('pregunta'.$indice.$numero,$texto,$valor['pregunta'.$numero],$clase,$readonly,$tipo);
}

function campoTextoFormularioConLogo($numero,$indice,$texto,$valor='',$logo='',$clase='input-large',$readonly=false,$tipo=0){
    echo "";
    campoTexto('pregunta'.$indice.$numero,$texto,$valor['pregunta'.$numero],$clase,$readonly,$tipo);
    echo "</div>";
}

function campoTextoTablaFormulario($numero,$indice,$valor='',$clase='input-large',$disabled=false){
    campoTextoTabla('pregunta'.$indice.$numero,$valor['pregunta'.$numero],$clase,$disabled);
}


function campoFechaFormulario($numero,$indice,$texto,$valor=false,$solo=false,$disabled=false){//Modificada por Jose Luis el 15/09/2014 (añadido opción disabled).
    if(!$valor){
        //$valor=fecha();
    }
    else{
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }

    if($solo){
        campoTextoSolo('pregunta'.$indice.$numero,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
    else{
        campoTexto('pregunta'.$indice.$numero,$texto,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
}

function campoFechaTablaFormulario($numero,$indice,$valor=false){
    echo "<td>";
    campoFechaFormulario($numero,$indice,'',$valor,true);
    echo "</td>";
}

function campoRadioFormulario($numero,$indice,$texto,$valor='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){
    if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }
    echo "
    <div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$indice."".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function campoRadioFormularioConLogo($numero,$indice,$texto,$valor='',$logo='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){
     if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }

    echo "
    <div class='control-group'>                     
      <label class='control-label' style='background:url(../img/".$logo.") 95% bottom no-repeat;padding-right:50px;'>$texto:</label>
      <div class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$indice."".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function campoRadioTablaFormulario($numero,$indice,$valor='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){//MODIFICACIÓN 09/04/2015 NO RETROCOMPATIBLE: añadido el parámetro $valorPorDefecto | MODIFICACIÓN 05/01/2015: añadido el parámetro $salto | MODIFICACIÓN 21/07/2014 DE OFICINA: $valor antes que textos. $campos, $textosCampos y $valoresCampos arrays con misma longitud
     if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }

    echo "
      <td class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$indice."".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </td> <!-- /controls -->";
}

function areaTextoFormulario($numero,$indice,$texto,$valor='',$clase='areaTexto',$bloqueado=false){
    areaTexto('pregunta'.$indice.$numero,$texto,$valor['pregunta'.$numero],$clase,$bloqueado);
}

function areaTextoTablaFormulario($numero,$indice,$valor='',$clase='',$bloqueado=false){
    areaTextoTabla('pregunta'.$indice.$numero,$valor['pregunta'.$numero],$clase,$bloqueado);
}

function campoFirmaFormulario($numero,$indice,$texto,$valor){
    campoFirma($valor['pregunta'.$numero],'pregunta'.$indice.$numero,$texto);
}

function campoSelectFormulario($numero,$indice,$texto='',$valor=false,$tipo=0,$nombres=array('Si','NO'),$valores=array('SI','NO'),$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'"){
    if($tipo == 1){
        $clase=' selectpicker span2 show-tick';
    }
    campoSelect('pregunta'.$indice.$numero,$texto,$nombres,$valores,$valor['pregunta'.$numero],$clase,$busqueda,$tipo);
}

//Parte de clientes y posibles clientes

function camposSeguimientoCliente($datos){
    echo '<h3 class="apartadoFormulario">Seguimiento</h3>';
    
    cierraColumnaCampos();
    abreColumnaCampos();

    if($datos){
        campoRadio('procedeDeListado','Procede de listado',$datos);
        echo "<div class='hide' id='cajaProcedencia'>";
        campoSelect('procedenciaListado','Procedencia del listado',array('','Colaborador/Asesor 1º Nivel','Colaborador/Asesor 2º Nivel','Puerta Fría','Base de datos comprada','Cartera comercial','Histórico Cartera Academia','Otros'),array('','COLABORADOR1','COLABORADOR2','PUERTAFRIA','BASEDATOS','CARTERA','HISTORICO','OTROS'),$datos);
        echo "<div class='hide' id='cajaTextoProcedencia'>";
        campoTexto('textoProcedencia','Nombre comercial',$datos);
        echo "</div></div>";
    }
    else{
        campoOculto('NO','procedeDeListado');
        campoOculto('','procedenciaListado');
        campoOculto('','textoProcedencia');
    }

    campoRadio('llamado','Llamado',$datos);
    campoRadio('visita','Visita',$datos,'SI',array('Si','Seguimiento','No'),array('SI','SEGUIMIENTO','NO'));

    cierraColumnaCampos();
    abreColumnaCampos();

    campoRadio('resultadoVisita','Resultado Visita',$datos,'PREVENTA',array('Preventa','Seguimiento','No'),array('PREVENTA','SEGUIMIENTO','NO'));
    campoRadio('resultadoPreventa','Resultado Preventa',$datos,'OK',array('Ok','Faltan Datos','No'),array('OK','FALTANDATOS','NO'));
    echo '<br /><br />';

    cierraColumnaCampos();
    abreColumnaCampos('sinFlotar');//Se cierra en el formulario de la función de gestión, de forma que estás columnas quedan de forma transparente a ella    
}



//Fin parte de clientes y posibles clientes

//Parte común de facturas y abonos

function insertaVencimientosFactura($datos,$codigoFactura){
    //Los registros de vencimientos_facturas no se pueden eliminar al actualizar de forma normal, pues están vinculados con las remesas

    $res=true;
    $datos=arrayFormulario();

    conexionBD();

    for($i=0;isset($datos["codigoVencimiento$i"]) || isset($datos["medioPago$i"]);$i++){
        if(isset($datos['codigoVencimiento'.$i]) && isset($datos['medioPago'.$i])){//Actualización
            $medioPago=$datos['medioPago'.$i];
            $fechaVencimiento=$datos['fechaVencimiento'.$i];
            $importeVencimiento=formateaNumeroWeb($datos['importeVencimiento'.$i],true);
            $estado=$datos['estado'.$i];
            $concepto=$datos['conceptoVencimiento'.$i];
            $fechaDevolucion=$datos['fechaDevolucion'.$i];
            $gastosDevolucion=formateaNumeroWeb($datos['gastosDevolucion'.$i],true);
            $llamadaAsesoria=compruebaValorCheckVencimiento('llamadaAsesoria'.$i,$datos);
            $llamadaEmpresa=compruebaValorCheckVencimiento('llamadaEmpresa'.$i,$datos);

            $res=$res && consultaBD("UPDATE vencimientos_facturas SET medioPago='$medioPago', fechaVencimiento='$fechaVencimiento', fechaDevolucion='$fechaDevolucion', gastosDevolucion='$gastosDevolucion', importe=$importeVencimiento, estado='$estado', concepto='$concepto' WHERE codigo=".$datos['codigoVencimiento'.$i].";");
        }
        elseif(!isset($datos['codigoVencimiento'.$i]) && isset($datos['medioPago'.$i])){//Inserción
            $medioPago=$datos['medioPago'.$i];
            $fechaVencimiento=$datos['fechaVencimiento'.$i];
            $importeVencimiento=formateaNumeroWeb($datos['importeVencimiento'.$i],true);
            $estado=$datos['estado'.$i];
            $concepto=$datos['conceptoVencimiento'.$i];
            $fechaDevolucion=$datos['fechaDevolucion'.$i];
            $gastosDevolucion=formateaNumeroWeb($datos['gastosDevolucion'.$i],true);
            $llamadaAsesoria=compruebaValorCheckVencimiento('llamadaAsesoria'.$i,$datos);
            $llamadaEmpresa=compruebaValorCheckVencimiento('llamadaEmpresa'.$i,$datos);

            $res=$res && consultaBD("INSERT INTO vencimientos_facturas VALUES(NULL,$codigoFactura,'$medioPago','$fechaVencimiento','$fechaDevolucion','$gastosDevolucion',$importeVencimiento,'$estado','$concepto','$llamadaAsesoria','$llamadaEmpresa','');");
        }
        else{//Eliminación
            $res=$res && consultaBD("DELETE FROM vencimientos_facturas WHERE codigo='".$datos['codigoVencimiento'.$i]."';");
        }
    }

    cierraBD();

    return $res;
}

function compruebaValorCheckVencimiento($indice,$datos){
    $res='NO';

    if(isset($datos[$indice])){
        $res=$datos[$indice];
    }

    return $res;
}

//Fin parte común de facturas y abonos


//Parte común entre clientes y posibles clientes

function campoFirma($valor,$nombreCampo='firma',$texto='Firma para documentos'){
    $valor=compruebaValorCampo($valor,$nombreCampo);

    echo "
    <h3 class='apartadoFormulario'>$texto</h3>
    <!-- Área de firma -->
    <div class='centro ".$nombreCampo."'>";
    campoOculto($valor,$nombreCampo,'','hide output');
    echo "
        <div class='clearButton'><a href='#clear' class='btn btn-danger noAjax'><i class='icon-trash'></i> Borrar</a></div>
        <canvas class='pad' width='1000' height='500'></canvas>
    </div>
    <!-- Fin ára de firma -->
    <br /><br />";
}

//Fin parte común entre clientes y posibles clientes

//Parte de filtro por ejercicios

function cajaFiltroEjercicio(){
    defineFiltroEjercicio();
    if(!isset($_SESSION['ejercicio'])){
        $anio=date('Y');
    } else {
        $anio=$_SESSION['ejercicio'];
    }
    echo '
    <ul class="nav pull-right cajaUsuario" id="cajaFiltroEjercicio" data-date="01-01-'.$anio.'" data-date-format="dd-mm-yyyy">
        <li class="dropdown"  id="target-5"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-calendar"></i> Ejercicio: ';
            
            campoFiltroEjercicio();

    echo '</a>
        </li>
    </ul>';
}

function campoFiltroEjercicio(){
    $valor=obtieneEjercicioFiltro();
    campoTexto('filtroEjercicio','',$valor,'filtroEjercicio',true,2);
}

function obtieneEjercicioFiltro(){
    $res='Todos';

    if(isset($_SESSION['ejercicio'])){
        $res=$_SESSION['ejercicio'];
    }

    return $res;
}

function defineFiltroEjercicio(){
    if(isset($_GET['ejercicio']) && $_GET['ejercicio']=='Todos'){
        unset($_SESSION['ejercicio']);
    }
    elseif(isset($_GET['ejercicio'])){
        $_SESSION['ejercicio']=$_GET['ejercicio'];
    }
}

//La siguiente función genera una condición WHERE/HAVING para todos los campos fecha de los listados
function obtieneWhereEjercicioListado($columnas,$listadoClientes){
   $cadenaColumnas=implode(',',$columnas);
   $res=' AND(';

   if(isset($_SESSION['ejercicio']) && strpos($cadenaColumnas,'fecha')){//Si se ha activado el filtro de ejercicios y el listado contiene fechas...
       $anio=$_SESSION['ejercicio'];
       $operador=defineOperadorFiltroEjercicios($listadoClientes);
       $fechasSinFiltro=array(     'fechaDevolucion',
                                   'fechaNacimiento', 
                                   'trabajos.fechaTomaDatos',
                                   'trabajos.fechaRevision',
                                   'trabajos.fechaEnvio',
                                   'trabajos.fechaMantenimiento',
                                   'trabajos.fechaPrevista',
                                   'trabajos.fechaEmision',
                                   'trabajos.fechaEntrega',
                                   'fechaCobroListadoVentas',
                                   'fechaFacturaListadoVentas',
                                   'fechaInicio',
                                   'fechaFin',
                                   'contratos.fecha',
                       );

       for($i=0,$filtradas=0;$i<count($columnas);$i++){


           if(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$fechasSinFiltro)){
               $res.="(YEAR(".$columnas[$i].")$operador $anio OR YEAR(".$columnas[$i].") IS NULL OR YEAR(".$columnas[$i].")=0) AND ";//...se aplica utilizando la función YEAR() de MySQL
               $filtradas++;
           }
       }

       if($filtradas>0 && isset($_GET['sSearch']) && $_GET['sSearch']!=''){//Si existe una búsqueda global, cierro la apertura del paréntesis y abro otro nuevo, porque en la búsqueda global se usa OR
           $res=quitaUltimaComa($res,5);
           $res.=') AND(';
       }
   }

   return $res;
}

function obtieneWhereEjercicioEstadisticas($campo,$estadisticasCliente=false){
    $res='';

    if(isset($_SESSION['ejercicio'])){//Si se ha activado el filtro de ejercicios...
        $operador=defineOperadorFiltroEjercicios($estadisticasCliente);
        $anio=$_SESSION['ejercicio'];

        $res=" AND ((YEAR($campo)$operador $anio) OR (YEAR($campo) IS NULL) OR (YEAR($campo)=0))";
    }

    return $res;
}

function defineOperadorFiltroEjercicios($seccionClientes){
    $res='=';

    if($seccionClientes){
        $res='<=';
    }
    
    return $res;
}

function mensajeResultadoAdicional($indice,$res,$campo,$eliminacion=false){
    if(isset($_REQUEST[$indice])){
        if($res && $eliminacion){
            mensajeOk("Eliminación realizada correctamente."); 
        }
        elseif($res && !$eliminacion){
            if($_REQUEST[$indice] == 'Guardar'){
                mensajeOk("Datos de $campo guardados correctamente."); 
            } else {
                mensajeOk("Datos de $campo guardados correctamente y gestión finalizada."); 
            }
        }
        else{
          mensajeError("se ha producido un error al gestionar los datos. Compruebe la información introducida."); 
        }
    }
}

function cierraVentanaGestionAdicional($textoFinalizado,$destino,$columnas=false,$botonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left'){
    if($columnas){
        echo "        </fieldset>
                      <fieldset class='sinFlotar'>";
    }

    echo "
                        <br />                      
                        <div class='form-actions'>";
    if($botonVolver){
        echo "            <a href='$destino' class='btn btn-default'><i class='$iconoVolver'></i> $textoVolver</a>";
    }

    if($botonGuardar){                      
        echo "            <button type='button' name='Guardar' value='NO' class='btn btn-propio submit'><i class='$icono'></i> $texto</button>";
    }
    
        echo "            <button type='button' name='Finalizar' value='SI' class='btn btn-propio submit'><i class='$icono'></i> $textoFinalizado</button>";


    echo "
                        </div> <!-- /form-actions -->
                      </fieldset>
                    </form>
                    </div>


                </div>
                <!-- /widget-content --> 
              </div>

          </div>
        </div>
        <!-- /container --> 
      </div>/
      <!-- /main-inner --> 
    </div>
    <!-- /main -->";
}

//Fin parte de filtro por ejercicios

function crearVencimientos(){
    $contratos=consultaBD('SELECT * FROM contratos WHERE tipoPlazo="NO"',true);
    $fecha=date('Y-m-d');
    $num=0;
    while($contrato=mysql_fetch_assoc($contratos)){
        $factura=datosRegistro('facturacion',$contrato['codigo'],'codigoContrato');
        if($factura){
            $vto=consultaBD('SELECT * FROM vencimientos_facturas WHERE codigoFactura='.$factura['codigo'].' ORDER BY fecha DESC LIMIT 1;',true,true);
            $cmp=comparaFechas($fecha,$vto['fecha']);
            if($cmp==-1){
                $fechaVto=explode('-', $vto['fecha']);
                $mes=date("m", strtotime("+1 month"));
                if($fechaVto[1] != $mes){
                    if($mes=='01'){
                        $anio=date("Y", strtotime("+1 year"));
                    } else {
                        $anio=date("Y");
                    }
                    $sql = "INSERT INTO vencimientos_facturas VALUES(NULL, '".$vto['importe']."', '".$anio."-".$mes."-02', 'NO', '".$factura['codigo']."',NULL);";
                    if($contrato['baja']=='NO'){
                        $res=consultaBD($sql,true);
                        $num++;
                    } else {
                        $fechaBaja=explode('-', $contrato['fechaBaja']);
                        if($fechaBaja[1] == date('m')-1 && $fechaBaja[2] > 15){
                            $res=consultaBD($sql,true);
                            $num++;
                        }
                    }
                }
            } 
        } 
    }

    return $num;
}

function mensajeResultadoCorreo($indice,$res,$campo){
    if(isset($_POST[$indice])){
        if($res){
          mensajeOk("Mensaje enviado correctamente."); 
        }
        else{
          mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos.");
        }
    }
}

function imprimeTareas($inicio=false){
    //$codigoS=$_SESSION['codigoS'];
    $where=compruebaPerfilParaWhere();

    conexionBD();
    if($inicio){
        $where.=" AND estado!='REALIZADA'";
    }
    else{
        $where.=" AND estado='REALIZADA'";
    }

    $consulta=consultaBD("SELECT tareas.codigo, clientesTareas.empresa, clientesTareas.contacto, clientesTareas.telefono, clientesTareas.localidad, tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad 
        FROM clientesTareas INNER JOIN tareas ON clientesTareas.codigoTarea=tareas.codigo $where ORDER BY tareas.fechaInicio ASC;");
    $prioridad=array('NORMAL'=>"<span class='label label-info'>Normal</span>",'ALTA'=>"<span class='label label-danger'>Alta</span>",'BAJA'=>"<span class='label'>Baja</span>");
    $estado=array('PENDIENTE'=>"<span class='label label-warning'>Pendiente</span>",'REALIZADA'=>"<span class='label label-success'>Realizada</span>");

    while($datos=mysql_fetch_assoc($consulta)){
        $fecha=formateaFechaWeb($datos['fechaInicio']);

        echo "
        <tr>
            <td> ".$datos['empresa']." </td>
            <td> ".$datos['localidad']." </td>
            <td> ".$datos['contacto']." </td>
            <td class='nowrap'> ".formateaTelefono($datos['telefono'])." </td>
            <td> ".$datos['tarea']." </td>
            <td> $fecha </td>
            <td> ".$prioridad[$datos['prioridad']]." </td>";
        
        if(!$inicio){
            echo "
            <td> ".$estado[$datos['estado']]." </td>
            <td class='centro'>
                <a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-zoom-in'></i> Ver datos</i></a>
            </td>";
        }
        else{
            echo "
            <td class='centro'>
                <a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-zoom-in'></i> Ver datos</i></a>
                <a href='index.php?codigo=".$datos['codigo']."&realizada' class='btn btn-success'><i class='icon-ok-sign'></i> Realizada</i></a>
            </td>";
        }

        echo "
            <td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
        </tr>";
    }
    cierraBD();
}

function estadisticasGenericasNueva($tabla){
    $where=compruebaPerfilParaWhere();
    $consulta=consultaBD("SELECT COUNT(codigo) AS total FROM $tabla $where;", true);
    return mysql_fetch_assoc($consulta);
}

function imprimeCorreos(){
    global $_CONFIG;
    $where=compruebaPerfilParaWhere('correos.codigoUsuario');
    $consulta=consultaBD("SELECT correos.codigo AS codigo, usuario, destinatarios, fecha, hora, asunto, SUBSTRING(mensaje,1,10) AS mensaje FROM correos INNER JOIN usuarios ON correos.codigoUsuario=usuarios.codigo $where ORDER BY fecha, hora DESC;",true);
    $datos=mysql_fetch_assoc($consulta);

    while($datos!=0){
        $fecha=formateaFechaWeb($datos['fecha']);
        $hora=formateaHoraWeb($datos['hora']);
        echo "
        <tr>
            <td> ".ucfirst($datos['usuario'])." </td>
            <td> ".$datos['destinatarios']." </td>
            <td class='centro'> $fecha<br>a las $hora </td>
            <td> ".$datos['asunto']." </td>
            <td> ".$datos['mensaje']."[...] </td>
            <td class='centro'>
                <a href='".$_CONFIG['raiz']."correos/detallesCorreo.php?codigo=".$datos['codigo']."' class='btn btn-primary noAjax'><i class='icon-search-plus'></i> Detalles</i></a>
            </td>
            <td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
        </tr>";
        $datos=mysql_fetch_assoc($consulta);
    }
}

function firmaUsuario(){
    $codigoU=$_SESSION['codigoU'];

    $consulta=consultaBD("SELECT firmaCorreo FROM usuarios WHERE codigo='$codigoU';",true,true);

    return $consulta['firmaCorreo'];
}

function actualizaFirma(){
    $res=true;

    $firma=$_POST['firma'];
    $codigoU=$_SESSION['codigoU'];

    return consultaBD("UPDATE usuarios SET firmaCorreo='$firma' WHERE codigo='$codigoU';",true);
}

function destinoFormulario($codigoU){
    global $_CONFIG;
    $seccion=array($_CONFIG['raiz'].'inicio.php',$_CONFIG['raiz'].'contactos/index.php',$_CONFIG['raiz'].'posibles-clientes/index.php',$_CONFIG['raiz'].'clientes/index.php','','','','',$_CONFIG['raiz'].'proveedores/index.php',$_CONFIG['raiz'].'colaboradores/index.php','','','',$_CONFIG['raiz'].'correos/index.php','','',$_CONFIG['raiz'].'usuarios/index.php');

    return $seccion[$codigoU];
}

function enviaCorreos(){
    $res=true;

    $datos=arrayFormulario();
    
    $codigoU=$_SESSION['codigoU'];
    conexionBD();
    $consulta=consultaBD("SELECT email FROM usuarios WHERE codigo='$codigoU';");
    cierraBD();
    $consulta=mysql_fetch_assoc($consulta);

    $headers="From: ".$consulta['email']."\r\n";
    $headers.= "MIME-Version: 1.0\r\n";

    if(isset($_FILES['adjunto']) && $_FILES['adjunto']['name']!=""){
        $aleatorio=md5(time());
        $limiteMime="==TecniBoundary_x{$aleatorio}x";
        $headers.="Content-Type: multipart/mixed;\r\n" . "boundary=\"{$limiteMime}\"";

        $mensaje="--{$limiteMime}\r\n"."Content-Type: text/html; charset=\"utf-8\"\r\n"."Content-Transfer-Encoding: 7bit\n\n".$datos['mensajeCorreo']."\n\n";

        foreach($_FILES as $adjunto){//Hecho con foreach para incluir en una futura actualización varios adjuntos
            $fp=fopen($adjunto["tmp_name"], "r");
            $tam=filesize($adjunto["tmp_name"]);
            $fichero=fread($fp,$tam);
            $ficheroAdjunto=chunk_split(base64_encode($fichero));
            fclose($fp);
            
            $mensaje.="--{$limiteMime}\r\n";
            $mensaje.="Content-Type: application/octet-stream; name=\"".$adjunto['name']."\"\r\n"."Content-Description:".$adjunto['name']."\r\n"."Content-Disposition: attachment;filename=\"".$adjunto["name"]."\";size=".$tam."\r\n"."Content-Transfer-Encoding:base64\r\n\r\n".$ficheroAdjunto."\r\n\r\n";
        }

        $mensaje .= "--{$limiteMime}--";
    }
    else{
        $headers.= "Content-Type: text/html; charset=UTF-8";
        $mensaje=$datos['mensajeCorreo'];
    }

    if (!mail($datos['destinatarios'], $datos['asunto'], $mensaje ,$headers)){
        $res=false;
    }
    else{
        $fecha=date('Y')."-".date('m')."-".date('d');
        $hora=date('H').":".date('i').":00";

        conexionBD();
        consultaBD("INSERT INTO correos VALUES(NULL,'".$datos['destinatarios']."', '$fecha', '$hora', '".$datos['asunto']."', '".$datos['mensajeCorreo']."', '', '$codigoU');");
        
        $id=mysql_insert_id();
        $nombreFichero=nombreFicheroAdjunto($_FILES['adjunto']['name'],$id);
        if (!move_uploaded_file($_FILES['adjunto']['tmp_name'], "adjuntosCorreos/$nombreFichero")){ 
             $nombreFichero='';
        }

        consultaBD("UPDATE correos SET adjunto='$nombreFichero' WHERE codigo='$id';");
        cierraBD();
    }
    
    return $res;
}

function nombreFicheroAdjunto($nombreTemporal,$id){
    $arrayNombre=explode('.',$nombreTemporal);//Creo un array con el nombre y la extensión en un índice distinto
    $nombre=reset($arrayNombre);//Extrae el nombre del fichero.
    $nombre=str_replace(' ','_',$nombre);//Quita los espacios
    $extension=end($arrayNombre);//Extrae la extensión del fichero.
    $nombreFichero=$nombre.'_'.$id.'.'.$extension;//El fichero pasa a igual, solo que entre el nombre y la extensión está el id (para evitar sobrescrituras)

    return $nombreFichero;
}

function imprimeConversaciones(){
    $codigoU=$_SESSION['codigoU'];

    conexionBD();
    $consulta=consultaBD("SELECT conversaciones.codigo AS codigo, fecha, hora, asunto, nombre, apellidos FROM (conversaciones INNER JOIN usuarios ON conversaciones.codigoUsuario=usuarios.codigo) INNER JOIN destinatarios_conversacion ON conversaciones.codigo=destinatarios_conversacion.codigoConversacion WHERE conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversacion.codigoUsuario='$codigoU' GROUP BY conversaciones.codigo;");
    while($datos=mysql_fetch_assoc($consulta)){
        $sinLeer=compruebaMensajesSinLeerConversacion($codigoU,$datos['codigo']);

        echo "
        <tr>
            <td> ".$datos['asunto']." $sinLeer</td>
            <td> ".formateaFechaWeb($datos['fecha'])." </td>
            <td> ".formateaHoraWeb($datos['hora'])." </td>
            <td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
            <td class='centro'>
                <a href='conversaciones.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Ver mensajes</i></a>
            </td>
            <td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
        </tr>";
    }
    cierraBD();
}

function compruebaMensajesPorLeer(){
    $inicio=false;
    date_default_timezone_set('Europe/Madrid');//Necesario para que el servidor no dé error.

    if(isset($_SESSION['ultimaComprobacion'])){//Si ya se ha comprobado anteriormente
        $ultimaComprobacion=$_SESSION['ultimaComprobacion'];
        $sinLeer=$_SESSION['mensajesSinLeer'];
    }
    else{//Si es la primera vez que se comprueba
        $ultimaComprobacion=new DateTime();
        $sinLeer=0;
        $inicio=true;
    }

    $ahora=new DateTime();

    $diferencia=date_diff($ultimaComprobacion,$ahora);
    $diferencia=date_interval_format($diferencia,'%i');//Transformación del intervalo en minutos

    if($diferencia>5 || $inicio){//Comprueba que haya mensajes nuevos cada 5 minutos
        $codigoU=$_SESSION['codigoU'];
        //La siguiente consulta contabiliza todos los mensajes no leídos de las conversaciones a los que el usuario está asociado, sin tener en cuenta sus propios mensajes
        $datos=consultaBD("SELECT COUNT(conversaciones.codigo) AS sinLeer FROM conversaciones 
        INNER JOIN mensajes ON conversaciones.codigo=mensajes.codigoConversacion 
        INNER JOIN destinatarios_mensajes ON mensajes.codigo=destinatarios_mensajes.codigoMensaje
        WHERE destinatarios_mensajes.codigoUsuario='$codigoU' AND leido='NO' AND mensajes.codigoUsuario!='$codigoU';",true,true);
        //$datos=consultaBD("SELECT COUNT(conversaciones.codigo) AS sinLeer FROM (conversaciones INNER JOIN destinatarios_conversacion ON conversaciones.codigo=destinatarios_conversacion.codigoConversacion) INNER JOIN mensajes ON conversaciones.codigo=mensajes.codigoConversacion WHERE (conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversacion.codigoUsuario='$codigoU') AND leido='NO' AND mensajes.codigoUsuario!='$codigoU';",true,true);
        $sinLeer=$datos['sinLeer'];
    }

    $_SESSION['ultimaComprobacion']=$ahora;//Actualizo la variable temporal de referencia
    $_SESSION['mensajesSinLeer']=$sinLeer;//Actualizo la variable de mensajes nuevos

    echo $sinLeer;
}


function compruebaMensajesSinLeerConversacion($codigoU,$codigoC){
    $sinLeer='';
    $numSinLeer=consultaBD("SELECT COUNT(destinatarios_mensajes.codigoMensaje) AS sinLeer FROM 
    mensajes INNER JOIN destinatarios_mensajes ON mensajes.codigo=destinatarios_mensajes.codigoMensaje
    INNER JOIN conversaciones ON mensajes.codigoConversacion=conversaciones.codigo 
    WHERE destinatarios_mensajes.codigoUsuario='$codigoU' AND leido='NO' AND mensajes.codigoUsuario!='$codigoU' AND conversaciones.codigo='$codigoC';",false,true);
    //$numSinLeer=consultaBD("SELECT COUNT(conversaciones.codigo) AS sinLeer FROM (conversaciones INNER JOIN destinatarios_conversacion ON conversaciones.codigo=destinatarios_conversacion.codigoConversacion) INNER JOIN mensajes ON conversaciones.codigo=mensajes.codigoConversacion WHERE (conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversacion.codigoUsuario='$codigoU') AND leido='NO' AND conversaciones.codigo='$codigoC' AND mensajes.codigoUsuario!='$codigoU';",false,true);
    if($numSinLeer['sinLeer']>0){
        $sinLeer="<span class='badge'>".$numSinLeer['sinLeer']."</span>";
    }

    return $sinLeer;
}

function campoDatoAvefor($texto,$valor,$id=''){//MODIFICACIÓN 28/10/2015: añadido el parámetro $id
    echo "
    <div class='control-group'>                     
      <label class='control-label' for='$id'>$texto:</label>
      <div class='controls datoSinInput' id='$id'>
        $valor
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function datosCorreo($codigo){
    return consultaBD("SELECT usuario, email AS remitente, destinatarios, fecha, hora, asunto, mensaje, adjunto FROM correos INNER JOIN usuarios ON correos.codigoUsuario=usuarios.codigo WHERE correos.codigo='$codigo';",true,true);
}

function compruebaAdjuntos($fichero){
    if(trim($fichero)!=''){
        echo '
        <div class="control-group">                     
          <label class="control-label" for="adjunto">Archivo adjunto:</label>
          <div class="controls enlaceForm">
            <a href="adjuntosCorreos/'.$fichero.'" id="adjunto" tarjet="_blank">'.$fichero.'</a>
          </div>
        </div>';
    }
}

function mensajeResultadoSolicitud($indice,$res,$campo='Factura'){
    if(isset($_GET[$indice])){
        if($res){
          mensajeOk("$campo enviada correctamente."); 
        }
        else{
          mensajeError("se ha producido un error al intentar enviar. Pruebe de nuevo más tarde."); 
        }
    }
}

//Fin parte de filtro por ejercicios